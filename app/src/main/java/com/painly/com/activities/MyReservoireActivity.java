package com.painly.com.activities;

import android.app.Activity;
import android.os.Bundle;

import com.painly.com.BaseActivity;
import com.painly.com.R;

/**
 * Created by android-da on 2/26/19.
 */

public class MyReservoireActivity extends BaseActivity {
    Activity mActivity = MyReservoireActivity.this;
    String TAG = MyReservoireActivity.this.getClass().getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myreservoire);
    }
}
