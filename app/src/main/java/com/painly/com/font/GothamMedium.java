package com.painly.com.font;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by android-da on 5/23/18.
 */

public class GothamMedium {


    String path = "GOTHAM-MEDIUM.TTF";
    Context mContext;
    public static Typeface mTypeface;
    public GothamMedium(Context context){
        mContext = context;
    }

    public Typeface getFontFamily(){
        try{
            if (mTypeface == null)
                mTypeface = Typeface.createFromAsset(mContext.getAssets(),path);
        }catch (Exception e){
            e.printStackTrace();
        }

        return mTypeface;
    }
}
