package com.painly.com.interfaces;

import com.painly.com.adapters.FormsAdapter;
import com.painly.com.beans.Conditions;

/**
 * Created by android-da on 2/13/19.
 */

public interface PremiumFeedInterface {
    public void getPremiumFeeds(Conditions mConditions,FormsAdapter.ViewHolder mViewHolder);
}
