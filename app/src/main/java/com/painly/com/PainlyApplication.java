package com.painly.com;

import android.app.Application;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.painly.com.beans.NotificationsModel;
import com.painly.com.fragments.NotificationFragment;
import com.painly.com.volley.LruBitmapCache;

import java.util.Random;

import io.fabric.sdk.android.Fabric;

/**
 * Created by android-da on 5/21/18.
 */

public class PainlyApplication extends Application {
    public static final String TAG = PainlyApplication.class
            .getSimpleName();
    public static final int CONNECTION_TIMEOUT = 120 * 1000;//120 Seconds
    public static int notiCount = 0;
    private static PainlyApplication mInstance;
    private RequestQueue mRequestQueue;
    private com.android.volley.toolbox.ImageLoader mImageLoader;
    DatabaseReference mRootRefrence;

    public static synchronized PainlyApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());

        mInstance = this;
        // UNIVERSAL IMAGE LOADER SETUP
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();

        com.nostra13.universalimageloader.core.ImageLoader.getInstance().init(config);
        // END - UNIVERSAL IMAGE LOADER SETUP
    }

    public int getRandom() {
        int mProfilePicArray[] = {R.drawable.icon_toy1, R.drawable.icon_toy2, R.drawable.icon_toy3, R.drawable.icon_toy4,
                R.drawable.icon_toy5, R.drawable.icon_toy6, R.drawable.icon_toy7, R.drawable.icon_toy8,
                R.drawable.icon_toy9, R.drawable.icon_toy10, R.drawable.icon_toy11, R.drawable.icon_toy12, R.drawable.icon_toy13,
                R.drawable.icon_toy14, R.drawable.icon_toy15, R.drawable.icon_toy16, R.drawable.icon_toy17, R.drawable.icon_toy18, R.drawable.icon_toy19, R.drawable.icon_toy20, R.drawable.icon_toy21,
                R.drawable.icon_toy22, R.drawable.icon_toy23, R.drawable.icon_toy24, R.drawable.icon_toy25, R.drawable.icon_toy26
        };

        int rnd = new Random().nextInt(mProfilePicArray.length);

        return mProfilePicArray[rnd];
    }








    /***************
     *Imaplement Volley
     **************/
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public com.android.volley.toolbox.ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new com.android.volley.toolbox.ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setRetryPolicy(new DefaultRetryPolicy(
                CONNECTION_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setRetryPolicy(new DefaultRetryPolicy(
                CONNECTION_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        req.setTag(TAG);
        getRequestQueue().add(req);
    }



    public  String getRandomFeedImage(){
        String mFeedImagesArray[] = {
                "https://images.unsplash.com/photo-1526403646408-57b94dc15399?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1525480763055-62049f6b306d?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1506824463062-f1da18416c77?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1455853828816-0c301a011711?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1513221323698-800407ce0781?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1543363575-d306a0b37c9c?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1523362628745-0c100150b504?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1514669585229-90e248ca7954?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1494597564530-871f2b93ac55?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1499728603263-13726abce5fd?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1452869998099-611551dbc276?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1519583916722-289d542b19a7?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1539767926543-f86eac3cef59?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1542818440-44f919f24dbb?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1504868173-db962b7c3757?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1537177963818-fe5268040356?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1519996529931-28324d5a630e?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1531299244174-d247dd4e5a66?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1528158830489-3ba27c0c9325?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1515745941901-0c81bd6cd402?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1476718406336-bb5a9690ee2a?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1453847668862-487637052f8a?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1488401318902-f7feae66db20?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1501171901942-5e8b5c41d6e1?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1517094014682-694379ea076d?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1502911862439-449f75634846?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1517931524326-bdd55a541177?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1519088641655-a49257c008e4?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1490843524522-ee99e561bb90?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ",
                "https://images.unsplash.com/photo-1532063218265-a4070fa31bf0?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&ixid=eyJhcHBfaWQiOjM1NTQwfQ"
        };

        int rnd = new Random().nextInt(mFeedImagesArray.length);

        return mFeedImagesArray[rnd];
    }
}
