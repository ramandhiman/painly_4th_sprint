package com.painly.com.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.painly.com.R;
import com.painly.com.beans.Conditions;
import com.painly.com.interfaces.SelectConditions;

import java.util.ArrayList;


/**
 * Created by android-raman on 2/5/18.
 */

public class ConditionssAdapter extends RecyclerView.Adapter<ConditionssAdapter.ViewHolder> {
    private ArrayList<Conditions> mArrayList;
    private ArrayList<Conditions> mSelectedArrayList = new ArrayList<Conditions>();
    private Activity mActivity;
    private SelectConditions mSelectConditions;

    public ConditionssAdapter(Activity mActivity, ArrayList<Conditions> mArrayList, SelectConditions mSelectConditions) {
        this.mArrayList = mArrayList;
        this.mActivity = mActivity;
        this.mSelectConditions = mSelectConditions;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_conditions, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Conditions mConditions = mArrayList.get(position);
        holder.itemTitleTV.setText(mConditions.getName());

        holder.itemCheckBox.setChecked(mArrayList.get(position).isSelected());

        if (mArrayList.get(position).isSelected()) {
            holder.imgCheckIV.setVisibility(View.VISIBLE);
            holder.itemTitleTV.setTextColor(mActivity.getResources().getColor(R.color.theme_color));

        } else {
            holder.imgCheckIV.setVisibility(View.GONE);
            holder.itemTitleTV.setTextColor(mActivity.getResources().getColor(R.color.dark_txt_color));
        }

        holder.itemCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.itemCheckBox.isChecked() == true) {
                    holder.imgCheckIV.setVisibility(View.VISIBLE);
                    mArrayList.get(holder.getAdapterPosition()).setSelected(true);
                    mSelectedArrayList.add(mArrayList.get(holder.getAdapterPosition()));
                    mSelectConditions.mSelectConditions(true, mSelectedArrayList);
                    holder.itemTitleTV.setTextColor(mActivity.getResources().getColor(R.color.theme_color));
                } else if (holder.itemCheckBox.isChecked() == false) {
                    holder.imgCheckIV.setVisibility(View.GONE);
                    mArrayList.get(holder.getAdapterPosition()).setSelected(false);
                    mSelectedArrayList.remove(mArrayList.get(holder.getAdapterPosition()));
                    mSelectConditions.mSelectConditions(false, mSelectedArrayList);
                    holder.itemTitleTV.setTextColor(mActivity.getResources().getColor(R.color.dark_txt_color));
                }
            }

        });

    }

    //This method will filter the list
    //here we are passing the filtered data
    //and assigning it to the list with notifydatasetchanged method
    public void filterList(ArrayList<Conditions> filterdNames) {
        this.mArrayList = filterdNames;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView itemTitleTV;
        public CheckBox itemCheckBox;
        public RelativeLayout itemLayoutLL;
        public ImageView imgCheckIV;
        public ViewHolder(View itemView) {
            super(itemView);
            itemTitleTV = (TextView) itemView.findViewById(R.id.itemTitleTV);
            itemCheckBox = (CheckBox) itemView.findViewById(R.id.itemCheckBox);
            itemLayoutLL = (RelativeLayout) itemView.findViewById(R.id.itemLayoutLL);
            imgCheckIV = (ImageView) itemView.findViewById(R.id.imgCheckIV);
        }
    }
}
