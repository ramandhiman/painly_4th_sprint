package com.painly.com.beans;

import java.io.Serializable;

/**
 * Created by android-da on 6/8/18.
 */

public class NotificationsModel implements Serializable {
    String notiUID = "";
    Object isNew;
    String message = "";
    String messageUID = "";
    String recieverAuthID = "";
    String senderAuthID = "";
    String type = "";
    String image = "";
    String commentID = "";
    String commentType = "";

    public Object getIsNew() {
        return isNew;
    }

    public void setIsNew(Object isNew) {
        this.isNew = isNew;
    }

    public String getCommentID() {
        return commentID;
    }

    public void setCommentID(String commentID) {
        this.commentID = commentID;
    }

    public String getCommentType() {
        return commentType;
    }

    public void setCommentType(String commentType) {
        this.commentType = commentType;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public String getMessage() {
        return message;
    }

    public String getNotiUID() {
        return notiUID;
    }

    public void setNotiUID(String notiUID) {
        this.notiUID = notiUID;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageUID() {
        return messageUID;
    }

    public void setMessageUID(String messageUID) {
        this.messageUID = messageUID;
    }

    public String getRecieverAuthID() {
        return recieverAuthID;
    }

    public void setRecieverAuthID(String recieverAuthID) {
        this.recieverAuthID = recieverAuthID;
    }

    public String getSenderAuthID() {
        return senderAuthID;
    }

    public void setSenderAuthID(String senderAuthID) {
        this.senderAuthID = senderAuthID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
