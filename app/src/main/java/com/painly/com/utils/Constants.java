package com.painly.com.utils;

/**
 * Created by android-da on 5/31/18.
 */

public class Constants {
    public static final String TERMS_LINK = "https://painly.com/terms";
    public static final String ABOUT_PAINLY_LINK = "https://painly.com/";
    public static final String FROM_SIGNUP = "from_signup";
    public static final String FROM_PROFILE = "from_profile";
    public static final String FROM_HOME = "from_home";
    public static final String CURRENT = "current";
    public static final String ANOTHER = "another";
    public static final int VIEW_TYPE_SENDER_MESSAGE = 1;
    public static final int VIEW_TYPE_RECIEVER_MESSAGE = 0;

    public static final int VIEW_TYPE_SENDER = 1;
    public static final int VIEW_TYPE_RECIEVER = 0;
    public static final String RSS_TYPE = "rss_type";
    public static final String ATOM_TYPE = "atom_type";


    //String Constants
    public static final String CHAT = "message";
    public static final String FAVORITE = "favorite";//favorite
    public static final String COMMENT = "comment";
    public static final String ADDPOST = "addPost";
    public static  String USER_ID = "user_id";
    public static  Double LAT = 0.0;
    public static  Double LONG = 0.0;
    public static  String FORM_UID = "form_uid";
    public static  String USER_NAME = "user_name";

    public static int FORUM_TYPE = 1;
    public static int HEADER_TYPE = 0;


    public static String POST_TITLE = "post_title";
    public static String POST_BODY = "post_body";
    public static String POST_LINK = "post_link";
    public static String POST_TYPE = "post_type";
    public static String POST_PICTURE_URI = "post_picture_uri";
    public static String POST_PICTURE_URL = "post_picture_url";
    public static String MESSAGE_PICTURE_URL = "message_picture_url";



    //NOTIFICATIONS MESSAGES
    public static final String YOU_HAVE_NEW_MESSAGE = "sent you a message!";
    public static final String FAVORITE_YOU = " favorited you!";
    public static final String FAVORITE_HEADING = "Favorited";
    public static final String COMMENT_MESSAGE = " added a comment to your post: ";
    public static final String ADD_POST_MESSAGE = "added the post: ";


    /*LIVE DATA BASE WebServices*/
    public static final String SERVER_URL = "https://painlynotifications.herokuapp.com/";
    public static final String PUSH_NOTIFICATION = SERVER_URL + "chat_notification.php";
    public static final String PUSH_NOTIFICATION_FAVORITE = SERVER_URL + "notification.php";
    public static final String PAINLY_IOS_NOTIFICATION = SERVER_URL + "index.php";
    public static final String PUSH_NOTIFICATION_ADD_POST = SERVER_URL + "fire.php?";
    public static final String GET_IMAGE_LINK = "http://dharmani.com/php_livepreview/Get_img.php";
//    public static final String MASS_PUSH_SEND_ALL = "http://dharmani.com/phpapp_dev/send_push_all.php?";
    //NewMass Push api
    public static final String MASS_PUSH_SEND_ALL = "https://www.dharmani.com/phpapp_dev/send_push_all.php?";

    /*Stagging DataBase Services*/
//    public static final String SERVER_URL = "";
//    public static final String PUSH_NOTIFICATION = SERVER_URL + "";
//    public static final String PUSH_NOTIFICATION_FAVORITE = SERVER_URL + "";
//    public static final String PAINLY_IOS_NOTIFICATION = SERVER_URL + "";
//    public static final String GET_IMAGE_LINK = "";
//    public static final String MASS_PUSH_SEND_ALL = "";
//    public static final String PUSH_NOTIFICATION_ADD_POST = "";
    //public static final String PUSH_NOTIFICATION_ADD_POST = "http://dharmani.com/phpapp_dev/fire.php?";


    /*Google GEOCODE API Get Latitude & Longitude From ZipCODE*/
    public static final String GOOGLE_MAP_API_GET_LAT_LONG = "https://maps.googleapis.com/maps/api/geocode/json?address=";
}

/***********************important data & links********************************/
