package com.painly.com.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.activities.NewAllUsersProfileActivity;
import com.painly.com.activities.OpenLinkActivity;
import com.painly.com.beans.User;
import com.painly.com.chathead.ChatHeadModel;
import com.painly.com.interfaces.GetMoreUsers;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


/*
 * Created by android-raman on 2/5/18.
 */

public class FormPremiumFeedsAdapter extends RecyclerView.Adapter<FormPremiumFeedsAdapter.ViewHolder> {
    private Activity mActivity;
    ArrayList<ChatHeadModel> mArrayList;


    public FormPremiumFeedsAdapter(Activity mActivity, ArrayList<ChatHeadModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_premium_, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ChatHeadModel mChatHeadModel = mArrayList.get(position);
        if (mChatHeadModel.getThumbtitle() != null && mChatHeadModel.getThumbtitle().length() > 0){
            holder.txtImgTitleTV.setText(mChatHeadModel.getThumbtitle());
        }
        if (mChatHeadModel.getLink() != null && mChatHeadModel.getLink().contains("http")){
            holder.txtImgLinkTV.setText(mChatHeadModel.getLink());
        }

        if (mChatHeadModel.getImage() != null && mChatHeadModel.getImage().contains("http")){
            Glide.with(mActivity)
                    .load(mChatHeadModel.getImage())
                    .apply(RequestOptions.placeholderOf(R.drawable.icon_round_ph).error(R.drawable.icon_round_ph))
                    .into(holder.roundFeedIV);
        }


        holder.cardCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aboutIntent = new Intent(mActivity, OpenLinkActivity.class);
                aboutIntent.putExtra("LINK", mChatHeadModel.getLink());
                mActivity.startActivity(aboutIntent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public RoundedImageView roundFeedIV;
        public CardView cardCV;
        public TextView txtImgTitleTV, txtImgLinkTV;


        public ViewHolder(View itemView) {
            super(itemView);
            roundFeedIV = (RoundedImageView) itemView.findViewById(R.id.roundFeedIV);
            txtImgTitleTV = (TextView) itemView.findViewById(R.id.txtImgTitleTV);
            txtImgLinkTV = (TextView) itemView.findViewById(R.id.txtImgLinkTV);
            cardCV = (CardView) itemView.findViewById(R.id.cardCV);
        }
    }
}
