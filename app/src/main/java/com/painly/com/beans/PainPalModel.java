package com.painly.com.beans;

import java.io.Serializable;

/**
 * Created by android-da on 5/29/18.
 */

public class PainPalModel implements Serializable {
    String name = "";
    String username = "";
    int image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
