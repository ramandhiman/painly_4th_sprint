package com.painly.com.chat;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.painly.com.R;
import com.painly.com.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by android-da on 6/7/18.
 */

public class ChatWithUserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Activity mActivity;
    ArrayList<MessageModel> mArrayList;
    private HashMap<String, String> mHashMap;

    public ChatWithUserAdapter(Activity mActivity, ArrayList<MessageModel> mArrayList/*, HashMap<String, String> mHashMap*/) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == Constants.VIEW_TYPE_SENDER_MESSAGE) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.item_chat_right, parent, false);
            return new ItemUserViewHolder(view);
        } else if (viewType == Constants.VIEW_TYPE_RECIEVER_MESSAGE) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.item_chat_left, parent, false);
            return new ItemFriendViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == Constants.VIEW_TYPE_SENDER_MESSAGE) {
            ((ItemUserViewHolder) holder).bindData(mActivity, (MessageModel) mArrayList.get(position));
        } else if (holder.getItemViewType() == Constants.VIEW_TYPE_RECIEVER_MESSAGE) {
            ((ItemFriendViewHolder) holder).bindData(mActivity, (MessageModel) mArrayList.get(position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mArrayList.get(position).getIntChatType() == 1)
            return Constants.VIEW_TYPE_SENDER_MESSAGE;
        else if (mArrayList.get(position).getIntChatType() == 0)
            return Constants.VIEW_TYPE_RECIEVER_MESSAGE;

        return 0;
    }

    @Override
    public int getItemCount() {
        if (mArrayList == null)
            return 0;
        return mArrayList.size();
    }
}
