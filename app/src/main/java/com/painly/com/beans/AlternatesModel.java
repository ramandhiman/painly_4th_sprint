package com.painly.com.beans;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by android-da on 2/4/19.
 */

public class AlternatesModel implements Serializable{
  HashMap<String,String> alternates = new HashMap<>();

    public HashMap<String, String> getAlternates() {
        return alternates;
    }

    public void setAlternates(HashMap<String, String> alternates) {
        this.alternates = alternates;
    }
}
