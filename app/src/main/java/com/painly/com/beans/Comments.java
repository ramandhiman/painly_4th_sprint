package com.painly.com.beans;

import java.io.Serializable;

/**
 * Created by android-raman on 3/5/18.
 */

public class Comments implements Serializable{
    String commenterUID = "";
    String text = "";
    String timestamp = "";
    String userName = "";
    String userImage = "";
    String mCommentID = "";

    public Comments(){

    }

    public String getmCommentID() {
        return mCommentID;
    }

    public void setmCommentID(String mCommentID) {
        this.mCommentID = mCommentID;
    }

    public String getCommenterUID() {
        return commenterUID;
    }

    public void setCommenterUID(String commenterUID) {
        this.commenterUID = commenterUID;
    }

    public String getText() {
        return text;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
