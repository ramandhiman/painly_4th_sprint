package com.painly.com.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.activities.NewAllUsersProfileActivity;
import com.painly.com.beans.User;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;

import java.util.ArrayList;



/*
 * Created by android-raman on 2/5/18.
 */

public class TopUsersAdapter extends RecyclerView.Adapter<TopUsersAdapter.ViewHolder> {
    private ArrayList<User> mArrayList;
    private Activity mActivity;

    public TopUsersAdapter(Activity mActivity, ArrayList<User> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_topusers, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final User mUser = mArrayList.get(position);
        if (mUser.getDetails() != null) {
            if (mUser.getDetails().getImage() != null) {
                if (mUser.getDetails().getImage() != null && mUser.getDetails().getImage().contains("http")) {
                    Glide.with(mActivity)
                            .load(mUser.getDetails().getImage())
                            .apply(RequestOptions.placeholderOf(PainlyApplication.getInstance().getRandom()).error(PainlyApplication.getInstance().getRandom()))
                            .into(holder.profileRIV);
                } else {
                    holder.profileRIV.setImageResource(PainlyApplication.getInstance().getRandom());
                }

                holder.userLL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "").equals(mArrayList.get(position).getDetails().getUid())) {
                            Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                            mIntent.putExtra("UID", mUser.getDetails().getUid());
                            mIntent.putExtra("TYPE", Constants.CURRENT);
                            mActivity.startActivity(mIntent);
                        } else {
                            Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                            mIntent.putExtra("UID", mUser.getDetails().getUid());
                            mIntent.putExtra("TYPE", Constants.ANOTHER);
                            mActivity.startActivity(mIntent);
                        }
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public de.hdodenhof.circleimageview.CircleImageView profileRIV;
        public LinearLayout userLL;

        public ViewHolder(View itemView) {
            super(itemView);
            profileRIV = (de.hdodenhof.circleimageview.CircleImageView) itemView.findViewById(R.id.profileRIV);
            userLL = (LinearLayout) itemView.findViewById(R.id.userLL);
        }
    }
}
