package com.painly.com.activities;

import android.app.Activity;
import android.os.Bundle;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.BaseActivity;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.adapters.AllUsersAdapter;
import com.painly.com.beans.User;
import com.painly.com.font.EditTextArialRegular;
import com.painly.com.interfaces.GetMoreUsers;
import com.painly.com.utils.Utilities;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AllUsersActivity extends BaseActivity {
    String TAG = AllUsersActivity.this.getClass().getSimpleName();
    Activity mActivity = AllUsersActivity.this;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.allUsersRV)
    RecyclerView allUsersRV;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
    DatabaseReference mRootRefrence;
    @BindView(R.id.editSearchET)
    EditTextArialRegular editSearchET;
    @BindView(R.id.imgCloseIV)
    ImageView imgCloseIV;

    AllUsersAdapter mAllUsersAdapter;
    ArrayList<User> mUsersArrayList = new ArrayList<User>();
    ArrayList<User> mTempArrayList = new ArrayList<User>();
    String mLastKey = "";
    boolean isLoadMore = false;

    boolean isPaggination = true;

    GetMoreUsers mGetMoreUsers = new GetMoreUsers() {
        @Override
        public void getMoreUsers() {
            if (Utilities.isNetworkAvailable(mActivity)) {
                if (isLoadMore) {
                    getMoreUsersData();
                } else {
                    Toast.makeText(mActivity, "No More Users!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(mActivity, getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_users);
        ButterKnife.bind(this);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        mRootRefrence.keepSynced(true);
        setUsersAdapter();
        setSearch();
        if (Utilities.isNetworkAvailable(mActivity)) {
            getUserData();
        } else {
            Toast.makeText(mActivity, getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.imgBackIV)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void getUserData() {
        showProgressDialog(mActivity);
        Query mUserRef = mRootRefrence.child("users").limitToLast(15);
        mUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideProgressDialog();
                try {
                    for (DataSnapshot mUserSnapshot : dataSnapshot.getChildren()) {
                        final User mUser = mUserSnapshot.getValue(User.class);
                        //mUser.getDetails().setPlaceholderImage(PainlyApplication.getInstance().getRandom());
                        mTempArrayList.add(mUser);
                    }
                    if (mTempArrayList.size() < 15) {
                        Collections.reverse(mTempArrayList);
                        mUsersArrayList.addAll(mTempArrayList);
                        mTempArrayList.clear();
                        mAllUsersAdapter.notifyDataSetChanged();
                    } else if (mTempArrayList.size() == 15) {
                        mLastKey = mTempArrayList.get(0).getDetails().getUid();
                        Collections.reverse(mTempArrayList);
                        mTempArrayList.remove(mTempArrayList.size() - 1);
                        mUsersArrayList.addAll(mTempArrayList);
                        mTempArrayList.clear();
                        mAllUsersAdapter.notifyDataSetChanged();
                        isLoadMore = true;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideProgressDialog();
                Log.e(TAG, "==Error==" + databaseError.toString());
            }
        });
    }


    private void getMoreUsersData() {
        mProgressBar.setVisibility(View.VISIBLE);
        Query mUserRef = mRootRefrence.child("users").orderByKey().endAt(mLastKey).limitToLast(15);
        mUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mProgressBar.setVisibility(View.GONE);
                try {
                    for (DataSnapshot mUserSnapshot : dataSnapshot.getChildren()) {
                        final User mUser = mUserSnapshot.getValue(User.class);
                        //mUser.getDetails().setPlaceholderImage(PainlyApplication.getInstance().getRandom());
                        mTempArrayList.add(mUser);
                    }
                    if (mTempArrayList.size() == 15) {
                        mLastKey = mTempArrayList.get(0).getDetails().getUid();
                        Collections.reverse(mTempArrayList);
                        mTempArrayList.remove(mTempArrayList.size() - 1);
                        mUsersArrayList.addAll(mTempArrayList);
                        mAllUsersAdapter.notifyDataSetChanged();
                        mTempArrayList.clear();
                    } else if (mTempArrayList.size() < 15) {
                        mLastKey = mTempArrayList.get(0).getDetails().getUid();
                        Collections.reverse(mTempArrayList);
                        mTempArrayList.remove(mTempArrayList.size() - 1);
                        mUsersArrayList.addAll(mTempArrayList);
                        mAllUsersAdapter.notifyDataSetChanged();
                        mTempArrayList.clear();
                        //Toast.makeText(mActivity, "No More Users!", Toast.LENGTH_SHORT).show();
                        isLoadMore = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "==Error==" + databaseError.toString());
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }


    private void setUsersAdapter() {
        allUsersRV.setLayoutManager(new GridLayoutManager(mActivity, 3));
        allUsersRV.setNestedScrollingEnabled(false);
        mAllUsersAdapter = new AllUsersAdapter(mActivity, mUsersArrayList, mGetMoreUsers, isPaggination);
        allUsersRV.setAdapter(mAllUsersAdapter);
    }

    private void setSearch() {
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    imgCloseIV.setVisibility(View.VISIBLE);
                } else {
                    imgCloseIV.setVisibility(View.GONE);
                }

//                String query = s.toString();
//                filter(query);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    isPaggination = false;
                    String searchText = editSearchET.getText().toString();
                    if (searchText != null && searchText.length() > 0) {
                        executeSearchByUserNames(searchText);
                    } else {
                        Toast.makeText(mActivity, "Please enter name or username!", Toast.LENGTH_SHORT).show();
                    }

                    return true;
                }
                return false;
            }
        });


        imgCloseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editSearchET.setText("");
                allUsersRV.setLayoutManager(new GridLayoutManager(mActivity, 3));
                mAllUsersAdapter = new AllUsersAdapter(mActivity, mUsersArrayList, mGetMoreUsers, isPaggination);
                allUsersRV.setAdapter(mAllUsersAdapter);
            }
        });
    }


    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<User> filterdNames = new ArrayList<User>();

        //looping through existing elements
        for (User s : mUsersArrayList) {
            //if the existing elements contains the search input
            if (s.getDetails().getName().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(s);
            }
        }

        //calling a method of the adapter class and passing the filtered list
        mAllUsersAdapter.filterList(filterdNames);
    }

    private void executeSearchByUserNames(final String searchText) {
        showProgressDialog(mActivity);
        mUsersArrayList.clear();
//        Query searchQueryUserName = mRootRefrence.child("users").orderByChild("details/username").startAt(searchText.replace("@","")).endAt(searchText.replace("@","") + "\uf8ff");
        Query searchQueryUserName = mRootRefrence.child("users").orderByChild("details/username").startAt(searchText.toLowerCase().replace("@", "")).endAt(searchText.toLowerCase().replace("@", "") + "\uf8ff");
        searchQueryUserName.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mUsersArrayList.clear();
                ArrayList<User> mUsernameAL = new ArrayList<>();
                for (DataSnapshot mDataSnapshot : dataSnapshot.getChildren()) {
                    User mUser = mDataSnapshot.getValue(User.class);
                    //mUser.getDetails().setPlaceholderImage(PainlyApplication.getInstance().getRandom());
                    Log.e(TAG, "===Name===" + mUser.getDetails().getName());
                    mUsernameAL.add(mUser);
                }

                executeSearchByName(mUsernameAL, searchText);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideProgressDialog();
                Log.e(TAG, "===ERROR===" + databaseError.toString());
            }
        });

    }

    private void executeSearchByName(final ArrayList<User> mUsernameAL, String searchText) {
        Log.e(TAG,"======="+searchText);
        Query searchQueryName = mRootRefrence.child("users").orderByChild("details/name").startAt(searchText.replace("@","")).endAt(searchText.replace("@","") + "\uf8ff");
        searchQueryName.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<User> mNameAL = new ArrayList<>();
                hideProgressDialog();
                for (DataSnapshot mDataSnapshot : dataSnapshot.getChildren()) {
                    User mUser = mDataSnapshot.getValue(User.class);
                    //mUser.getDetails().setPlaceholderImage(PainlyApplication.getInstance().getRandom());
                    Log.e(TAG, "===Name===" + mUser.getDetails().getName());
                    mNameAL.add(mUser);
                }

                ArrayList<User> finalTempAL = new ArrayList<>();
                finalTempAL.addAll(mUsernameAL);
                finalTempAL.addAll(mNameAL);

                for (int i = 0; i < finalTempAL.size(); i++) {
                    User mUser = finalTempAL.get(i);
                    if (contains(mUsersArrayList, mUser.getDetails().getUid()) == false) {
                        mUsersArrayList.add(mUser);
                    }
                }


                finalTempAL.clear();

                setUsersAdapter();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideProgressDialog();
                Log.e(TAG, "===ERROR===" + databaseError.toString());
            }
        });
    }


    boolean contains(ArrayList<User> list, String mUserID) {
        for (User item : list) {
            if (item.getDetails().getUid().equals(mUserID)) {
                return true;
            }
        }
        return false;
    }
}
