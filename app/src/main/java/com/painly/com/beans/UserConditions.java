package com.painly.com.beans;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by android-da on 5/21/18.
 */

public class UserConditions  implements Serializable{
    HashMap<String,String> conditions = new HashMap<String,String>();


    public HashMap<String, String> getConditions() {
        return conditions;
    }

    public void setConditions(HashMap<String, String> conditions) {
        this.conditions = conditions;
    }
}
