package com.painly.com.chat;

import android.content.Context;
import android.content.Intent;
import android.provider.SyncStateContract;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.painly.com.R;
import com.painly.com.activities.ViewChatImageActivity;
import com.painly.com.utils.Constants;
import com.painly.com.views.RelativeTimeTextView;

/**
 * Created by android-da on 6/7/18.
 */

public class ItemUserViewHolder extends RecyclerView.ViewHolder {
    public TextView txtMessageTV;
    public RelativeTimeTextView txtDateTimeTV;
    public RoundedImageView itemMessagePicIV;

    public ItemUserViewHolder(View itemView) {
        super(itemView);
        txtDateTimeTV = (RelativeTimeTextView) itemView.findViewById(R.id.txtDateTimeTV);
        txtDateTimeTV.setText("");
        txtMessageTV = (TextView) itemView.findViewById(R.id.txtMessageTV);
        itemMessagePicIV = (RoundedImageView) itemView.findViewById(R.id.itemMessagePicIV);
    }

    public void bindData(final Context context, final MessageModel tempValue) {
        if (tempValue.getImage() != null && tempValue.getImage().contains("http")) {
            itemMessagePicIV.setVisibility(View.VISIBLE);
            Glide.with(context.getApplicationContext())
                    .load(tempValue.getImage())
                    .apply(RequestOptions.placeholderOf(R.drawable.icon_round_ph).error(R.drawable.icon_round_ph))
                    .into(itemMessagePicIV);
            txtMessageTV.setVisibility(View.GONE);
        } else {
            itemMessagePicIV.setVisibility(View.GONE);
            txtMessageTV.setVisibility(View.VISIBLE);
            if (tempValue.getText() != null && tempValue.getText().length() > 0)
                txtMessageTV.setText(tempValue.getText());
        }



        itemMessagePicIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(context, ViewChatImageActivity.class);
                mIntent.putExtra(Constants.MESSAGE_PICTURE_URL,tempValue.getImage());
                context.startActivity(mIntent);
            }
        });

    }
}
