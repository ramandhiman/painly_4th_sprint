package com.painly.com.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.activities.NewAllUsersProfileActivity;
import com.painly.com.beans.User;
import com.painly.com.interfaces.GetMoreUsers;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


/*
 * Created by android-raman on 2/5/18.
 */

public class AllUsersAdapter extends RecyclerView.Adapter<AllUsersAdapter.ViewHolder> {
    GetMoreUsers mGetMoreUsers;
    private ArrayList<User> mArrayList;
    private Activity mActivity;
    Boolean mIsPaginations;


    public AllUsersAdapter(Activity mActivity, ArrayList<User> mArrayList, GetMoreUsers mGetMoreUsers,Boolean mIsPaginations) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mGetMoreUsers = mGetMoreUsers;
        this.mIsPaginations = mIsPaginations;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_painpals, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final User mUser = mArrayList.get(position);
        if (mUser.getDetails() != null) {
            //By Glide
            if (mUser.getDetails().getImage() != null && mUser.getDetails().getImage().contains("https")) {
                Glide.with(mActivity)
                        .load(mUser.getDetails().getImage())
                        .apply(RequestOptions.placeholderOf(PainlyApplication.getInstance().getRandom()).error(PainlyApplication.getInstance().getRandom()))
                        .into(holder.profileRIV);
            } else {
                holder.profileRIV.setImageResource(PainlyApplication.getInstance().getRandom());
            }

            holder.txtNameTV.setText(mUser.getDetails().getName());
            if (mUser.getDetails().getUsername().contains("@")) {
                holder.txtUserNameTV.setText(mUser.getDetails().getUsername());
            } else {
                holder.txtUserNameTV.setText("@" + mUser.getDetails().getUsername());
            }

        }

        holder.itemLayoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUser.getDetails() != null) {
                    if (PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "").equals(mArrayList.get(position).getDetails().getUid())) {
                        Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                        mIntent.putExtra("UID", mUser.getDetails().getUid());
                        mIntent.putExtra("TYPE", Constants.CURRENT);
                        mActivity.startActivity(mIntent);
                    } else {
                        Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                        mIntent.putExtra("UID", mUser.getDetails().getUid());
                        mIntent.putExtra("TYPE", Constants.ANOTHER);
                        mActivity.startActivity(mIntent);
                    }
                }
            }
        });


        if (mIsPaginations){
            if (mArrayList.size() - 1 == position) {
                if (mGetMoreUsers != null)
                    mGetMoreUsers.getMoreUsers();
            }
        }

    }



    //This method will filter the list
    //here we are passing the filtered data
    //and assigning it to the list with notifydatasetchanged method
    public void filterList(ArrayList<User> filterdNames) {
        this.mArrayList = filterdNames;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView profileRIV;
        public TextView txtNameTV, txtUserNameTV;
        public LinearLayout itemLayoutLL;


        public ViewHolder(View itemView) {
            super(itemView);
            profileRIV = (CircleImageView) itemView.findViewById(R.id.profileRIV);
            txtNameTV = (TextView) itemView.findViewById(R.id.txtNameTV);
            txtUserNameTV = (TextView) itemView.findViewById(R.id.txtUserNameTV);
            itemLayoutLL = (LinearLayout) itemView.findViewById(R.id.itemLayoutLL);
        }
    }
}
