package com.painly.com.beans;

import java.io.Serializable;

/**
 * Created by android-da on 6/7/18.
 */

public class MessagesModel implements Serializable {
    private int intChatType;

    public int getIntChatType() {
        return intChatType;
    }

    public void setIntChatType(int intChatType) {
        this.intChatType = intChatType;
    }
}
