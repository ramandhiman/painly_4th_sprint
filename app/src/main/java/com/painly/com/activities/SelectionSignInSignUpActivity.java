package com.painly.com.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.BaseActivity;
import com.painly.com.R;
import com.painly.com.font.ButtonGothamMedium;
import com.painly.com.font.TextViewArialRegular;
import com.painly.com.utils.AlertDialogManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectionSignInSignUpActivity extends BaseActivity {
    String TAG = SelectionSignInSignUpActivity.this.getClass().getSimpleName();
    Activity mActivity = SelectionSignInSignUpActivity.this;

    @BindView(R.id.btnSignUp)
    ButtonGothamMedium btnSignUp;
    @BindView(R.id.txtAleradyAccountTV)
    TextViewArialRegular txtAleradyAccountTV;

    DatabaseReference mRootRefrence;
    ArrayList<String> mUsersNameAL = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_loginsignup_selection);
        ButterKnife.bind(this);

        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        getAllUsersNames();
    }

    @OnClick({R.id.btnSignUp, R.id.txtAleradyAccountTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSignUp:
                Intent mSignUpIntent = new Intent(mActivity, SignUp1Activity.class);
                mSignUpIntent.putExtra("UNAL", mUsersNameAL);
                startActivity(mSignUpIntent);
                finish();
                break;
            case R.id.txtAleradyAccountTV:
                startActivity(new Intent(mActivity, LoginActivity.class));
                finish();
                break;
        }
    }

    private void getAllUsersNames() {
        mRootRefrence.child("usernames").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                AlertDialogManager.hideProgressDialog();
                HashMap<String, String> mUserNameHM = (HashMap<String, String>) dataSnapshot.getValue();
                if (mUserNameHM != null){
                    Collection<String> values = mUserNameHM.values();
                    mUsersNameAL = new ArrayList<String>(values);
                    Log.e(TAG, "=======USER Name SIZE=====" + mUserNameHM.size());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                AlertDialogManager.hideProgressDialog();
            }
        });
    }

}
