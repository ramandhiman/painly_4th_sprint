package com.painly.com.adapters;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.painly.com.R;
import com.painly.com.beans.TabsModel;

import java.util.ArrayList;


/*
 * Created by android-raman on 2/5/18.
 */

public class TabsAdapter extends RecyclerView.Adapter<TabsAdapter.ViewHolder> {
    private ArrayList<TabsModel> mArrayList;
    private Activity mActivity;

    public TabsAdapter(Activity mActivity, ArrayList<TabsModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tabs, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TabsModel mTabsModel = mArrayList.get(position);
        holder.txtTabTV.setText(mTabsModel.getTabTitle());

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTabTV;
        public CardView cardCV;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTabTV = (TextView) itemView.findViewById(R.id.txtTabTV);
            cardCV = (CardView) itemView.findViewById(R.id.cardCV);
        }
    }
}
