package com.painly.com.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.painly.com.R;
import com.painly.com.SplashActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by android-da on 6/6/18.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    String TAG = MyFirebaseMessagingService.this.getClass().getSimpleName();

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload:::::: " + remoteMessage.getData().toString());

        }


        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "==============Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
        // Check if message contains a notification payload.
//        if (remoteMessage.getNotification() != null) {
//            try{
//                //JSONObject mJson = new JSONObject();
//                sendNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//
//        }

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void sendNotification(String strData, String strTitle) throws JSONException {

        JSONObject mJson = new JSONObject(strData);

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        NotificationManager notificationManager = (NotificationManager) getSystemService(this.NOTIFICATION_SERVICE);
        Date now = new Date();
        long uniqueId = now.getTime();//use date to generate an unique id to differentiate the notifications.
        Intent notificationIntent = new Intent(this, SplashActivity.class);
        notificationIntent.putExtra("DATA", "" + strData);
        notificationIntent.setAction("com.painly.com" + uniqueId);

        PendingIntent contentIntent = PendingIntent.getActivity(this,
                0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        Notification.Builder builder = new Notification.Builder(this);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(largeIcon)
                .setAutoCancel(true)
                .setSound(uri)
                .setContentTitle(strTitle)
                .setContentText(mJson.getString("message"));


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setSmallIcon(R.drawable.ic_marker);
        } else {
            builder.setSmallIcon(R.mipmap.ic_launcher);
        }
        Notification n = builder.build();
        notificationManager.notify((111111 + (int) (Math.random() * 999999)), n);

        Log.e(TAG, "NOTIFICATION WORKING");
    }


}
