package com.painly.com.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.activities.NewAllUsersProfileActivity;
import com.painly.com.beans.User;
import com.painly.com.font.TextViewArialBold;
import com.painly.com.font.TextViewArialRegular;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


/*
 * Created by yarolegovich on 07.03.2017.
 */

public class PainPalsAdapter extends RecyclerView.Adapter<PainPalsAdapter.ViewHolder> {
    private ArrayList<User> mArrayList;
    private Activity mActivity;

    public PainPalsAdapter(Activity mActivity, ArrayList<User> mArrayList/*, UpdateHomeAdapter mUpdateHomeAdapter*/) {
        this.mArrayList = mArrayList;
        this.mActivity = mActivity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_painpal_picker, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final User tempValue = mArrayList.get(position);
        holder.txtNameTV.setText(tempValue.getDetails().getName());
        holder.txtUserNameTV.setText("@" + tempValue.getDetails().getUsername());

        if (tempValue.getDetails().getImage() != null && tempValue.getDetails().getImage().contains("http")) {
            Glide.with(mActivity)
                    .load(tempValue.getDetails().getImage())
                    .apply(RequestOptions.placeholderOf(PainlyApplication.getInstance().getRandom()).error(PainlyApplication.getInstance().getRandom()))
                    .into(holder.itemProfilePicCIV);
        } else {
            holder.itemProfilePicCIV.setImageResource(PainlyApplication.getInstance().getRandom());
        }
        holder.itemLayoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempValue.getDetails() != null) {
                    if (PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "").equals(mArrayList.get(position).getDetails().getUid())) {
                        Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                        mIntent.putExtra("UID", tempValue.getDetails().getUid());
                        mIntent.putExtra("TYPE", Constants.CURRENT);
                        mActivity.startActivity(mIntent);
                    } else {
                        Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                        mIntent.putExtra("UID", tempValue.getDetails().getUid());
                        mIntent.putExtra("TYPE", Constants.ANOTHER);
                        mActivity.startActivity(mIntent);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtNameTV)
        TextViewArialBold txtNameTV;
        @BindView(R.id.txtUserNameTV)
        TextViewArialRegular txtUserNameTV;
        @BindView(R.id.itemProfilePicCIV)
        CircleImageView itemProfilePicCIV;
        @BindView(R.id.itemLayoutLL)
        LinearLayout itemLayoutLL;

        public ViewHolder(View mView) {
            super(mView);
            ButterKnife.bind(this, mView);
        }
    }
}
