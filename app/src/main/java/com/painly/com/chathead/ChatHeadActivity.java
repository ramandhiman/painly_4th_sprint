package com.painly.com.chathead;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.einmalfel.earl.AtomFeed;
import com.einmalfel.earl.EarlParser;
import com.einmalfel.earl.Feed;
import com.einmalfel.earl.Item;
import com.einmalfel.earl.RSSFeed;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.BaseActivity;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.beans.Conditions;
import com.painly.com.beans.UserConditions;
import com.painly.com.font.EditTextArialRegular;
import com.painly.com.font.TextViewGothamMedium;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlySingleton;
import com.painly.com.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatHeadActivity extends BaseActivity {


    Activity mActivity = ChatHeadActivity.this;
    String TAG = ChatHeadActivity.this.getClass().getSimpleName();
    @BindView(R.id.llCloseLL)
    LinearLayout llCloseLL;
    @BindView(R.id.defaultQuesLL)
    LinearLayout defaultQuesLL;
    @BindView(R.id.askQuestionRV)
    RecyclerView askQuestionRV;
    @BindView(R.id.editQuestionET)
    EditTextArialRegular editQuestionET;
    @BindView(R.id.txtAskTV)
    TextViewGothamMedium txtAskTV;

    @BindView(R.id.txtInfoAboutTV)
    TextView txtInfoAboutTV;
    @BindView(R.id.txtFindArticleTV)
    TextView txtFindArticleTV;

    ChatHeadAdapter mChatHeadAdapter;

    ArrayList<ChatHeadModel> mFinalArrayList = PainlySingleton.getInstance().getmChatHeadArrayList();
    ArrayList<Conditions> mConditionsArrayList = PainlySingleton.getInstance().getmConditionsArrayList();

    DatabaseReference mRootReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_head);
        setUpOnCreate();
    }

    private void setUpOnCreate() {
        //Chatbot Condition Size
        Log.e(TAG, "====Condions SIZE===" + mConditionsArrayList.size());
        //Butter Knife
        ButterKnife.bind(this);
        //Chatbot Service
        if (ChatHeadService.chatheadView != null)
            ChatHeadService.chatheadView.setVisibility(View.GONE);

        //Database Refrence
        mRootReference = FirebaseDatabase.getInstance().getReference();

        //Set Adatper
        setAdapter();

        //setUp Information about chathead:
        setUpChatBotInfo();
    }

    private void setUpChatBotInfo() {
        if (mConditionsArrayList.size() > 0) {
            txtFindArticleTV.setText(getString(R.string.find_my_article) + " " + getRandomConditionName());
            txtInfoAboutTV.setText(getString(R.string.infor_about) + " " + getRandomConditionName() + " " + "and" + " " + getRandomConditionName());
        } else {
            txtFindArticleTV.setText(getString(R.string.find_my_article11));
            txtInfoAboutTV.setText(getString(R.string.infor_about11));
        }
    }

    private String getRandomConditionName() {
        int rnd = new Random().nextInt(mConditionsArrayList.size());
        return mConditionsArrayList.get(rnd).getName();
    }


    @Override
    protected void onResume() {
        super.onResume();
        setLayoutVisibility();
        if (editQuestionET.hasFocus()) {
            askQuestionRV.scrollToPosition(mFinalArrayList.size() - 1);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (ChatHeadService.chatheadView != null)
            ChatHeadService.chatheadView.setVisibility(View.VISIBLE);
    }


    @OnClick({R.id.llCloseLL, R.id.txtAskTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llCloseLL:
                onBackPressed();
                break;
            case R.id.txtAskTV:
                Utilities.hideKeyBoad(mActivity, txtAskTV);
                askQuestionRV.scrollToPosition(mFinalArrayList.size() - 1);
                askQuestion();
                break;
        }
    }

    private void askQuestion() {
        if (editQuestionET.getText().toString().trim().equals("")) {
            Toast.makeText(mActivity, getString(R.string.please_enter_your_interest), Toast.LENGTH_SHORT).show();
        } else if (!Utilities.isNetworkAvailable(mActivity)) {
            Toast.makeText(mActivity, getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
        } else {
            /*Exreecute Your Ask Question Query*/
            String askQuestion = editQuestionET.getText().toString().trim().toLowerCase();
            editQuestionET.setText("");
            updateRightAdapter(askQuestion);
            parseText(askQuestion);
        }
    }

    private void parseText(String mText) {
        String mCommomPharseMsg = parseCommonPhrases(mText);
        if (mCommomPharseMsg != null && mCommomPharseMsg.length() > 0) {
            String mQuestionRply = mCommomPharseMsg;
            ChatHeadModel mModel = new ChatHeadModel();
            mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);
            mModel.setTitle(mQuestionRply);

            setUpLeftAdapterHandler(mModel);
        } else {
            /****Fetch From Sources****/
            //first find condition
            getMessageFromCondtion(mText);
        }

    }

    private void getMessageFromCondtion(String mText) {
        if (findCondition(mText) != null) {
            if (findSourcesUnderCondition(findCondition(mText)) != null && findSourcesUnderCondition(findCondition(mText)).length() > 4) {
                getChatBotMsgFromCondition(findSourcesUnderCondition(findCondition(mText)));
            } else {
                ChatHeadModel mModel = new ChatHeadModel();
                mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);
                mModel.setTitle(getRandomFailureResponse());
                setUpLeftAdapterHandler(mModel);
                Log.e(TAG, "======Failure Reponse=====");
            }

        } else if (mText.contains("me") || mText.contains("my interests")) {
            //no condition, check for "me"
            getCurrentUserConditions(mText);

        } else {
            //Check For partials:
            if (getCheckForPartialsCondition(mText) != null) {
                if (findSourcesUnderCondition(getCheckForPartialsCondition(mText)) != null && findSourcesUnderCondition(getCheckForPartialsCondition(mText)).length() > 4) {
                    getChatBotMsgFromCondition(findSourcesUnderCondition(getCheckForPartialsCondition(mText)));
                } else {
                    ChatHeadModel mModel = new ChatHeadModel();
                    mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);
                    mModel.setTitle(getRandomFailureResponse());
                    setUpLeftAdapterHandler(mModel);
                    Log.e(TAG, "======Failure Reponse=====");
                }
            } else {
                ChatHeadModel mModel = new ChatHeadModel();
                mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);
                mModel.setTitle(getRandomFailureResponse());
                setUpLeftAdapterHandler(mModel);
                Log.e(TAG, "======Failure Reponse=====");
            }

        }


    }

    private Conditions getCheckForPartialsCondition(String mText) {
        Conditions mFinalConditions = new Conditions();
        for (int i = 0; i < mConditionsArrayList.size(); i++) {
            Conditions mConditions = mConditionsArrayList.get(i);
            if (mConditions != null && mConditions.getAlternates() != null && mConditions.getAlternates().size() > 0) {
                for (Map.Entry<String, String> e : mConditions.getAlternates().entrySet()) {
                    if (mText.contains(e.getValue())) {
                        mFinalConditions = mConditions;
                        break;
                    }
                }
            }
        }


        return mFinalConditions;
    }


    private void getChatBotMsgFromCondition(String sourcesUnderCondition) {
        /*GEtting two Types Feed
        * 1) RSS Feed Result Simple Result
        * 2) ATOM Feed Result with Youtube Video
        * */
        getRssAndAtomWithData(sourcesUnderCondition);
    }


    //Find Random Source from the Condition
    private String findSourcesUnderCondition(Conditions condition) {
        String randomSourceURL = "";

        if (condition.getSources() != null && condition.getSources().size() > 0) {
            Collection<String> values = condition.getSources().values();
            ArrayList<String> mSourcesUrlAL = new ArrayList<String>(values);

            int rnd = new Random().nextInt(mSourcesUrlAL.size());

            randomSourceURL = mSourcesUrlAL.get(rnd);
        }

        return randomSourceURL;
    }


    private Conditions findCondition(String mText) {
        Conditions mFinalCondition = null;
        for (int i = 0; i < mConditionsArrayList.size(); i++) {
            Conditions mConditions = mConditionsArrayList.get(i);
            if (mConditions.getName().toLowerCase().contains(mText)) {
                mFinalCondition = mConditions;
                break;
            }
        }

        return mFinalCondition;
    }

    private String parseCommonPhrases(String mText) {
        String thanks[] = {"thankyou", "thanks", "thank you"};
        String hello[] = {"hello", "hi", "hey", "howdy", "what's up", "how's it going"};

        if (Arrays.asList(thanks).contains(mText) == true) {
            return getRandomThanks();
        } else if (Arrays.asList(hello).contains(mText) == true) {
            return getRandomGreetings();
        } else {
            return "";
        }
    }


    private String getRandomGreetings() {
        String responses[] = {"Hello to you!", "Hi!", "Hello there!"};
        return responses[new Random().nextInt(responses.length)];
    }

    private String getRandomThanks() {
        String responses[] = {"You're welcome!", "Of course!", "No Problem!", "Definitely!", "Awesome!", "All good!"};
        return responses[new Random().nextInt(responses.length)];
    }

    private String getRandomFailureResponse() {
        String responses[] = {"Hmm...I can't find anything on that. Try something else.", "I couldn't find anything about that. Maybe use a different term?", "Sorry, I couldn't find anything.", "Nothing came up. Try again?"};
        return responses[new Random().nextInt(responses.length)];
    }

    private String getRandomSuccessResponse() {
        String responses[] = {"How about this: ", "Here's something I found: ", "Does this work for you? ", "An article I found for you :) ", "Found this! ", "Here you go!", "What about this?", "Based on your interests, I suggest this:"};
        return responses[new Random().nextInt(responses.length)];
    }

    private String getRandomName() {
        String responsesFN[] = {"Mark", "Sally", "Sue", "Jamal", "Kyogi"};
        String responsesLN[] = {"Simpson", "Stewart", "Jones", "Jonas", "Sanchez"};
        return responsesFN[new Random().nextInt(responsesFN.length)] + " " + responsesLN[new Random().nextInt(responsesLN.length)];
    }


    private void setUpLeftAdapterHandler(final ChatHeadModel mModel) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateLeftAdapter(mModel);
            }
        });

    }


    private void updateRightAdapter(String askQuestion) {
        ChatHeadModel mModel = new ChatHeadModel();
        mModel.setTitle(askQuestion);
        mModel.setIntChatType(Constants.VIEW_TYPE_SENDER);
        mFinalArrayList.add(mModel);

        setLayoutVisibility();

        setAdapter();
    }


    private void updateLeftAdapter(ChatHeadModel mModel) {
        mFinalArrayList.add(mModel);
        setAdapter();
    }

    private void setLayoutVisibility() {
        if (mFinalArrayList.size() > 0) {
            defaultQuesLL.setVisibility(View.GONE);
            askQuestionRV.setVisibility(View.VISIBLE);
        } else {
            defaultQuesLL.setVisibility(View.VISIBLE);
            askQuestionRV.setVisibility(View.GONE);
        }
    }


    private void setAdapter() {
        mChatHeadAdapter = new ChatHeadAdapter(mActivity, mFinalArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        askQuestionRV.setNestedScrollingEnabled(false);
        askQuestionRV.setLayoutManager(mLayoutManager);
        askQuestionRV.setItemAnimator(new DefaultItemAnimator());
        askQuestionRV.setAdapter(mChatHeadAdapter);

        if (editQuestionET.hasFocus()) {
            askQuestionRV.scrollToPosition(mFinalArrayList.size() - 1);
        }
        askQuestionRV.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                askQuestionRV.scrollToPosition(mFinalArrayList.size() - 1);
            }
        });

        askQuestionRV.scrollToPosition(mFinalArrayList.size() - 1);
    }


    /*
    * RSS & ATOM Parser Library.
    * Execute Api In Asyn Task.
    * */
    private void getRssAndAtomWithData(String strSourceUrl) {
        new RetrieveFeedTask(strSourceUrl).execute();
    }

    /*
    * Getting Youtube ID
    * Details.
    * */
    private void getYoutubeVideoDetailsData(String youtubeVideoID) {
        String idArray[] = youtubeVideoID.split(":");
        String strActualID = idArray[2];
        Log.e(TAG, "===URL===" + strActualID);
        String strAPIURL = "https://www.googleapis.com/youtube/v3/videos?id=" + strActualID + "&key=AIzaSyCczCiLpXLCcL0HfrLZUvBImC948LC-d4E&part=snippet";
        Log.e(TAG, "===URL===" + strAPIURL);
        final StringRequest request = new StringRequest(Request.Method.GET, strAPIURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "===RESPONSE===" + response);
                try {
                    JSONObject mJsonObject = new JSONObject(response);
                    JSONArray mJsonArray = mJsonObject.getJSONArray("items");
                    ArrayList<String> mSplashList = new ArrayList<>();

                    JSONObject mItemObj = mJsonArray.getJSONObject(0);
                    String strVideoID = mItemObj.getString("id");
                    JSONObject mSniptObj = mItemObj.getJSONObject("snippet");
                    String strTitle = mSniptObj.getString("title");
                    JSONObject mThumbnailsObj = mSniptObj.getJSONObject("thumbnails");
                    JSONObject mHighObj = mThumbnailsObj.getJSONObject("high");
                    String mYoutubeImageUrl = mHighObj.getString("url");
                    String strVideoLink = "https://www.youtube.com/watch?v=" + mItemObj.getString("id");

                    Log.e(TAG, "=====Youtube Image====" + mYoutubeImageUrl);
                    Log.e(TAG, "=====Youtube Link====" + strVideoLink);
                    Log.e(TAG, "=====Youtube ID====" + strVideoID);
                    Log.e(TAG, "=====Youtube Title ====" + strTitle);


                    ChatHeadModel mModel = new ChatHeadModel();
                    mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);

                    mModel.setTitle(getRandomSuccessResponse());

                    mModel.setFeedType(Constants.ATOM_TYPE);
                    mModel.setThumbtitle(strTitle);
                    mModel.setLink(strVideoLink);
                    mModel.setImage(mYoutubeImageUrl);

                    setUpLeftAdapterHandler(mModel);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "===ERROR===" + error);
                ChatHeadModel mModel = new ChatHeadModel();
                mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);
                mModel.setTitle(getRandomFailureResponse());
                setUpLeftAdapterHandler(mModel);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                return headers;
            }

        };
        PainlyApplication.getInstance().addToRequestQueue(request);
    }

    /*Getting
    * Splash Images
    * 30 Images.
    * */
    private void getSplashRandomImages(final Item mItemObj) {
        String strAPIURL = "https://api.unsplash.com/photos/random?client_id=9f9ad7c37d9ebdd98e9f2c51dfcf6fc0fc6d0eb2b9c5ebaa0f7fe896ae627c01&count=30&query=health";
        final StringRequest request = new StringRequest(Request.Method.GET, strAPIURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "===RESPONSE===" + response);
                try {
                    JSONArray mJsonArray = new JSONArray(response);
                    ArrayList<String> mSplashList = new ArrayList<>();
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                        JSONObject mUrlObj = mJsonObject.getJSONObject("urls");
                        if (!mUrlObj.isNull("thumb")) {
                            mSplashList.add(mUrlObj.getString("thumb"));
                        }
                    }

                    Log.e(TAG, "=====Splash Images SIZE====" + mSplashList.size());

                    int rnd = new Random().nextInt(mSplashList.size());
                    String randomImageUrl = mSplashList.get(rnd);

                    ChatHeadModel mModel = new ChatHeadModel();
                    mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);
                    mModel.setTitle(getRandomSuccessResponse());
                    mModel.setFeedType(Constants.RSS_TYPE);
                    mModel.setThumbtitle(mItemObj.getTitle());
                    mModel.setLink(mItemObj.getLink());
                    mModel.setImage(randomImageUrl);
                    setUpLeftAdapterHandler(mModel);

                    Log.e(TAG, "=====Random Image URL====" + randomImageUrl);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "===ERROR===" + error);
                ChatHeadModel mModel = new ChatHeadModel();
                mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);
                mModel.setTitle(getRandomFailureResponse());
                setUpLeftAdapterHandler(mModel);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                return headers;
            }

        };
        PainlyApplication.getInstance().addToRequestQueue(request);
    }

    private void getCurrentUserConditions(final String mText) {
        mRootReference.child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<String> mCurrenUserConditionsAL = new ArrayList<>();
                if (dataSnapshot != null) {
                    UserConditions mUserConditions = dataSnapshot.getValue(UserConditions.class);
                    if (mUserConditions.getConditions() != null) {
                        Collection<String> values = mUserConditions.getConditions().keySet();
                        mCurrenUserConditionsAL = new ArrayList<String>(values);
                        Log.e(TAG, "onDataChange: " + mCurrenUserConditionsAL.size());

                        Conditions mConditions = getRandomMyInterestConditionName(mCurrenUserConditionsAL);

                        if (findSourcesUnderCondition(mConditions) != null && findSourcesUnderCondition(mConditions).length() > 4) {
                            Log.e(TAG, "======Sucess Reponse=====" + findSourcesUnderCondition(mConditions));
                            getChatBotMsgFromCondition(findSourcesUnderCondition(mConditions));
                        } else {
                            ChatHeadModel mModel = new ChatHeadModel();
                            mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);
                            mModel.setTitle(getRandomFailureResponse());
                            setUpLeftAdapterHandler(mModel);
                            Log.e(TAG, "======Failure Reponse=====");
                        }
                    } else {
                        if (findCondition(mText) != null) {
                            Conditions mConditions = findCondition(mText);
                            String chatBotMessage = "You don't seem to have any interests. What do you want to look for?" + " " + mConditions.getName();
                            ChatHeadModel mModel = new ChatHeadModel();
                            mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);
                            mModel.setTitle(chatBotMessage);
                            setUpLeftAdapterHandler(mModel);
                            Log.e(TAG, "======Failure Reponse=====");
                        } else {
                            ChatHeadModel mModel = new ChatHeadModel();
                            mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);
                            mModel.setTitle(getRandomFailureResponse());
                            setUpLeftAdapterHandler(mModel);
                            Log.e(TAG, "======Failure Reponse=====");
                        }
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                ChatHeadModel mModel = new ChatHeadModel();
                mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);
                mModel.setTitle(getRandomFailureResponse());
                setUpLeftAdapterHandler(mModel);
                Log.e(TAG, "======Failure Reponse=====");
            }
        });

    }

    private Conditions getRandomMyInterestConditionName(ArrayList<String> mCurrenUserConditionsAL) {
        ArrayList<Conditions> mConditionNameArrayList = new ArrayList<>();
        for (int i = 0; i < mConditionsArrayList.size(); i++) {
            for (int j = 0; j < mCurrenUserConditionsAL.size(); j++) {
                if (mConditionsArrayList.get(i).getConditionID().equals(mCurrenUserConditionsAL.get(j))) {
                    mConditionNameArrayList.add(mConditionsArrayList.get(i));
                }
            }
        }

        int rnd = new Random().nextInt(mConditionNameArrayList.size());
        Conditions mConditions = mConditionNameArrayList.get(rnd);

        return mConditions;
    }

    class RetrieveFeedTask extends AsyncTask<String, Void, List<Item>> {
        String strSourceUrl = "";

        private RetrieveFeedTask(String strSourceUrl) {
            this.strSourceUrl = strSourceUrl;
        }

        @Override
        protected List<Item> doInBackground(String... strings) {
            List<Item> result = new ArrayList<>();
            try {

                if (strSourceUrl != null && strSourceUrl.contains("http")) {
                    InputStream inputStream = new URL(strSourceUrl).openConnection().getInputStream();
                    Feed feed = EarlParser.parseOrThrow(inputStream, 0);
                    // media and itunes RSS extensions allow to assign keywords to feed items
                    if (RSSFeed.class.isInstance(feed)) {
                        RSSFeed rssFeed = (RSSFeed) feed;
                        Log.e(TAG, "==RSS FEED Count==" + rssFeed.getItems().size());
                        int rnd = new Random().nextInt(rssFeed.getItems().size());
                        Item mItemObj = rssFeed.getItems().get(rnd);
                        Log.e(TAG, "==Title==" + mItemObj.getTitle());
                        Log.e(TAG, "==Link==" + mItemObj.getLink());
                        Log.e(TAG, "==Image Url==" + mItemObj.getImageLink());
                        if (mItemObj.getImageLink() != null && mItemObj.getImageLink().contains("http")) {
                            ChatHeadModel mModel = new ChatHeadModel();
                            mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);
                            mModel.setTitle(getRandomSuccessResponse());
                            mModel.setThumbtitle(mItemObj.getTitle());
                            mModel.setFeedType(Constants.RSS_TYPE);
                            mModel.setLink(mItemObj.getLink());
                            mModel.setImage(mItemObj.getImageLink());
                            setUpLeftAdapterHandler(mModel);
                        } else {
                            //getSplashRandomImages(mItemObj);
                            ChatHeadModel mModel = new ChatHeadModel();
                            mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);
                            mModel.setTitle(getRandomSuccessResponse());
                            mModel.setThumbtitle(mItemObj.getTitle());
                            mModel.setFeedType(Constants.RSS_TYPE);
                            mModel.setLink(mItemObj.getLink());
                            mModel.setImage(PainlyApplication.getInstance().getRandomFeedImage());
                            setUpLeftAdapterHandler(mModel);

                        }
                    } else if (AtomFeed.class.isInstance(feed)) {
                        AtomFeed atomFeed = (AtomFeed) feed;
                        Log.e(TAG, "==ATOM Feed Count==" + atomFeed.getItems().size());
                        int rnd = new Random().nextInt(atomFeed.getItems().size());
                        Item mItemObj = atomFeed.getItems().get(rnd);
                        Log.e(TAG, "==YouTube Video ID==" + mItemObj.getId());

                        getYoutubeVideoDetailsData(mItemObj.getId());

                    } else {
                        ChatHeadModel mModel = new ChatHeadModel();
                        mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);
                        mModel.setTitle(getRandomFailureResponse());
                        setUpLeftAdapterHandler(mModel);
                    }

                } else {
                    ChatHeadModel mModel = new ChatHeadModel();
                    mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);
                    mModel.setTitle(getRandomFailureResponse());
                    setUpLeftAdapterHandler(mModel);
                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG,"===ERROR=="+e.toString());

                ChatHeadModel mModel = new ChatHeadModel();
                mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER);
                mModel.setTitle(getRandomFailureResponse());
                setUpLeftAdapterHandler(mModel);
            }

            return result;
        }


        @Override
        protected void onPostExecute(List<Item> items) {
            super.onPostExecute(items);

        }
    }
}
