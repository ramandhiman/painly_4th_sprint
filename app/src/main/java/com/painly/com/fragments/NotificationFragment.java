package com.painly.com.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.R;
import com.painly.com.activities.HomeActivity;
import com.painly.com.adapters.NotificationsAdapter;
import com.painly.com.adapters.PastNotificationsAdapter;
import com.painly.com.beans.NotificationsModel;
import com.painly.com.font.TextViewArialBold;
import com.painly.com.interfaces.NotificationsUpdateInterface;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.PainlyPreference;
import com.painly.com.utils.Utilities;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class NotificationFragment extends Fragment {
    String TAG = NotificationFragment.this.getClass().getSimpleName();
    View mView;
    @BindView(R.id.txtMarkAllAsReadTV)
    TextViewArialBold txtMarkAllAsReadTV;
    @BindView(R.id.mNotificationsRV)
    RecyclerView mNotificationsRV;
    @BindView(R.id.mPastNotificationsRV)
    RecyclerView mPastNotificationsRV;
    @BindView(R.id.mainLL)
    LinearLayout mainLL;
    @BindView(R.id.mNewNotiLL)
    LinearLayout mNewNotiLL;
    @BindView(R.id.withoutUserLL)
    LinearLayout withoutUserLL;
    @BindView(R.id.mNestedScrollView)
    NestedScrollView mNestedScrollView;
    @BindView(R.id.loadMoreProgressBar)
    ProgressBar loadMoreProgressBar;
    Unbinder unbinder;
    ArrayList<NotificationsModel> mNewNotificationsAL = new ArrayList<NotificationsModel>();
    ArrayList<NotificationsModel> mPastNotificationsAL = new ArrayList<NotificationsModel>();
    DatabaseReference mRootRefrence;
    NotificationsAdapter mNotificationsAdapter;
    PastNotificationsAdapter mPastNotificationsAdapter;

    NotificationsUpdateInterface mNotificationsUpdateInterface = new NotificationsUpdateInterface() {
        @Override
        public void mUpdateNotifications(NotificationsModel mModel, int position) {
            String strUserID = PainlyPreference.readString(getActivity(), PainlyPreference.USER_ID, "");

            mRootRefrence.child("users").child(strUserID).child("notifications").child(mModel.getNotiUID()).child("isNew").setValue(false);
            //Remove
            mNewNotificationsAL.remove(position);
            mNotificationsRV.removeViewAt(position);
            mNotificationsAdapter.notifyItemRemoved(position);
            mNotificationsAdapter.notifyItemRangeChanged(position, mNewNotificationsAL.size());

            //Add
            //mPastNotificationsAL.add(mModel);
            mPastNotificationsAL.add(mModel);
            //mPastNotificationsAdapter.notifyItemInserted(0);

            if (mNewNotificationsAL.size() == 0) {
                mNewNotiLL.setVisibility(View.GONE);
            } else {
                mNewNotiLL.setVisibility(View.VISIBLE);
            }
            HomeActivity activity = (HomeActivity) getActivity();
            HomeActivity.NOTIFICATIONS_COUNTS = 0;
            activity.updateNotificationsCount(HomeActivity.NOTIFICATIONS_COUNTS--);
        }
    };
    private Dialog progressDialog;


    public NotificationFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_notification, container, false);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        unbinder = ButterKnife.bind(this, mView);

        if (Utilities.isNetworkAvailable(getActivity())) {
            getNotificationsData();
            mainLL.setVisibility(View.VISIBLE);
            withoutUserLL.setVisibility(View.GONE);
        } else {
            mainLL.setVisibility(View.GONE);
            withoutUserLL.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
        }


        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void getNotificationsData() {
        showProgressDialog(getActivity());
//        mNewNotificationsAL.clear();
//        mPastNotificationsAL.clear();
//        mNotificationsRV.removeAllViews();
//        mPastNotificationsRV.removeAllViews();
        String strUserID = PainlyPreference.readString(getActivity(), PainlyPreference.USER_ID, "");
        DatabaseReference mRootRefrence = FirebaseDatabase.getInstance().getReference();
        Query notiQuery = mRootRefrence.child("users").child(strUserID).child("notifications").limitToLast(30);
        notiQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideProgressDialog();
                mNewNotificationsAL.clear();
                mPastNotificationsAL.clear();
                if (mNotificationsRV != null)
                    mNotificationsRV.removeAllViews();
                if (mPastNotificationsRV != null)
                    mPastNotificationsRV.removeAllViews();
                for (DataSnapshot mDataSnapshot1 : dataSnapshot.getChildren()) {
                    String key = mDataSnapshot1.getKey();
                    final NotificationsModel mNotificationsModel = mDataSnapshot1.getValue(NotificationsModel.class);
                    mNotificationsModel.setNotiUID(key);
                    if (mNotificationsModel.getMessage() != null && mNotificationsModel.getMessage().length() > 6) {
                        Log.e(TAG, "onDataChange: " + mNotificationsModel.getIsNew());
                        if (mNotificationsModel.getCommentID() != null && mNotificationsModel.getCommentID().length() > 0) {
                            if (contains(mNewNotificationsAL, mNotificationsModel.getCommentID()) == false) {
                                if ((Boolean) mNotificationsModel.getIsNew() == true)
                                    mNewNotificationsAL.add(mNotificationsModel);
                                else if ((Boolean) mNotificationsModel.getIsNew() == false)
                                    mPastNotificationsAL.add(mNotificationsModel);
                            }
                        } else {
                            if ((Boolean) mNotificationsModel.getIsNew() == true)
                                mNewNotificationsAL.add(mNotificationsModel);
                            else if ((Boolean) mNotificationsModel.getIsNew() == false)
                                mPastNotificationsAL.add(mNotificationsModel);
                        }
                    }
                }

                Collections.reverse(mNewNotificationsAL);
                Collections.reverse(mPastNotificationsAL);

                Log.e(TAG, "===New List Size==" + mNewNotificationsAL.size());
                Log.e(TAG, "===Past List Size==" + mPastNotificationsAL.size());

                if (mNewNotificationsAL.size() > 0 || mPastNotificationsAL.size() > 0) {
                    if (mainLL != null) {
                        mainLL.setVisibility(View.VISIBLE);
                        withoutUserLL.setVisibility(View.GONE);
                        setNewAdapter();
                        setPastAdapter();
                    }
                } else {
                    if (mainLL != null) {
                        mainLL.setVisibility(View.GONE);
                        withoutUserLL.setVisibility(View.VISIBLE);
                    }
                }

                if (mNewNotificationsAL.size() == 0) {
                    if (mNewNotiLL != null)
                        mNewNotiLL.setVisibility(View.GONE);
                } else {
                    if (mNewNotiLL != null)
                        mNewNotiLL.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "===ERROR===" + databaseError);
                AlertDialogManager.hideProgressDialog();
            }
        });
    }

    boolean contains(ArrayList<NotificationsModel> list, String mCommentID) {
        for (NotificationsModel item : list) {
            if (item.getCommentID().equals(mCommentID)) {
                return true;
            }
        }
        return false;
    }

    private void setNewAdapter() {
        mNotificationsAdapter = new NotificationsAdapter(getActivity(), mNewNotificationsAL, mNotificationsUpdateInterface);
        mNotificationsRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mNotificationsRV.setLayoutManager(mLayoutManager);
        mNotificationsRV.setItemAnimator(new DefaultItemAnimator());
        mNotificationsRV.setAdapter(mNotificationsAdapter);
    }

    private void setPastAdapter() {
        mPastNotificationsAdapter = new PastNotificationsAdapter(getActivity(), mPastNotificationsAL, mNotificationsUpdateInterface);
        mPastNotificationsRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mPastNotificationsRV.setLayoutManager(mLayoutManager);
        mPastNotificationsRV.setItemAnimator(new DefaultItemAnimator());
        mPastNotificationsRV.setAdapter(mPastNotificationsAdapter);
    }

    @OnClick(R.id.txtMarkAllAsReadTV)
    public void onViewClicked() {
        markReadAll();
    }

    private void markReadAll() {
        if (loadMoreProgressBar != null)
            loadMoreProgressBar.setVisibility(View.VISIBLE);
        String strUserID = PainlyPreference.readString(getActivity(), PainlyPreference.USER_ID, "");
        if (mNewNotificationsAL.size() > 0) {
            for (int i = 0; i < mNewNotificationsAL.size(); i++) {
                DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
                mRootRef.child("users").child(strUserID).child("notifications").child(mNewNotificationsAL.get(i).getNotiUID()).child("isNew").setValue(false);
                if (i == mNewNotificationsAL.size() - 1) {
                    mPastNotificationsAL.addAll(mNewNotificationsAL);
                    mNotificationsAdapter.notifyDataSetChanged();
                    mPastNotificationsAdapter.notifyDataSetChanged();
                    if (mNewNotificationsAL.size() == 0) {
                        if (mNewNotiLL != null)
                            mNewNotiLL.setVisibility(View.GONE);
                    } else {
                        if (mNewNotiLL != null)
                            mNewNotiLL.setVisibility(View.VISIBLE);
                    }
                    HomeActivity.NOTIFICATIONS_COUNTS = 0;
                    HomeActivity activity = (HomeActivity) getActivity();
                    activity.updateNotificationsCount(0);
                }
            }
        }

        hideLoader();
    }

    private void hideLoader() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (loadMoreProgressBar != null)
                    loadMoreProgressBar.setVisibility(View.GONE);
            }
        }, 3000);
    }

    private void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    private void hideProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }

    }

}
