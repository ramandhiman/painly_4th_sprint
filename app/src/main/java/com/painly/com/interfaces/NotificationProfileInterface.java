package com.painly.com.interfaces;

import com.painly.com.beans.NotificationsModel;

/**
 * Created by android-da on 8/8/18.
 */

public interface NotificationProfileInterface {
    public void showNotificationImage(NotificationsModel mNotificationsModel);
}
