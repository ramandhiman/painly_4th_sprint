package com.painly.com.activities;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.painly.com.BaseActivity;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.beans.User;
import com.painly.com.font.TextViewArialBold;
import com.painly.com.font.TextViewArialRegular;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class PainPalLocationActivity extends BaseActivity implements OnMapReadyCallback {
    String TAG = PainPalLocationActivity.this.getClass().getSimpleName();
    Activity mActivity = PainPalLocationActivity.this;
    GoogleMap mMap;
    Marker marker;

    @BindView(R.id.txtUserNameTV)
    TextView txtUserNameTV;
    @BindView(R.id.txtPainPalsCountTV)
    TextViewArialRegular txtPainPalsCountTV;
    @BindView(R.id.txtNameTV)
    TextViewArialBold txtNameTV;
    @BindView(R.id.txtPostsCountTV)
    TextViewArialRegular txtPostsCountTV;
    @BindView(R.id.txtUserBioTV)
    TextViewArialRegular txtUserBioTV;
    @BindView(R.id.imgCrossIV)
    ImageView imgCrossIV;
    @BindView(R.id.imgRightIV)
    ImageView imgRightIV;
    @BindView(R.id.mProfilePicCIV)
    CircleImageView mProfilePicCIV;
    @BindView(R.id.llBackLL)
    LinearLayout llBackLL;

    String imageUrl = "";
    Double mLatitude = 0.0, mLogitude = 0.0;

    User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pain_pal_location);
        ButterKnife.bind(this);
        //Setup map
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        //Get User Object
        if (getIntent() != null && (User) getIntent().getSerializableExtra("MODEL") != null) {
            mUser = (User) getIntent().getSerializableExtra("MODEL");
            setUpWidgetData(mUser);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setUpAnimateUserLocation();
            }
        }, 2000);

    }

    private void setUpWidgetData(User mUser) {
        if (mUser.getDetails() != null) {
            mLatitude = Double.parseDouble(mUser.getDetails().getLocation().getLatitude().toString());
            mLogitude = Double.parseDouble(mUser.getDetails().getLocation().getLongitude().toString());
            imageUrl = mUser.getDetails().getImage();
            //Set User Profile Pic
            if (mUser.getDetails().getImage() != null && mUser.getDetails().getImage().contains("http")) {

                Glide.with(mActivity.getApplicationContext())
                        .load(mUser.getDetails().getImage())
                        .apply(RequestOptions.placeholderOf(mUser.getDetails().getPlaceholderImage()).error(mUser.getDetails().getPlaceholderImage()))
                        .into(mProfilePicCIV);
            }else{
                mProfilePicCIV.setImageResource(mUser.getDetails().getPlaceholderImage());
            }

            //Set Name,UserName,Painpals,Posts etc...
            //Name
            if (mUser.getDetails().getName() != null) {
                txtNameTV.setText(mUser.getDetails().getName());
            }
            //UserName
            if (mUser.getDetails().getUsername() != null) {
                if (mUser.getDetails().getUsername().contains("@")) {
                    txtUserNameTV.setText(mUser.getDetails().getUsername());
                } else {
                    txtUserNameTV.setText("@" + mUser.getDetails().getUsername());
                }
            }

            //User Bio
            if (mUser.getDetails().getBio() != null) {
                txtUserBioTV.setText(mUser.getDetails().getBio());
            }

            //UserPosts
            if (mUser.getForums() != null) {
                txtPostsCountTV.setText("" + mUser.getForums().size());
            }
            //UserPainPals
            if (mUser.getFavorites() != null) {
                txtPainPalsCountTV.setText("" + mUser.getFavorites().size());
            }
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void setUpAnimateUserLocation() {

        LatLng coordinate = new LatLng(mLatitude, mLogitude);
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15);
        if (mMap != null)
            mMap.animateCamera(yourLocation);

        try {
            Glide.with(getApplicationContext())
                    .asBitmap()
                    .load(imageUrl)
                    .into(new SimpleTarget<Bitmap>(100, 100) {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            Bitmap mFinalBitmap = scaleBitmap(resource, 130, 130);
                            marker = mMap.addMarker(new MarkerOptions().position(new LatLng(mLatitude, mLogitude)).icon(BitmapDescriptorFactory.fromBitmap(mFinalBitmap)));
                            //marker.setTag(mFavoriteUserArrayList.get(finalI).getDetails().getUid());
                            marker.setTag("0W0njSGBYNUDwz3yM6cXkjFteht1");
                        }

                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            super.onLoadFailed(errorDrawable);
                            marker = createUserPicMarker(mMap, mLatitude, mLogitude);
                            marker.setTag("0W0njSGBYNUDwz3yM6cXkjFteht1");
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.e(TAG, "onMapReady: ");
        mMap = googleMap;
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

    }


    public Bitmap scaleBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);
        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return getCircleBitmap1(resizedBitmap);
    }


    public Bitmap getCircleBitmap1(Bitmap source) {
        int size = Math.min(source.getHeight(), source.getWidth());
        Bitmap output = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, size, size);
        RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(source, rect, rect, paint);

        return addBorderToBitmap(output, 7, getResources().getColor(R.color.colorSplash));
    }


    // Custom method to add a border around bitmap
    protected Bitmap addBorderToBitmap(Bitmap srcBitmap, int borderWidth, int borderColor) {
        // Initialize a new Bitmap to make it bordered bitmap
        Bitmap dstBitmap = Bitmap.createBitmap(
                srcBitmap.getWidth() + borderWidth * 2, // Width
                srcBitmap.getHeight() + borderWidth * 2, // Height
                Bitmap.Config.ARGB_8888 // Config
        );
        // Initialize a new Canvas instance
        Canvas canvas = new Canvas(dstBitmap);
        // Initialize a new Paint instance to draw border
        Paint paint = new Paint();
        paint.setColor(borderColor);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderWidth);
        paint.setAntiAlias(true);
        Rect rect = new Rect(
                borderWidth / 2,
                borderWidth / 2,
                canvas.getWidth() - borderWidth / 2,
                canvas.getHeight() - borderWidth / 2
        );

        RectF rectF = new RectF(rect);
        // Draw a rectangle as a border/shadow on canvas
        canvas.drawOval(rectF, paint);
        // Draw source bitmap to canvas
        canvas.drawBitmap(srcBitmap, borderWidth, borderWidth, null);
        srcBitmap.recycle();
        return dstBitmap;
    }

    protected Marker createUserPicMarker(GoogleMap googleMap, double latitude, double longitude) {

        if (mActivity != null) {
            Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), mUser.getDetails().getPlaceholderImage());
            Bitmap mFinalBitmap = scaleBitmap(mBitmap, 130, 130);
            return googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
                    .icon(BitmapDescriptorFactory.fromBitmap(mFinalBitmap)));
        } else {
            return googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
                    .icon(BitmapDescriptorFactory.fromResource(PainlyApplication.getInstance().getRandom())));
        }
    }

    @OnClick(R.id.llBackLL)
    public void onViewClicked() {
    }

    @OnClick({R.id.imgCrossIV, R.id.imgRightIV, R.id.llBackLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgCrossIV:
                finish();
                break;
            case R.id.imgRightIV:
                finish();
                break;
            case R.id.llBackLL:
                onBackPressed();
                break;
        }
    }
}
