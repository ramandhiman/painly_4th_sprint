package com.painly.com.interfaces;

import com.painly.com.beans.User;

import java.util.ArrayList;

/**
 * Created by android-da on 2/16/19.
 */

public interface PainPalsSelectionInterface {
    public void getPainPalsAsFavorite(ArrayList<User> mArrayList);
}
