package com.painly.com.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.R;
import com.painly.com.adapters.LibraryPostsAdapter;
import com.painly.com.adapters.ProfilePostsAdapter;
import com.painly.com.beans.FormsModel;
import com.painly.com.beans.LibraryModel;
import com.painly.com.beans.User;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Constants;
import com.painly.com.utils.SimpleDividerItemDecoration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyLibraryActivity extends AppCompatActivity {
    String TAG = MyLibraryActivity.this.getClass().getSimpleName();
    Activity mActivity = MyLibraryActivity.this;

    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.mMyLibraryRV)
    RecyclerView mMyLibraryRV;
    @BindView(R.id.withoutSavedLL)
    LinearLayout withoutSavedLL;

    ArrayList<LibraryModel> mFormsArrayList = new ArrayList<LibraryModel>();
    LibraryPostsAdapter mLibraryPostsAdapter;

    DatabaseReference mRootReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_library);
        ButterKnife.bind(this);
        mRootReference = FirebaseDatabase.getInstance().getReference();
        /*getting my library Data*/
        gettingLibraryData();

    }

    private void gettingLibraryData() {
        AlertDialogManager.showProgressDialog(mActivity);
        mRootReference.child("users").child(FirebaseAuth.getInstance().getUid()).child("saved").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                AlertDialogManager.hideProgressDialog();
                try {
                    for (DataSnapshot mDataSnapshot : dataSnapshot.getChildren()){
                        LibraryModel mLibraryModel = mDataSnapshot.getValue(LibraryModel.class);
                        mLibraryModel.setmFormID(mDataSnapshot.getKey());
                        mFormsArrayList.add(mLibraryModel);
                    }

                    if (mFormsArrayList.size() > 0){
                        withoutSavedLL.setVisibility(View.GONE);
                        mMyLibraryRV.setVisibility(View.VISIBLE);
                        setAdapter();
                    }else{
                        withoutSavedLL.setVisibility(View.VISIBLE);
                        mMyLibraryRV.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "==ERROR==");
                AlertDialogManager.hideProgressDialog();
            }
        });
    }

    private void setAdapter() {
        mLibraryPostsAdapter = new LibraryPostsAdapter(mActivity, mFormsArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mMyLibraryRV.setLayoutManager(mLayoutManager);
        mMyLibraryRV.addItemDecoration(new SimpleDividerItemDecoration(mActivity));
        mMyLibraryRV.setAdapter(mLibraryPostsAdapter);
    }

    @OnClick(R.id.imgBackIV)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
