package com.painly.com.beans;

import java.io.Serializable;

/**
 * Created by android-da on 5/16/18.
 */

public class UserDetails implements Serializable {

    public String bio = "";
    public String coverPhoto = "";
    public String gender = "";
    public String image = "";
    public String name = "";
    public String pushToken = "";
    public String uid = "";
    public String username = "";
    public String deviceType = "";
    public int placeholderImage;
    public Boolean isMachineLearningEnabled;
    public Boolean isPremium;
    public long lastLogin;

    Location location;

    public Boolean getMachineLearningEnabled() {
        return isMachineLearningEnabled;
    }

    public void setMachineLearningEnabled(Boolean machineLearningEnabled) {
        isMachineLearningEnabled = machineLearningEnabled;
    }

    public Boolean getPremium() {
        return isPremium;
    }

    public void setPremium(Boolean premium) {
        isPremium = premium;
    }

    public long getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(long lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getBio() {
        return bio;
    }

    public int getPlaceholderImage() {
        return placeholderImage;
    }

    public void setPlaceholderImage(int placeholderImage) {
        this.placeholderImage = placeholderImage;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
