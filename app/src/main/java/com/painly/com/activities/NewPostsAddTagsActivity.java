package com.painly.com.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.painly.com.BaseActivity;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.adapters.NewTagsAdapter;
import com.painly.com.beans.Conditions;
import com.painly.com.beans.FormsModel;
import com.painly.com.beans.PostImageModel;
import com.painly.com.font.ButtonArialRegular;
import com.painly.com.font.EditTextArialRegular;
import com.painly.com.interfaces.SelectConditions;
import com.painly.com.utils.Constants;
import com.painly.com.utils.Utilities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewPostsAddTagsActivity extends BaseActivity {
    Activity mActivity = NewPostsAddTagsActivity.this;
    String TAG = NewPostsAddTagsActivity.this.getClass().getSimpleName();
    @BindView(R.id.leftLL)
    LinearLayout leftLL;
    @BindView(R.id.editSearchET)
    EditTextArialRegular editSearchET;
    @BindView(R.id.conditionsRV)
    RecyclerView conditionsRV;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.btnPublishPostB)
    ButtonArialRegular btnPublishPostB;


    NewTagsAdapter mNewTagsAdapter;
    ArrayList<Conditions> mConditionsAL = new ArrayList<Conditions>();
    ArrayList<Conditions> mSelectedTagsArrayList = new ArrayList<Conditions>();

    String strPostType = "", strTitle = "", strBody = "", strLink = "", strPostPictureURL = "", mForumKey = "", mForumTitle = "";
    //byte[] mImageByteArray = null;

    Uri mImageUri;

    Bitmap mImageBitmap;

    DatabaseReference mRootRefrence, mFormRef;
    SelectConditions mSelectConditions = new SelectConditions() {
        @Override
        public void mSelectConditions(Boolean mBoolean, ArrayList<Conditions> mArrayList) {
            mSelectedTagsArrayList = mArrayList;
        }
    };
    private StorageReference mStorageRef;


    ArrayList<PostImageModel> mPostImageArrayList= new ArrayList<PostImageModel>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_posts_add_tags);
        ButterKnife.bind(this);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        getPostData();

        executeTagsData();
        setSearch();
    }

    private void getPostData() {
        if (getIntent() != null) {
            if (getIntent().getStringExtra(Constants.POST_TYPE) != null) {
                if (getIntent().getStringExtra(Constants.POST_TYPE).equals(getString(R.string.link_type))) {
                    strTitle = getIntent().getStringExtra(Constants.POST_TITLE);
                    strBody = getIntent().getStringExtra(Constants.POST_BODY);
                    strLink = getIntent().getStringExtra(Constants.POST_LINK);
                    if (getIntent().getStringExtra(Constants.POST_PICTURE_URL) != null && getIntent().getStringExtra(Constants.POST_PICTURE_URL).contains("http")){
                        strPostPictureURL = getIntent().getStringExtra(Constants.POST_PICTURE_URL);
                    }else if (getIntent().getStringExtra(Constants.POST_PICTURE_URI) != null && getIntent().getStringExtra(Constants.POST_PICTURE_URI).length() > 0 ){
                        mImageUri =Uri.parse(getIntent().getStringExtra(Constants.POST_PICTURE_URI));
                        try {
                            mImageBitmap = getThumbnail(mImageUri);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    strPostType = getIntent().getStringExtra(Constants.POST_TYPE);
                }

                if (getIntent().getStringExtra(Constants.POST_TYPE).equals(getString(R.string.video_type))) {
                    strTitle = getIntent().getStringExtra(Constants.POST_TITLE);
                    strBody = getIntent().getStringExtra(Constants.POST_BODY);
                    strLink = getIntent().getStringExtra(Constants.POST_LINK);
                    if (getIntent().getStringExtra(Constants.POST_PICTURE_URL) != null && getIntent().getStringExtra(Constants.POST_PICTURE_URL).contains("http")){
                        strPostPictureURL = getIntent().getStringExtra(Constants.POST_PICTURE_URL);
                    }else if (getIntent().getStringExtra(Constants.POST_PICTURE_URI) != null && getIntent().getStringExtra(Constants.POST_PICTURE_URI).length() > 0){
                        mImageUri =Uri.parse(getIntent().getStringExtra(Constants.POST_PICTURE_URI));
                        try {
                            mImageBitmap = getThumbnail(mImageUri);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    strPostType = getIntent().getStringExtra(Constants.POST_TYPE);
                }

                if (getIntent().getStringExtra(Constants.POST_TYPE).equals(getString(R.string.picture_type))) {
                    strTitle = getIntent().getStringExtra(Constants.POST_TITLE);
                    strBody = getIntent().getStringExtra(Constants.POST_BODY);

                    if (getIntent().getStringExtra(Constants.POST_PICTURE_URI) != null && getIntent().getStringExtra(Constants.POST_PICTURE_URI).length() > 0) {
                        mImageUri = Uri.parse(getIntent().getStringExtra(Constants.POST_PICTURE_URI));
                        try {
                            mImageBitmap = getThumbnail(mImageUri);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    strPostType = getIntent().getStringExtra(Constants.POST_TYPE);
                }

                if (getIntent().getStringExtra(Constants.POST_TYPE).equals(getString(R.string.text_type))) {
                    strTitle = getIntent().getStringExtra(Constants.POST_TITLE);
                    strBody = getIntent().getStringExtra(Constants.POST_BODY);
                    strPostType = getIntent().getStringExtra(Constants.POST_TYPE);
                }

            }
        }

    }

    private void setSearch() {
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable mEditable) {
                //after the change calling the method and passing the search input
                filter(mEditable.toString());
            }
        });
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<Conditions> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (Conditions s : mConditionsAL) {
            //if the existing elements contains the search input
            if (s.getName().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(s);
            }
        }
        //calling a method of the adapter class and passing the filtered list
        mNewTagsAdapter.filterList(filterdNames);
    }

    private void executeTagsData() {
        if (Utilities.isNetworkAvailable(mActivity))
            getConditionsData();
        else
            Toast.makeText(mActivity, getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
    }

    @OnClick({R.id.leftLL, R.id.btnPublishPostB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.leftLL:
                onBackPressed();
                break;
            case R.id.btnPublishPostB:
                publishPost();
                break;
        }
    }

    private void publishPost() {
        if (mSelectedTagsArrayList.size() == 0) {
            Toast.makeText(mActivity, getString(R.string.atleast_one_conditions), Toast.LENGTH_SHORT).show();
        } else {
            showProgressDialog(mActivity);
            if (mImageBitmap != null) {
                uploadImage();
            } else {
                putPost();
            }
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void getConditionsData() {
        showProgressDialog(mActivity);
        mRootRefrence.child("conditions").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideProgressDialog();
                mConditionsAL.clear();
                for (DataSnapshot formsSnapshot : dataSnapshot.getChildren()) {
                    String conditionID = formsSnapshot.getKey();
                    final Conditions mConditions = formsSnapshot.getValue(Conditions.class);
                    mConditions.setConditionID(conditionID);
                    mConditionsAL.add(mConditions);

                }

                Collections.sort(mConditionsAL, new Comparator<Conditions>() {
                    @Override
                    public int compare(Conditions item, Conditions t1) {
                        String s1 = item.getName();
                        String s2 = t1.getName();
                        return s1.compareToIgnoreCase(s2);
                    }
                });
                setAdapter();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideProgressDialog();
                Log.e(TAG, "Error::" + databaseError.toString());
                Toast.makeText(mActivity, databaseError.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setAdapter() {
        mNewTagsAdapter = new NewTagsAdapter(mActivity, mConditionsAL, mSelectConditions);
        conditionsRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        conditionsRV.setLayoutManager(mLayoutManager);
        conditionsRV.setItemAnimator(new DefaultItemAnimator());
        conditionsRV.setAdapter(mNewTagsAdapter);
    }


    /*PUBLISH POST ACCORDING TO THEIR TYPE*/
    private void uploadImage() {
        showProgressDialog(mActivity);
        try {
            //Upload Image:::::
            mFormRef = FirebaseDatabase.getInstance().getReference().child("forums").push();
            StorageReference storageRef = mStorageRef.child("forums/").child(mFormRef.getKey());
            //Bitmap mBitmap = BitmapFactory.decodeByteArray(mImageByteArray, 0, mImageByteArray.length);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            mImageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteArray = baos.toByteArray();

            UploadTask uploadTask = storageRef.putBytes(byteArray);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    hideProgressDialog();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    strPostPictureURL = taskSnapshot.getDownloadUrl().toString();
                    putPost();
                }
            });

        } catch (Exception mE) {
            Log.e(TAG, "=====ERROR====" + mE);
        }
    }

    private void putPost() {
        HashMap<String, String> mConditions = new HashMap<String, String>();
        for (int i = 0; i < mSelectedTagsArrayList.size(); i++) {
            mConditions.put(mSelectedTagsArrayList.get(i).getConditionID(), mSelectedTagsArrayList.get(i).getConditionID());
        }

        HashMap data = new HashMap<>();

        data.put("opUID", FirebaseAuth.getInstance().getUid());
        data.put("body", strBody);
        data.put("timestamp", Utilities.getCurrentTimeStamp());
        data.put("title", strTitle);
        data.put("conditions", mConditions);
        if (strPostType.equals(getString(R.string.link_type))) {
            data.put("type", getResources().getString(R.string.link_type));
            data.put("link", strLink);
            data.put("picture", strPostPictureURL);
        }
        if (strPostType.equals(getString(R.string.video_type))) {
            data.put("type", getResources().getString(R.string.video_type));
            data.put("video", strLink);
            data.put("picture", strPostPictureURL);
        }
        if (strPostType.equals(getString(R.string.picture_type))) {
            data.put("type", getResources().getString(R.string.picture_type));
            data.put("picture", strPostPictureURL);
        }
        if (strPostType.equals(getString(R.string.text_type))) {
            data.put("type", getResources().getString(R.string.text_type));
        }

        mForumKey = mRootRefrence.child("forums").push().getKey();
        mRootRefrence.child("forums").child(mForumKey).setValue(data);
        mRootRefrence.child("users").child(Constants.USER_ID).child("forums").child(mForumKey).setValue(mForumKey);

        //Get Form Title
        mRootRefrence.child("forums").child(mForumKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                FormsModel mFormsModel = dataSnapshot.getValue(FormsModel.class);
                if (mFormsModel != null && mFormsModel.getTitle() != null){
                    mForumTitle = mFormsModel.getTitle();
                }

                //Execute Notification API
                executeNotificationAPI(mForumKey, mForumTitle);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideProgressDialog();
            }
        });
    }

    private void executeNotificationAPI(String strFormKey, String strFormTitle) {
        String userName = Constants.USER_NAME;
        String strAPIURL = Constants.PUSH_NOTIFICATION_ADD_POST + "&commentID=" + strFormKey + "&commentType=" + getString(R.string.link_type) + "&message=" + userName + " " + Constants.ADD_POST_MESSAGE + " " + strFormTitle + "&type=" + Constants.COMMENT + "&senderAuthID=" + Constants.USER_ID;
        strAPIURL = strAPIURL.replace(" ", "%20");
        Log.e(TAG, "=====API URL====" + strAPIURL);
        StringRequest request = new StringRequest(Request.Method.GET, strAPIURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "===RESPONSE===" + response);
                try {
                    Log.e(TAG, "===SUCESSFULL NOTIFICATIONS===");
                    hideProgressDialog();
                    finishPostOpenHome();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                finishPostOpenHome();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }

        };
        PainlyApplication.getInstance().addToRequestQueue(request);

    }

    private void finishPostOpenHome() {
        Intent newIntent = new Intent(mActivity, HomeActivity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(newIntent);
        finish();
    }
}
