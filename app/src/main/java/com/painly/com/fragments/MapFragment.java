package com.painly.com.fragments;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.activities.HomeActivity;
import com.painly.com.activities.NewAllUsersProfileActivity;
import com.painly.com.beans.FavoriteModel;
import com.painly.com.beans.User;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;
import com.painly.com.utils.PainlySingleton;
import com.painly.com.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {
    private final int REQUEST_PERMISSION_CODE = 919;
    View mView;
    Unbinder unbinder;
    String TAG = MapFragment.this.getClass().getSimpleName();
    ArrayList<User> mAllUsersArrayList = PainlySingleton.getInstance().getAllUsersArrayList();
    ArrayList<User> mFavoriteUserArrayList = new ArrayList<>();
    DatabaseReference mRootRefrence;
    Marker marker;
    /*AllUser/PainPals/TurnOff Locations*/
    TextView txtPainPalsTV, txtAllUsersTV, txtTurnOffTV;
    Double mZipCodeLat = 0.0;
    Double mZipCodeLong = 0.0;
    boolean isZipCode = false;
    boolean isfirstTime = true;
    private String mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION;
    private String mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;
    private GoogleMap mMap;
    private TextView txtAllowLocationTV, txtZipCodeET;
    private LinearLayout mapLL, allowLocationLL;
    /*Progress Dialog*/
    private Dialog progressDialog;


    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.e(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_map, container, false);
        unbinder = ButterKnife.bind(this, mView);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        AlertDialogManager.hideProgressDialog();

        setWidgetIDs(mView);

        setClickListner();

        if (Utilities.isNetworkAvailable(getActivity())) {
            if (mAllUsersArrayList.size() == 0)
                getAllUsersDataForMap();
        } else {
            Toast.makeText(getActivity(), getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
        }

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return mView;
    }

    private void setWidgetIDs(View v) {
        txtPainPalsTV = v.findViewById(R.id.txtPainPalsTV);
        txtAllUsersTV = v.findViewById(R.id.txtAllUsersTV);
        txtTurnOffTV = v.findViewById(R.id.txtTurnOffTV);
        txtZipCodeET = v.findViewById(R.id.txtZipCodeET);
        txtAllowLocationTV = v.findViewById(R.id.txtAllowLocationTV);
        mapLL = v.findViewById(R.id.mapLL);
        allowLocationLL = v.findViewById(R.id.allowLocationLL);

        setAllUsersClick();
        if (PainlyPreference.readBoolean(getActivity(), PainlyPreference.ALLOW_LOCATIONS_FIRST_TIME, false)) {
            mapLL.setVisibility(View.VISIBLE);
            allowLocationLL.setVisibility(View.GONE);
        } else {
            allowLocationLL.setVisibility(View.VISIBLE);
            mapLL.setVisibility(View.GONE);
        }
    }

    private void setPainPalClick() {
        txtPainPalsTV.setVisibility(View.GONE);
        txtAllUsersTV.setVisibility(View.VISIBLE);
    }

    private void setAllUsersClick() {
        txtPainPalsTV.setVisibility(View.VISIBLE);
        txtAllUsersTV.setVisibility(View.GONE);
    }

    private void setClickListner() {
        txtTurnOffTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                turnOffLocations();
            }
        });
        txtPainPalsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPainPalClick();
//                if (Utilities.isNetworkAvailable(getActivity())) {
//                    getAllUsersDataForMap();
//                } else {
//                    Toast.makeText(getActivity(), getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
//                }

                refreshgoogleMapWithAllUsers();
            }
        });
        txtAllUsersTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAllUsersClick();
//                if (Utilities.isNetworkAvailable(getActivity())) {
//                    getAllUsersDataForMap();
//                } else {
//                    Toast.makeText(getActivity(), getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
//                }

                refreshgoogleMapWithPainPals();
            }
        });


        txtZipCodeET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterZipCodeDialog();
            }
        });

        txtAllowLocationTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity mActivity = getActivity();
                if (mActivity instanceof HomeActivity) {
                    HomeActivity mHomeActivity = (HomeActivity) mActivity;
                    if (checkPermission()) {
                        if (!isZipCode) {
                            mHomeActivity.checkLocationSettings();
                            turnOnLocations();
                            setAllUsersClick();
                            updateLocationOfUser();
                            //getFavoriteUsersID();
                            animateCameraToCurrentLocation();
                        } else {
                            isZipCode = false;
                            mHomeActivity.checkLocationSettings();
                            turnOnLocations();
                            setAllUsersClick();
                            updateLocationOfUser();
                            animateCameraToZipCodeLocation();
                        }
                    } else {
                        requestPermission();
                    }
                }
            }
        });
    }

    private void getAllUsersDataForMap() {
        showProgressDialog(getActivity());
        // let ref = Globals.baseRef.child("users").queryOrdered(byChild: "details/location/latitude").queryStarting(atValue: -999999.00)
        mRootRefrence.child("users").orderByChild("details/location/latitude").startAt(-999999.00).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideProgressDialog();
                try {
                    for (DataSnapshot mUserSnapshot : dataSnapshot.getChildren()) {
                        final User mUser = mUserSnapshot.getValue(User.class);
                        mAllUsersArrayList.add(mUser);
                    }

                    Log.e(TAG, "==Location User Count==" + mAllUsersArrayList.size());
                    /*Getting Current User Favorite/PainPlals Users IDs*/
                    getFavoriteUsersID();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideProgressDialog();
                Log.e(TAG, "==Error==" + databaseError.toString());
            }
        });
    }

    private void getFavoriteUsersID() {
        String strUserID = Constants.USER_ID;
        mRootRefrence.child("users").child(strUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                FavoriteModel mFavoriteModel = dataSnapshot.getValue(FavoriteModel.class);
                Collection<String> values = mFavoriteModel.getFavorites().keySet();
                ArrayList<String> mFavUserIDsAL = new ArrayList<String>(values);

                getPainPalsFromUsers(mFavUserIDsAL);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "====Error+===" + databaseError.toString());
                hideProgressDialog();
            }
        });
    }

    private void getPainPalsFromUsers(ArrayList<String> mFavUserIDsAL) {
        for (int i = 0; i < mAllUsersArrayList.size(); i++) {
            for (int j = 0; j < mFavUserIDsAL.size(); j++) {
                if (mAllUsersArrayList.get(i).getDetails() != null) {
                    if (mFavUserIDsAL.get(j).equals(mAllUsersArrayList.get(i).getDetails().getUid())) {
                        mFavoriteUserArrayList.add(mAllUsersArrayList.get(i));
                    }
                }

            }
        }

        /*set PainPals Marker on Map*/
        refreshgoogleMapWithPainPals();

    }


    private void turnOnLocations() {
        PainlyPreference.writeBoolean(getActivity(), PainlyPreference.ALLOW_LOCATIONS_FIRST_TIME, true);
        mapLL.setVisibility(View.VISIBLE);
        allowLocationLL.setVisibility(View.GONE);
    }

    private void turnOffLocations() {
        PainlyPreference.writeBoolean(getActivity(), PainlyPreference.ALLOW_LOCATIONS_FIRST_TIME, false);
        mapLL.setVisibility(View.GONE);
        allowLocationLL.setVisibility(View.VISIBLE);
    }

    private void updateLocationOfUser() {
       /*Details HasMap*/
        HashMap mDetailsHM = new HashMap<>();
        mDetailsHM.put("latitude", Double.longBitsToDouble(PainlyPreference.readLong(getActivity(), PainlyPreference.CURRENT_LOCATION_LATITUDE, 0)));
        mDetailsHM.put("longitude", Double.longBitsToDouble(PainlyPreference.readLong(getActivity(), PainlyPreference.CURRENT_LOCATION_LONGITUDE, 0)));

        mRootRefrence.child("users").child(PainlyPreference.readString(getActivity(), PainlyPreference.USER_ID, "")).child("details").child("location").setValue(mDetailsHM);
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: ");
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        Log.e(TAG, "onMapReady: ");
        mMap = googleMap;
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
    }

    private void refreshgoogleMapWithPainPals() {
        Log.e(TAG, "refreshgoogleMapWithPainPals: ");
        if (mMap != null)
            mMap.clear();
        if (isfirstTime) {
            if (Constants.LAT != 0 && Constants.LONG != 0) {
                LatLng coordinate = new LatLng(Constants.LAT, Constants.LONG);
                CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15);
                if (mMap != null)
                    mMap.animateCamera(yourLocation);
            }
            isfirstTime = false;
        }

        Activity mActivity = getActivity();
        if (mActivity != null) {
            if (mFavoriteUserArrayList.size() > 0) {
                for (int i = 0; i < mFavoriteUserArrayList.size(); i++) {
                    if (mFavoriteUserArrayList.get(i) != null) {
                        if (mFavoriteUserArrayList.get(i).getDetails() != null) {
                            if (mFavoriteUserArrayList.get(i).getDetails().getLocation() != null) {

                                final Double mLat = Double.parseDouble(mFavoriteUserArrayList.get(i).getDetails().getLocation().getLatitude().toString());
                                final Double mLong = Double.parseDouble(mFavoriteUserArrayList.get(i).getDetails().getLocation().getLongitude().toString());
                                final int finalI = i;

                                try {
                                    Glide.with(getActivity().getApplicationContext())
                                            .asBitmap()
                                            .load(mFavoriteUserArrayList.get(finalI).getDetails().getImage())
                                            .into(new SimpleTarget<Bitmap>(100, 100) {
                                                @Override
                                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                    Bitmap mFinalBitmap = scaleBitmap(resource, 130, 130);
                                                    marker = mMap.addMarker(new MarkerOptions().position(new LatLng(mLat, mLong)).icon(BitmapDescriptorFactory.fromBitmap(mFinalBitmap)));
                                                    marker.setTag(mFavoriteUserArrayList.get(finalI).getDetails().getUid());
                                                }

                                                @Override
                                                public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                                    super.onLoadFailed(errorDrawable);
                                                    marker = createUserPicMarker(mMap, mLat, mLong);
                                                    marker.setTag(mFavoriteUserArrayList.get(finalI).getDetails().getUid());
                                                }
                                            });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        }


        if (mMap != null) {
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    String strString = String.valueOf(marker.getTag());
                    if (strString != null) {
                        if (PainlyPreference.readString(getActivity(), PainlyPreference.USER_ID, "").equals(strString)) {
                            Intent mIntent = new Intent(getActivity(), NewAllUsersProfileActivity.class);
                            mIntent.putExtra("UID", strString);
                            mIntent.putExtra("TYPE", Constants.CURRENT);
                            startActivity(mIntent);
                        } else {
                            Intent mIntent = new Intent(getActivity(), NewAllUsersProfileActivity.class);
                            mIntent.putExtra("UID", strString);
                            mIntent.putExtra("TYPE", Constants.ANOTHER);
                            startActivity(mIntent);
                        }
                    }
                    return false;
                }
            });

        }
    }


    protected Marker createUserPicMarker(GoogleMap googleMap, double latitude, double longitude) {

        if (getActivity() != null) {
            Bitmap mBitmap = BitmapFactory.decodeResource(getActivity().getResources(), PainlyApplication.getInstance().getRandom());
            Bitmap mFinalBitmap = scaleBitmap(mBitmap, 130, 130);
            return googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
                    .icon(BitmapDescriptorFactory.fromBitmap(mFinalBitmap)));
        } else {
            return googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
                    .icon(BitmapDescriptorFactory.fromResource(PainlyApplication.getInstance().getRandom())));
        }
    }


    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void refreshgoogleMapWithAllUsers() {
        if (mMap != null)
            mMap.clear();
        if (Constants.LAT != 0 && Constants.LONG != 0) {
            LatLng coordinate = new LatLng(Constants.LAT, Constants.LONG);
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15);
            if (mMap != null)
                mMap.animateCamera(yourLocation);
        }

        if (getActivity() != null) {
            Log.e(TAG, "onMapReadyAllUserSIZE: " + mAllUsersArrayList.size());
            if (mAllUsersArrayList.size() > 0) {
                for (int i = 0; i < mAllUsersArrayList.size(); i++) {
                    if (mAllUsersArrayList.get(i) != null) {
                        if (mAllUsersArrayList.get(i).getDetails() != null) {
                            if (mAllUsersArrayList.get(i).getDetails().getLocation() != null) {
                                final Double mLat = Double.parseDouble(mAllUsersArrayList.get(i).getDetails().getLocation().getLatitude().toString());
                                final Double mLong = Double.parseDouble(mAllUsersArrayList.get(i).getDetails().getLocation().getLongitude().toString());

                                try {
                                    final int finalI = i;
                                    Glide.with(getActivity())
                                            .asBitmap()
                                            .load(mAllUsersArrayList.get(finalI).getDetails().getImage())
                                            .into(new SimpleTarget<Bitmap>(50, 50) {
                                                @Override
                                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                    Bitmap mFinalBitmap = scaleBitmap(resource, 130, 130);
                                                    marker = mMap.addMarker(new MarkerOptions().position(new LatLng(mLat, mLong)).icon(BitmapDescriptorFactory.fromBitmap(mFinalBitmap)));
                                                    marker.setTag(mAllUsersArrayList.get(finalI).getDetails().getUid());
                                                }

                                                @Override
                                                public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                                    super.onLoadFailed(errorDrawable);
                                                    marker = createUserPicMarker(mMap, mLat, mLong);
                                                    marker.setTag(mAllUsersArrayList.get(finalI).getDetails().getUid());
                                                }
                                            });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        }


        if (mMap != null) {
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    String strString = String.valueOf(marker.getTag());
                    if (strString != null) {
                        if (PainlyPreference.readString(getActivity(), PainlyPreference.USER_ID, "").equals(strString)) {
                            Intent mIntent = new Intent(getActivity(), NewAllUsersProfileActivity.class);
                            mIntent.putExtra("UID", strString);
                            mIntent.putExtra("TYPE", Constants.CURRENT);
                            startActivity(mIntent);
                        } else {
                            Intent mIntent = new Intent(getActivity(), NewAllUsersProfileActivity.class);
                            mIntent.putExtra("UID", strString);
                            mIntent.putExtra("TYPE", Constants.ANOTHER);
                            startActivity(mIntent);
                        }
                    }
                    return false;
                }
            });

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     **********/
    private boolean checkPermission() {
        int mlocationFineP = ContextCompat.checkSelfPermission(getActivity(), mAccessFineLocation);
        int mlocationCourseP = ContextCompat.checkSelfPermission(getActivity(), mAccessCourseLocation);

        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        requestPermissions(new String[]{mAccessFineLocation, mAccessCourseLocation}, REQUEST_PERMISSION_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Activity mActivity = getActivity();
                    if (mActivity instanceof HomeActivity) {
                        HomeActivity mHomeActivity = (HomeActivity) mActivity;
                        mHomeActivity.checkLocationSettings();
                        turnOnLocations();
                        setAllUsersClick();
                        updateLocationOfUser();
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //requestPermission();
                }
                return;
            }

        }
    }

    private void enterZipCodeDialog() {
        final Dialog alertDialog = new Dialog(getActivity(), R.style.PauseDialog);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_enterzipcode);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final EditText editZipCodeET = alertDialog.findViewById(R.id.editZipCodeET);
        Button btnSaveB = alertDialog.findViewById(R.id.btnSaveB);
        Button btnCancelB = alertDialog.findViewById(R.id.btnCancelB);
        final ProgressBar mProgressBarPB = alertDialog.findViewById(R.id.mProgressBarPB);

        btnCancelB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        btnSaveB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editZipCodeET.getText().toString().trim().equals("")) {
                    editZipCodeET.setError(getString(R.string.please_enter_zip_code));
                } else {
                    //alertDialog.dismiss();
                    executeGoogleMapAPIGetLatLong(alertDialog, editZipCodeET.getText().toString().trim(), mProgressBarPB);
                }
            }
        });

        alertDialog.show();
    }


    private void executeGoogleMapAPIGetLatLong(final Dialog mDialog, String strZipCode, final ProgressBar mProgressBar) {
        mProgressBar.setVisibility(View.VISIBLE);
        //GEOcODE aPI KEY
        //String strAPIURL = Constants.GOOGLE_MAP_API_GET_LAT_LONG + strZipCode + "&key=AIzaSyAA5ZeSxnn5AdDcZ-Q7-kINt_Ml8Jjk038";
        String strAPIURL = Constants.GOOGLE_MAP_API_GET_LAT_LONG + strZipCode + "&key=AIzaSyC0LmY7ZkLyxS8p3hbrhOfg1bO3Up8K_7o";//AIzaSyDrVgv8TTfbzuCN-V1PgX1OJjIVtZeg73k
        Log.e(TAG, "=====Google Map API====" + strAPIURL);
        StringRequest request = new StringRequest(Request.Method.GET, strAPIURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressBar.setVisibility(View.GONE);
                try {
                    Log.e(TAG, "===RESPONSE===" + response);
                    JSONObject mJsonObject = new JSONObject(response);
                    if (mJsonObject.getString("status").equals("OK")) {
                        isZipCode = true;
                        if (!mJsonObject.isNull("results")) {
                            JSONArray mJsonArray = mJsonObject.getJSONArray("results");
                            for (int i = 0; i < mJsonArray.length(); i++) {
                                if (i == 0) {
                                    JSONObject mJsonObj = mJsonArray.getJSONObject(i);
                                    if (!mJsonObj.isNull("geometry")) {
                                        JSONObject mGeoObj = mJsonObj.getJSONObject("geometry");
                                        if (!mGeoObj.isNull("location")) {
                                            JSONObject mLocationObj = mGeoObj.getJSONObject("location");
                                            mZipCodeLat = mLocationObj.getDouble("lat");
                                            mZipCodeLong = mLocationObj.getDouble("lng");
                                            Log.e(TAG, "===LAT===" + mZipCodeLat + "==LONG==" + mZipCodeLong);
                                        }
                                    }

                                }
                            }
                        }

                    } else {
                        Toast.makeText(getActivity(), "Server Not Found!", Toast.LENGTH_SHORT).show();
                    }
                    mDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "***Error**" + error.toString());
                mProgressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Server Not Found!", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };
        PainlyApplication.getInstance().addToRequestQueue(request);
    }

    private void animateCameraToZipCodeLocation() {
        Log.e(TAG, "animateCameraToZipCodeLocation: ");
        LatLng coordinate = new LatLng(mZipCodeLat, mZipCodeLong);
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15);
        if (mMap != null)
            mMap.animateCamera(yourLocation);
    }

    private void animateCameraToCurrentLocation() {
        if (Constants.LAT != 0 && Constants.LONG != 0) {
            LatLng coordinate = new LatLng(Constants.LAT, Constants.LONG);
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15);
            if (mMap != null)
                mMap.animateCamera(yourLocation);
        }
    }


    private void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }

    }

    public Bitmap scaleBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);
        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return getCircleBitmap1(resizedBitmap);
    }


    public Bitmap getCircleBitmap1(Bitmap source) {
        int size = Math.min(source.getHeight(), source.getWidth());
        Bitmap output = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, size, size);
        RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(source, rect, rect, paint);

        return addBorderToBitmap(output, 7, Color.BLACK);
    }


    // Custom method to add a border around bitmap
    protected Bitmap addBorderToBitmap(Bitmap srcBitmap, int borderWidth, int borderColor) {
        // Initialize a new Bitmap to make it bordered bitmap
        Bitmap dstBitmap = Bitmap.createBitmap(
                srcBitmap.getWidth() + borderWidth * 2, // Width
                srcBitmap.getHeight() + borderWidth * 2, // Height
                Bitmap.Config.ARGB_8888 // Config
        );
        // Initialize a new Canvas instance
        Canvas canvas = new Canvas(dstBitmap);
        // Initialize a new Paint instance to draw border
        Paint paint = new Paint();
        paint.setColor(borderColor);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderWidth);
        paint.setAntiAlias(true);
        Rect rect = new Rect(
                borderWidth / 2,
                borderWidth / 2,
                canvas.getWidth() - borderWidth / 2,
                canvas.getHeight() - borderWidth / 2
        );

        RectF rectF = new RectF(rect);
        // Draw a rectangle as a border/shadow on canvas
        canvas.drawOval(rectF, paint);
        // Draw source bitmap to canvas
        canvas.drawBitmap(srcBitmap, borderWidth, borderWidth, null);
        srcBitmap.recycle();
        return dstBitmap;
    }

}




