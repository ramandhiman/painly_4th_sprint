package com.painly.com.chat;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by android-da on 6/12/18.
 */

public class MessageR implements Serializable {
    private String messageID = "";
    private HashMap<String, MessageDataR> messages = new HashMap<>();
    private HashMap<String, MessageUserR> users = new HashMap<>();

   public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public HashMap<String, MessageDataR> getMessages() {
        return messages;
    }

    public void setMessages(HashMap<String, MessageDataR> messages) {
        this.messages = messages;
    }

    public HashMap<String, MessageUserR> getUsers() {
        return users;
    }

    public void setUsers(HashMap<String, MessageUserR> users) {
        this.users = users;
    }
}
