package com.painly.com.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.painly.com.R;
import com.painly.com.font.TextViewArialBold;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdminActivity extends AppCompatActivity {
    String TAG = AdminActivity.this.getClass().getSimpleName();
    Activity mActivity = AdminActivity.this;

    @BindView(R.id.imgBackLL)
    LinearLayout imgBackLL;
    @BindView(R.id.txtAddConditionsTV)
    TextViewArialBold txtAddConditionsTV;
    @BindView(R.id.txtSendMassPushTV)
    TextViewArialBold txtSendMassPushTV;
    @BindView(R.id.txtRemovePostTV)
    TextViewArialBold txtRemovePostTV;
    @BindView(R.id.txtBanUserTV)
    TextViewArialBold txtBanUserTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.imgBackLL, R.id.txtAddConditionsTV, R.id.txtSendMassPushTV, R.id.txtRemovePostTV, R.id.txtBanUserTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackLL:
                onBackPressed();
                break;
            case R.id.txtAddConditionsTV:
                break;
            case R.id.txtSendMassPushTV:
                startActivity(new Intent(mActivity,MassPushActivity.class));
                break;
            case R.id.txtRemovePostTV:
                break;
            case R.id.txtBanUserTV:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
