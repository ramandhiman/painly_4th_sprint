package com.painly.com.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.activities.NewAllUsersProfileActivity;
import com.painly.com.activities.NewForumsDetailsActivity;
import com.painly.com.activities.TextDetailsActivity;
import com.painly.com.beans.FormsModel;
import com.painly.com.beans.NotificationsModel;
import com.painly.com.chat.ChatwithUserActivity;
import com.painly.com.interfaces.NotificationsUpdateInterface;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Constants;

import java.util.ArrayList;

/**
 * Created by android-raman on 2/5/18.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {
    String TAG = "NotificationsAdapter";
    String strImageURl = "";
    DatabaseReference mRootRefrence;
    private ArrayList<NotificationsModel> mArrayList;
    private Activity mActivity;
    private NotificationsUpdateInterface mNotificationsUpdateInterface;

    public NotificationsAdapter(Activity mActivity, ArrayList<NotificationsModel> mArrayList, NotificationsUpdateInterface mNotificationsUpdateInterface) {
        this.mArrayList = mArrayList;
        this.mActivity = mActivity;
        this.mNotificationsUpdateInterface = mNotificationsUpdateInterface;
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notifications, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final NotificationsModel mNotificationsModel = mArrayList.get(position);

        holder.txtMessageTV.setText(mNotificationsModel.getMessage());

        if ((Boolean) mNotificationsModel.getIsNew() == true) {
            Log.e(TAG, "======ISNEW=====" + mNotificationsModel.getIsNew());
            holder.imgISReadNotIV.setVisibility(View.VISIBLE);
        } else {
            Log.e(TAG, "======ISNEW=====" + mNotificationsModel.getIsNew());
            holder.imgISReadNotIV.setVisibility(View.GONE);
        }

        if (mNotificationsModel.getType().equals(Constants.CHAT)) {
            holder.txtHeadingTV.setText(mActivity.getResources().getString(R.string.new_message));
        } else if (mNotificationsModel.getType().equals(Constants.FAVORITE)) {
            holder.txtHeadingTV.setText(Constants.FAVORITE_HEADING);
        } else if (mNotificationsModel.getType().toLowerCase().equals(Constants.COMMENT.toLowerCase()) && mNotificationsModel.getMessage().toLowerCase().contains("added the post")) {
            holder.txtHeadingTV.setText(mActivity.getResources().getString(R.string.new_post));
        } else if (mNotificationsModel.getType().toLowerCase().equals(Constants.COMMENT.toLowerCase()) && mNotificationsModel.getMessage().toLowerCase().contains("comment")) {
            holder.txtHeadingTV.setText(mActivity.getResources().getString(R.string.new_comment));
        }

        holder.itemLayoutClickLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mNotificationsModel.getType().equals(Constants.FAVORITE)) {
                    mNotificationsUpdateInterface.mUpdateNotifications(mNotificationsModel, position);
                    Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                    mIntent.putExtra("UID", mNotificationsModel.getSenderAuthID());
                    mIntent.putExtra("TYPE", Constants.ANOTHER);
                    mActivity.startActivity(mIntent);

                } else if (mNotificationsModel.getType().equals(Constants.CHAT)) {
                    mNotificationsUpdateInterface.mUpdateNotifications(mNotificationsModel, position);
                    Intent intent = new Intent(mActivity, ChatwithUserActivity.class);
                    intent.putExtra("UID", mNotificationsModel.getSenderAuthID());
                    mActivity.startActivity(intent);

                } else if (mNotificationsModel.getType().toLowerCase().equals(Constants.COMMENT.toLowerCase()) && mNotificationsModel.getCommentType().equals(mActivity.getResources().getString(R.string.video_type))) {
                    Log.e(TAG, "====Form ID===" + mNotificationsModel.getCommentID());
                    mNotificationsUpdateInterface.mUpdateNotifications(mNotificationsModel, position);
                    checkPostDeletedOrNot(mNotificationsModel);

                } else if (mNotificationsModel.getType().toLowerCase().equals(Constants.COMMENT.toLowerCase()) && mNotificationsModel.getCommentType().equals(mActivity.getResources().getString(R.string.picture_type))) {
                    mNotificationsUpdateInterface.mUpdateNotifications(mNotificationsModel, position);
                    Log.e(TAG, "====Form ID===" + mNotificationsModel.getCommentID());
                    checkPostDeletedOrNot(mNotificationsModel);
                } else if (mNotificationsModel.getType().toLowerCase().equals(Constants.COMMENT.toLowerCase()) && mNotificationsModel.getCommentType().equals(mActivity.getResources().getString(R.string.link_type))) {
                    Log.e(TAG, "====Form ID===" + mNotificationsModel.getCommentID());
                    mNotificationsUpdateInterface.mUpdateNotifications(mNotificationsModel, position);
                    checkPostDeletedOrNot(mNotificationsModel);
                } else if (mNotificationsModel.getType().toLowerCase().equals(Constants.COMMENT.toLowerCase()) && mNotificationsModel.getCommentType().equals(mActivity.getResources().getString(R.string.text_type))) {
                    Log.e(TAG, "====Form ID===" + mNotificationsModel.getCommentID());
                    mNotificationsUpdateInterface.mUpdateNotifications(mNotificationsModel, position);
                    checkPostDeletedOrNot(mNotificationsModel);
                }
            }
        });


        mRootRefrence.child("users").child(mNotificationsModel.getSenderAuthID()).child("details").child("image").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String strSenderPP = (String) dataSnapshot.getValue();
                Log.e(TAG, "=======USER PROFILE PIC=====" + strSenderPP);
                //By Glide
                mNotificationsModel.setImage(strSenderPP);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AlertDialogManager.hideProgressDialog();
                if (mNotificationsModel.getImage() != null && mNotificationsModel.getImage().contains("http")) {
                    Glide.with(mActivity.getApplicationContext())
                            .load(mNotificationsModel.getImage())
                            .apply(RequestOptions.placeholderOf(PainlyApplication.getInstance().getRandom()).error(PainlyApplication.getInstance().getRandom()))
                            .into(holder.itemProfilePicCIV);
                } else {
                    holder.itemProfilePicCIV.setImageResource(PainlyApplication.getInstance().getRandom());
                }

            }
        }, 1000);


    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    private void checkPostDeletedOrNot(final NotificationsModel mNotificationsModel) {

        mRootRefrence.child("forums").child(mNotificationsModel.getCommentID()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue(FormsModel.class) != null) {
                    FormsModel mFormsModel = dataSnapshot.getValue(FormsModel.class);
                    if (mFormsModel.getIsHidden() == null) {
                        if (mNotificationsModel.getType().toLowerCase().equals(Constants.COMMENT.toLowerCase()) && mNotificationsModel.getCommentType().equals(mActivity.getResources().getString(R.string.video_type))) {
                            Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                            mIntent.putExtra("FORM_UID", mNotificationsModel.getCommentID());
                            mActivity.startActivity(mIntent);
                        } else if (mNotificationsModel.getType().toLowerCase().equals(Constants.COMMENT.toLowerCase()) && mNotificationsModel.getCommentType().equals(mActivity.getResources().getString(R.string.picture_type))) {
                            Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                            mIntent.putExtra("FORM_UID", mNotificationsModel.getCommentID());
                            mActivity.startActivity(mIntent);
                        } else if (mNotificationsModel.getType().toLowerCase().equals(Constants.COMMENT.toLowerCase()) && mNotificationsModel.getCommentType().equals(mActivity.getResources().getString(R.string.link_type))) {
                            Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                            mIntent.putExtra("FORM_UID", mNotificationsModel.getCommentID());
                            mActivity.startActivity(mIntent);
                        } else if (mNotificationsModel.getType().toLowerCase().equals(Constants.COMMENT.toLowerCase()) && mNotificationsModel.getCommentType().equals(mActivity.getResources().getString(R.string.text_type))) {
                            Intent mIntent = new Intent(mActivity, TextDetailsActivity.class);
                            mIntent.putExtra("FORM_UID", mNotificationsModel.getCommentID());
                            mActivity.startActivity(mIntent);
                        }
                        //AlertDialogManager.showAlertDialog(mActivity, "Error!", mActivity.getResources().getString(R.string.looks_like));
                    } else {
                        if (mFormsModel.getIsHidden() instanceof Boolean == true) {
                            AlertDialogManager.showAlertDialog(mActivity, "Error!", mActivity.getResources().getString(R.string.looks_like));
                        }
                    }
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, "Error!", mActivity.getResources().getString(R.string.looks_like));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "==ERROR==" + databaseError.toString());
            }
        });
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtMessageTV, txtHeadingTV;
        public com.makeramen.roundedimageview.RoundedImageView itemProfilePicCIV;
        //         public de.hdodenhof.circleimageview.CircleImageView itemProfilePicCIV;
        public LinearLayout itemLayoutClickLL;
        public ImageView imgISReadNotIV;

        public ViewHolder(View itemView) {
            super(itemView);
            txtMessageTV = (TextView) itemView.findViewById(R.id.txtMessageTV);
            txtHeadingTV = (TextView) itemView.findViewById(R.id.txtHeadingTV);
            itemLayoutClickLL = (LinearLayout) itemView.findViewById(R.id.itemLayoutClickLL);
            itemProfilePicCIV = (com.makeramen.roundedimageview.RoundedImageView) itemView.findViewById(R.id.itemProfilePicCIV);
            imgISReadNotIV = (ImageView) itemView.findViewById(R.id.imgISReadNotIV);

        }
    }


}
