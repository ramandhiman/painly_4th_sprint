package com.painly.com.font;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Dharmani Apps on 1/18/2018.
 */

public class ArialBold {

    String path = "ARIAL_BOLD.TTF";
    Context mContext;
    public static Typeface mTypeface;
    public ArialBold(Context context){
        mContext = context;
    }

    public Typeface getFontFamily(){
        try{
            if (mTypeface == null)
                mTypeface = Typeface.createFromAsset(mContext.getAssets(),path);
        }catch (Exception e){
            e.printStackTrace();
        }

        return mTypeface;
    }
}
