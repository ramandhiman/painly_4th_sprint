package com.painly.com.interfaces;

import com.makeramen.roundedimageview.RoundedImageView;
import com.painly.com.beans.FormsModel;

/**
 * Created by android-da on 7/18/18.
 */

public interface CreatePreviewInterface {
    public void createPreview(FormsModel mFormsModel, RoundedImageView mRoundedImageView);
}
