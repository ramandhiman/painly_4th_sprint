package com.painly.com.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.activities.NewAllUsersProfileActivity;
import com.painly.com.beans.FormsModel;
import com.painly.com.beans.UserDetails;
import com.painly.com.interfaces.ForumClickInterface;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;
import com.painly.com.utils.Utilities;

import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Created by android-raman on 2/5/18.
 */

public class ProfileFormsAdapter extends RecyclerView.Adapter<ProfileFormsAdapter.ViewHolder> {
    String TAG = ProfileFormsAdapter.this.getClass().getSimpleName();
    List<String> mFinalImageList = new ArrayList<String>();
    DatabaseReference mRootRefrence;
    ForumClickInterface mForumClickInterface;
    FormItemTagsAdatper mFormItemTagsAdatper;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    private ArrayList<FormsModel> mArrayList;
    private Activity mActivity;

    public ProfileFormsAdapter(Activity mActivity, ArrayList<FormsModel> mArrayList, ForumClickInterface mForumClickInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mForumClickInterface = mForumClickInterface;
        mRootRefrence = FirebaseDatabase.getInstance().getReference();

        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true)
                .build();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_forms, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final FormsModel mFormsModel = mArrayList.get(position);
        Log.e(TAG, "***Post Type***::::" + mFormsModel.getType());

        try {
            if (mFormsModel.getTimestamp() instanceof String) {
                holder.txtDateTimeTV.setText(Utilities.gettingTimeFormat(mFormsModel.getTimestamp().toString()));
            } else if (mFormsModel.getTimestamp() instanceof Long) {
                holder.txtDateTimeTV.setText(Utilities.gettingLongToFormatedTime(Long.parseLong(mFormsModel.getTimestamp().toString())));
            } else if (mFormsModel.getTimestamp() instanceof Double) {
                holder.txtDateTimeTV.setText(Utilities.gettingDoubleToFormatedTime(Double.parseDouble(mFormsModel.getTimestamp().toString())));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.txtBodyTV.setText(mFormsModel.getTitle());
        holder.txtBodyDescriptionTV.setText(mFormsModel.getBody());

        if (mFormsModel.getType().equals("picture")) {
            holder.imgPostRIV.setVisibility(View.VISIBLE);
            holder.imgYouTubeIconIV.setVisibility(View.GONE);
            imageLoader.displayImage(mFormsModel.getPicture(), holder.imgPostRIV, options);

        } else if (mFormsModel.getType().equals("video")) {
            holder.imgPostRIV.setVisibility(View.VISIBLE);
            String mVideoURL = mFormsModel.getVideo();
            generateVideoThumbnail(mVideoURL, holder.imgPostRIV);
            holder.imgYouTubeIconIV.setVisibility(View.VISIBLE);
            holder.imgYouTubeIconIV.setImageResource(R.drawable.icon_youtube);
        } else if (mFormsModel.getType().equals("link")) {
            holder.imgPostRIV.setVisibility(View.VISIBLE);
            holder.imgYouTubeIconIV.setVisibility(View.GONE);
              /*Preview*/
            if (mFormsModel.getPicture() != null && mFormsModel.getPicture().contains("http")) {
                Glide.with(mActivity).load(mFormsModel.getPicture()).into(holder.imgPostRIV);
            } else {
                String strAPIURL = Constants.GET_IMAGE_LINK;
                Log.e(TAG, "=====API URL====" + strAPIURL);
                StringRequest request = new StringRequest(Request.Method.POST, strAPIURL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "===RESPONSE===" + response);
                        try {
                            JSONObject mJsonObject = new JSONObject(response);
                            String strStatus = mJsonObject.getString("status");
                            if (strStatus.equals("200")) {
                                if (mJsonObject.getString("img").contains("http")) {
                                    //String strLinkImageUrl = mJsonObject.getString("img");
                                    Log.e(TAG, "==Image Url Link==" + mJsonObject.getString("img"));
                                    //mFormsModel.setPicture(strLinkImageUrl);
                                    //imageLoader.displayImage(mJsonObject.getString("img"),holder.imgPostRIV, options);
                                    Glide.with(mActivity).load(mJsonObject.getString("img")).into(holder.imgPostRIV);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "===ERROR===" + error.toString());
                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String, String> mParam = new HashMap<String, String>();
                        mParam.put("url", mFormsModel.getLink());
                        return mParam;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/x-www-form-urlencoded");
                        return headers;
                    }

                };
                PainlyApplication.getInstance().addToRequestQueue(request);
            }


        } else {
            holder.imgPostRIV.setVisibility(View.GONE);
            holder.imgYouTubeIconIV.setVisibility(View.GONE);
        }

        /*Update The User Data*/
        mRootRefrence.child("users").child(mFormsModel.getOpUID()).child("details").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserDetails mUserDetails = dataSnapshot.getValue(UserDetails.class);
                mFormsModel.setmUserDetails(mUserDetails);


                if (mFormsModel.getmUserDetails() != null) {
                    if (mFormsModel.getmUserDetails().getImage() != null) {

                        DisplayImageOptions pp = new DisplayImageOptions.Builder()
                                .showImageOnLoading(R.drawable.icon_pp_ph)
                                .showImageForEmptyUri(R.drawable.icon_pp_ph)
                                .showImageOnFail(R.drawable.icon_pp_ph)
                                .cacheInMemory(true)
                                .considerExifParams(true)
                                .bitmapConfig(Bitmap.Config.RGB_565)
                                .build();

                        ImageLoader.getInstance().displayImage(mFormsModel.getmUserDetails().getImage(), holder.profileRIV, pp);
                    }

                    if (mFormsModel.getmUserDetails().getName() != null) {
                        if (mFormsModel.getPicture() != null) {
                            Log.e(TAG, "onBindViewHolder: " + mFormsModel.getPicture());

                        }
                        Log.e(TAG, "onDataChange: " + mFormsModel.getmUserDetails().getName());
                        holder.txtUserNameTV.setText(mFormsModel.getmUserDetails().getName());

                    } else {
                        if (mFormsModel.getPicture() != null) {
                            Log.e(TAG, "onBindViewHolder: " + mFormsModel.getPicture());
                        }
                        Log.e(TAG, "onDataChange: " + mFormsModel.getmUserDetails().getUsername());
                    }
                    if (mFormsModel.getmUserDetails().getUsername() != null)
                        if (mFormsModel.getmUserDetails().getUsername().contains("@")) {
                            Log.e(TAG, "onDataChange: " + mFormsModel.getmUserDetails().getName());

                            holder.txtUserDescriptionTV.setText(mFormsModel.getmUserDetails().getUsername());
                        } else {
                            holder.txtUserDescriptionTV.setText("@" + " " + mFormsModel.getmUserDetails().getUsername());
                        }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        /*Set Click Listner*/
        holder.cardCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mForumClickInterface.forumClick(mFormsModel, position);
            }
        });

        holder.profileRIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "").equals(mFormsModel.getOpUID())) {
                    Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                    mIntent.putExtra("UID", mFormsModel.getOpUID());
                    mIntent.putExtra("TYPE", Constants.CURRENT);
                    mActivity.startActivity(mIntent);
                } else {
                    Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                    mIntent.putExtra("UID", mFormsModel.getOpUID());
                    mIntent.putExtra("TYPE", Constants.ANOTHER);
                    mActivity.startActivity(mIntent);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private void generateVideoThumbnail(String mVideoURL, RoundedImageView mRoundedImageView) {
        try {
            if (mVideoURL != null){
                String videoId = Utilities.extractYoutubeId(mVideoURL);
                Log.e("VideoId is->", "" + videoId);
                String img_url = "http://img.youtube.com/vi/" + videoId + "/0.jpg";


                DisplayImageOptions mDisplayImageOptions = new DisplayImageOptions.Builder()
                        .showImageOnLoading(R.drawable.icon_post_ph)
                        .showImageForEmptyUri(R.drawable.icon_post_ph)
                        .showImageOnFail(R.drawable.icon_post_ph)
                        .cacheInMemory(true)
                        .considerExifParams(true)
                        .bitmapConfig(Bitmap.Config.RGB_565)
                        .build();

                ImageLoader.getInstance().displayImage(img_url, mRoundedImageView, mDisplayImageOptions);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtUserNameTV, txtUserDescriptionTV, txtDateTimeTV, txtBodyTV, txtBodyDescriptionTV;
        public RoundedImageView imgPostRIV;
        public de.hdodenhof.circleimageview.CircleImageView profileRIV;
        public CardView cardCV;
        public ImageView imgYouTubeIconIV;
        public RecyclerView formItemTagsRV;

        public ViewHolder(View itemView) {
            super(itemView);
            txtUserNameTV = (TextView) itemView.findViewById(R.id.txtUserNameTV);
            txtUserDescriptionTV = (TextView) itemView.findViewById(R.id.txtUserDescriptionTV);
            txtDateTimeTV = (TextView) itemView.findViewById(R.id.txtDateTimeTV);
            txtBodyTV = (TextView) itemView.findViewById(R.id.txtBodyTV);
            txtBodyDescriptionTV = (TextView) itemView.findViewById(R.id.txtBodyDescriptionTV);
            imgPostRIV = (RoundedImageView) itemView.findViewById(R.id.imgPostRIV);
            profileRIV = (de.hdodenhof.circleimageview.CircleImageView) itemView.findViewById(R.id.profileRIV);
            cardCV = (CardView) itemView.findViewById(R.id.cardCV);
            imgYouTubeIconIV = (ImageView) itemView.findViewById(R.id.imgYouTubeIconIV);
            formItemTagsRV = (RecyclerView) itemView.findViewById(R.id.formItemTagsRV);
        }
    }
}
