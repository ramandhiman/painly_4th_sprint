package com.painly.com.youtubeSearch;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.painly.com.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by android-raman on 2/5/18.
 */

public class YoutubeSearchAdapter extends RecyclerView.Adapter<YoutubeSearchAdapter.ViewHolder> {
    private Context mContext;
    private List<VideoItem> mVideoList = new ArrayList<>();
    YoutubeUrlInterface mYoutubeUrlInterface;

    public YoutubeSearchAdapter(Context mContext, List<VideoItem> mVideoList, YoutubeUrlInterface mYoutubeUrlInterface) {
        this.mContext = mContext;
        this.mVideoList = mVideoList;
        this.mYoutubeUrlInterface = mYoutubeUrlInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_youtube_search, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final VideoItem singleVideo = mVideoList.get(position);

        //replace the default text with id, title and description with setText method
        holder.txtTitleTV.setText(singleVideo.getTitle());
        holder.txtDescriptionTV.setText(singleVideo.getDescription());

        //Picasso library allows for hassle-free image loading
        // in your application—often in one line of code!
        //Features :
        //-Handling ImageView recycling and download cancelation in an adapter
        //-Complex image transformations with minimal memory use
        //-Automatic memory and disk caching

        //placing the thumbnail with picasso library
        //by resizing it to the size of thumbnail

        //with method gives access to the global default Picasso instance
        //load method starts an image request using the specified path may be a remote URL, file resource, etc.
        //resize method resizes the image to the specified size in pixels wrt width and height
        //centerCrop crops an image inside of the bounds specified by resize(int, int) rather than distorting the aspect ratio
        //into method asynchronously fulfills the request into the specified Target
        Picasso.with(mContext)
                .load(singleVideo.getThumbnailURL())
                .resize(480, 270)
                .centerCrop()
                .into(holder.imgPostRIV);

        holder.cardCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mYoutubeUrlInterface.getYoutubeVideoUrl(singleVideo);
            }
        });

    }


    @Override
    public int getItemCount() {
        return mVideoList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout cardCV;
        public TextView txtTitleTV, txtDescriptionTV;
        public com.makeramen.roundedimageview.RoundedImageView imgPostRIV;

        public ViewHolder(View itemView) {
            super(itemView);
            cardCV = (LinearLayout) itemView.findViewById(R.id.cardCV);
            imgPostRIV = (com.makeramen.roundedimageview.RoundedImageView) itemView.findViewById(R.id.imgPostRIV);
            txtTitleTV = (TextView) itemView.findViewById(R.id.txtTitleTV);
            txtDescriptionTV = (TextView) itemView.findViewById(R.id.txtDescriptionTV);
        }
    }
}
