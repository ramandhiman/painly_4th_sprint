package com.painly.com.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.painly.com.R;
import com.painly.com.activities.NewAllUsersProfileActivity;
import com.painly.com.beans.Comments;
import com.painly.com.beans.UserDetails;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;
import com.painly.com.utils.Utilities;
import com.painly.com.views.RelativeTimeTextView;

import java.util.ArrayList;


/**
 * Created by android-raman on 2/5/18.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {
    DatabaseReference mRootRefrence;
    private ArrayList<Comments> mArrayList;
    private Activity mActivity;
    String strForumID;

    public CommentsAdapter(Activity mActivity, ArrayList<Comments> mArrayList,String strForumID) {
        this.mArrayList = mArrayList;
        this.mActivity = mActivity;
        this.strForumID = strForumID;
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comments, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Comments mComments = mArrayList.get(position);

        holder.txtCommentTV.setText(mComments.getText());
        holder.txtDateTimeTV.setReferenceTime(Utilities.getTimeInLong(mComments.getTimestamp()));

        String strCurrentUserID = PainlyPreference.readString(mActivity,PainlyPreference.USER_ID,"");

        if (strCurrentUserID.equals(mComments.getCommenterUID())){
            holder.swipe_audio_layout.setSwipeEnabled(true);
            holder.llEditDeleteLL.setVisibility(View.VISIBLE);
        }else{
            holder.swipe_audio_layout.setSwipeEnabled(false);
            holder.llEditDeleteLL.setVisibility(View.GONE);
        }


        /*User Details*/
        /*Update The User Data*/
        mRootRefrence.child("users").child(mComments.getCommenterUID()).child("details").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserDetails mUserDetails = dataSnapshot.getValue(UserDetails.class);
                if (mUserDetails.getImage() != null)
                    mComments.setUserImage(mUserDetails.getImage());
                if (mUserDetails.getName() != null)
                    mComments.setUserName(mUserDetails.getName());

                holder.txtUserNameTV.setText(mComments.getUserName());

                DisplayImageOptions pp = new DisplayImageOptions.Builder()
                        .showImageOnLoading(R.drawable.icon_pp_ph)
                        .showImageForEmptyUri(R.drawable.icon_pp_ph)
                        .showImageOnFail(R.drawable.icon_pp_ph)
                        .cacheInMemory(true)
                        .considerExifParams(true)
                        .bitmapConfig(Bitmap.Config.RGB_565)
                        .build();

                ImageLoader.getInstance().displayImage(mComments.getUserImage(), holder.profileRIV, pp);



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mRootRefrence.child("forums").child(strForumID).child("comments").child(mComments.getmCommentID()).removeValue();
                Toast.makeText(mActivity,"Deleted!",Toast.LENGTH_SHORT).show();
            }
        });


        holder.profileRIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if (Constants.USER_ID.equals(mArrayList.get(position).getCommenterUID())) {
                        Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                        mIntent.putExtra("UID", mArrayList.get(position).getCommenterUID());
                        mIntent.putExtra("TYPE", Constants.CURRENT);
                        mActivity.startActivity(mIntent);
                    } else {
                        Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                        mIntent.putExtra("UID", mArrayList.get(position).getCommenterUID());
                        mIntent.putExtra("TYPE", Constants.ANOTHER);
                        mActivity.startActivity(mIntent);
                    }
                }
        });

        holder.llEditDeleteLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRootRefrence.child("forums").child(strForumID).child("comments").child(mComments.getmCommentID()).removeValue();
                Toast.makeText(mActivity,"Deleted!",Toast.LENGTH_SHORT).show();
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtUserNameTV, txtCommentTV;
        public RelativeTimeTextView txtDateTimeTV;
        public de.hdodenhof.circleimageview.CircleImageView profileRIV;
        public com.daimajia.swipe.SwipeLayout swipe_audio_layout;
        public LinearLayout llEditDeleteLL;
        public ViewHolder(View itemView) {
            super(itemView);
            txtUserNameTV = (TextView) itemView.findViewById(R.id.txtUserNameTV);
            txtCommentTV = (TextView) itemView.findViewById(R.id.txtCommentTV);
            txtDateTimeTV = (RelativeTimeTextView) itemView.findViewById(R.id.txtDateTimeTV);
            profileRIV = (de.hdodenhof.circleimageview.CircleImageView) itemView.findViewById(R.id.profileRIV);
            swipe_audio_layout = (com.daimajia.swipe.SwipeLayout)itemView.findViewById(R.id.swipe_audio_layout);
            llEditDeleteLL = (LinearLayout)itemView.findViewById(R.id.llEditDeleteLL);

        }


    }
}
