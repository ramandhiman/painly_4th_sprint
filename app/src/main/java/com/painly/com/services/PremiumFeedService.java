package com.painly.com.services;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;

import com.einmalfel.earl.AtomFeed;
import com.einmalfel.earl.EarlParser;
import com.einmalfel.earl.Feed;
import com.einmalfel.earl.Item;
import com.einmalfel.earl.RSSFeed;
import com.painly.com.PainlyApplication;
import com.painly.com.adapters.FormPremiumFeedsAdapter;
import com.painly.com.adapters.FormsAdapter;
import com.painly.com.beans.Conditions;
import com.painly.com.chathead.ChatHeadModel;
import com.painly.com.utils.Constants;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * Created by android-da on 2/13/19.
 */

public class PremiumFeedService extends Service {
    String TAG = PremiumFeedService.this.getClass().getSimpleName();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //we have some options for service
        if (intent != null) {

            Conditions mConditions = (Conditions) intent.getSerializableExtra("OBJ");
            FormsAdapter.ViewHolder mViewHolder = (FormsAdapter.ViewHolder) intent.getSerializableExtra("HOLDER");

            if (mConditions != null) {
                if (findSourcesUnderCondition(mConditions) != null && findSourcesUnderCondition(mConditions).length() > 4) {
                    getChatBotMsgFromCondition(findSourcesUnderCondition(mConditions), mViewHolder);
                }
            }
        }

        //start sticky means service will be explicity started and stopped
        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        //stopping the player when service is destroyed
    }


    //Find Random Source from the Condition
    private String findSourcesUnderCondition(Conditions condition) {
        String randomSourceURL = "";

        if (condition.getSources() != null) {
            Collection<String> values = condition.getSources().values();
            ArrayList<String> mSourcesUrlAL = new ArrayList<String>(values);

            int rnd = new Random().nextInt(mSourcesUrlAL.size());

            randomSourceURL = mSourcesUrlAL.get(rnd);
        }

        return randomSourceURL;
    }


    private void getChatBotMsgFromCondition(String sourcesUnderCondition, FormsAdapter.ViewHolder mViewHolder) {
        /*GEtting two Types Feed
        * 1) RSS Feed Result Simple Result
        * 2) ATOM Feed Result with Youtube Video
        * */
        getRssAndAtomWithData(sourcesUnderCondition, mViewHolder);
    }

    /*
   * RSS & ATOM Parser Library.
   * Execute Api In Asyn Task.
   * */
    private void getRssAndAtomWithData(String strSourceUrl, FormsAdapter.ViewHolder mViewHolder) {
        new RetrieveFeedTask(strSourceUrl, mViewHolder).execute();
    }


    class RetrieveFeedTask extends AsyncTask<String, Void, List<Item>> {
        String strSourceUrl = "";
        FormsAdapter.ViewHolder mViewHolder;

        private RetrieveFeedTask(String strSourceUrl, FormsAdapter.ViewHolder mViewHolder) {
            this.strSourceUrl = strSourceUrl;
            this.mViewHolder = mViewHolder;
        }

        @Override
        protected List<Item> doInBackground(String... strings) {
            List<Item> result = new ArrayList<>();
            try {
                if (strSourceUrl != null && strSourceUrl.contains("http")) {
                    InputStream inputStream = new URL(strSourceUrl).openConnection().getInputStream();
                    Feed feed = EarlParser.parseOrThrow(inputStream, 0);
                    // media and itunes RSS extensions allow to assign keywords to feed items
                    if (RSSFeed.class.isInstance(feed)) {
                        RSSFeed rssFeed = (RSSFeed) feed;
                        Log.e(TAG, "==RSS FEED Count==" + rssFeed.getItems().size());

                        ArrayList<ChatHeadModel> mRssArrayList = new ArrayList<>();

                        if (rssFeed.getItems().size() < 6) {
                            for (int i = 0; i < rssFeed.getItems().size(); i++) {
                                int rnd = new Random().nextInt(rssFeed.getItems().size());
                                Item mItemObj = rssFeed.getItems().get(rnd);
                                ChatHeadModel mModel = new ChatHeadModel();
                                if (mItemObj.getImageLink() != null && mItemObj.getImageLink().contains("http")) {
                                    mModel.setThumbtitle(mItemObj.getTitle());
                                    mModel.setFeedType(Constants.RSS_TYPE);
                                    mModel.setLink(mItemObj.getLink());
                                    mModel.setImage(mItemObj.getImageLink());
                                } else {
                                    mModel.setThumbtitle(mItemObj.getTitle());
                                    mModel.setFeedType(Constants.RSS_TYPE);
                                    mModel.setLink(mItemObj.getLink());
                                    mModel.setImage(PainlyApplication.getInstance().getRandomFeedImage());
                                }

                                mRssArrayList.add(mModel);
                            }

                            Log.e(TAG, "===Feed List Size===" + mRssArrayList.size());
                            FormPremiumFeedsAdapter mAdapter = new FormPremiumFeedsAdapter((Activity) getApplicationContext(), mRssArrayList);
                            LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                            mViewHolder.premiumRecyclerViewRV.setLayoutManager(horizontalLayoutManager);
                            mViewHolder.premiumRecyclerViewRV.setAdapter(mAdapter);

                        } else if (rssFeed.getItems().size() > 6) {
                            for (int i = 0; i < 6; i++) {
                                int rnd = new Random().nextInt(rssFeed.getItems().size());
                                Item mItemObj = rssFeed.getItems().get(rnd);
                                ChatHeadModel mModel = new ChatHeadModel();
                                if (mItemObj.getImageLink() != null && mItemObj.getImageLink().contains("http")) {
                                    mModel.setThumbtitle(mItemObj.getTitle());
                                    mModel.setFeedType(Constants.RSS_TYPE);
                                    mModel.setLink(mItemObj.getLink());
                                    mModel.setImage(mItemObj.getImageLink());
                                } else {
                                    mModel.setThumbtitle(mItemObj.getTitle());
                                    mModel.setFeedType(Constants.RSS_TYPE);
                                    mModel.setLink(mItemObj.getLink());
                                    mModel.setImage(PainlyApplication.getInstance().getRandomFeedImage());
                                }
                                mRssArrayList.add(mModel);
                            }

                            Log.e(TAG, "===Feed List Size===" + mRssArrayList.size());

                            FormPremiumFeedsAdapter mAdapter = new FormPremiumFeedsAdapter((Activity) getApplicationContext(), mRssArrayList);
                            LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                            mViewHolder.premiumRecyclerViewRV.setLayoutManager(horizontalLayoutManager);
                            mViewHolder.premiumRecyclerViewRV.setAdapter(mAdapter);
                        }


                    } else if (AtomFeed.class.isInstance(feed)) {
                        AtomFeed atomFeed = (AtomFeed) feed;
                        Log.e(TAG, "==ATOM Feed ==");

                    } else {
                        Log.e(TAG, "==Not Any Feed ==");
                    }

                } else {
                    Log.e(TAG, "==Not Valid Url==");
                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "===ERROR==" + e.toString());
            }

            return result;
        }


        @Override
        protected void onPostExecute(List<Item> items) {
            super.onPostExecute(items);

        }
    }


}
