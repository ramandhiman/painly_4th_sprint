package com.painly.com.font;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by android-da on 5/23/18.
 */

public class ButtonGothamMedium extends Button {
    public ButtonGothamMedium(Context context) {
        super(context);
    }

    public ButtonGothamMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ButtonGothamMedium(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ButtonGothamMedium(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new GothamMedium(context).getFontFamily());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
