package com.painly.com.beans;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by android-da on 6/5/18.
 */

public class FavoriteModel implements Serializable{
    HashMap<String,String> favorites = new HashMap<String, String>();

    public HashMap<String, String> getFavorites() {
        return favorites;
    }

    public void setFavorites(HashMap<String, String> favorites) {
        this.favorites = favorites;
    }
}
