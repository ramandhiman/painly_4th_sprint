package com.painly.com.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.chat.ChatwithUserActivity;
import com.painly.com.chat.MessageUserR;
import com.painly.com.utils.Utilities;
import com.painly.com.views.RelativeTimeTextView;

import java.util.ArrayList;


/**
 * Created by android-raman on 2/5/18.
 */

public class ChatUsersAdapter extends RecyclerView.Adapter<ChatUsersAdapter.ViewHolder> {
    String TAG = ChatUsersAdapter.this.getClass().getSimpleName();
    DatabaseReference mRootReference;
    private ArrayList<MessageUserR> mArrayList;
    private Activity mActivity;

    public ChatUsersAdapter(Activity mActivity, ArrayList<MessageUserR> mArrayList) {
        this.mArrayList = mArrayList;
        this.mActivity = mActivity;
        mRootReference = FirebaseDatabase.getInstance().getReference();


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chatsuser, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final MessageUserR mMessageUserR = mArrayList.get(position);
        holder.itemtxtUserNameTV.setText(mMessageUserR.getName());

        if (mMessageUserR.getImage() != null && mMessageUserR.getImage().contains("http")) {
            Glide.with(mActivity)
                    .load(mMessageUserR.getImage())
                    .apply(RequestOptions.placeholderOf(PainlyApplication.getInstance().getRandom()).error(PainlyApplication.getInstance().getRandom()))
                    .into(holder.itemProfileRIV);
        } else {
            holder.itemProfileRIV.setImageResource(PainlyApplication.getInstance().getRandom());
        }

        /*
        * Show Last Msg DoT*/
//        if (mMessageUserR.isHasUnread()) {
//            holder.itemDotIV.setVisibility(View.VISIBLE);
//        } else {
//            holder.itemDotIV.setVisibility(View.GONE);
//        }


        holder.itemParentLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent msgIntent = new Intent(mActivity, ChatwithUserActivity.class);
                msgIntent.putExtra("UID", mMessageUserR.getuID());
                msgIntent.putExtra("POSITION", position);
                mActivity.startActivity(msgIntent);
            }
        });

        holder.itemLayoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent msgIntent = new Intent(mActivity, ChatwithUserActivity.class);
                msgIntent.putExtra("UID", mMessageUserR.getuID());
                msgIntent.putExtra("POSITION", position);
                mActivity.startActivity(msgIntent);
            }
        });


        try {
            holder.itemtxtCommentTV.setText(mMessageUserR.getStrLastMessageText());
            holder.itemtxtDateTimeTV.setReferenceTime(Utilities.getTimeInLong(mMessageUserR.getStrLastMessageTimeStamp()));
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.e(TAG, "onBindViewHolder: " + mMessageUserR.getuID());

        /*Getting User Name*/
        mRootReference.child("users").child(mMessageUserR.getuID()).child("details").child("username").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    String mUserName = dataSnapshot.getValue(String.class);
                    if (mUserName != null) {
                        if (mUserName.contains("@")) {
                            holder.itemtxtUserNTV.setText(mUserName);
                        } else {
                            holder.itemtxtUserNTV.setText("@" + mUserName);
                        }
                    } else {
                        holder.itemtxtUserNTV.setText("@");
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "===ERROR===" + databaseError.toString());
            }
        });
    }


    //This method will filter the list
    //here we are passing the filtered data
    //and assigning it to the list with notifydatasetchanged method
    public void filterList(ArrayList<MessageUserR> filterdNames) {
        this.mArrayList = filterdNames;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView itemtxtUserNameTV, itemtxtUserNTV, itemtxtCommentTV;
        public RelativeTimeTextView itemtxtDateTimeTV;
        public com.makeramen.roundedimageview.RoundedImageView itemProfileRIV;
        public LinearLayout itemParentLL, itemLayoutLL;
        public ImageView itemDotIV;

        public ViewHolder(View itemView) {
            super(itemView);
            itemtxtUserNTV = (TextView) itemView.findViewById(R.id.itemtxtUserNTV);
            itemtxtUserNameTV = (TextView) itemView.findViewById(R.id.itemtxtUserNameTV);
            itemtxtCommentTV = (TextView) itemView.findViewById(R.id.itemtxtCommentTV);
            itemtxtDateTimeTV = (RelativeTimeTextView) itemView.findViewById(R.id.itemtxtDateTimeTV);
            itemProfileRIV = (com.makeramen.roundedimageview.RoundedImageView) itemView.findViewById(R.id.itemProfileRIV);
            itemParentLL = (LinearLayout) itemView.findViewById(R.id.itemParentLL);
            itemLayoutLL = (LinearLayout) itemView.findViewById(R.id.itemLayoutLL);
            itemDotIV = (ImageView) itemView.findViewById(R.id.itemDotIV);
        }

    }
}
