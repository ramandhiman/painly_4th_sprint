package com.painly.com.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.painly.com.BaseActivity;
import com.painly.com.R;
import com.painly.com.utils.Constants;
import com.painly.com.utils.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OpenLinkActivity extends BaseActivity {
    Activity mActivity = OpenLinkActivity.this;
    String TAG = OpenLinkActivity.this.getClass().getSimpleName();
    @BindView(R.id.openLinkWV)
    WebView openLinkWV;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.imgShareIV)
    ImageView imgShareIV;

    String strLinkURL = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_link);
        ButterKnife.bind(this);
        if (getIntent() != null) {
            strLinkURL = getIntent().getStringExtra("LINK");
        }

        if (!Utilities.isNetworkAvailable(mActivity)){
            Toast.makeText(mActivity,getString(R.string.internetconnection),Toast.LENGTH_LONG).show();
        }else{
            openWebView();
        }

    }

    private void openWebView() {
        openLinkWV.getSettings().setJavaScriptEnabled(true);
        openLinkWV.getSettings().setLoadWithOverviewMode(true);
        openLinkWV.getSettings().setUseWideViewPort(true);
        openLinkWV.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                mProgressBar.setVisibility(View.VISIBLE);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                mProgressBar.setVisibility(View.GONE);
            }
        });
        openLinkWV.loadUrl(strLinkURL);
    }

    @OnClick({R.id.imgBackIV, R.id.imgShareIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.imgShareIV:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, mActivity.getString(R.string.share_title));
                if (strLinkURL.equals(Constants.TERMS_LINK))
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_pain_term_));
                else if (strLinkURL.equals(Constants.ABOUT_PAINLY_LINK))
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_pain_about_));
                else
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, strLinkURL);
                mActivity.startActivity(Intent.createChooser(sharingIntent, mActivity.getString(R.string.share_forum)));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
