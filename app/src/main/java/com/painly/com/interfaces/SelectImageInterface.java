package com.painly.com.interfaces;

import android.net.Uri;

/**
 * Created by android-da on 1/8/19.
 */

public interface SelectImageInterface {
    public void getSelectedImgByteArray(Uri imageUri);
    public void getSelectedImgUrl(String strUrl);

}
