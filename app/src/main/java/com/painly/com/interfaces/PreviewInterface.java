package com.painly.com.interfaces;

import com.makeramen.roundedimageview.RoundedImageView;
import com.painly.com.beans.FormsModel;

/**
 * Created by android-da on 8/7/18.
 */

public interface PreviewInterface {
    public void showPreview(String strLink, RoundedImageView roundedImageView, FormsModel mModel);
}
