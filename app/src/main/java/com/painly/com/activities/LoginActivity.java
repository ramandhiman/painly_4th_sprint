package com.painly.com.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.painly.com.BaseActivity;
import com.painly.com.R;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;
import com.painly.com.utils.Utilities;


public class LoginActivity extends BaseActivity {
    Activity mActivity = LoginActivity.this;
    String TAG = LoginActivity.this.getClass().getSimpleName();

    EditText emailETL;
    EditText passwordETL;
    Button btnLoginBL;
    TextView txtForgotPwdTV;
    ImageView imgBackIV;

    DatabaseReference mRootRefrence;

    FirebaseAuth mFirebaseAuth;

    String strUpdatePushToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_signin);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        strUpdatePushToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "======Token====" + strUpdatePushToken);
        setWidgetIDs();

        btnLoginBL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpLoginClick();
            }
        });

        txtForgotPwdTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Show ForgotPwd Dialog*/
                forgotPwdDialog();
            }
        });

        imgBackIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    private void setWidgetIDs() {
        emailETL = (EditText) findViewById(R.id.emailETL);
        passwordETL = (EditText) findViewById(R.id.passwordETL);
        btnLoginBL = (Button) findViewById(R.id.btnLoginBL);
        txtForgotPwdTV = (TextView) findViewById(R.id.txtForgotPwdTV);
        imgBackIV = (ImageView) findViewById(R.id.imgBackIV);
    }


    private void setUpLoginClick() {
        if (emailETL.getText().toString().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_email));
        } else if (!Utilities.isValidEmaillId(emailETL.getText().toString())) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_valid_email));
        } else if (passwordETL.getText().toString().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_password));
        } else {
            //Execute Api
            String email = emailETL.getText().toString().trim();
            String password = passwordETL.getText().toString().trim();

            if (Utilities.isNetworkAvailable(mActivity)) {
                executeLogin(email, password);
            } else {
                Toast.makeText(mActivity, getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
            }


        }
    }

    private void executeLogin(String email, String password) {
        try {
            AlertDialogManager.showProgressDialog(mActivity);
            mFirebaseAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            AlertDialogManager.hideProgressDialog();
                            if (task.isSuccessful()) {
                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                Log.e(TAG, "**USER ID**" + user.getUid());

                                PainlyPreference.writeBoolean(mActivity, PainlyPreference.IS_LOGIN, true);
                                PainlyPreference.writeString(mActivity, PainlyPreference.USER_ID, user.getUid());
                                PainlyPreference.writeString(mActivity, PainlyPreference.USER_EMAIL_ID, user.getEmail());
                                // PainlyPreference.writeString(mActivity, PainlyPreference.USER_NAME, strName);
                                //Update PushToken:
                                mRootRefrence.child("users").child(user.getUid()).child("details").child("pushToken").setValue(strUpdatePushToken);
                                mRootRefrence.child("users").child(user.getUid()).child("details").child("deviceType").setValue("android");//ios
                                //Getting Current User Data
                                //gettingCurrentUserData();
                                //Getting UserName:

                                Toast.makeText(mActivity, getString(R.string.login_sucess), Toast.LENGTH_LONG).show();
                                Intent mIntent = new Intent(mActivity, HomeActivity.class);
                                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(mIntent);
                                finish();
                                overridePendingTransitionEnter();

                            } else {
                                String strError = task.getException().toString();
                                Log.e(TAG, "====ERROR===" + strError);
                                String mErrorArray[] = strError.split(":");
                                String strMessage = mErrorArray[1];
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), strMessage);
                            }

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        Intent mIntent = new Intent(mActivity, SelectionSignInSignUpActivity.class);
        startActivity(mIntent);
        finish();
    }


    private void forgotPwdDialog() {
        final Dialog alertDialog = new Dialog(mActivity, R.style.PauseDialog);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_forgot_pwd);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final EditText editEmailET = (EditText) alertDialog.findViewById(R.id.editEmailET);
        Button btnSaveB = (Button) alertDialog.findViewById(R.id.btnSaveB);
        Button btnCancelB = (Button) alertDialog.findViewById(R.id.btnCancelB);

        btnCancelB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        btnSaveB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editEmailET.getText().toString().trim().equals("")) {
                    editEmailET.setError(getString(R.string.please_enter_email));
                } else if (!Utilities.isValidEmaillId(editEmailET.getText().toString().trim())) {
                    editEmailET.setError(getString(R.string.please_enter_valid_email));
                } else {
                    alertDialog.dismiss();
                    /*Send Email forgot Password*/
                    sendEmaiforForgotPassword(editEmailET.getText().toString().trim());
                }

            }
        });

        alertDialog.show();
    }

    private void sendEmaiforForgotPassword(String strEmail) {
        mFirebaseAuth.sendPasswordResetEmail(strEmail).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "Email sent.");
                    Toast.makeText(mActivity, "Email sent Sucessfully", Toast.LENGTH_LONG).show();
                } else {
                    String strError = task.getException().toString();
                    Log.e(TAG, "====ERROR===" + strError);
                    String mErrorArray[] = strError.split(":");
                    String strMessage = mErrorArray[1];
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), strMessage);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "====ERROR===" + e.toString());
            }
        });
    }


}

