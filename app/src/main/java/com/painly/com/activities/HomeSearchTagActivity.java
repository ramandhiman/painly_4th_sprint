package com.painly.com.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.R;
import com.painly.com.adapters.TagsAdapter;
import com.painly.com.beans.Conditions;
import com.painly.com.font.EditTextArialRegular;
import com.painly.com.interfaces.SelectConditionInterface;
import com.painly.com.interfaces.SelectConditions;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeSearchTagActivity extends AppCompatActivity {
    String TAG = HomeSearchTagActivity.this.getClass().getSimpleName();
    Activity mActivity = HomeSearchTagActivity.this;
    @BindView(R.id.leftLL)
    LinearLayout leftLL;
    @BindView(R.id.editSearchET)
    EditTextArialRegular editSearchET;
    @BindView(R.id.imgSearchIV)
    ImageView imgSearchIV;
    @BindView(R.id.conditionsRV)
    RecyclerView conditionsRV;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;

    DatabaseReference mRootRefrence;

    TagsAdapter mTagsAdapter;

    ArrayList<Conditions> mConditionsAL = new ArrayList<Conditions>();

    String strType = "";


    SelectConditionInterface mSelectConditions = new SelectConditionInterface() {
        @Override
        public void mSelectConditions(Conditions mConditions) {
            Intent mIntent = new Intent();
            mIntent.putExtra("LIST", mConditions);
            setResult(393, mIntent);
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_search_tag);
        ButterKnife.bind(this);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        if (getIntent() != null) {
            strType = getIntent().getStringExtra("TYPE");
        }
        if (Utilities.isNetworkAvailable(mActivity)) {
            getConditionsData();
        } else {
            Toast.makeText(mActivity, getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
        }


        setSearch();
    }


    private void setSearch() {
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable mEditable) {
                //after the change calling the method and passing the search input
                filter(mEditable.toString());
            }
        });
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<Conditions> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (Conditions s : mConditionsAL) {
            //if the existing elements contains the search input
            if (s.getName().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(s);
            }
        }

        //calling a method of the adapter class and passing the filtered list
        mTagsAdapter.filterList(filterdNames);
    }


    @OnClick({R.id.leftLL, R.id.imgSearchIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.leftLL:
                onBackPressed();
                break;
            case R.id.imgSearchIV:
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    private void getConditionsData() {
        AlertDialogManager.showProgressDialog(mActivity);
        mRootRefrence.child("conditions").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                AlertDialogManager.hideProgressDialog();
                for (DataSnapshot mSnapshot : dataSnapshot.getChildren()) {
                    String conditionID = mSnapshot.getKey();
                    final Conditions mConditions = mSnapshot.getValue(Conditions.class);
                    mConditions.setConditionID(conditionID);
                    mConditionsAL.add(mConditions);

                }

                Collections.sort(mConditionsAL, new Comparator<Conditions>() {
                    @Override
                    public int compare(Conditions item, Conditions t1) {
                        String s1 = item.getName();
                        String s2 = t1.getName();
                        return s1.compareToIgnoreCase(s2);
                    }

                });
                setAdapter();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "Error::" + databaseError.toString());
                Toast.makeText(mActivity, databaseError.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setAdapter() {
        mTagsAdapter = new TagsAdapter(mActivity, mConditionsAL, mSelectConditions);
        conditionsRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        conditionsRV.setLayoutManager(mLayoutManager);
        conditionsRV.setItemAnimator(new DefaultItemAnimator());
        conditionsRV.setAdapter(mTagsAdapter);

    }

}
