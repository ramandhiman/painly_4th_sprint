package com.painly.com.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.painly.com.BaseActivity;
import com.painly.com.R;
import com.painly.com.adapters.SignUpCondtionAdapter;
import com.painly.com.beans.Conditions;
import com.painly.com.font.ButtonGothamMedium;
import com.painly.com.font.TextViewArialRegular;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Constants;
import com.painly.com.utils.ImageUtils;
import com.painly.com.utils.PainlyPreference;
import com.painly.com.utils.Utilities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class SignUp4Activity extends BaseActivity {
    public static final int CAMERA_REQUEST = 333;
    public static final int GALLERY_REQUEST = 222;
    public static final int REQUEST_PERMISSION_CODE = 919;
    public Bitmap mBitmapImage;
    Activity mActivity = SignUp4Activity.this;
    String TAG = SignUp4Activity.this.getClass().getSimpleName();
    String strUserName = "", strName = "", strBio = "", strEmail = "", strPassword = "", strPushToken = "", mCurrentPhotoPath, mStoragePath = "", strImageBase64 = "", mPictureType = "";
    boolean isMachineLearningEnabled;
    ArrayList<Conditions> mConditionsArrayList = new ArrayList<Conditions>();
    Uri mProfileUrl, mCoverUrl;
    DatabaseReference mRootRefrence, mProfileRef, mCoverRef;
    boolean isProfilePic = false, isCoverPic = false;
    boolean isfirstTimeSignUp = false;
    String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    String writeCamera = Manifest.permission.CAMERA;
    StorageReference mStorageRef;
    FirebaseAuth mFirebaseAuth;
    SignUpCondtionAdapter mSignUpCondtionAdapter;

    @BindView(R.id.imgCoverPicIV)
    ImageView imgCoverPicIV;
    @BindView(R.id.imgBackLL)
    LinearLayout imgBackLL;
    @BindView(R.id.txtNameTV)
    TextViewArialRegular txtNameTV;
    @BindView(R.id.txtUserNameTV)
    TextViewArialRegular txtUserNameTV;
    @BindView(R.id.imgAddProfileIV)
    CircleImageView imgAddProfileIV;
    @BindView(R.id.txtEmailAddressTV)
    TextViewArialRegular txtEmailAddressTV;
    @BindView(R.id.txtPasswordTV)
    TextViewArialRegular txtPasswordTV;
    @BindView(R.id.txtBioTV)
    TextViewArialRegular txtBioTV;
    @BindView(R.id.conditionsRV)
    RecyclerView conditionsRV;
    @BindView(R.id.btnFinishB)
    ButtonGothamMedium btnFinishB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_signup_step4);
        ButterKnife.bind(this);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        strPushToken = FirebaseInstanceId.getInstance().getToken();

        if (getIntent() != null) {
            strUserName = getIntent().getStringExtra("USERNAME");
            strName = getIntent().getStringExtra("NAME");
            strEmail = getIntent().getStringExtra("EMAIL");
            strPassword = getIntent().getStringExtra("PASSWORD");
            strBio = getIntent().getStringExtra("BIO");
            isMachineLearningEnabled = getIntent().getBooleanExtra("M_LANGUAGE", false);
            mConditionsArrayList = (ArrayList<Conditions>) getIntent().getSerializableExtra("LIST");
            setUpCollectedData();
        }
    }

    private void setUpCollectedData() {
        if (mConditionsArrayList.size() > 0) {
            setAdapter();
        }
        txtBioTV.setText(strBio);
        txtEmailAddressTV.setText(strEmail);
        txtNameTV.setText(strName);
        if (!strUserName.contains("@"))
            txtUserNameTV.setText("@" + strUserName);
        else
            txtUserNameTV.setText(strUserName);

        txtPasswordTV.setText(strPassword);

    }

    private void setAdapter() {
        mSignUpCondtionAdapter = new SignUpCondtionAdapter(mActivity, mConditionsArrayList);
        conditionsRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        conditionsRV.setLayoutManager(mLayoutManager);
        conditionsRV.setItemAnimator(new DefaultItemAnimator());
        conditionsRV.setAdapter(mSignUpCondtionAdapter);
    }


    @OnClick({R.id.imgCoverPicIV, R.id.imgBackLL, R.id.imgAddProfileIV, R.id.btnFinishB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgCoverPicIV:
                mPictureType = getString(R.string.coverpic);
                setUpCameraGallery();
                break;
            case R.id.imgBackLL:
                onBackPressed();
                break;
            case R.id.imgAddProfileIV:
                mPictureType = getString(R.string.profilepic);
                setUpCameraGallery();
                break;
            case R.id.btnFinishB:
                /*execute SignUp Process*/
                if (Utilities.isNetworkAvailable(mActivity)) {
                    signUpAuthentication();
                } else {
                    Toast.makeText(mActivity, getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void setUpCameraGallery() {
        if (checkPermission())
            openCameraGalleryDialog();
        else
            requestPermission();
    }

    public void openCameraGalleryDialog() {
        final Dialog dialog = new Dialog(mActivity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_display_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView text_camra = (TextView) dialog.findViewById(R.id.txt_camra);
        TextView text_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);

        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCamera();
            }
        });
        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Log.e("error is occured", dialog.toString());
    }

    private void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri imageURI = FileProvider.getUriForFile(mActivity, "com.painly.com.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        imageURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    private void openGallery() {
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(intent, GALLERY_REQUEST);

        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, REQUEST_PERMISSION_CODE);
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    openCameraGalleryDialog();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    requestPermission();
                }
                return;
            }

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {


            try {
                if (data != null) {
                    try {
                        Uri uri = data.getData();
                        File finalFile = new File(getRealPathFromURI(uri));
                        Bitmap thumb = ImageUtils.getInstant().rotateBitmapOrientation(finalFile.getPath());//getCompressedBitmap(mStoragePath);

                        mBitmapImage = thumb;

                        if (mPictureType.equals(getString(R.string.profilepic))) {
                            imgAddProfileIV.setImageBitmap(mBitmapImage);
                            isProfilePic = true;
                        } else if (mPictureType.equals(getString(R.string.coverpic))) {
                            imgCoverPicIV.setImageBitmap(mBitmapImage);
                            isCoverPic = true;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e(TAG, "*****No Picture Selected****");
                    return;
                }

            } finally {
            }
        }




           /* Uri uri = data.getData();
            File finalFile = new File(getRealPathFromURI(uri));
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 0;
            Bitmap mBitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
//                          For COnvert And rotate image
            ExifInterface exifInterface = null;
            try {
                exifInterface = new ExifInterface(finalFile.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            Matrix matrix = new Matrix();
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.setRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.setRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.setRotate(270);
                    break;
                case ExifInterface.ORIENTATION_NORMAL:
                default:
            }
            Bitmap rotate = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);

            mBitmapImage = rotate;


            if (mPictureType.equals(getString(R.string.profilepic))) {
                imgAddProfileIV.setImageBitmap(mBitmapImage);
                isProfilePic = true;
            } else if (mPictureType.equals(getString(R.string.coverpic))) {
                imgCoverPicIV.setImageBitmap(mBitmapImage);
                isCoverPic = true;
            }*/


           /* try {
                Bitmap bitmap = null;
                stream = mActivity.getContentResolver().openInputStream(data.getData());
                bitmap = BitmapFactory.decodeStream(stream);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                mBitmapImage = bitmap;

                if (mPictureType.equals(getString(R.string.profilepic))) {
                    imgAddProfileIV.setImageBitmap(mBitmapImage);
                    isProfilePic = true;
                } else if (mPictureType.equals(getString(R.string.coverpic))) {
                    imgCoverPicIV.setImageBitmap(mBitmapImage);
                    isCoverPic = true;
                }

                //strImageBase64 = "" + Utilities.bitmapToBase64(mBitmapImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }*/

        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bitmap bitmap = ImageUtils.getInstant().rotateBitmapOrientation(mStoragePath);
            //bitmap = Bitmap.createScaledBitmap(bitmap, 260, 173, false);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            //bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            mBitmapImage = bitmap;
            if (mPictureType.equals(getString(R.string.profilepic))) {
                imgAddProfileIV.setImageBitmap(mBitmapImage);
            } else if (mPictureType.equals(getString(R.string.coverpic))) {
                imgCoverPicIV.setImageBitmap(mBitmapImage);
            }
            //strImageBase64 = "" + Utilities.bitmapToBase64(mBitmapImage);
        }



}

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void signUpAuthentication() {
        AlertDialogManager.showProgressDialog(mActivity);
        mFirebaseAuth.createUserWithEmailAndPassword(strEmail, strPassword)
                .addOnCompleteListener(mActivity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "****Exceptions***" + task.getException());
                        } else {
                            FirebaseUser user = mFirebaseAuth.getCurrentUser();
                            String strUserID = user.getUid();
                            Log.e(TAG, "**USER ID**" + strUserID);
                            PainlyPreference.writeBoolean(mActivity, PainlyPreference.IS_LOGIN, true);
                            PainlyPreference.writeString(mActivity, PainlyPreference.USER_ID, strUserID);
                            PainlyPreference.writeString(mActivity, PainlyPreference.USER_EMAIL_ID, user.getEmail());
                            if (isProfilePic && isCoverPic) {
                                bothProfilePic();
                                return;
                            } else if (isCoverPic) {
                                uploadCoverPic();
                                return;
                            } else if (isProfilePic) {
                                uploadProfilePic();
                                return;
                            } else {
                                putSignUp();
                                return;
                            }

                        }
                    }
                });
    }


    private void uploadProfilePic() {
        try {
            //Upload Profile Pic:::::
            mProfileRef = FirebaseDatabase.getInstance().getReference().child("users").push();
            StorageReference profileSRef = mStorageRef.child("profilePictures/").child(mProfileRef.getKey());
            imgAddProfileIV.setDrawingCacheEnabled(true);
            imgAddProfileIV.buildDrawingCache();
            Bitmap bitmap = ((BitmapDrawable) imgAddProfileIV.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteArray = baos.toByteArray();

            UploadTask uploadTask = profileSRef.putBytes(byteArray);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.e(TAG, "****onFailure***");
                    AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.error), exception.toString());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.e(TAG, "****onSuccess***");
                    mProfileUrl = taskSnapshot.getDownloadUrl();
                    putSignUp();
                }
            });

        } catch (Exception mE) {
            Log.e(TAG, "=====ERROR====" + mE);
        }
    }

    private void uploadCoverPic() {
        try {
            //Upload Profile Pic:::::
            mCoverRef = FirebaseDatabase.getInstance().getReference().child("users").push();
            StorageReference profileSRef = mStorageRef.child("coverPhotos/").child(mCoverRef.getKey());
            imgCoverPicIV.setDrawingCacheEnabled(true);
            imgCoverPicIV.buildDrawingCache();
            Bitmap bitmap = ((BitmapDrawable) imgCoverPicIV.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteArray = baos.toByteArray();

            UploadTask uploadTask = profileSRef.putBytes(byteArray);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.e(TAG, "****onFailure***");
                    AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.error), exception.toString());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.e(TAG, "****onSuccess***");
                    mCoverUrl = taskSnapshot.getDownloadUrl();
                    putSignUp();
                }
            });

        } catch (Exception mE) {
            Log.e(TAG, "=====ERROR====" + mE);
        }
    }


    private void bothProfilePic() {
        try {
            //Upload Profile Pic:::::
            mProfileRef = FirebaseDatabase.getInstance().getReference().child("users").push();
            StorageReference profileSRef = mStorageRef.child("profilePictures/").child(mProfileRef.getKey());
            imgAddProfileIV.setDrawingCacheEnabled(true);
            imgAddProfileIV.buildDrawingCache();
            Bitmap bitmap = ((BitmapDrawable) imgAddProfileIV.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteArray = baos.toByteArray();

            UploadTask uploadTask = profileSRef.putBytes(byteArray);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.e(TAG, "****onFailure***");
                    AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.error), exception.toString());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.e(TAG, "****onSuccess***");
                    mProfileUrl = taskSnapshot.getDownloadUrl();
                    bothCoverPic();
                }
            });

        } catch (Exception mE) {
            Log.e(TAG, "=====ERROR====" + mE);
        }
    }

    private void bothCoverPic() {
        try {
            //Upload Profile Pic:::::
            mCoverRef = FirebaseDatabase.getInstance().getReference().child("users").push();
            StorageReference profileSRef = mStorageRef.child("coverPhotos/").child(mCoverRef.getKey());
            imgCoverPicIV.setDrawingCacheEnabled(true);
            imgCoverPicIV.buildDrawingCache();
            Bitmap bitmap = ((BitmapDrawable) imgCoverPicIV.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteArray = baos.toByteArray();

            UploadTask uploadTask = profileSRef.putBytes(byteArray);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.e(TAG, "****onFailure***");
                    AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.error), exception.toString());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                    Log.e(TAG, "****onSuccess***");
                    mCoverUrl = taskSnapshot.getDownloadUrl();
                    putSignUp();
                }
            });

        } catch (Exception mE) {
            Log.e(TAG, "=====ERROR====" + mE);
        }
    }


    private void putSignUp() {
      /*ADDING DATA IN FIREBASE*/
      /*Conditions HasMap*/
        HashMap<String, String> mConditionsHM = new HashMap<String, String>();
        for (int i = 0; i < mConditionsArrayList.size(); i++) {
            mConditionsHM.put(mConditionsArrayList.get(i).getConditionID(), mConditionsArrayList.get(i).getName());
        }
        /*Details HasMap*/
        HashMap mDetailsHM = new HashMap<>();
        HashMap mFavoritesHM = new HashMap<>();
        mDetailsHM.put("username", strUserName.toLowerCase());
        mDetailsHM.put("pushToken", strPushToken);
        mDetailsHM.put("name", strName);
        mDetailsHM.put("gender", "");
        mDetailsHM.put("deviceType", "android");//ios
        mDetailsHM.put("isMachineLearningEnabled", isMachineLearningEnabled);
        mDetailsHM.put("uid", PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, ""));
        if (strName != null)
            PainlyPreference.writeString(mActivity, PainlyPreference.USER_NAME, strName);
        if (isProfilePic) {
            mDetailsHM.put("image", mProfileUrl.toString());
            if (mProfileUrl != null)
                PainlyPreference.writeString(mActivity, PainlyPreference.USER_IMAGE, mProfileUrl.toString());
        }
        if (isCoverPic) {
            mDetailsHM.put("coverPhoto", mCoverUrl.toString());
        }

        /**/
        HashMap mNotifications = new HashMap<>();
        /*Full HashMap*/
        HashMap<String, HashMap<String, String>> parentHM = new HashMap<>();

        parentHM.put("conditions", mConditionsHM);
        parentHM.put("details", mDetailsHM);
        parentHM.put("favorites", mFavoritesHM);


        mRootRefrence.child("users").child(PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "")).setValue(parentHM);
        isfirstTimeSignUp = true;
        mRootRefrence.child("usernames").child(PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "")).setValue(strUserName);


        AlertDialogManager.hideProgressDialog();


        Toast.makeText(mActivity, getString(R.string.signup_sucess), Toast.LENGTH_LONG).show();
//        Intent intent = new Intent(getApplicationContext(), PainPalActivity.class);
        Intent intent = new Intent(getApplicationContext(), NewPainPalsActivity.class);
        intent.putExtra("TYPE", Constants.FROM_SIGNUP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }


}
