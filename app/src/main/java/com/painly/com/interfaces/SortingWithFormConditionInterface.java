package com.painly.com.interfaces;

import com.painly.com.beans.Conditions;

/**
 * Created by android-da on 8/2/18.
 */

public interface SortingWithFormConditionInterface {
    public void getSortedCondition(Conditions mCondition);
}
