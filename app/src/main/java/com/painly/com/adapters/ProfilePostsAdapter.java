package com.painly.com.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.painly.com.R;
import com.painly.com.activities.NewForumsDetailsActivity;
import com.painly.com.activities.TextDetailsActivity;
import com.painly.com.beans.FormsModel;

import java.util.ArrayList;


/*
 * Created by android-raman on 2/5/18.
 */

public class ProfilePostsAdapter extends RecyclerView.Adapter<ProfilePostsAdapter.ViewHolder> {
    private ArrayList<FormsModel> mArrayList;
    private Activity mActivity;

    public ProfilePostsAdapter(Activity mActivity, ArrayList<FormsModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_posts, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final FormsModel mFormsModel = mArrayList.get(position);
        if (mFormsModel.getPicture() != null && mFormsModel.getPicture().contains("http")) {
            holder.itemPostImageIV.setVisibility(View.VISIBLE);

            Glide.with(mActivity.getApplicationContext())
                    .load(mFormsModel.getPicture())
                    .apply(RequestOptions.placeholderOf(R.drawable.icon_p_p_ph).error(R.drawable.icon_p_p_ph))
                    .into(holder.itemPostImageIV);
        } else {
            holder.itemPostImageIV.setVisibility(View.GONE);
        }
        if (mFormsModel.getTitle() != null)
            holder.itemPostTitleTV.setText(mFormsModel.getTitle());
        if (mFormsModel.getBody() != null)
            holder.itemPostDetailsTV.setText(mFormsModel.getBody());

        holder.itemLayoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFormsModel.getType().equals("video")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                    mActivity.startActivity(mIntent);
                } else if (mFormsModel.getType().equals("link")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                    mActivity.startActivity(mIntent);
                } else if (mFormsModel.getType().equals("text")) {
                    Intent mIntent = new Intent(mActivity, TextDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                    mActivity.startActivity(mIntent);
                } else if (mFormsModel.getType().equals("picture")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                    mActivity.startActivity(mIntent);
                }
            }
        });
        holder.itemPostDetailsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFormsModel.getType().equals("video")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                    mActivity.startActivity(mIntent);
                } else if (mFormsModel.getType().equals("link")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                    mActivity.startActivity(mIntent);
                } else if (mFormsModel.getType().equals("text")) {
                    Intent mIntent = new Intent(mActivity, TextDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                    mActivity.startActivity(mIntent);
                } else if (mFormsModel.getType().equals("picture")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                    mActivity.startActivity(mIntent);
                }
            }
        });
        holder.itemPostTitleTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFormsModel.getType().equals("video")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                    mActivity.startActivity(mIntent);
                } else if (mFormsModel.getType().equals("link")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                    mActivity.startActivity(mIntent);
                } else if (mFormsModel.getType().equals("text")) {
                    Intent mIntent = new Intent(mActivity, TextDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                    mActivity.startActivity(mIntent);
                } else if (mFormsModel.getType().equals("picture")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                    mActivity.startActivity(mIntent);
                }
            }
        });
        holder.itemPostImageIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFormsModel.getType().equals("video")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                    mActivity.startActivity(mIntent);
                } else if (mFormsModel.getType().equals("link")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                    mActivity.startActivity(mIntent);
                } else if (mFormsModel.getType().equals("text")) {
                    Intent mIntent = new Intent(mActivity, TextDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                    mActivity.startActivity(mIntent);
                } else if (mFormsModel.getType().equals("picture")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                    mActivity.startActivity(mIntent);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public com.makeramen.roundedimageview.RoundedImageView itemPostImageIV;
        public TextView itemPostTitleTV, itemPostDetailsTV;
        public LinearLayout itemLayoutLL;


        public ViewHolder(View itemView) {
            super(itemView);
            itemPostImageIV = (com.makeramen.roundedimageview.RoundedImageView) itemView.findViewById(R.id.itemPostImageIV);
            itemLayoutLL = (LinearLayout) itemView.findViewById(R.id.itemLayoutLL);
            itemPostTitleTV = (TextView) itemView.findViewById(R.id.itemPostTitleTV);
            itemPostDetailsTV = (TextView) itemView.findViewById(R.id.itemPostDetailsTV);
        }
    }
}
