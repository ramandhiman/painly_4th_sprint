package com.painly.com.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.painly.com.BaseActivity;
import com.painly.com.R;
import com.painly.com.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewChatImageActivity extends BaseActivity {
    String TAG = ViewChatImageActivity.this.getClass().getSimpleName();
    Activity mActivity = ViewChatImageActivity.this;
    @BindView(R.id.llCloseLL)
    LinearLayout llCloseLL;
    @BindView(R.id.imgMessageIV)
    ImageView imgMessageIV;
    @BindView(R.id.imgProgressLoadPB)
    ProgressBar imgProgressLoadPB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_chat_image);
        ButterKnife.bind(this);
        setUpMessagePicture();

    }

    /*
    *  */

    private void setUpMessagePicture() {
        if (getIntent() != null && getIntent().getStringExtra(Constants.MESSAGE_PICTURE_URL) != null) {
            if (getIntent().getStringExtra(Constants.MESSAGE_PICTURE_URL).contains("http")) {
                String strMsgURL = getIntent().getStringExtra(Constants.MESSAGE_PICTURE_URL);
                DisplayImageOptions mDisplayImageOptions = new DisplayImageOptions.Builder()
                        .showImageOnLoading(R.drawable.bg_black)
                        .showImageForEmptyUri(R.drawable.bg_black)
                        .showImageOnFail(R.drawable.bg_black)
                        .cacheInMemory(true)
                        .considerExifParams(true)
                        .bitmapConfig(Bitmap.Config.RGB_565)
                        .build();

                ImageLoader.getInstance().displayImage(strMsgURL, imgMessageIV, mDisplayImageOptions, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        imgProgressLoadPB.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        imgProgressLoadPB.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        imgProgressLoadPB.setVisibility(View.GONE);
                    }
                });
            }
        }

        imgMessageIV.setOnTouchListener(new ImageMatrixTouchHandler(mActivity));
    }

    @OnClick(R.id.llCloseLL)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
