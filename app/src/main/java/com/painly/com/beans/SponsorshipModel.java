package com.painly.com.beans;

import java.io.Serializable;

/**
 * Created by android-da on 2/18/19.
 */

public class SponsorshipModel implements Serializable {
    String image;
    String name;


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
