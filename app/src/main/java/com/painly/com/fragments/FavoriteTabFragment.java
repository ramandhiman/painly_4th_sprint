package com.painly.com.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.R;
import com.painly.com.adapters.FavoriteUsersAdapter;
import com.painly.com.beans.FavoriteModel;
import com.painly.com.beans.User;
import com.painly.com.beans.UserDetails;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.PainlyPreference;
import com.painly.com.utils.Utilities;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteTabFragment extends Fragment {
    String TAG = FavoriteTabFragment.this.getClass().getSimpleName();
    View mRootView;

    @BindView(R.id.favoriteUsersRV)
    RecyclerView favoriteUsersRV;
    @BindView(R.id.withoutFavoriteLL)
    LinearLayout withoutFavoriteLL;
    Unbinder unbinder;

    ArrayList<String> mFavUserIDsAL = new ArrayList<String>();
    ArrayList<UserDetails> mFavoriteUserArrayList = new ArrayList<UserDetails>();
    FavoriteUsersAdapter mFavoriteUsersAdapter;
    DatabaseReference mRootRefrence;


    public FavoriteTabFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_favorite_tab, container, false);
        unbinder = ButterKnife.bind(this, mRootView);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        AlertDialogManager.hideProgressDialog();
        if (Utilities.isNetworkAvailable(getActivity())) {
            getFavoriteUsersID();
        } else {
            Toast.makeText(getActivity(), getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
        }
        return mRootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void getFavoriteUsersID() {
        AlertDialogManager.showProgressDialog(getActivity());
        mRootRefrence.child("users").child(PainlyPreference.readString(getActivity(), PainlyPreference.USER_ID, "")).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mFavUserIDsAL.clear();
                FavoriteModel mFavoriteModel = dataSnapshot.getValue(FavoriteModel.class);
                Collection<String> values = mFavoriteModel.getFavorites().keySet();
                mFavUserIDsAL = new ArrayList<String>(values);

                getFavoriteUserDataDetails();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "====Error+===" + databaseError.toString());
            }
        });
    }

    private void getFavoriteUserDataDetails() {
        mFavoriteUserArrayList.clear();
        for (int i = 0; i < mFavUserIDsAL.size(); i++) {
            Log.e(TAG, "======User IDs Size====" + mFavUserIDsAL.get(i));
            mRootRefrence.child("users").child(mFavUserIDsAL.get(i)).child("details").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    AlertDialogManager.hideProgressDialog();
                    final UserDetails mUser = dataSnapshot.getValue(UserDetails.class);
                    mFavoriteUserArrayList.add(mUser);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    AlertDialogManager.hideProgressDialog();
                    Toast.makeText(getActivity(), databaseError.toString(), Toast.LENGTH_LONG).show();
                }
            });

        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AlertDialogManager.hideProgressDialog();
                setFavoriteUserAdapter();
            }
        }, 2000);
    }


    private void setFavoriteUserAdapter() {
        if (favoriteUsersRV != null) {
            if (mFavoriteUserArrayList.size() > 0){
                withoutFavoriteLL.setVisibility(View.GONE);
            }else{
                withoutFavoriteLL.setVisibility(View.VISIBLE);
            }

            mFavoriteUsersAdapter = new FavoriteUsersAdapter(getActivity(), mFavoriteUserArrayList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            favoriteUsersRV.setLayoutManager(mLayoutManager);
            favoriteUsersRV.setItemAnimator(new DefaultItemAnimator());
            favoriteUsersRV.setAdapter(mFavoriteUsersAdapter);
            mFavoriteUsersAdapter.notifyDataSetChanged();
        }
    }

}
