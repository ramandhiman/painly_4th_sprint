package com.painly.com.chathead;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.painly.com.R;
import com.painly.com.activities.OpenLinkActivity;
import com.painly.com.activities.PlayVideoActivity;
import com.painly.com.utils.Constants;

/**
 * Created by android-da on 2/4/19.
 */

public class ItemRecieverViewHolder extends RecyclerView.ViewHolder {

    public TextView txtMessageTV, txtImgTitleTV, txtImgLinkTV;
    public RoundedImageView itemMessagePicIV;
    public LinearLayout mainLL, findPostLL, loadingLL;
    public ImageView imgYoutubeIV;

    public ItemRecieverViewHolder(View itemView) {
        super(itemView);
        txtImgTitleTV = (TextView) itemView.findViewById(R.id.txtImgTitleTV);
        txtImgLinkTV = (TextView) itemView.findViewById(R.id.txtImgLinkTV);
        txtMessageTV = (TextView) itemView.findViewById(R.id.txtMessageTV);
        itemMessagePicIV = (RoundedImageView) itemView.findViewById(R.id.itemMessagePicIV);
        mainLL = (LinearLayout) itemView.findViewById(R.id.mainLL);
        findPostLL = (LinearLayout) itemView.findViewById(R.id.findPostLL);
        imgYoutubeIV = (ImageView) itemView.findViewById(R.id.imgYoutubeIV);
    }

    public void bindData(final Context context, final ChatHeadModel tempValue/*,boolean isLoading*/) {
        if (tempValue.getTitle() != null && tempValue.getLink() != null) {
            mainLL.setVisibility(View.VISIBLE);
            findPostLL.setVisibility(View.VISIBLE);
            txtMessageTV.setText(tempValue.getTitle());
            Glide.with(context.getApplicationContext())
                    .load(tempValue.getImage())
                    .apply(RequestOptions.placeholderOf(R.drawable.icon_round_ph).error(R.drawable.icon_round_ph))
                    .into(itemMessagePicIV);
            txtImgLinkTV.setText(tempValue.getLink());
            txtImgTitleTV.setText(tempValue.getThumbtitle());
            if (tempValue.getFeedType().equals(Constants.RSS_TYPE)){
                imgYoutubeIV.setVisibility(View.GONE);
            }else if (tempValue.getFeedType().equals(Constants.ATOM_TYPE)){
                imgYoutubeIV.setVisibility(View.VISIBLE);
            }
        } else {
            mainLL.setVisibility(View.VISIBLE);
            findPostLL.setVisibility(View.GONE);
            txtMessageTV.setText(tempValue.getTitle());
        }



        findPostLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempValue.getFeedType().equals(Constants.RSS_TYPE)){
                    Intent aboutIntent = new Intent(context, OpenLinkActivity.class);
                    aboutIntent.putExtra("LINK", tempValue.getLink());
                    context.startActivity(aboutIntent);
                }else if (tempValue.getFeedType().equals(Constants.ATOM_TYPE)){
                    Intent mIntent = new Intent(context, PlayVideoActivity.class);
                    mIntent.putExtra("VIDEO_URL", tempValue.getLink());
                    context.startActivity(mIntent);
                }

            }
        });

    }

    private void setAnimation(View mView) {
        TranslateAnimation animate = new TranslateAnimation(0, mView.getWidth(), 0, 0);
        animate.setDuration(500);
        mView.startAnimation(animate);
    }
}
