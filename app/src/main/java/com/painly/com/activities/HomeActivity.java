package com.painly.com.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.R;
import com.painly.com.beans.NotificationsModel;
import com.painly.com.beans.User;
import com.painly.com.beans.UserDetails;
import com.painly.com.chat.ChatwithUserActivity;
import com.painly.com.chathead.ChatHeadService;
import com.painly.com.fragments.ChatFragment;
import com.painly.com.fragments.HomeFragment;
import com.painly.com.fragments.MapFragment;
import com.painly.com.fragments.NewProfileFragment;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;
import com.painly.com.utils.Utilities;
import com.painly.com.views.HomeWatcher;
import com.painly.com.views.OnHomePressedListener;

import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class HomeActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        ResultCallback<LocationSettingsResult>{

    public  int OVERLAY_PERMISSION_REQ_CODE_CHATHEAD = 1234;


    public static final int REQUEST_PERMISSION_CODE = 919;
    /************************
     *Fused Google Location
     **************/
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    protected final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    protected final static String KEY_LOCATION = "location";
    protected final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
    public static ArrayList<User> mUserArrayList = new ArrayList<User>();
    public static int NOTIFICATIONS_COUNTS = 0;
    public static int MSG_NOTIFICATIONS_COUNTS = 0;
    public long mLatitude;
    public long mLongitude;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected Location mCurrentLocation;
    protected Boolean mRequestingLocationUpdates;
    protected String mLastUpdateTime;
    Activity mActivity = HomeActivity.this;
    String TAG = HomeActivity.this.getClass().getSimpleName();
    ImageView tab1IV, tab2IV, tab3IV, tab4IV, tab5IV;
    ImageView tab1SelIV, tab3SelIV, tab4SelIV, tab5SelIV;
    LinearLayout bottomTabsLL;
    FrameLayout containerLL;
    RelativeLayout layoutTab1LL, layoutTab2LL, layoutTab3LL, layoutTab4LL, layoutTab5LL;
    DatabaseReference mRootRefrence;
    boolean isProfileSigleClick = false;
    com.nex3z.notificationbadge.NotificationBadge mNotificationsBadge;
    public static com.nex3z.notificationbadge.NotificationBadge mMsgNotiBadge;
    private String mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION;
    private String mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;

    /********
     *Replace Fragment In Activity
     **********/
    public void addFragmentToView(Fragment fragment, boolean addToStack, Bundle bundle) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            // Replace current fragment by this new one
            ft.replace(R.id.containerLL, fragment);
            if (addToStack)
                ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        /*************
         *Google Location
         **************/
        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";
       /*Update values using data stored in the Bundle.*/
        updateValuesFromBundle(savedInstanceState);
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
        //checkLocationSettings();
        updateValuesFromBundle(savedInstanceState);
        /*******************************************/
        setContentView(R.layout.activity_home);
        Log.e(TAG,"onCreate");

        //setHomeButtonPress();

        gettingCurrentUserData();

        mNotificationsBadge = (com.nex3z.notificationbadge.NotificationBadge) findViewById(R.id.mNotificationsBadge);
        mMsgNotiBadge = (com.nex3z.notificationbadge.NotificationBadge) findViewById(R.id.mMsgNotiBadge);
        tab1IV = (ImageView) findViewById(R.id.tab1IV);
        tab2IV = (ImageView) findViewById(R.id.tab2IV);
        tab3IV = (ImageView) findViewById(R.id.tab3IV);
        tab4IV = (ImageView) findViewById(R.id.tab4IV);
        tab5IV = (ImageView) findViewById(R.id.tab5IV);

        tab1SelIV = (ImageView) findViewById(R.id.tab1SelIV);
        tab3SelIV = (ImageView) findViewById(R.id.tab3SelIV);
        tab4SelIV = (ImageView) findViewById(R.id.tab4SelIV);
        tab5SelIV = (ImageView) findViewById(R.id.tab5SelIV);

        bottomTabsLL = (LinearLayout) findViewById(R.id.bottomTabsLL);
        containerLL = (FrameLayout) findViewById(R.id.containerLL);

        layoutTab1LL = (RelativeLayout) findViewById(R.id.layoutTab1LL);
        layoutTab2LL = (RelativeLayout) findViewById(R.id.layoutTab2LL);
        layoutTab3LL = (RelativeLayout) findViewById(R.id.layoutTab3LL);
        layoutTab4LL = (RelativeLayout) findViewById(R.id.layoutTab4LL);
        layoutTab5LL = (RelativeLayout) findViewById(R.id.layoutTab5LL);


        if (getIntent().getStringExtra("STR_NOTI") != null) {
            String strData = getIntent().getStringExtra("STR_NOTI");
            Log.e(TAG, "====Notificatons Data====" + strData);
            notificationWork(strData);
            addFragmentToView(new HomeFragment(), false, null);
            homeClick();
        } else {
            addFragmentToView(new HomeFragment(), false, null);
            homeClick();
        }

        Constants.USER_ID = PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "");
        Constants.LAT = Double.longBitsToDouble(PainlyPreference.readLong(mActivity, PainlyPreference.CURRENT_LOCATION_LATITUDE, 0));
        Constants.LONG = Double.longBitsToDouble(PainlyPreference.readLong(mActivity, PainlyPreference.CURRENT_LOCATION_LONGITUDE, 0));

        /*Getting User Name*/
        mRootRefrence.child("users").child(Constants.USER_ID).child("details").child("name").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String strUserName = dataSnapshot.getValue(String.class);
                PainlyPreference.writeString(mActivity, PainlyPreference.USER_NAME, strUserName);
                Constants.USER_NAME = strUserName;
                Log.e(TAG, "***UserName***" + strUserName);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "***ERROR***" + databaseError);
            }
        });
        Log.e(TAG, "==UID==" + Constants.USER_ID);
        Log.e(TAG, "==UserName==" + Constants.USER_NAME);

        setclickListner();
        /*Getting Notifications Counts*/
        getNotificationsCounts();
    }

    private void setHomeButtonPress() {
        HomeWatcher mHomeWatcher = new HomeWatcher(this);
        mHomeWatcher.setOnHomePressedListener(new OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                Log.e(TAG,"onHomePressed");
                stopService(new Intent(HomeActivity.this, ChatHeadService.class));
            }

            @Override
            public void onHomeLongPressed() {

            }
        });


        mHomeWatcher.startWatch();
    }


    private void startChatHead() {
        startService(new Intent(HomeActivity.this, ChatHeadService.class));
    }

    private void needPermissionDialog() {
        final Dialog dialog = new Dialog(HomeActivity.this, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_chat_head);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button btnCancelB = (Button) dialog.findViewById(R.id.btnCancelB);
        Button btnAllowB = (Button) dialog.findViewById(R.id.btnAllowB);

        btnCancelB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnAllowB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                requestOverLayPermission();

            }
        });

        dialog.show();
    }


    private void requestOverLayPermission() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
        intent.setData(Uri.parse("package:" + getPackageName()));
        startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE_CHATHEAD);
    }

    private void gettingCurrentUserData() {
        mRootRefrence.child("users").child(PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "")).child("details").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    UserDetails mUserDetails = dataSnapshot.getValue(UserDetails.class);
                    if (mUserDetails.getImage() != null)
                        PainlyPreference.writeString(mActivity, PainlyPreference.USER_IMAGE, mUserDetails.getImage());
                    if (mUserDetails.getName() != null)
                        PainlyPreference.writeString(mActivity, PainlyPreference.USER_NAME, mUserDetails.getName());


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void setclickListner() {
        layoutTab1LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialogManager.hideProgressDialog();
                if (new HomeFragment() != null) {
                    addFragmentToView(new HomeFragment(), false, null);
                    homeClick();
                }

            }
        });
        layoutTab3LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialogManager.hideProgressDialog();
                if (new MapFragment() != null) {
                    addFragmentToView(new MapFragment(), false, null);
                    locationClick();
                }

            }
        });
        layoutTab4LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialogManager.hideProgressDialog();
                if (new ChatFragment() != null) {
                    addFragmentToView(new ChatFragment(), false, null);
                    chatClick();
                }

            }
        });
        layoutTab5LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isProfileSigleClick) {
                    isProfileSigleClick = false;
                    AlertDialogManager.hideProgressDialog();
                    if (new NewProfileFragment() != null) {
                        addFragmentToView(new NewProfileFragment(), false, null);
                        profileClick();
                    }

                }
            }
        });

    }

    private void notificationWork(String strData) {
        try {
            JSONObject mJsonObject = new JSONObject(strData);

            if (mJsonObject.getString("notiType").equals(Constants.CHAT)) {
                Intent intent = new Intent(getApplicationContext(), ChatwithUserActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("UID", mJsonObject.getString("uID"));
                startActivity(intent);
                finish();
            } else if (mJsonObject.getString("notiType").equals(Constants.FAVORITE)) {
                Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                mIntent.putExtra("UID", mJsonObject.getString("uID"));
                mIntent.putExtra("TYPE", Constants.ANOTHER);
                mActivity.startActivity(mIntent);
            } else if (mJsonObject.getString("notiType").toLowerCase().equals(Constants.COMMENT)) {
                if (mJsonObject.getString("commentType").equals(getString(R.string.video_type))) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mJsonObject.getString("forumID"));
                    mActivity.startActivity(mIntent);
                } else if (mJsonObject.getString("commentType").equals(getString(R.string.picture_type))) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mJsonObject.getString("forumID"));
                    mActivity.startActivity(mIntent);
                } else if (mJsonObject.getString("commentType").equals(getString(R.string.link_type))) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mJsonObject.getString("forumID"));
                    mActivity.startActivity(mIntent);
                } else if (mJsonObject.getString("commentType").equals(getString(R.string.text_type))) {
                    Intent mIntent = new Intent(mActivity, TextDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mJsonObject.getString("forumID"));
                    mActivity.startActivity(mIntent);
                }
            } else if (mJsonObject.getString("notiType").equals(Constants.ADDPOST)) {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void homeClick() {
        isProfileSigleClick = true;
        tab1IV.setImageResource(R.drawable.icon_tab1_s);
        tab2IV.setImageResource(R.drawable.icon_tab2_us);
        tab3IV.setImageResource(R.drawable.icon_tab3_us);
        tab4IV.setImageResource(R.drawable.icon_tab4_us);
        tab5IV.setImageResource(R.drawable.icon_tab5_us);
        tab1SelIV.setVisibility(View.VISIBLE);
        tab3SelIV.setVisibility(View.GONE);
        tab4SelIV.setVisibility(View.GONE);
        tab5SelIV.setVisibility(View.GONE);
    }

    public void notificationClick() {
        isProfileSigleClick = true;
        tab1IV.setImageResource(R.drawable.icon_tab1_us);
        tab2IV.setImageResource(R.drawable.icon_tab2_s);
        tab3IV.setImageResource(R.drawable.icon_tab3_us);
        tab4IV.setImageResource(R.drawable.icon_tab4_us);
        tab5IV.setImageResource(R.drawable.icon_tab5_us);
    }

    public void locationClick() {
        isProfileSigleClick = true;
        tab1IV.setImageResource(R.drawable.icon_tab1_us);
        tab2IV.setImageResource(R.drawable.icon_tab2_us);
        tab3IV.setImageResource(R.drawable.icon_tab3_s);
        tab4IV.setImageResource(R.drawable.icon_tab4_us);
        tab5IV.setImageResource(R.drawable.icon_tab5_us);
        tab1SelIV.setVisibility(View.GONE);
        tab3SelIV.setVisibility(View.VISIBLE);
        tab4SelIV.setVisibility(View.GONE);
        tab5SelIV.setVisibility(View.GONE);
    }

    public void chatClick() {
        isProfileSigleClick = true;
        tab1IV.setImageResource(R.drawable.icon_tab1_us);
        tab2IV.setImageResource(R.drawable.icon_tab2_us);
        tab3IV.setImageResource(R.drawable.icon_tab3_us);
        tab4IV.setImageResource(R.drawable.icon_tab4_s);
        tab5IV.setImageResource(R.drawable.icon_tab5_us);
        tab1SelIV.setVisibility(View.GONE);
        tab3SelIV.setVisibility(View.GONE);
        tab4SelIV.setVisibility(View.VISIBLE);
        tab5SelIV.setVisibility(View.GONE);
    }

    public void profileClick() {
        tab1IV.setImageResource(R.drawable.icon_tab1_us);
        tab2IV.setImageResource(R.drawable.icon_tab2_us);
        tab3IV.setImageResource(R.drawable.icon_tab3_us);
        tab4IV.setImageResource(R.drawable.icon_tab4_us);
        tab5IV.setImageResource(R.drawable.icon_tab5_s);
        tab1SelIV.setVisibility(View.GONE);
        tab3SelIV.setVisibility(View.GONE);
        tab4SelIV.setVisibility(View.GONE);
        tab5SelIV.setVisibility(View.VISIBLE);
    }


    /******************
     *Google Location
     ************/
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }
            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
            }
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i("", "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Uses a {@link LocationSettingsRequest.Builder} to build
     * a {@link LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest).setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} method, with the results provided through a {@code PendingResult}.
     */
    public void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    /**
     * The callback invoked when
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} is called. Examines the
     * {@link LocationSettingsResult} object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i("", "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i("", "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i("", "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i("", "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
            default:
                // finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i("", "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i("", "User chose not to make required location settings changes.");
                        break;
                    default:
                        // finish();
                }
        }
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    mLocationRequest,
                    this
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    mRequestingLocationUpdates = true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG,"onStart");
        if (mGoogleApiClient != null)
        mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG,"onResume");
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }

        /*
        * Set Chatbot functionality
        * In HomeActivity
        * */
//        if (Utilities.canDrawOverlays(HomeActivity.this))
//            startChatHead();
//        else {
//            needPermissionDialog();
//        }


    }

    /*'''
    * Chatbot functionality
    * */
    public void setUpChatHead() {
        if (Utilities.canDrawOverlays(HomeActivity.this))
            startChatHead();
        else {
            needPermissionDialog();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG,"onPause");
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG,"onStop");
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG,"onRestart");
    }


    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i("", "Connected to GoogleApiClient");
        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        }
    }

    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        mLatitude = Double.doubleToRawLongBits(mCurrentLocation.getLatitude());
        mLongitude = Double.doubleToRawLongBits(mCurrentLocation.getLongitude());
        Log.e(TAG, "*********LATITUDE********" + mLatitude);
        Log.e(TAG, "*********LONGITUDE********" + mLongitude);
        PainlyPreference.writeLong(mActivity, PainlyPreference.CURRENT_LOCATION_LATITUDE, mLatitude);
        PainlyPreference.writeLong(mActivity, PainlyPreference.CURRENT_LOCATION_LONGITUDE, mLongitude);

        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(Double.longBitsToDouble(mLatitude), Double.longBitsToDouble(mLongitude), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i("", "Connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i("", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }
    /*****************************************/

    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     **********/
    public boolean checkPermission() {
        int mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation);
        int mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation);
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{mAccessFineLocation, mAccessCourseLocation}, REQUEST_PERMISSION_CODE);
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    checkLocationSettings();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    // requestPermission();
                }
                return;
            }

        }
    }


    public void updateNotificationsCount(int mCount) {
        if (mCount > 0) {
            mNotificationsBadge.setVisibility(View.VISIBLE);
            mNotificationsBadge.setNumber(mCount);
        } else {
            mNotificationsBadge.setVisibility(View.GONE);
        }
    }


   /* public void updateMsgNotiCount(int mCount) {
        if (mCount > 0) {
            if (mMsgNotiBadge != null) {
                mMsgNotiBadge.setVisibility(View.VISIBLE);
                mMsgNotiBadge.setNumber(mCount);
            }
        } else {
            if (mMsgNotiBadge != null) {
                mMsgNotiBadge.setVisibility(View.GONE);
            }
        }
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG,"onDestroy");
//        stopService(new Intent(HomeActivity.this, ChatHeadService.class));
    }

    /*Getting Notifications Count*/
    private void getNotificationsCounts() {
        final ArrayList<NotificationsModel> mNewNotificationsAL = new ArrayList<>();
        if (FirebaseAuth.getInstance().getUid() != null) {
            mNewNotificationsAL.clear();
            mRootRefrence.child("users").child(FirebaseAuth.getInstance().getUid()).child("notifications").limitToLast(30).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mNewNotificationsAL.clear();
                    NOTIFICATIONS_COUNTS = 0;
                    for (DataSnapshot mDataSnapshot : dataSnapshot.getChildren()) {
                        NotificationsModel mNotificationsModel = mDataSnapshot.getValue(NotificationsModel.class);
                        if (mNotificationsModel.getCommentID() != null && mNotificationsModel.getCommentID().length() > 0) {
                            if (contains(mNewNotificationsAL, mNotificationsModel.getCommentID()) == false) {
                                if ((Boolean) mNotificationsModel.getIsNew() == true) {
                                    mNewNotificationsAL.add(mNotificationsModel);
                                    NOTIFICATIONS_COUNTS++;
                                    Log.e(TAG, "==Add Post Count===" + NOTIFICATIONS_COUNTS);
                                }
                            }
                        } else {
                            if ((Boolean) mNotificationsModel.getIsNew() == true) {
                                mNewNotificationsAL.add(mNotificationsModel);
                                NOTIFICATIONS_COUNTS++;
                                Log.e(TAG, "==Other Noti Count===" + NOTIFICATIONS_COUNTS);
                            }
                        }
                    }


                    updateNotificationsCount(NOTIFICATIONS_COUNTS);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, "==ERROR==" + databaseError.toString());
                }
            });
        }
    }

    boolean contains(ArrayList<NotificationsModel> list, String mCommentID) {
        for (NotificationsModel item : list) {
            if (item.getCommentID().equals(mCommentID)) {
                return true;
            }
        }
        return false;
    }


}
