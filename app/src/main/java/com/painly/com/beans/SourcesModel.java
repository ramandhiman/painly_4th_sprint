package com.painly.com.beans;

import java.io.Serializable;

/**
 * Created by android-da on 2/4/19.
 */

public class SourcesModel implements Serializable {
    String id;
    String sources;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSources() {
        return sources;
    }

    public void setSources(String sources) {
        this.sources = sources;
    }
}
