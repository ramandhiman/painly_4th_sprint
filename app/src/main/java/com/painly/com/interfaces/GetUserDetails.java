package com.painly.com.interfaces;

import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.painly.com.beans.FormsModel;

/**
 * Created by android-da on 5/16/18.
 */

public interface GetUserDetails {
    public void getUserDetails(FormsModel mFormsModel, RoundedImageView mRoundedImageView, TextView mTextView1, TextView mTextView2);
}
