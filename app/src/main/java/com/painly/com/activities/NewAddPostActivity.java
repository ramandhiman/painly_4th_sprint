package com.painly.com.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.leocardz.link.preview.library.LinkPreviewCallback;
import com.leocardz.link.preview.library.SourceContent;
import com.leocardz.link.preview.library.TextCrawler;
import com.painly.com.BaseActivity;
import com.painly.com.R;
import com.painly.com.adapters.PostImagesAdapter;
import com.painly.com.beans.PostImageModel;
import com.painly.com.font.ButtonArialRegular;
import com.painly.com.font.EditTextArialRegular;
import com.painly.com.interfaces.SelectImageInterface;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Constants;
import com.painly.com.utils.Utilities;
import com.painly.com.views.CutCopyPasteTextView;
import com.painly.com.youtubeSearch.VideoItem;
import com.painly.com.youtubeSearch.YoutubeSearchActivity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewAddPostActivity extends BaseActivity {
    public static final int GALLERY_REQUEST = 222;
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;
    public Bitmap mBitmapImage = null;
    Activity mActivity = NewAddPostActivity.this;
    String TAG = NewAddPostActivity.this.getClass().getSimpleName();

    @BindView(R.id.editTitleET)
    EditTextArialRegular editTitleET;
    @BindView(R.id.editLinkET)
    CutCopyPasteTextView editLinkET;
    @BindView(R.id.editAddTextET)
    EditTextArialRegular editAddTextET;

    @BindView(R.id.btnAddPhotoB)
    ButtonArialRegular btnAddPhotoB;
    @BindView(R.id.btnSearchYoutubeB)
    ButtonArialRegular btnSearchYoutubeB;
    @BindView(R.id.layoutSearchAddPhotoLL)
    LinearLayout layoutSearchAddPhotoLL;
    @BindView(R.id.imgAddOwnIV)
    ImageView imgAddOwnIV;
    @BindView(R.id.previewLL)
    LinearLayout previewLL;
    @BindView(R.id.llBackLL)
    LinearLayout llBackLL;
    @BindView(R.id.btnNextB)
    ButtonArialRegular btnNextB;

    @BindView(R.id.imagesRV)
    RecyclerView imagesRV;

    PostImagesAdapter mPostImagesAdapter;
    ArrayList<PostImageModel> mPostImgArrayList = new ArrayList<>();

    String mSelectedImgUrl = "";
    Uri mImageURI;

    // Create an instance of the TextCrawler to parse your url into a preview.
    TextCrawler textCrawler;


    SelectImageInterface mSelectImageInterface = new SelectImageInterface() {
        @Override
        public void getSelectedImgByteArray(Uri ImageUri) {
            mImageURI = ImageUri;
            mSelectedImgUrl = "";
        }

        @Override
        public void getSelectedImgUrl(String strUrl) {
            mSelectedImgUrl = strUrl;
            mImageURI = null;
        }
    };


    LinkPreviewCallback linkPreviewCallback = new LinkPreviewCallback() {
        @Override
        public void onPre() {
            showProgressDialog(mActivity);
            Log.e(TAG, "====onPre===");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    hideProgressDialog();
                }
            },7000);
        }

        @Override
        public void onPos(SourceContent sourceContent, boolean isNull) {
            hideProgressDialog();
            Log.e(TAG, "====onPos===");
            if (isNull || sourceContent.getFinalUrl().equals("")) {
                /**
                 * Inflating the content layout into Main View LinearLayout
                 */
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.that_does_not));
            } else {
                List<String> mImagesList = sourceContent.getImages();

                Log.e(TAG, "====onPos===");

                if (sourceContent.getTitle().length() > 0 || sourceContent.getDescription().length() > 0) {

                    if (sourceContent.getTitle() != null) {
                        if (editTitleET.getText().toString().trim().length() == 0)
                            editTitleET.setText(sourceContent.getTitle());
                    }
                    if (sourceContent.getDescription() != null) {
                        if (editAddTextET.getText().toString().trim().length() == 0)
                            editAddTextET.setText(sourceContent.getDescription());
                    }

                    if (mImagesList.size() > 0) {

                        for (int i = 0; i < mImagesList.size(); i++) {
                            PostImageModel mModel = new PostImageModel();
                            mModel.setStrImageUrl(mImagesList.get(i));

                            mPostImgArrayList.add(mModel);
                        }

                        mPostImagesAdapter.notifyDataSetChanged();
                    }

                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.that_does_not));
                }

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_add_post);
        ButterKnife.bind(this);
        textCrawler = new TextCrawler();
        getLinkPreView();
        setPostImagesAdapter();
        setUpBodyTextScroll();
    }

    private void setUpBodyTextScroll() {
        editAddTextET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.editAddTextET) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
    }

    private void setPostImagesAdapter() {
        mPostImagesAdapter = new PostImagesAdapter(mActivity, mPostImgArrayList, mSelectImageInterface);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        imagesRV.setLayoutManager(horizontalLayoutManager);
        imagesRV.setAdapter(mPostImagesAdapter);
    }

    @OnClick({R.id.llBackLL, R.id.btnAddPhotoB, R.id.btnSearchYoutubeB, R.id.imgAddOwnIV, R.id.btnNextB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llBackLL:
                onBackPressed();
                break;
            case R.id.btnAddPhotoB:
                addPhotoClick();
                break;
            case R.id.btnSearchYoutubeB:
                Intent mIntent23 = new Intent(mActivity, YoutubeSearchActivity.class);
                startActivityForResult(mIntent23, 232);
                break;
            case R.id.imgAddOwnIV:
                openGallerWithPermission();
                break;
            case R.id.btnNextB:
                submitPostToTags();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void addPhotoClick() {
        layoutSearchAddPhotoLL.setVisibility(View.GONE);
        previewLL.setVisibility(View.VISIBLE);
        imagesRV.setVisibility(View.GONE);
    }

    private void pasteUrlYouTubeLink() {
        layoutSearchAddPhotoLL.setVisibility(View.GONE);
        previewLL.setVisibility(View.VISIBLE);
        imagesRV.setVisibility(View.VISIBLE);
    }

    private void submitPostToTags() {
        String mTitle = editTitleET.getText().toString().trim();
        String mBody = editAddTextET.getText().toString().trim();
        String mLink = editLinkET.getText().toString().trim();

        if (mLink != null && mLink.length() > 0 && mLink.contains("http") && !mLink.contains("watch?v")) {
            Intent mIntent = new Intent(mActivity, NewPostsAddTagsActivity.class);
            if (mTitle != null && mTitle.length() > 0)
                mIntent.putExtra(Constants.POST_TITLE, mTitle);
            if (mBody != null && mBody.length() > 0)
                mIntent.putExtra(Constants.POST_BODY, mBody);
            mIntent.putExtra(Constants.POST_LINK, mLink);
            if (mSelectedImgUrl != null && mSelectedImgUrl.contains("http"))
                mIntent.putExtra(Constants.POST_PICTURE_URL, mSelectedImgUrl);
            else if (mImageURI != null)
                mIntent.putExtra(Constants.POST_PICTURE_URI, mImageURI.toString());
            mIntent.putExtra(Constants.POST_TYPE, getString(R.string.link_type));
            startActivity(mIntent);

        } else if (mLink != null && mLink.length() > 0 && mLink.contains("http") && mLink.contains("watch?v")) {
            Intent mIntent = new Intent(mActivity, NewPostsAddTagsActivity.class);
            if (mTitle != null && mTitle.length() > 0)
                mIntent.putExtra(Constants.POST_TITLE, mTitle);
            if (mBody != null && mBody.length() > 0)
                mIntent.putExtra(Constants.POST_BODY, mBody);

            mIntent.putExtra(Constants.POST_LINK, mLink);

            if (mSelectedImgUrl != null && mSelectedImgUrl.contains("http") && mLink.contains("watch?v"))
                mIntent.putExtra(Constants.POST_PICTURE_URL, mSelectedImgUrl);
            else if (mImageURI != null)
                mIntent.putExtra(Constants.POST_PICTURE_URI, mImageURI.toString());

            mIntent.putExtra(Constants.POST_TYPE, getString(R.string.video_type));
            startActivity(mIntent);
        } else if (mLink.length() == 0 && mBitmapImage != null) {
            Intent mIntent = new Intent(mActivity, NewPostsAddTagsActivity.class);
            if (mTitle != null && mTitle.length() > 0)
                mIntent.putExtra(Constants.POST_TITLE, mTitle);
            if (mBody != null && mBody.length() > 0)
                mIntent.putExtra(Constants.POST_BODY, mBody);

            mIntent.putExtra(Constants.POST_PICTURE_URI, mImageURI.toString());

            mIntent.putExtra(Constants.POST_TYPE, getString(R.string.picture_type));
            startActivity(mIntent);
        } else if (mLink.length() == 0 && mBitmapImage == null) {
            Intent mIntent = new Intent(mActivity, NewPostsAddTagsActivity.class);
            if (mTitle != null && mTitle.length() > 0)
                mIntent.putExtra(Constants.POST_TITLE, mTitle);
            if (mBody != null && mBody.length() > 0)
                mIntent.putExtra(Constants.POST_BODY, mBody);
            mIntent.putExtra(Constants.POST_TYPE, getString(R.string.text_type));
            startActivity(mIntent);
        } else {
            Intent mIntent = new Intent(mActivity, NewPostsAddTagsActivity.class);
            if (mTitle != null && mTitle.length() > 0)
                mIntent.putExtra(Constants.POST_TITLE, mTitle);
            if (mBody != null && mBody.length() > 0)
                mIntent.putExtra(Constants.POST_BODY, mBody);
            mIntent.putExtra(Constants.POST_TYPE, getString(R.string.text_type));
            startActivity(mIntent);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        InputStream stream = null;
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            try {
                InputStream inputStream = null;
                Uri mImageUri = null;
                try {
                    mImageUri = data.getData();
                    inputStream = mActivity.getContentResolver().openInputStream(data.getData());
                    mBitmapImage = BitmapFactory.decodeStream(inputStream);
                    try {
                        mBitmapImage = getCorrectlyOrientedImage(mActivity, data.getData());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                imagesRV.setVisibility(View.VISIBLE);

                //Convert Bitmap To ByteArray

                ByteArrayOutputStream bStream = new ByteArrayOutputStream();
                mBitmapImage.compress(Bitmap.CompressFormat.PNG, 100, bStream);
                byte[] byteArray = bStream.toByteArray();


                //String imagePath = RealPathUtils.getRealPathFromURI_API19(mActivity, data.getData());
                //Log.e(TAG,"====Path==="+imagePath);
                PostImageModel mModel = new PostImageModel();
                mModel.setByteArray(byteArray);
                mModel.setmImageUri(mImageUri);
                //mModel.setStrImagePath(imagePath);
                mPostImgArrayList.add(mModel);
                mPostImagesAdapter.notifyDataSetChanged();
            } finally {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        if (resultCode == 232) {
            VideoItem mVideoItem = (VideoItem) data.getSerializableExtra("MODEL");
            if (editTitleET.getText().toString().trim().length() == 0)
                editTitleET.setText(mVideoItem.getTitle());
            if (editAddTextET.getText().toString().trim().length() == 0)
                editAddTextET.setText(mVideoItem.getDescription());

            pasteUrlYouTubeLink();

            mSelectedImgUrl = mVideoItem.getThumbnailURL();
            editLinkET.setText(mVideoItem.getVideoLink());

            PostImageModel mPostImageModel = new PostImageModel();
            mPostImageModel.setStrImageUrl(mSelectedImgUrl);
            mPostImgArrayList.add(mPostImageModel);
            mPostImagesAdapter.notifyDataSetChanged();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /*Get Preview*/
    private void getLinkPreView() {
        editLinkET.setOnCutCopyPasteListener(new CutCopyPasteTextView.OnCutCopyPasteListener() {
            @Override
            public void onCut() {
                Log.e(TAG, "onCut()");
            }

            @Override
            public void onCopy() {
                Log.e(TAG, "onCopy()");
            }

            @Override
            public void onPaste() {
                Log.e(TAG, "onPaste()");
                Utilities.hideKeyBoad(mActivity, editLinkET);
                pasteUrlYouTubeLink();
                textCrawler.makePreview(linkPreviewCallback, editLinkET.getText().toString().trim());
            }
        });


        editLinkET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                  /* Write your logic here that will be executed when user taps next button */
                    pasteUrlYouTubeLink();
                    if (!editLinkET.getText().toString().trim().contains("http")) {
                        editLinkET.setText("http://" + editLinkET.getText().toString().trim());
                    }
                    textCrawler.makePreview(linkPreviewCallback, editLinkET.getText().toString().trim());
                    handled = true;
                }
                return handled;
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        textCrawler.cancel();
    }

    private void openGallerWithPermission() {
        if (checkPermission())
            openGallery();
        else
            requestPermission();
    }

    private void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORConAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    openGallery();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    // requestPermission();
                    Log.e(TAG, "==permission denied==");
                }
                break;


        }
    }


}
