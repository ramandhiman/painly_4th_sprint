package com.painly.com.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.R;
import com.painly.com.adapters.ConditionssAdapter;
import com.painly.com.beans.Conditions;
import com.painly.com.font.ButtonGothamMedium;
import com.painly.com.font.EditTextArialRegular;
import com.painly.com.interfaces.SelectConditions;
import com.painly.com.utils.AlertDialogManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUp3Activity extends AppCompatActivity {
    String TAG = SignUp3Activity.this.getClass().getSimpleName();
    Activity mActivity = SignUp3Activity.this;
    String strUserName = "", strName = "", strEmail = "", strPassword = "", strBio = "";
    boolean isMachineLearningEnabled;

    @BindView(R.id.imgBackLL)
    LinearLayout imgBackLL;
    @BindView(R.id.editSearchET)
    EditTextArialRegular editSearchET;
    @BindView(R.id.conditionsRV)
    RecyclerView conditionsRV;
    @BindView(R.id.btnSignUP)
    ButtonGothamMedium btnSignUP;

    ConditionssAdapter mConditionssAdapter;
    ArrayList<Conditions> mConditionsAL = new ArrayList<Conditions>();
    ArrayList<Conditions> filteredList = new ArrayList<Conditions>();
    ArrayList<Conditions> mSelectedTagsArrayList = new ArrayList<Conditions>();
    DatabaseReference mRootRefrence;


    SelectConditions mSelectConditions = new SelectConditions() {
        @Override
        public void mSelectConditions(Boolean mBoolean, ArrayList<Conditions> mArrayList) {
            mSelectedTagsArrayList = mArrayList;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_signup_step3);
        ButterKnife.bind(this);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        if (getIntent() != null) {
            strUserName = getIntent().getStringExtra("USERNAME");
            strName = getIntent().getStringExtra("NAME");
            strEmail = getIntent().getStringExtra("EMAIL");
            strPassword = getIntent().getStringExtra("PASSWORD");
            strBio = getIntent().getStringExtra("BIO");
            isMachineLearningEnabled = getIntent().getBooleanExtra("M_LANGUAGE", false);
        }
        /*Get conditions*/
        getConditionsData();
        setSearch();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @OnClick({R.id.imgBackLL, R.id.btnSignUP})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackLL:
                onBackPressed();
                break;
            case R.id.btnSignUP:
                setNextClick();
                break;
        }
    }

    private void setNextClick() {
        if (mSelectedTagsArrayList.size() > 0) {
            Intent mIntent = new Intent(mActivity, SignUp4Activity.class);
            mIntent.putExtra("LIST", mSelectedTagsArrayList);
            mIntent.putExtra("USERNAME", strUserName.toLowerCase());
            mIntent.putExtra("NAME", strName);
            mIntent.putExtra("EMAIL", strEmail);
            mIntent.putExtra("PASSWORD", strPassword);
            mIntent.putExtra("BIO", strBio);
            mIntent.putExtra("M_LANGUAGE", isMachineLearningEnabled);
//            mIntent.putExtra("GENDER", strGender);
            startActivity(mIntent);
        } else {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_select_atleast_one_condition));
        }
    }

    private void setSearch() {
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable mEditable) {
                //after the change calling the method and passing the search input
                filter(mEditable.toString());
            }
        });
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<Conditions> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (Conditions s : mConditionsAL) {
            //if the existing elements contains the search input
            if (s.getName().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(s);
            }
        }

        //calling a method of the adapter class and passing the filtered list
        if (filterdNames != null && filterdNames.size() > 0)
            mConditionssAdapter.filterList(filterdNames);
    }


    private void getConditionsData() {
        AlertDialogManager.showProgressDialog(mActivity);
        mRootRefrence.child("conditions").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                AlertDialogManager.hideProgressDialog();
                for (DataSnapshot formsSnapshot : dataSnapshot.getChildren()) {
                    String conditionID = formsSnapshot.getKey();
                    final Conditions mConditions = formsSnapshot.getValue(Conditions.class);
                    mConditions.setConditionID(conditionID);
                    mConditionsAL.add(mConditions);
                }

                Collections.sort(mConditionsAL, new Comparator<Conditions>() {
                    @Override
                    public int compare(Conditions item, Conditions t1) {
                        String s1 = item.getName();
                        String s2 = t1.getName();
                        return s1.compareToIgnoreCase(s2);
                    }

                });

                setAdapter();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "Error::" + databaseError.toString());
            }
        });
    }

    private void setAdapter() {
        mConditionssAdapter = new ConditionssAdapter(mActivity, mConditionsAL, mSelectConditions);
        conditionsRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        conditionsRV.setLayoutManager(mLayoutManager);
        conditionsRV.setItemAnimator(new DefaultItemAnimator());
        conditionsRV.setAdapter(mConditionssAdapter);

    }
}
