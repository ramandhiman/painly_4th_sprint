package com.painly.com.chat;

import com.painly.com.utils.Utilities;

import java.io.Serializable;

/**
 * Created by android-da on 6/12/18.
 */

public class MessageUserR implements Serializable,Comparable<MessageUserR> {
    String mMessagesID = "";
    String uID = "";
    boolean hasUnread = false;
    String image = "";
    String name = "";
    String strLastMessageText = "";
    String strLastMessageTimeStamp = "";


    public boolean isHasUnread() {
        return hasUnread;
    }

    public void setHasUnread(boolean hasUnread) {
        this.hasUnread = hasUnread;
    }

    public String getImage() {
        return image;
    }

    public String getmMessagesID() {
        return mMessagesID;
    }

    public void setmMessagesID(String mMessagesID) {
        this.mMessagesID = mMessagesID;
    }

    public String getStrLastMessageText() {
        return strLastMessageText;
    }

    public void setStrLastMessageText(String strLastMessageText) {
        this.strLastMessageText = strLastMessageText;
    }

    public String getStrLastMessageTimeStamp() {
        return strLastMessageTimeStamp;
    }

    public void setStrLastMessageTimeStamp(String strLastMessageTimeStamp) {
        this.strLastMessageTimeStamp = strLastMessageTimeStamp;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getuID() {
        return uID;
    }

    public void setuID(String uID) {
        this.uID = uID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(MessageUserR f) {
        if (Utilities.getTimeInLong(strLastMessageTimeStamp) > Utilities.getTimeInLong(f.strLastMessageTimeStamp)) {
            return 1;
        }
        else if (Utilities.getTimeInLong(strLastMessageTimeStamp) < Utilities.getTimeInLong(f.strLastMessageTimeStamp)) {
            return -1;
        }
        else {
            return 0;
        }

    }
}
