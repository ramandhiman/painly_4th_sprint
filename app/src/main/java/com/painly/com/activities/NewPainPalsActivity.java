package com.painly.com.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.BaseActivity;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.adapters.NewPainPalsAdapter;
import com.painly.com.beans.User;
import com.painly.com.beans.UserConditions;
import com.painly.com.font.TextViewArialBold;
import com.painly.com.interfaces.PainPalsSelectionInterface;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;
import com.painly.com.utils.Utilities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewPainPalsActivity extends BaseActivity {
    Activity mActivity = NewPainPalsActivity.this;
    String TAG = NewPainPalsActivity.this.getClass().getSimpleName();

    @BindView(R.id.llBackLL)
    LinearLayout llBackLL;
    @BindView(R.id.llRefeshLL)
    LinearLayout llRefeshLL;
    @BindView(R.id.recyclerViewRV)
    RecyclerView recyclerViewRV;
    @BindView(R.id.txtSaveTV)
    TextViewArialBold txtSaveTV;

    ArrayList<User> mPainPalsArrayList = new ArrayList<>();
    ArrayList<User> mAddToFavoritePainPalAL = new ArrayList<>();
    NewPainPalsAdapter mNewPainPalsAdapter;

    DatabaseReference mRootRefrence;

    String strUserID = "";

    String strType = "";


    PainPalsSelectionInterface mPainPalsSelectionInterface = new PainPalsSelectionInterface() {
        @Override
        public void getPainPalsAsFavorite(ArrayList<User> mArrayList) {
            mAddToFavoritePainPalAL = mArrayList;
            Log.e(TAG, "===Total User===" + mArrayList.size());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_pain_pals);
        ButterKnife.bind(this);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        strUserID = PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "");
        Log.e(TAG, "==UserID==" + strUserID);

        if (getIntent() != null && getIntent().getStringExtra("TYPE") != null) {
            strType = getIntent().getStringExtra("TYPE");
        }
        setAdapter(mPainPalsArrayList);

        getPainPalsReloads();
    }

    private void getPainPalsReloads() {
        if (Utilities.isNetworkAvailable(mActivity))
            getUserConditions();
        else
            Toast.makeText(mActivity, getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
    }

    private void getUserConditions() {
        showProgressDialog(mActivity);

        mRootRefrence.child("users").child(strUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    UserConditions mUserConditions = dataSnapshot.getValue(UserConditions.class);
                    if (mUserConditions.getConditions() != null) {
                        if (mUserConditions != null) {

                            if (!mUserConditions.getConditions().isEmpty()){
                                int rnd = new Random().nextInt(mUserConditions.getConditions().size());

                                String key = (String) mUserConditions.getConditions().keySet().toArray()[rnd];
                                String value = mUserConditions.getConditions().get(key);

                                Log.e(TAG,"===KEY===" + key + "====VALUE===="+value);

                                getAllUsersAsPainPals(key,value);

                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "====Error+===" + databaseError.toString());
                hideProgressDialog();
            }
        });
    }

    private void getAllUsersAsPainPals(String key,String value) {
        Log.e(TAG,"==Key=="+key + "==Value=="+value);
        Query mQuery = mRootRefrence.child("users").orderByChild("conditions/" + key).startAt(value).endAt(value + "\uf8ff").limitToLast(16);
        mQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot mData : dataSnapshot.getChildren()) {
                    User mUser = mData.getValue(User.class);
                    if (mUser.getDetails() != null) {
                        mUser.getDetails().setPlaceholderImage(PainlyApplication.getInstance().getRandom());
                        if (contains(mPainPalsArrayList, mUser.getDetails().getUid()) == false) {
                            mPainPalsArrayList.add(mUser);
                        }
                    }

                }

                hideProgressDialog();
                Log.e(TAG, "===PainPals Users Size===" + mPainPalsArrayList.size());
                setPainPalsAdapter();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideProgressDialog();
                Log.e(TAG, "===ERROR===" + databaseError.toString());
            }
        });
    }

//    private void getAllUsersAsPainPals(final ArrayList<String> mConditionsAL) {
//        mPainPalsArrayList.clear();
//        for (int i = 0; i < mConditionsAL.size(); i++) {
//            Query mQuery = mRootRefrence.child("users").orderByChild("conditions/" + mConditionsAL.get(i));
//            final int finalI = i;
//            mQuery.addListenerForSingleValueEvent(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//
//                    for (DataSnapshot mData : dataSnapshot.getChildren()) {
//                        User mUser = mData.getValue(User.class);
//                        if (mUser.getDetails() != null) {
//                            mUser.getDetails().setPlaceholderImage(PainlyApplication.getInstance().getRandom());
//                            if (contains(mPainPalsArrayList, mUser.getDetails().getUid()) == false) {
//                                mPainPalsArrayList.add(mUser);
//                                //mNewPainPalsAdapter.notifyDataSetChanged();
//                            }
//                        }
//
//                    }
//
//                    if (finalI == mConditionsAL.size() - 1) {
//                        hideProgressDialog();
//                        Log.e(TAG, "===PainPals Users Size===" + mPainPalsArrayList.size());
//                        setPainPalsAdapter();
//                    }
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//                    hideProgressDialog();
//                    Log.e(TAG, "===ERROR===" + databaseError.toString());
//                }
//            });
//
//        }
//    }

    private void setPainPalsAdapter() {
        for (int i = 0; i < mPainPalsArrayList.size(); i++) {
            if (mPainPalsArrayList.get(i).getDetails().getUid().equals(strUserID)) {
                mPainPalsArrayList.remove(i);
            }
        }

        ArrayList<User> mFinalPainPalsUserAL = new ArrayList<>();

        if (mPainPalsArrayList.size() <= 16) {
            mFinalPainPalsUserAL.clear();
            for (int i = 0; i < mPainPalsArrayList.size(); i++) {
                mFinalPainPalsUserAL.add(mPainPalsArrayList.get(i));
            }
            setAdapter(mFinalPainPalsUserAL);
        } else if (mPainPalsArrayList.size() > 16) {
            mFinalPainPalsUserAL.clear();
            for (int i = 0; i < mPainPalsArrayList.size(); i++) {
                User mUser = getRandomUser(mPainPalsArrayList);
                if (contains(mFinalPainPalsUserAL, mUser.getDetails().getUid()) == false) {
                    if (mFinalPainPalsUserAL.size() < 16) {
                        mFinalPainPalsUserAL.add(mUser);
                    } else {
                        break;
                    }
                }
            }
            setAdapter(mFinalPainPalsUserAL);
        }
    }


    private User getRandomUser(ArrayList<User> mArrayList) {
        int rnd = new Random().nextInt(mArrayList.size());

        return mArrayList.get(rnd);
    }

    boolean contains(ArrayList<User> list, String mUserID) {
        for (User item : list) {
            if (item.getDetails().getUid().equals(mUserID)) {
                return true;
            }
        }
        return false;
    }

    private void setAdapter(ArrayList<User> mArrayList) {
        mNewPainPalsAdapter = new NewPainPalsAdapter(mActivity, mArrayList, mPainPalsSelectionInterface);
        recyclerViewRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        recyclerViewRV.setLayoutManager(mLayoutManager);
        recyclerViewRV.setAdapter(mNewPainPalsAdapter);
    }

    @OnClick({R.id.llBackLL, R.id.llRefeshLL, R.id.txtSaveTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llBackLL:
                onBackPressed();
                break;
            case R.id.llRefeshLL:
                getPainPalsReloads();
                break;
            case R.id.txtSaveTV:
                showProgressDialog(mActivity);
                saveToFavorites();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void saveToFavorites() {
        if (strType.equals(Constants.FROM_PROFILE)) {
            HashMap<String, String> mFavoriteHM = new HashMap<String, String>();

            for (int i = 0; i < mAddToFavoritePainPalAL.size(); i++) {
                if (mAddToFavoritePainPalAL.get(i).getDetails() != null && mAddToFavoritePainPalAL.get(i).getDetails().getUid() != null) {
                    if (mAddToFavoritePainPalAL.get(i).isSelected() == true) {
                        mFavoriteHM.put(mPainPalsArrayList.get(i).getDetails().getUid(), mPainPalsArrayList.get(i).getDetails().getUid());
                    }
                }

            }
            mRootRefrence.child("users").child(strUserID).child("favorites").setValue(mFavoriteHM);
            hideProgressDialog();
            finish();
        } else if (strType.equals(Constants.FROM_SIGNUP)) {
            HashMap<String, String> mFavoriteHM = new HashMap<String, String>();

            for (int i = 0; i < mAddToFavoritePainPalAL.size(); i++) {
                if (mAddToFavoritePainPalAL.get(i).getDetails() != null && mAddToFavoritePainPalAL.get(i).getDetails().getUid() != null) {
                    if (mAddToFavoritePainPalAL.get(i).isSelected() == true) {
                        mFavoriteHM.put(mPainPalsArrayList.get(i).getDetails().getUid(), mPainPalsArrayList.get(i).getDetails().getUid());
                    }
                }

            }
            mRootRefrence.child("users").child(strUserID).child("favorites").setValue(mFavoriteHM);
            hideProgressDialog();
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

}
