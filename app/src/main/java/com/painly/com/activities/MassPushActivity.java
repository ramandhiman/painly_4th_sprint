package com.painly.com.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.font.ButtonArialBold;
import com.painly.com.font.EditTextArialRegular;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Constants;
import com.painly.com.utils.Utilities;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MassPushActivity extends AppCompatActivity {
    String TAG = MassPushActivity.this.getClass().getSimpleName();
    Activity mActivity = MassPushActivity.this;
    @BindView(R.id.imgBackLL)
    LinearLayout imgBackLL;
    @BindView(R.id.editAddMessageET)
    EditTextArialRegular editAddMessageET;
    @BindView(R.id.btnMassPushB)
    ButtonArialBold btnMassPushB;

    Intent startServiceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mass_push);
        ButterKnife.bind(this);

    }

    @OnClick({R.id.imgBackLL, R.id.btnMassPushB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackLL:
                onBackPressed();
                break;
            case R.id.btnMassPushB:
                validateMassPush();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void validateMassPush() {
        if (editAddMessageET.getText().toString().trim().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_mass_push_message));
        } else {
            /*Execute Api*/
            if (Utilities.isNetworkAvailable(mActivity)) {
                executeNotificationAPI();
                finish();

            } else {
                Toast.makeText(mActivity, getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void executeNotificationAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        String strMessege = editAddMessageET.getText().toString().trim();
        String strAPIURL = Constants.MASS_PUSH_SEND_ALL + "message=" + strMessege + "&senderAuthID=" + Constants.USER_ID;
        strAPIURL = strAPIURL.replace(" ", "%20");
        Log.e(TAG, "=====API URL====" + strAPIURL);
        StringRequest request = new StringRequest(Request.Method.GET, strAPIURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "===RESPONSE===" + response);
                try {
                    Log.e(TAG, "===SUCESSFULL NOTIFICATIONS===");

                    AlertDialogManager.hideProgressDialog();
                    finish();
                    Toast.makeText(mActivity, "Mass Push Sucessfully Send!", Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "***Error**" + error.toString());
                // Toast.makeText(mActivity,"Server Not Respond!",Toast.LENGTH_SHORT).show();
                AlertDialogManager.hideProgressDialog();
                finish();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }

        };
        PainlyApplication.getInstance().addToRequestQueue(request);

    }


}
