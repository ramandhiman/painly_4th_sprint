package com.painly.com.interfaces;

import com.painly.com.beans.Conditions;

/**
 * Created by android-da on 5/25/18.
 */

public interface SelectedTags {
    public void setSelectedTag(Boolean mBoolean ,  Conditions mConditions);
}
