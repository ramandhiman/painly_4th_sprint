package com.painly.com.beans;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by android-raman on 2/5/18.
 */

public class Conditions implements Serializable {
    String conditionID = "";
    boolean added;
    String name;
    String type;
    boolean isSelected;

    SponsorshipModel sponsorship;



    HashMap<String, String> alternates;
    HashMap<String, String> sources;

    public SponsorshipModel getSponsorship() {
        return sponsorship;
    }

    public void setSponsorship(SponsorshipModel sponsorship) {
        this.sponsorship = sponsorship;
    }

    public HashMap<String, String> getSources() {
        return sources;
    }

    public void setSources(HashMap<String, String> sources) {
        this.sources = sources;
    }



    public HashMap<String, String> getAlternates() {
        return alternates;
    }

    public void setAlternates(HashMap<String, String> alternates) {
        this.alternates = alternates;
    }

    public String getConditionID() {
        return conditionID;
    }

    public void setConditionID(String conditionID) {
        this.conditionID = conditionID;
    }

    public boolean isAdded() {
        return added;
    }

    public void setAdded(boolean added) {
        this.added = added;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
