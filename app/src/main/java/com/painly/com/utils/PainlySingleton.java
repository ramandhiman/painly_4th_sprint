package com.painly.com.utils;

import com.painly.com.beans.Conditions;
import com.painly.com.beans.FormsModel;
import com.painly.com.beans.NotificationsModel;
import com.painly.com.beans.User;
import com.painly.com.beans.UserDetails;
import com.painly.com.chat.MessageUserR;
import com.painly.com.chathead.ChatHeadModel;

import java.util.ArrayList;

/**
 * Created by android-da on 5/19/18.
 */

public class PainlySingleton {

    private static PainlySingleton ourInstance;

    ArrayList<ChatHeadModel> mChatHeadArrayList = new ArrayList<ChatHeadModel>();
    ArrayList<Conditions> mConditionsArrayList = new ArrayList<>();
    ArrayList<User> mAllUsersArrayList = new ArrayList<User>();

    private PainlySingleton() {

    }


    public static PainlySingleton getInstance() {
        if (ourInstance == null) {
            ourInstance = new PainlySingleton();
        }
        return ourInstance;

    }

    public ArrayList<User> getAllUsersArrayList() {
        return mAllUsersArrayList;
    }

    public ArrayList<ChatHeadModel> getmChatHeadArrayList() {
        return mChatHeadArrayList;
    }


    public ArrayList<Conditions> getmConditionsArrayList() {
        return mConditionsArrayList;
    }
}
