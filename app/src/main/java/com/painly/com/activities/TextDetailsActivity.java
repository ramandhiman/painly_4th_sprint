package com.painly.com.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.BaseActivity;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.adapters.CommentsAdapter;
import com.painly.com.adapters.DetailsCondtionsAdapter;
import com.painly.com.beans.Comments;
import com.painly.com.beans.Conditions;
import com.painly.com.beans.FormsModel;
import com.painly.com.beans.UserDetails;
import com.painly.com.font.EditTextArialRegular;
import com.painly.com.font.TextViewArialBold;
import com.painly.com.font.TextViewArialRegular;
import com.painly.com.font.TextViewGothamMedium;
import com.painly.com.interfaces.DetailsConditionsClick;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;
import com.painly.com.utils.Utilities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class TextDetailsActivity extends BaseActivity {
    Activity mActivity = TextDetailsActivity.this;
    String TAG = TextDetailsActivity.this.getClass().getSimpleName();


    FormsModel mFormsModel;

    DatabaseReference mRootRefrence;
    ArrayList<Comments> mCommentArrayList = new ArrayList<Comments>();
    ArrayList<Conditions> mConditionsAL = new ArrayList<Conditions>();
    ArrayList<Conditions> mFormConditionsAL = new ArrayList<Conditions>();
    Collection<String> mConditionKeys = new ArrayList<String>();
    DetailsCondtionsAdapter mDetailsCondtionsAdapter;
    CommentsAdapter mCommentsAdapter;


    @BindView(R.id.mConditionsRV)
    RecyclerView mConditionsRV;
    @BindView(R.id.profileRIV)
    CircleImageView profileRIV;
    @BindView(R.id.txtUserNameTV)
    TextViewGothamMedium txtUserNameTV;
    @BindView(R.id.txtUserDescriptionTV)
    TextView txtUserDescriptionTV;
    @BindView(R.id.txtForumTypeTV)
    TextView txtForumTypeTV;
    @BindView(R.id.txtDateTimeTV)
    TextView txtDateTimeTV;
    @BindView(R.id.txtPostTitleTV)
    TextViewArialBold txtPostTitleTV;
    @BindView(R.id.txtBodyDescriptionTV)
    TextViewArialRegular txtBodyDescriptionTV;
    @BindView(R.id.commentsRV)
    RecyclerView commentsRV;
    @BindView(R.id.imgCommentIV)
    ImageView imgCommentIV;
    @BindView(R.id.bottomLayoutLL)
    LinearLayout bottomLayoutLL;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.imgShareIV)
    ImageView imgShareIV;
    @BindView(R.id.editCommentET)
    EditTextArialRegular editCommentET;
    @BindView(R.id.imgDeleteIV)
    ImageView imgDeleteIV;

    String strRecieverUID = "";
    String strRecieverToken = "";
    String strRecieverDeviceType = "";
    String mFormUID = "";
    String strUserName;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.imgMoreMenuIV)
    ImageView imgMoreMenuIV;


    DetailsConditionsClick mDetailsConditionsClick = new DetailsConditionsClick() {
        @Override
        public void getConditionsClick(Conditions mConditions) {
            Intent mIntent = new Intent();
            mIntent.putExtra("LIST", mConditions);
            setResult(393, mIntent);
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_details);
        ButterKnife.bind(this);


        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        strUserName = Constants.USER_NAME;

        if (getIntent() != null) {
            mFormUID = getIntent().getStringExtra("FORM_UID");
            getForumData();
        }

    }

    private void getForumData() {
        mRootRefrence.child("forums").child(mFormUID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String strFormKey = dataSnapshot.getKey();

                mFormsModel = dataSnapshot.getValue(FormsModel.class);

                if (strFormKey != null)
                    mFormsModel.setFormID(strFormKey);
                mConditionKeys = mFormsModel.getConditions().values();
                if (Constants.USER_ID.equals(mFormsModel.getOpUID())) {
                    imgDeleteIV.setVisibility(View.VISIBLE);
                } else {
                    imgDeleteIV.setVisibility(View.GONE);
                }
                setDataOnWidgets();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void gettingReciverData() {
        mRootRefrence.child("users").child(mFormsModel.getOpUID()).child("details").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserDetails mDetails = dataSnapshot.getValue(UserDetails.class);
                strRecieverUID = mDetails.getUid();
                strRecieverToken = mDetails.getPushToken();
                strRecieverDeviceType = mDetails.getDeviceType();
                Log.e(TAG, "======UID======" + strRecieverUID);
                Log.e(TAG, "======TOKEN======" + strRecieverToken);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setDataOnWidgets() {
        mProgressBar.setVisibility(View.VISIBLE);
        getConditions();
        try {
            if (mFormsModel.getTimestamp() instanceof String) {
                txtDateTimeTV.setText(Utilities.gettingTimeFormat(mFormsModel.getTimestamp().toString()));
            } else if (mFormsModel.getTimestamp() instanceof Long) {
                txtDateTimeTV.setText(Utilities.gettingLongToFormatedTime(Long.parseLong(mFormsModel.getTimestamp().toString())));
            } else if (mFormsModel.getTimestamp() instanceof Double) {
                txtDateTimeTV.setText(Utilities.gettingDoubleToFormatedTime(Double.parseDouble(mFormsModel.getTimestamp().toString())));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        txtForumTypeTV.setText(mFormsModel.getType());

        txtPostTitleTV.setText(mFormsModel.getTitle());
        if (mFormsModel.getBody().length() > 0) {
            txtBodyDescriptionTV.setText(mFormsModel.getBody());
        }
        mProgressBar.setVisibility(View.GONE);
        getFormUserDetails();
        gettingReciverData();
        getAllComments();
    }

    private void getFormUserDetails() {
        mRootRefrence.child("users").child(mFormsModel.getOpUID()).child("details").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserDetails mDetails = dataSnapshot.getValue(UserDetails.class);

                setFormUserDetails(mDetails);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setFormUserDetails(final UserDetails mDetails) {
        if (mDetails.getImage() != null && mDetails.getImage().contains("http")) {
            Glide.with(mActivity)
                    .load(mDetails.getImage())
                    .apply(RequestOptions.placeholderOf(PainlyApplication.getInstance().getRandom()).error(PainlyApplication.getInstance().getRandom()))
                    .into(profileRIV);
        } else {
            profileRIV.setImageResource(PainlyApplication.getInstance().getRandom());
        }


        if (mDetails.getUsername() != null) {
            txtUserNameTV.setText(mDetails.getUsername());
        }
        if (mDetails.getUsername() != null) {
            txtUserDescriptionTV.setText("@" + mDetails.getUsername());
        }


        profileRIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.USER_ID.equals(mDetails.getUid())) {
                    Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                    mIntent.putExtra("UID", mDetails.getUid());
                    mIntent.putExtra("TYPE", Constants.CURRENT);
                    mActivity.startActivity(mIntent);
                } else {
                    Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                    mIntent.putExtra("UID", mDetails.getUid());
                    mIntent.putExtra("TYPE", Constants.ANOTHER);
                    mActivity.startActivity(mIntent);
                }
            }
        });
    }

    private void getConditions() {
        mConditionsAL.clear();
        mFormConditionsAL.clear();
        mRootRefrence.child("conditions").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot conditionsSnapshot : dataSnapshot.getChildren()) {
                    String mConditionKey = conditionsSnapshot.getKey();
                    final Conditions mConditions = conditionsSnapshot.getValue(Conditions.class);
                    mConditions.setConditionID(mConditionKey);
                    mConditionsAL.add(mConditions);
                }

                Log.e(TAG, "*****CONDITIONS LIST SIZE*****" + mConditionsAL.size());

                if (mConditionKeys.size() > 0) {
                    for (int i = 0; i < mConditionKeys.toArray().length; i++) {
                        for (int j = 0; j < mConditionsAL.size(); j++) {
                            if (mConditionKeys.toArray()[i].toString().equals(mConditionsAL.get(j).getConditionID())) {
                                Conditions mConditions = mConditionsAL.get(j);
                                mFormConditionsAL.add(mConditions);
                            }
                        }


                    }
                }


                setConditionsAdapter();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "Error::" + databaseError.toString());
                Toast.makeText(mActivity, databaseError.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setConditionsAdapter() {
        mDetailsCondtionsAdapter = new DetailsCondtionsAdapter(mActivity, mFormConditionsAL, mDetailsConditionsClick);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        mConditionsRV.setLayoutManager(horizontalLayoutManager);
        mConditionsRV.setAdapter(mDetailsCondtionsAdapter);
        mDetailsCondtionsAdapter.notifyDataSetChanged();
    }

    private void getAllComments() {
        mProgressBar.setVisibility(View.VISIBLE);
        mRootRefrence.child("forums").child(mFormsModel.getFormID()).child("comments").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mProgressBar.setVisibility(View.GONE);
                //mActualCommentArrayList.clear();
                mCommentArrayList.clear();

                for (DataSnapshot formsSnapshot : dataSnapshot.getChildren()) {
                    String strCommentID = formsSnapshot.getKey();
                    final Comments mComments = formsSnapshot.getValue(Comments.class);
                    mComments.setmCommentID(strCommentID);
                    mCommentArrayList.add(mComments);
                }


                setAdapter();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mProgressBar.setVisibility(View.GONE);
                Toast.makeText(mActivity, databaseError.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }


    private void setAdapter() {
        mCommentsAdapter = new CommentsAdapter(mActivity, mCommentArrayList, mFormUID);
        commentsRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        commentsRV.setLayoutManager(mLayoutManager);
        commentsRV.setItemAnimator(new DefaultItemAnimator());
        commentsRV.setAdapter(mCommentsAdapter);
        mCommentsAdapter.notifyDataSetChanged();
        mProgressBar.setVisibility(View.GONE);
    }

    @OnClick({R.id.imgShareIV, R.id.imgCommentIV, R.id.imgDeleteIV, R.id.imgBackIV, R.id.imgMoreMenuIV, R.id.profileRIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgShareIV:
                Utilities.SharePainPalsPainly(mActivity, mFormsModel.getBody());
                break;
            case R.id.imgCommentIV:
                validate(view);
                break;
            case R.id.imgDeleteIV:
                forumDeleteDialog();
                break;
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.imgMoreMenuIV:
                showPictureialog();
                break;
            case R.id.profileRIV:
                //showMenu(view);

                break;
        }
    }


    private void showPictureialog() {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        // Setting dialogview
        Window window = dialog.getWindow();
        window.setGravity(Gravity.RIGHT | Gravity.TOP);

        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_menu_);
        dialog.setCancelable(true);

        TextView txtSharePostTV = (TextView) dialog.findViewById(R.id.txtSharePostTV);
        TextView txtGoToProfileTV = (TextView) dialog.findViewById(R.id.txtGoToProfileTV);
        TextView txtOpenInBrowserTV = (TextView) dialog.findViewById(R.id.txtOpenInBrowserTV);
        final TextView txtSaveToLibraryTV = (TextView) dialog.findViewById(R.id.txtSaveToLibraryTV);
        TextView txtDeletePostTV = (TextView) dialog.findViewById(R.id.txtDeletePostTV);
        TextView txtCancelTV = (TextView) dialog.findViewById(R.id.txtCancelTV);

         /*If Link Type Post then Show Open Browser Option*/
        if (mFormsModel.getType().toLowerCase().equalsIgnoreCase(getString(R.string.link_type))) {
            txtOpenInBrowserTV.setVisibility(View.VISIBLE);
        } else {
            txtOpenInBrowserTV.setVisibility(View.GONE);
        }

        /*If itSelf User Then Show Delete Option*/
        if (FirebaseAuth.getInstance().getCurrentUser().getUid().equals(mFormsModel.getOpUID())) {
            txtDeletePostTV.setVisibility(View.VISIBLE);
        } else {
            txtDeletePostTV.setVisibility(View.GONE);
        }

        /*If Post is Not Saved in Library then Show Saved Library Options*/
        mRootRefrence.child("users").child(FirebaseAuth.getInstance().getUid()).child("saved").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    if (dataSnapshot.hasChild(mFormUID)) {
                        txtSaveToLibraryTV.setVisibility(View.GONE);
                    } else {
                        txtSaveToLibraryTV.setVisibility(View.VISIBLE);
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "===ERROR==" + databaseError.toString());
            }
        });


        txtSharePostTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Utilities.SharePainPalsPainly(mActivity, mFormsModel.getTitle() + " :: " + mFormsModel.getBody());
            }
        });

        txtGoToProfileTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                profilePicClick();
            }
        });

        txtSaveToLibraryTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                saveToUserLibrary();
            }
        });


        txtDeletePostTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                deleteForum();
            }
        });

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    /* public void showMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(this);// to implement on click event on items of menu
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_youtube_post_details, popup.getMenu());
        final Menu popupMenu = popup.getMenu();

        popupMenu.findItem(R.id.actionOpenInBrowser).setVisible(false);
        this.invalidateOptionsMenu();

         *//*If itSelf User Then Show Delete Option*//*
        if (FirebaseAuth.getInstance().getCurrentUser().getUid().equals(mFormsModel.getOpUID())){
            popupMenu.findItem(R.id.actionDeletePost).setVisible(true);
            this.invalidateOptionsMenu();
        }else{
            popupMenu.findItem(R.id.actionDeletePost).setVisible(false);
            this.invalidateOptionsMenu();
        }

           *//*If Post is Not Saved in Library then Show Saved Library Options*//*
        mRootRefrence.child("users").child(FirebaseAuth.getInstance().getUid()).child("saved").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(mFormUID)) {
                    // run some code
                    popupMenu.findItem(R.id.actionSaveToLibrary).setVisible(false);
                } else {
                    popupMenu.findItem(R.id.actionOpenInBrowser).setVisible(true);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "===ERROR==" + databaseError.toString());
            }
        });


        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.actionSharePost:
                if (mFormsModel.getType().toLowerCase().equalsIgnoreCase(getString(R.string.text_type))) {
                    Utilities.SharePainPalsPainly(mActivity, mFormsModel.getTitle() + " :: " + mFormsModel.getBody());
                }
                return true;
            case R.id.actionGoToProfile:
                profilePicClick();
                return true;
            case R.id.actionSaveToLibrary:
                 *//*click Listner*//*
                saveToUserLibrary();
                return true;
            case R.id.actionOpenInBrowser:
                return true;
            case R.id.actionDeletePost:
                deleteForum();
                return true;
        }
        return false;
    }
*/
    private void saveToUserLibrary() {
        HashMap<String, String> mSavedLibraryFormHM = new HashMap<>();
        mSavedLibraryFormHM.put("description", mFormsModel.getBody());
        mSavedLibraryFormHM.put("title", mFormsModel.getTitle());
        mSavedLibraryFormHM.put("type", "post");
        mRootRefrence.child("users").child(FirebaseAuth.getInstance().getUid()).child("saved").child(mFormUID).setValue(mSavedLibraryFormHM);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void profilePicClick() {
        if (PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "").equals(mFormsModel.getOpUID())) {
            Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
            mIntent.putExtra("UID", mFormsModel.getOpUID());
            mIntent.putExtra("TYPE", Constants.CURRENT);
            mActivity.startActivity(mIntent);
        } else {
            Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
            mIntent.putExtra("UID", mFormsModel.getOpUID());
            mIntent.putExtra("TYPE", Constants.ANOTHER);
            mActivity.startActivity(mIntent);
        }
    }

    private void validate(View mView) {
        if (editCommentET.getText().toString().trim().equals("")) {
            Toast.makeText(mActivity, getResources().getString(R.string.please_enter_comment), Toast.LENGTH_LONG).show();
        } else {
            putCommentOnForum(mView);
        }
    }

    private void putCommentOnForum(View mView) {
          /*ADDING DATA IN FIREBASE*/
        try {
            HashMap data = new HashMap<>();
            Log.e(TAG, "Commenter UserID" + PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, ""));
            data.put("commenterUID", PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, ""));
            data.put("text", editCommentET.getText().toString().trim());
            data.put("timestamp", Utilities.getCurrentTimeStampComment());

            String mCommentKey = mRootRefrence.child("forums").child(mFormsModel.getFormID()).child("comments").push().getKey();

            mRootRefrence.child("forums").child(mFormsModel.getFormID()).child("comments").child(mCommentKey).setValue(data);
            editCommentET.setText("");
            Utilities.hideKeyBoad(mActivity, mView);

            if (!mFormsModel.getOpUID().equals(Constants.USER_ID)) {
              /*Update User Notifications Notifications*/
                createNodePutNotificationsData();
            }
        } catch (Exception mE) {
            Log.e(TAG, "=====ERROR====" + mE);
        }
    }

    private void createNodePutNotificationsData() {
        HashMap mNotificationHM = new HashMap();
        mNotificationHM.put("commentID", mFormsModel.getFormID());
        mNotificationHM.put("commentType", getString(R.string.text_type));
        mNotificationHM.put("isNew", true);
        mNotificationHM.put("message", strUserName + " " + Constants.COMMENT_MESSAGE + " " + mFormsModel.getTitle());
        mNotificationHM.put("recieverAuthID", strRecieverUID);
        mNotificationHM.put("senderAuthID", Constants.USER_ID);
        mNotificationHM.put("type", Constants.COMMENT);

        mRootRefrence.child("users").child(strRecieverUID).child("notifications").push().setValue(mNotificationHM);

        if (strRecieverDeviceType.equals("android")) {
            executeApiForAndroid();
        } else {
            executeApiForiOS();
        }
    }


    private void executeApiForAndroid() {//uID,notiType,message,tokenString
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, Constants.PUSH_NOTIFICATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "===RESPONSE===" + response);
                try {
                    Log.e(TAG, "===SUCESSFULL NOTIFICATIONS===");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "***Error**" + error.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("tokenString", strRecieverToken);
                params.put("message", strUserName + " " + Constants.COMMENT_MESSAGE + " " + mFormsModel.getTitle());
                params.put("notiType", Constants.COMMENT);
                params.put("uID", Constants.USER_ID);
                params.put("commentType", getString(R.string.text_type));
                params.put("forumID", mFormsModel.getFormID());
                if (strRecieverDeviceType.equals("android")) {
                    params.put("deviceType", "android");
                } else {
                    params.put("deviceType", "ios");
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }

        };
        PainlyApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }


    //Execute According To IOs Style
    private void executeApiForiOS() {
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, Constants.PAINLY_IOS_NOTIFICATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "===RESPONSE===" + response);
                try {
                    Log.e(TAG, "===SUCESSFULL NOTIFICATIONS===");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "***Error**" + error.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("tokenString", strRecieverToken);
                params.put("message", strUserName + " " + Constants.COMMENT_MESSAGE + " " + mFormsModel.getTitle());
                Log.e(TAG, "====Receiver Token===" + strRecieverToken);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }

        };
        PainlyApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void forumDeleteDialog() {
        final Dialog alertDialog = new Dialog(mActivity, R.style.PauseDialog);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_delete_forum);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button btnCancelB = (Button) alertDialog.findViewById(R.id.btnCancelB);
        Button btnDeleteB = (Button) alertDialog.findViewById(R.id.btnDeleteB);

        btnCancelB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        btnDeleteB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                deleteForum();
            }
        });

        alertDialog.show();
    }

    private void deleteForum() {
        mRootRefrence.child("forums").child(mFormUID).child("isHidden").setValue(true);
        mRootRefrence.child("users").child(FirebaseAuth.getInstance().getUid()).child("forums").child(mFormUID).removeValue();
        finish();
    }

}
