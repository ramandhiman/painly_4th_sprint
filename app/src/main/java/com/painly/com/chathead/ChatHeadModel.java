package com.painly.com.chathead;

import java.io.Serializable;

/**
 * Created by android-da on 2/4/19.
 */

public class ChatHeadModel implements Serializable {
    String id;
    String link;
    String thumbtitle;
    String title;
    String description;

    String image;
    boolean isLoading = false;
    String feedType;


    public String getFeedType() {
        return feedType;
    }

    public void setFeedType(String feedType) {
        this.feedType = feedType;
    }

    public String getThumbtitle() {
        return thumbtitle;
    }

    public void setThumbtitle(String thumbtitle) {
        this.thumbtitle = thumbtitle;
    }


    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }



    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    private int intChatType;

    public String getId() {
        return id;
    }

    public int getIntChatType() {
        return intChatType;
    }

    public void setIntChatType(int intChatType) {
        this.intChatType = intChatType;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
