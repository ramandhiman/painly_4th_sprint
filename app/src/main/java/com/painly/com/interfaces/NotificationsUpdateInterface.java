package com.painly.com.interfaces;

import com.painly.com.beans.NotificationsModel;

/**
 * Created by android-da on 7/7/18.
 */

public interface NotificationsUpdateInterface {
    public void mUpdateNotifications(NotificationsModel mModel, int position);
}
