package com.painly.com.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/*
 * Created by android-raman on 3/5/18.
 */

public class FormsModel implements Serializable, Comparable<FormsModel> {
    String formID = "";
    String body = "";
    String opUID = "";
    String picture = "";
    //long timestamp ;
    Object timestamp;
    String title = "";
    String type = "";
    String video = "";
    String link = "";
    Object isHidden;

    String ForumHeader = "";

    UserDetails mUserDetails;

    HashMap<String, String> conditions;

    ArrayList<Comments> mCommentArrayList = new ArrayList<Comments>();
    ArrayList<Conditions> mFormConditionsAL = new ArrayList<Conditions>();

    public String getForumHeader() {
        return ForumHeader;
    }

    public ArrayList<Conditions> getmFormConditionsAL() {
        return mFormConditionsAL;
    }

    public void setmFormConditionsAL(ArrayList<Conditions> mFormConditionsAL) {
        this.mFormConditionsAL = mFormConditionsAL;
    }

    public void setForumHeader(String forumHeader) {
        ForumHeader = forumHeader;
    }

    public Object getIsHidden() {
        return isHidden;
    }

    public void setIsHidden(Object isHidden) {
        this.isHidden = isHidden;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public UserDetails getmUserDetails() {
        return mUserDetails;
    }

    public void setmUserDetails(UserDetails mUserDetails) {
        this.mUserDetails = mUserDetails;
    }

    public HashMap<String, String> getConditions() {
        return conditions;
    }

    public void setConditions(HashMap<String, String> conditions) {
        this.conditions = conditions;
    }

    public String getFormID() {
        return formID;
    }

    public void setFormID(String formID) {
        this.formID = formID;
    }


    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getOpUID() {
        return opUID;
    }

    public void setOpUID(String opUID) {
        this.opUID = opUID;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Object getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Object timestamp) {
        this.timestamp = timestamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public ArrayList<Comments> getmCommentArrayList() {
        return mCommentArrayList;
    }

    public void setmCommentArrayList(ArrayList<Comments> mCommentArrayList) {
        this.mCommentArrayList = mCommentArrayList;
    }


   /* @Override
    public int compareTo(FormsModel f) {
        if ((Long) timestamp  > (Long) f.timestamp) {
            return 1;
        } else if ((Long) timestamp < (Long) f.timestamp) {
            return -1;
        } else {
            return 0;
        }
    }*/
//LHvnxcLHo8l3KxfAXP2
    @Override
    public int compareTo(FormsModel f) {
        if (Double.parseDouble(timestamp.toString())  > Double.parseDouble(f.timestamp.toString())) {
            return 1;
        } else if (Double.parseDouble(timestamp.toString()) < Double.parseDouble(f.timestamp.toString())) {
            return -1;
        } else {
            return 0;
        }
    }
}
