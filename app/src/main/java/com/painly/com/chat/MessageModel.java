package com.painly.com.chat;

import java.io.Serializable;

/**
 * Created by android-da on 6/12/18.
 */

public class MessageModel implements Serializable {
    String msgID = "";
    String sender = "";
    String text = "";
    String timestamp = "";
    String image = "";
    String type = "";

    private int intChatType;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMsgID() {
        return msgID;
    }

    public void setMsgID(String msgID) {
        this.msgID = msgID;
    }

    public int getIntChatType() {
        return intChatType;
    }

    public void setIntChatType(int intChatType) {
        this.intChatType = intChatType;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
