package com.painly.com.beans;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by android-raman on 2/5/18.
 */

public class User implements Serializable {
    UserDetails details;
    boolean isSelected = true;
    HashMap<String, String> conditions;
    HashMap<String, Boolean> messages;
    HashMap<String, String> favorites;
    HashMap<String, String> blocked;
    HashMap<String, String> forums;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public HashMap<String, String> getForums() {
        return forums;
    }

    public void setForums(HashMap<String, String> forums) {
        this.forums = forums;
    }

    public UserDetails getDetails() {
        return details;
    }

    public HashMap<String, String> getFavorites() {
        return favorites;
    }

    public void setFavorites(HashMap<String, String> favorites) {
        this.favorites = favorites;
    }

    public HashMap<String, String> getBlocked() {
        return blocked;
    }

    public void setBlocked(HashMap<String, String> blocked) {
        this.blocked = blocked;
    }

    public void setDetails(UserDetails details) {


        this.details = details;
    }

    public HashMap<String, String> getConditions() {
        return conditions;
    }

    public void setConditions(HashMap<String, String> conditions) {
        this.conditions = conditions;
    }

    public HashMap<String, Boolean> getMessages() {
        return messages;
    }

    public void setMessages(HashMap<String, Boolean> messages) {
        this.messages = messages;
    }
}
