package com.painly.com.interfaces;

import com.painly.com.beans.Conditions;

/**
 * Created by android-da on 5/25/18.
 */

public interface OnItemClickListener {
    public  void onItemClick(Conditions item);
}
