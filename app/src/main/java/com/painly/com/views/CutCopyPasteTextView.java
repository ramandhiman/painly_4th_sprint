package com.painly.com.views;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

import com.painly.com.font.ArialRegular;

/**
 * Created by android-da on 8/29/18.
 */

public class CutCopyPasteTextView extends EditText {

    private OnCutCopyPasteListener mOnCutCopyPasteListener;

    /*
        Just the constructors to create a new EditText...
     */
    public CutCopyPasteTextView(Context context) {
        super(context);
    }

    public CutCopyPasteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CutCopyPasteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CutCopyPasteTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }

    /**
     * Set a OnCutCopyPasteListener.
     *
     * @param listener
     */
    public void setOnCutCopyPasteListener(OnCutCopyPasteListener listener) {
        mOnCutCopyPasteListener = listener;
    }

    /**
     * <p>This is where the "magic" happens.</p>
     * <p>The menu used to cut/copy/paste is a normal ContextMenu, which allows us to
     * overwrite the consuming method and react on the different events.</p>
     *
     * @see <a href="http://grepcode.com/file/repository.grepcode.com/java/ext/com.google.android/android/2.3_r1/android/widget/TextView.java#TextView.onTextContextMenuItem%28int%29">Original Implementation</a>
     */
    @Override
    public boolean onTextContextMenuItem(int id) {
        // Do your thing:
        boolean consumed = super.onTextContextMenuItem(id);
        // React:
        switch (id) {
            case android.R.id.cut:
                onCut();
                break;
            case android.R.id.copy:
                onCopy();
                break;
            case android.R.id.paste:
                onPaste();
        }
        return consumed;
    }

    /**
     * Text was cut from this EditText.
     */
    public void onCut() {
        if (mOnCutCopyPasteListener != null)
            mOnCutCopyPasteListener.onCut();
    }

    /**
     * Text was copied from this EditText.
     */
    public void onCopy() {
        if (mOnCutCopyPasteListener != null)
            mOnCutCopyPasteListener.onCopy();
    }

    /**
     * Text was pasted into the EditText.
     */
    public void onPaste() {
        if (mOnCutCopyPasteListener != null)
            mOnCutCopyPasteListener.onPaste();
    }

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new ArialRegular(context).getFontFamily());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface OnCutCopyPasteListener {
        void onCut();

        void onCopy();

        void onPaste();
    }
}

