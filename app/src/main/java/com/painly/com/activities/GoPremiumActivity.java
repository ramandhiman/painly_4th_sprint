package com.painly.com.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.painly.com.R;
import com.painly.com.font.TextViewArialBold;
import com.painly.com.font.TextViewArialRegular;
import com.painly.com.font.TextViewGothamMedium;
import com.painly.com.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GoPremiumActivity extends AppCompatActivity {
    Activity mActivity = GoPremiumActivity.this;
    String TAG = GoPremiumActivity.this.getClass().getSimpleName();
    @BindView(R.id.leftLL)
    LinearLayout leftLL;
    @BindView(R.id.txtAlredyPruchasedTV)
    TextViewGothamMedium txtAlredyPruchasedTV;
    @BindView(R.id.personallyCuratedLL)
    LinearLayout personallyCuratedLL;
    @BindView(R.id.contextualInteligenceLL)
    LinearLayout contextualInteligenceLL;
    @BindView(R.id.earlyAccessLL)
    LinearLayout earlyAccessLL;
    @BindView(R.id.btnInquireNow1)
    TextViewArialBold btnInquireNow1;
    @BindView(R.id.forSchoolLL)
    LinearLayout forSchoolLL;
    @BindView(R.id.btnInquireNow2)
    TextViewArialBold btnInquireNow2;
    @BindView(R.id.forEnterprisesLL)
    LinearLayout forEnterprisesLL;
    @BindView(R.id.txtTermsOFUseTV)
    TextViewArialRegular txtTermsOFUseTV;
    @BindView(R.id.txtPrivacyPolicyTV)
    TextViewArialRegular txtPrivacyPolicyTV;
    @BindView(R.id.upgradePremiumLL)
    LinearLayout upgradePremiumLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_premium);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.leftLL, R.id.txtAlredyPruchasedTV, R.id.btnInquireNow1, R.id.btnInquireNow2, R.id.txtTermsOFUseTV, R.id.txtPrivacyPolicyTV, R.id.upgradePremiumLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.leftLL:
                onBackPressed();
                break;
            case R.id.txtAlredyPruchasedTV:
                break;
            case R.id.btnInquireNow1:
                inquirePainly();
                break;
            case R.id.btnInquireNow2:
                inquirePainly();
                break;
            case R.id.txtTermsOFUseTV:
                Intent termsIntent = new Intent(mActivity, OpenLinkActivity.class);
                termsIntent.putExtra("LINK", Constants.TERMS_LINK);
                startActivity(termsIntent);
                break;
            case R.id.txtPrivacyPolicyTV:
                Intent aboutIntent = new Intent(mActivity, OpenLinkActivity.class);
                aboutIntent.putExtra("LINK", Constants.ABOUT_PAINLY_LINK);
                startActivity(aboutIntent);
                break;
            case R.id.upgradePremiumLL:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void inquirePainly() {
        String uriText =
                "mailto:sales@painly.com" +
                        "?subject=" + Uri.encode("Request More Information about Painly Premium") +
                        "&body=" + Uri.encode("Name:\nInstitution/Company Name:\nNumber of Subscriptions:\nContact Number:");

        Uri uri = Uri.parse(uriText);

        Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
        sendIntent.setData(uri);
        if (sendIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(Intent.createChooser(sendIntent, "Send email"));
        }
    }


}
