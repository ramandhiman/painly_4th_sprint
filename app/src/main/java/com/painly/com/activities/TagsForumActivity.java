package com.painly.com.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.BaseActivity;
import com.painly.com.R;
import com.painly.com.adapters.FormsAdapter;
import com.painly.com.beans.Conditions;
import com.painly.com.beans.FormsModel;
import com.painly.com.font.TextViewArialBold;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TagsForumActivity extends BaseActivity {
    String TAG = TagsForumActivity.this.getClass().getSimpleName();
    Activity mActivity = TagsForumActivity.this;
    Conditions mConditions;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.backLL)
    LinearLayout backLL;
    @BindView(R.id.txtConditionsNameTV)
    TextViewArialBold txtConditionsNameTV;
    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;

    DatabaseReference mRootRefrence;
    ArrayList<FormsModel> mFormsList = new ArrayList<FormsModel>();
    ArrayList<FormsModel> mSortedForumArrayList = new ArrayList<FormsModel>();
    FormsAdapter mFormsAdapter;
    String strConditionID = "-KPOhWbX59L2v1FlN2d5";//Eye Pain

   /* ForumClickInterface mForumClickInterface = new ForumClickInterface() {
        @Override
        public void forumClick(FormsModel mFormsModel, int position) {
            if (mFormsModel.getType().equals("video")) {
                Intent mIntent = new Intent(mActivity, VideoDetailsActivity.class);
                mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                startActivity(mIntent);
            } else if (mFormsModel.getType().equals("link")) {
                Intent mIntent = new Intent(mActivity, LinkDetailsActivity.class);
                mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                startActivity(mIntent);
            } else if (mFormsModel.getType().equals("text")) {
                Intent mIntent = new Intent(mActivity, TextDetailsActivity.class);
                mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                startActivity(mIntent);
            } else if (mFormsModel.getType().equals("picture")) {
                Intent mIntent = new Intent(mActivity, PictureDetailsActivity.class);
                mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                startActivity(mIntent);
            }
        }
    };


    SortingWithFormConditionInterface mSortingWithFormConditionInterface = new SortingWithFormConditionInterface() {
        @Override
        public void getSortedCondition(Conditions mCondition) {
            Intent mIntent = new Intent(mActivity, TagsForumActivity.class);
            mIntent.putExtra("MODEL", mCondition);
            mIntent.putExtra("LIST", mFormsList);
            startActivity(mIntent);
        }
    };*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tags_forum);
        ButterKnife.bind(this);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
       /* if (getIntent() != null) {
            mConditions = (Conditions) getIntent().getSerializableExtra("MODEL");
            mFormsList = (ArrayList<FormsModel>) getIntent().getSerializableExtra("LIST");
            txtConditionsNameTV.setText(mConditions.getName());
            strConditionID = mConditions.getConditionID();
            Log.e(TAG, "===COND_ID===" + strConditionID);
            Log.e(TAG, "===List SIZE===" + mFormsList.size());
            setSortingAccordingTags();
        }*/

       /*Sorting Tags*/
        setSortingTags();
    }

    private void setSortingTags() {
        mRootRefrence.child("forums").orderByChild("conditions/" + strConditionID).equalTo(strConditionID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e(TAG,"=====KEY===="+dataSnapshot.getKey());
                Log.e(TAG,"=====KEY===="+dataSnapshot.toString());
                for (DataSnapshot mDataSnapshot : dataSnapshot.getChildren()){
                    FormsModel mFormsModel = mDataSnapshot.getValue(FormsModel.class);
                    mSortedForumArrayList.add(mFormsModel);

                    Log.e(TAG, "+++++++SIZE+++++"+mSortedForumArrayList.size());
                }

//                mFormsAdapter = new FormsAdapter(mActivity, mSortedForumArrayList,null);
//                mRecyclerView.setNestedScrollingEnabled(false);
//                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
//                mRecyclerView.setLayoutManager(mLayoutManager);
//                mRecyclerView.setItemAnimator(new DefaultItemAnimator());
//                mRecyclerView.setAdapter(mFormsAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: "+databaseError.toString() );
            }
        });

    }

   /* private void setSortingAccordingTags() {
        for (int i = 0; i < mFormsList.size(); i++) {
            if (mFormsList.get(i).getConditions() != null) {
                Iterator trav = mFormsList.get(i).getConditions().entrySet().iterator();
                while (trav.hasNext()) {
                    Map.Entry record = (Map.Entry) trav.next();
                    if (strConditionID.equals(record.getKey())) {
                        if (!mSortedForumArrayList.contains(mFormsList.get(i)))
                            mSortedForumArrayList.add(mFormsList.get(i));
                    }
                }

            }
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               // setSortedAdapter();
            }
        }, 1000);

    }*/

   /* private void setSortedAdapter() {
        mFormsAdapter = new FormsAdapter(mActivity, mSortedForumArrayList, mForumClickInterface, mSortingWithFormConditionInterface);
        mRecyclerView.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mFormsAdapter);
    }*/

    @OnClick({R.id.imgBackIV, R.id.backLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.backLL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
