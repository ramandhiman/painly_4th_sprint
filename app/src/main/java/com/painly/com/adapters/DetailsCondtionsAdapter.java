package com.painly.com.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.painly.com.R;
import com.painly.com.beans.Conditions;
import com.painly.com.interfaces.DetailsConditionsClick;

import java.util.ArrayList;



/*
 * Created by android-raman on 2/5/18.
 */

public class DetailsCondtionsAdapter extends RecyclerView.Adapter<DetailsCondtionsAdapter.ViewHolder> {
    private ArrayList<Conditions> mArrayList;
    private Activity mActivity;
    private DetailsConditionsClick mDetailsConditionsClick;

    public DetailsCondtionsAdapter(Activity mActivity, ArrayList<Conditions> mArrayList,DetailsConditionsClick mDetailsConditionsClick) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mDetailsConditionsClick = mDetailsConditionsClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_details_conditions, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Conditions mConditions = mArrayList.get(position);
        holder.txtTabTV.setText(mConditions.getName());
        /*Set Click Listner*/
        holder.txtTabTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDetailsConditionsClick.getConditionsClick(mConditions);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTabTV;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTabTV = (TextView) itemView.findViewById(R.id.txtTabTV);
        }
    }
}
