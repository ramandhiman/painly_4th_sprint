package com.painly.com.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.einmalfel.earl.AtomFeed;
import com.einmalfel.earl.EarlParser;
import com.einmalfel.earl.Feed;
import com.einmalfel.earl.Item;
import com.einmalfel.earl.RSSFeed;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.activities.NewAllUsersProfileActivity;
import com.painly.com.beans.Conditions;
import com.painly.com.beans.FormsModel;
import com.painly.com.beans.UserDetails;
import com.painly.com.chathead.ChatHeadModel;
import com.painly.com.interfaces.ForumClickInterface;
import com.painly.com.interfaces.SortingWithFormConditionInterface;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;
import com.painly.com.utils.Utilities;

import java.io.InputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

/*
 * Created by android-raman on 2/5/18.
 */

public class FormsAdapter extends RecyclerView.Adapter<FormsAdapter.ViewHolder> {
    String TAG = FormsAdapter.this.getClass().getSimpleName();
    DatabaseReference mRootRefrence;
    ForumClickInterface mForumClickInterface;
    FormItemTagsAdatper mFormItemTagsAdatper;
    SortingWithFormConditionInterface mSortingWithFormConditionInterface;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    private ArrayList<FormsModel> mArrayList;
    private Activity mActivity;

    public FormsAdapter(Activity mActivity, ArrayList<FormsModel> mArrayList, ForumClickInterface mForumClickInterface, SortingWithFormConditionInterface mSortingWithFormConditionInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mForumClickInterface = mForumClickInterface;
        this.mSortingWithFormConditionInterface = mSortingWithFormConditionInterface;
        mRootRefrence = FirebaseDatabase.getInstance().getReference();

        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true)
                .build();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_forms, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final FormsModel mFormsModel = mArrayList.get(position);
        Log.e(TAG, "***Post Type***::::" + mFormsModel.getType());

        try {
            if (mFormsModel.getTimestamp() instanceof String) {
                holder.txtDateTimeTV.setText(Utilities.gettingTimeFormat(mFormsModel.getTimestamp().toString()));
            } else if (mFormsModel.getTimestamp() instanceof Long) {
                holder.txtDateTimeTV.setText(Utilities.gettingLongToFormatedTime(Long.parseLong(mFormsModel.getTimestamp().toString())));
            } else if (mFormsModel.getTimestamp() instanceof Double) {
                holder.txtDateTimeTV.setText(Utilities.gettingDoubleToFormatedTime(Double.parseDouble(mFormsModel.getTimestamp().toString())));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.txtBodyTV.setText(mFormsModel.getTitle());
        holder.txtBodyDescriptionTV.setText(mFormsModel.getBody());

        if (mFormsModel.getType().equals("picture")) {
            holder.imgPostRIV.setVisibility(View.VISIBLE);
            holder.imgYouTubeIconIV.setVisibility(View.GONE);
            imageLoader.displayImage(mFormsModel.getPicture(), holder.imgPostRIV, options);

        } else if (mFormsModel.getType().equals("video")) {
            holder.imgPostRIV.setVisibility(View.VISIBLE);
            String mVideoURL = mFormsModel.getVideo();
            generateVideoThumbnail(mFormsModel.getPicture(), mVideoURL, holder.imgPostRIV);
            holder.imgYouTubeIconIV.setVisibility(View.VISIBLE);
            holder.imgYouTubeIconIV.setImageResource(R.drawable.icon_youtube);
        } else if (mFormsModel.getType().equals("link")) {
            holder.imgPostRIV.setVisibility(View.VISIBLE);
            holder.imgYouTubeIconIV.setVisibility(View.GONE);
              /*Preview*/
            if (mFormsModel.getPicture() != null && mFormsModel.getPicture().contains("http")) {
                holder.imgPostRIV.setVisibility(View.VISIBLE);
                Glide.with(mActivity).load(mFormsModel.getPicture()).into(holder.imgPostRIV);
            } else {
                holder.imgPostRIV.setVisibility(View.GONE);
            }
        } else {
            holder.imgPostRIV.setVisibility(View.GONE);
            holder.imgYouTubeIconIV.setVisibility(View.GONE);
        }

        /*Update The User Data*/
        mRootRefrence.child("users").child(mFormsModel.getOpUID()).child("details").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserDetails mUserDetails = dataSnapshot.getValue(UserDetails.class);
                mFormsModel.setmUserDetails(mUserDetails);


                if (mFormsModel.getmUserDetails() != null) {
                    if (mFormsModel.getmUserDetails().getImage() != null && mFormsModel.getmUserDetails().getImage().contains("http")) {
                        Glide.with(mActivity.getApplicationContext())
                                .load(mFormsModel.getmUserDetails().getImage())
                                .apply(RequestOptions.placeholderOf(PainlyApplication.getInstance().getRandom()).error(PainlyApplication.getInstance().getRandom()))
                                .into(holder.profileRIV);
                    } else {
                        holder.profileRIV.setImageResource(PainlyApplication.getInstance().getRandom());
                    }

                    if (mFormsModel.getmUserDetails().getName() != null) {
                        if (mFormsModel.getPicture() != null) {
                            Log.e(TAG, "onBindViewHolder: " + mFormsModel.getPicture());

                        }
                        Log.e(TAG, "onDataChange: " + mFormsModel.getmUserDetails().getName());
                        holder.txtUserNameTV.setText(mFormsModel.getmUserDetails().getName());

                    } else {
                        if (mFormsModel.getPicture() != null) {
                            Log.e(TAG, "onBindViewHolder: " + mFormsModel.getPicture());
                        }
                        Log.e(TAG, "onDataChange: " + mFormsModel.getmUserDetails().getUsername());
                    }
                    if (mFormsModel.getmUserDetails().getUsername() != null)
                        if (mFormsModel.getmUserDetails().getUsername().contains("@")) {
                            Log.e(TAG, "onDataChange: " + mFormsModel.getmUserDetails().getName());

                            holder.txtUserDescriptionTV.setText(mFormsModel.getmUserDetails().getUsername());
                        } else {
                            holder.txtUserDescriptionTV.setText("@" + " " + mFormsModel.getmUserDetails().getUsername());
                        }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        /*Item Tags RecyclerView*/
        if (mFormsModel.getConditions() != null && mFormsModel.getConditions().size() > 0) {
            final ArrayList<Conditions> mItemArrayList = new ArrayList<Conditions>();
            mItemArrayList.clear();
            Iterator it = mFormsModel.getConditions().entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                String mKeyValue = (String) pair.getKey();
                mRootRefrence.child("conditions").child(mKeyValue).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot != null) {
                            if (dataSnapshot.getKey() != null){
                                String mKey = dataSnapshot.getKey();

                                Log.e(TAG, "===KEY==" + mKey);
                                Conditions mConditions = dataSnapshot.getValue(Conditions.class);
                                mConditions.setConditionID(mKey);

                                mItemArrayList.add(mConditions);

                                mFormItemTagsAdatper = new FormItemTagsAdatper(mActivity, mItemArrayList, mSortingWithFormConditionInterface);
                                LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
                                holder.formItemTagsRV.setLayoutManager(horizontalLayoutManager);
                                holder.formItemTagsRV.setAdapter(mFormItemTagsAdatper);
                            }

                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, "===Error===" + databaseError.toString());
                    }
                });
            }

        }




        /*Set Click Listner*/
        holder.cardCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mForumClickInterface.forumClick(mFormsModel, position);
            }
        });

        holder.profileRIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FirebaseAuth.getInstance().getCurrentUser().getUid().equals(mFormsModel.getOpUID())) {
                    Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                    mIntent.putExtra("UID", mFormsModel.getOpUID());
                    mIntent.putExtra("TYPE", Constants.CURRENT);
                    mActivity.startActivity(mIntent);
                } else {
                    Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                    mIntent.putExtra("UID", mFormsModel.getOpUID());
                    mIntent.putExtra("TYPE", Constants.ANOTHER);
                    mActivity.startActivity(mIntent);
                }
            }
        });


        /*Premium feeds functionality*/


      /*  if (mFormsModel.getConditions() != null){
            Collection<String> values = mFormsModel.getConditions().values();
            ArrayList<String> listOfValues = new ArrayList<String>(values);

            Log.e(TAG, "==SIZE==" + listOfValues.size());

            mRootRefrence.child("conditions").child(getRandomConditionID(listOfValues)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (dataSnapshot != null) {
                        final Conditions mConditions = dataSnapshot.getValue(Conditions.class);
                   *//*
                    * Preminum functionality
                    *   *//*
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // Stuff that updates the UI
                                if (findSourcesUnderCondition(mConditions) != null && findSourcesUnderCondition(mConditions).length() > 0) {
                                    getChatBotMsgFromCondition(findSourcesUnderCondition(mConditions), holder);
                                }
                            }
                        });

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, "==ERROR==" + databaseError.toString());
                }
            });

        }*/

    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private void generateVideoThumbnail(String strPicture, String mVideoURL, RoundedImageView mRoundedImageView) {
        try {
            if (strPicture != null && strPicture.length() > 0 && strPicture.contains("http")) {
                Glide.with(mActivity.getApplicationContext()).load(strPicture).into(mRoundedImageView);
            } else {
                if (mVideoURL != null) {
                    String videoId = Utilities.extractYoutubeId(mVideoURL);
                    Log.e("VideoId is->", "" + videoId);
                    String img_url = "http://img.youtube.com/vi/" + videoId + "/0.jpg";
                    Glide.with(mActivity.getApplicationContext()).load(img_url).into(mRoundedImageView);
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private String getRandomConditionID(ArrayList<String> mConditionList) {
        int rnd = new Random().nextInt(mConditionList.size());

        return mConditionList.get(rnd);
    }


    /*
    * Premium functionality of Forms Feeds
    *
    * */

    /*Find Random Conditions*/
    private Conditions getRandomCondition(ArrayList<Conditions> mConditionList) {
        int rnd = new Random().nextInt(mConditionList.size());

        return mConditionList.get(rnd);
    }

    //Find Random Source from the Condition
    private String findSourcesUnderCondition(Conditions condition) {
        String randomSourceURL = "";

        if (condition.getSources() != null && condition.getSources().size() > 0) {
            Collection<String> values = condition.getSources().values();
            ArrayList<String> mSourcesUrlAL = new ArrayList<String>(values);

            int rnd = new Random().nextInt(mSourcesUrlAL.size());

            randomSourceURL = mSourcesUrlAL.get(rnd);


//            randomSourceURL = mSourcesUrlAL.get(0);
        }

        return randomSourceURL;
    }

    private void getChatBotMsgFromCondition(String sourcesUnderCondition, ViewHolder mViewHolder) {
        /*GEtting two Types Feed
        * 1) RSS Feed Result Simple Result
        * 2) ATOM Feed Result with Youtube Video
        * */
        getRssAndAtomWithData(sourcesUnderCondition, mViewHolder);
    }

    /*
   * RSS & ATOM Parser Library.
   * Execute Api In Asyn Task.
   * */
    private void getRssAndAtomWithData(String strSourceUrl, ViewHolder mViewHolder) {
        new RetrieveFeedTask(strSourceUrl, mViewHolder).execute();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements Serializable {
        public TextView txtUserNameTV, txtUserDescriptionTV, txtDateTimeTV, txtBodyTV, txtBodyDescriptionTV;
        public RoundedImageView imgPostRIV;
        public de.hdodenhof.circleimageview.CircleImageView profileRIV;
        public CardView cardCV;
        public ImageView imgYouTubeIconIV;
        public LinearLayout premiumFeedLL;
        public RecyclerView formItemTagsRV, premiumRecyclerViewRV;

        public ViewHolder(View itemView) {
            super(itemView);
            txtUserNameTV = (TextView) itemView.findViewById(R.id.txtUserNameTV);
            txtUserDescriptionTV = (TextView) itemView.findViewById(R.id.txtUserDescriptionTV);
            txtDateTimeTV = (TextView) itemView.findViewById(R.id.txtDateTimeTV);
            txtBodyTV = (TextView) itemView.findViewById(R.id.txtBodyTV);
            txtBodyDescriptionTV = (TextView) itemView.findViewById(R.id.txtBodyDescriptionTV);
            imgPostRIV = (RoundedImageView) itemView.findViewById(R.id.imgPostRIV);
            profileRIV = (de.hdodenhof.circleimageview.CircleImageView) itemView.findViewById(R.id.profileRIV);
            cardCV = (CardView) itemView.findViewById(R.id.cardCV);
            imgYouTubeIconIV = (ImageView) itemView.findViewById(R.id.imgYouTubeIconIV);
            formItemTagsRV = (RecyclerView) itemView.findViewById(R.id.formItemTagsRV);
            premiumRecyclerViewRV = (RecyclerView) itemView.findViewById(R.id.premiumRecyclerViewRV);
            premiumFeedLL = (LinearLayout) itemView.findViewById(R.id.premiumFeedLL);

        }
    }

    class RetrieveFeedTask extends AsyncTask<String, Void, List<Item>> {
        String strSourceUrl = "";
        ViewHolder mViewHolder;

        private RetrieveFeedTask(String strSourceUrl, ViewHolder mViewHolder) {
            this.strSourceUrl = strSourceUrl;
            this.mViewHolder = mViewHolder;
        }

        @Override
        protected List<Item> doInBackground(String... strings) {
            List<Item> result = new ArrayList<>();
            try {
                if (strSourceUrl != null && strSourceUrl.contains("http")) {
                    InputStream inputStream = new URL(strSourceUrl).openConnection().getInputStream();
                    Feed feed = EarlParser.parseOrThrow(inputStream, 0);
                    // media and itunes RSS extensions allow to assign keywords to feed items

                    if (RSSFeed.class.isInstance(feed)) {
                        RSSFeed rssFeed = (RSSFeed) feed;
                        Log.e(TAG, "==RSS FEED Count==" + rssFeed.getItems().size());

                        final ArrayList<ChatHeadModel> mRssArrayList = new ArrayList<>();

                        if (rssFeed.getItems().size() > 0 &&  rssFeed.getItems().size() < 6) {
                            for (int i = 0; i < rssFeed.getItems().size(); i++) {
                                 int rnd = new Random().nextInt(rssFeed.getItems().size());
                                Item mItemObj = rssFeed.getItems().get(rnd);
                                ChatHeadModel mModel = new ChatHeadModel();
                                if (mItemObj.getImageLink() != null && mItemObj.getImageLink().contains("http")) {
                                    mModel.setThumbtitle(mItemObj.getTitle());
                                    mModel.setFeedType(Constants.RSS_TYPE);
                                    mModel.setLink(mItemObj.getLink());
                                    mModel.setImage(mItemObj.getImageLink());
                                } else {
                                    mModel.setThumbtitle(mItemObj.getTitle());
                                    mModel.setFeedType(Constants.RSS_TYPE);
                                    mModel.setLink(mItemObj.getLink());
                                    mModel.setImage(PainlyApplication.getInstance().getRandomFeedImage());
                                }

                                mRssArrayList.add(mModel);
                            }


                            mActivity.runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    // Stuff that updates the UI
                                    Log.e(TAG, "===Feed List Size===" + mRssArrayList.size());
                                    FormPremiumFeedsAdapter mAdapter = new FormPremiumFeedsAdapter(mActivity, mRssArrayList);
                                    LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
                                    mViewHolder.premiumRecyclerViewRV.setLayoutManager(horizontalLayoutManager);
                                    mViewHolder.premiumRecyclerViewRV.setAdapter(mAdapter);
                                }
                            });

                        } else if (rssFeed.getItems().size() > 6) {
                            for (int i = 0; i < 6; i++) {
                                int rnd = new Random().nextInt(rssFeed.getItems().size());
                                Item mItemObj = rssFeed.getItems().get(rnd);
                                ChatHeadModel mModel = new ChatHeadModel();
                                if (mItemObj.getImageLink() != null && mItemObj.getImageLink().contains("http")) {
                                    mModel.setThumbtitle(mItemObj.getTitle());
                                    mModel.setFeedType(Constants.RSS_TYPE);
                                    mModel.setLink(mItemObj.getLink());
                                    mModel.setImage(mItemObj.getImageLink());
                                } else {
                                    mModel.setThumbtitle(mItemObj.getTitle());
                                    mModel.setFeedType(Constants.RSS_TYPE);
                                    mModel.setLink(mItemObj.getLink());
                                    mModel.setImage(PainlyApplication.getInstance().getRandomFeedImage());
                                }
                                mRssArrayList.add(mModel);
                            }

                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e(TAG, "===Feed List Size===" + mRssArrayList.size());
                                    FormPremiumFeedsAdapter mAdapter = new FormPremiumFeedsAdapter(mActivity, mRssArrayList);
                                    LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
                                    mViewHolder.premiumRecyclerViewRV.setLayoutManager(horizontalLayoutManager);
                                    mViewHolder.premiumRecyclerViewRV.setAdapter(mAdapter);
                                }
                            });

                        }else if (rssFeed.getItems().size() == 0){
                            Log.e(TAG, "==Size==");
                        }else{
                            Log.e(TAG, "==Nothing==");
                        }

                    } else if (AtomFeed.class.isInstance(feed)) {
                        AtomFeed atomFeed = (AtomFeed) feed;
                        Log.e(TAG, "==ATOM Feed ==");

                    } else {
                        Log.e(TAG, "==Not Any Feed ==");
                    }

                } else {
                    Log.e(TAG, "==Not Valid Url==");
                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "===ERROR==String====Error" + e.toString());
            }
            return result;
        }

        @Override
        protected void onPostExecute(List<Item> items) {
            super.onPostExecute(items);

        }
    }

}
