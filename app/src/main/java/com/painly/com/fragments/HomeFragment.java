package com.painly.com.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.R;
import com.painly.com.activities.AllUsersActivity;
import com.painly.com.activities.GoPremiumActivity;
import com.painly.com.activities.HomeSearchTagActivity;
import com.painly.com.activities.NewAddPostActivity;
import com.painly.com.activities.NewForumsDetailsActivity;
import com.painly.com.activities.TextDetailsActivity;
import com.painly.com.adapters.FormsAdapter;
import com.painly.com.adapters.TopUsersAdapter;
import com.painly.com.beans.Conditions;
import com.painly.com.beans.FormsModel;
import com.painly.com.beans.User;
import com.painly.com.interfaces.ForumClickInterface;
import com.painly.com.interfaces.SortingWithFormConditionInterface;
import com.painly.com.services.PremiumFeedService;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Utilities;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;


public class HomeFragment extends Fragment {
    static boolean isGoPremium = true;
    public int OVERLAY_PERMISSION_REQ_CODE_CHATHEAD = 1234;
    View mView;
    String TAG = HomeFragment.this.getClass().getSimpleName();
    @BindView(R.id.topUsersRV)
    RecyclerView topUsersRV;
    @BindView(R.id.layoutOpenConditionsLL)
    ImageView layoutOpenConditionsLL;
    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.imgAddPostIV)
    ImageView imgAddPostIV;
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeToRefresh;
    @BindView(R.id.txtSeeAllTV)
    TextView txtSeeAllTV;
    @BindView(R.id.loadMoreProgressBar)
    ProgressBar loadMoreProgressBar;
    @BindView(R.id.mNestedScrollView)
    NestedScrollView mNestedScrollView;
    @BindView(R.id.imgCloseIV)
    ImageView imgCloseIV;
    @BindView(R.id.txtPriceTV)
    TextView txtPriceTV;
    @BindView(R.id.goPremiumLL)
    LinearLayout goPremiumLL;

    //Sponsorship Layout
    @BindView(R.id.sponserShipLL)
    LinearLayout sponserShipLL;
    @BindView(R.id.sponsershipIV)
    CircleImageView sponsershipIV;
    @BindView(R.id.sponserShipNameTV)
    TextView sponserShipNameTV;
    @BindView(R.id.filterTextTV)
    TextView filterTextTV;
    @BindView(R.id.placePostTV)
    TextView placePostTV;
    @BindView(R.id.closeFilterLL)
    ImageView closeFilterLL;

    Unbinder unbinder;

    DatabaseReference mRootRefrence;
    ArrayList<Conditions> mTagsArrayList = new ArrayList<Conditions>();
    ArrayList<User> mUserArrayList = new ArrayList<>();
    ArrayList<FormsModel> mFormsList = new ArrayList<FormsModel>();
    FormsAdapter mFormsAdapter;
    TopUsersAdapter mTopUsersAdapter;


    boolean mGettingMoreImages = false;
    boolean mLoadMore = false;
    boolean mSortingLoadMore = false;
    String mLastKey = "";
    String mSortingLastKey = "";
    Conditions mSortCondition;

    boolean isSorting = false;

    //Forum click Interface.....
    ForumClickInterface mForumClickInterface = new ForumClickInterface() {
        @Override
        public void forumClick(FormsModel mFormsModel, int position) {
            if (mFormsModel.getType().equals("video")) {
                Intent mIntent = new Intent(getActivity(), NewForumsDetailsActivity.class);
                mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                startActivityForResult(mIntent, 393);
            } else if (mFormsModel.getType().equals("link")) {
                Intent mIntent = new Intent(getActivity(), NewForumsDetailsActivity.class);
                mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                startActivityForResult(mIntent, 393);
            } else if (mFormsModel.getType().equals("text")) {
                Intent mIntent = new Intent(getActivity(), TextDetailsActivity.class);
                mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                startActivityForResult(mIntent, 393);
            } else if (mFormsModel.getType().equals("picture")) {
                Intent mIntent = new Intent(getActivity(), NewForumsDetailsActivity.class);
                mIntent.putExtra("FORM_UID", mFormsModel.getFormID());
                startActivityForResult(mIntent, 393);
            }
        }
    };

    SortingWithFormConditionInterface mSortingWithFormConditionInterface = new SortingWithFormConditionInterface() {
        @Override
        public void getSortedCondition(Conditions mConditions) {
            mSortCondition = mConditions;
            setSortingTags(mSortCondition);
            //Sponsorship functionality
            setChangesOfSorting(mSortCondition);

        }
    };

    private void setChangesOfSorting(Conditions mConditions) {
        if (mConditions != null) {
            filterTextTV.setText(mConditions.getName());
            placePostTV.setVisibility(View.VISIBLE);
            closeFilterLL.setVisibility(View.VISIBLE);
            layoutOpenConditionsLL.setVisibility(View.GONE);
            if (mConditions.getSponsorship() != null) {
                sponserShipLL.setVisibility(View.VISIBLE);
                placePostTV.setVisibility(View.GONE);
                if (mConditions.getSponsorship().getImage() != null && mConditions.getSponsorship().getImage().contains("http")) {
                    sponserShipNameTV.setText(getString(R.string.sponsored_by) + " " + mConditions.getSponsorship().getName());
                    //Sponsorship Image
                    Glide.with(getActivity())
                            .load(mConditions.getSponsorship().getImage())
                            .apply(RequestOptions.placeholderOf(R.drawable.icon_round_ph).error(R.drawable.icon_round_ph))
                            .into(sponsershipIV);

                }
            } else {
                sponserShipLL.setVisibility(View.GONE);
                placePostTV.setVisibility(View.VISIBLE);
            }


        } else {
            closeFilterLL.setVisibility(View.GONE);
            layoutOpenConditionsLL.setVisibility(View.VISIBLE);
        }

        closeFilterLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutOpenConditionsLL.setVisibility(View.VISIBLE);
                closeFilterLL.setVisibility(View.GONE);
                filterTextTV.setText(getString(R.string.the_source));
                sponserShipLL.setVisibility(View.GONE);
                placePostTV.setVisibility(View.VISIBLE);

                /*OnResume SetUP*/
                isSorting = false;
                if (!isSorting) {
                    if (Utilities.isNetworkAvailable(getActivity())) {
                        mUserArrayList.clear();
                        mFormsList.clear();
                        setHomeDate();
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().stopService(new Intent(getActivity(), PremiumFeedService.class));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, mView);

        mRootRefrence = FirebaseDatabase.getInstance().getReference();

        AlertDialogManager.hideProgressDialog();

//        if (isGoPremium)
//            goPremiumLL.setVisibility(View.VISIBLE);

        setPaginations();
        swipeRefresh();
        setAdapter();

        /*OnResume SetUP*/
        if (!isSorting) {
            if (Utilities.isNetworkAvailable(getActivity())) {
                mUserArrayList.clear();
                mFormsList.clear();
                setHomeDate();
            } else {
                Toast.makeText(getActivity(), getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
            }
        }

        return mView;
    }

    private void swipeRefresh() {
        isSorting = false;
        swipeToRefresh.setColorSchemeResources(R.color.dark_red);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeToRefresh.setRefreshing(false);

                if (layoutOpenConditionsLL != null)
                    layoutOpenConditionsLL.setVisibility(View.VISIBLE);
                if (closeFilterLL != null)
                    closeFilterLL.setVisibility(View.GONE);
                filterTextTV.setText(getActivity().getResources().getString(R.string.the_source));
                if (sponserShipLL != null)
                    sponserShipLL.setVisibility(View.GONE);
                if (placePostTV != null)
                    placePostTV.setVisibility(View.VISIBLE);


                if (Utilities.isNetworkAvailable(getActivity())) {
                    mUserArrayList.clear();
                    mFormsList.clear();
                    setHomeDate();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setHomeDate() {
        if (Utilities.isNetworkAvailable(getActivity())) {
            getUsersData();
        } else {
            Toast.makeText(getActivity(), getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
        }
    }

    private void setPaginations() {
        mNestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (mNestedScrollView != null) {
                    int scrollY = mNestedScrollView.getScrollY(); // For ScrollView
                    if (scrollY == (mNestedScrollView.getChildAt(0).getMeasuredHeight() - mNestedScrollView.getMeasuredHeight())) {
                        if (!isSorting) {
                            if (!mLoadMore) {
                                loadMoreProgressBar.setVisibility(View.VISIBLE);
                                getMoreForums();
                            } else {
                                Toast toast = Toast.makeText(getActivity(), "No More Forums!", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER | Gravity.BOTTOM, 0, 0);
                                toast.show();
                            }
                        } else {

                        }
                    }
                }
            }
        });


    }

    private void getUsersData() {
        AlertDialogManager.showProgressDialog(getActivity());
        DatabaseReference mUserRef = mRootRefrence.child("users");
        Query mUserQuery = mUserRef.limitToLast(35);
        mUserQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mUserArrayList.clear();
                mFormsList.clear();
                for (DataSnapshot formsSnapshot : dataSnapshot.getChildren()) {
                    try {
                        //String strKey = formsSnapshot.getKey();
                        final User mUser = formsSnapshot.getValue(User.class);
                        //mUser.getDetails().setUid(strKey);
                        if (!mUserArrayList.contains(mUser) && mUser.getDetails().getImage() != null && mUser.getDetails().getImage().contains("http")) {
                            if (mUser.getDetails().getUid() != null)
                                if (mUser.getDetails().getUid().length() > 0)
                                    if (!mUserArrayList.contains(mUser))
                                        mUserArrayList.add(mUser);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                if (mUserArrayList.size() > 0 && topUsersRV != null) {
                    Log.e(TAG, "==UserList Size==" + mUserArrayList.size());
                    setTopUsersAdapter();

                }


                getForms();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "===ERROR===" + databaseError.toString());
            }
        });
    }

    private void getForms() {
        mFormsList.clear();
        isSorting = false;
        final ArrayList<FormsModel> mTempList = new ArrayList<>();
        final Query formsQuery = mRootRefrence.child("forums").limitToLast(7);
        formsQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                AlertDialogManager.hideProgressDialog();
                for (DataSnapshot mDataSnapshot : dataSnapshot.getChildren()) {
                    FormsModel mFormsModel = mDataSnapshot.getValue(FormsModel.class);
                    String key = mDataSnapshot.getKey();
                    Log.e(TAG, "====FORM_ID===" + key);
                    mFormsModel.setFormID(key);
                    mTempList.add(mFormsModel);
                }

                if (mTempList.size() == 7) {
                    mLastKey = mTempList.get(0).getFormID();
                    Collections.reverse(mTempList);

                    mTempList.remove(mTempList.size() - 1);
                    for (int i = 0; i < mTempList.size(); i++) {
                        if (mTempList.get(i).getIsHidden() instanceof Boolean == false) {
                            if (contains(mFormsList, mTempList.get(i).getFormID()) == false) {
                                mFormsList.add(mTempList.get(i));
                            }
                        }
                    }
                    mFormsAdapter.notifyDataSetChanged();
                    mTempList.clear();

                } else if (mTempList.size() < 7) {
                    Collections.reverse(mTempList);
                    for (int i = 0; i < mTempList.size(); i++) {
                        if (mTempList.get(i).getIsHidden() instanceof Boolean == false) {
                            if (contains(mFormsList, mTempList.get(i).getFormID()) == false) {
                                mFormsList.add(mTempList.get(i));
                            }
                        }
                    }
                    mFormsAdapter.notifyDataSetChanged();
                    mTempList.clear();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "====ERROR====" + databaseError);
            }
        });
    }

    private void getMoreForums() {
        AlertDialogManager.hideProgressDialog();
        if (!mGettingMoreImages) {
            mGettingMoreImages = true;
            final ArrayList<FormsModel> mMoreList = new ArrayList<FormsModel>();
            Query getMoreFormsQuery = mRootRefrence.child("forums").orderByKey().endAt(mLastKey).limitToLast(7);
            getMoreFormsQuery.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (loadMoreProgressBar != null)
                        loadMoreProgressBar.setVisibility(View.GONE);

                    for (DataSnapshot mDataSnapshot : dataSnapshot.getChildren()) {
                        FormsModel mFormsModel = mDataSnapshot.getValue(FormsModel.class);
                        String key = mDataSnapshot.getKey();
                        mFormsModel.setFormID(key);
                        mMoreList.add(mFormsModel);
                    }

                    if (mMoreList.size() == 7) {
                        mLastKey = mMoreList.get(0).getFormID();
                        Collections.reverse(mMoreList);
                        mMoreList.remove(mMoreList.size() - 1);
                        //mFormsList.addAll(mMoreList);
                        for (int i = 0; i < mMoreList.size(); i++) {
                            if (mMoreList.get(i).getIsHidden() instanceof Boolean == false) {
                                mFormsList.add(mMoreList.get(i));
                            }
                        }
                        mMoreList.clear();
                        mGettingMoreImages = false;
                        mFormsAdapter.notifyDataSetChanged();
                        return;
                    }

                    if (mLastKey.equalsIgnoreCase(mMoreList.get(0).getFormID())) {
                        Collections.reverse(mMoreList);
                        //mFormsList.addAll(mMoreList);
                        for (int i = 0; i < mMoreList.size(); i++) {
                            if (mMoreList.get(i).getIsHidden() instanceof Boolean == false) {
                                mFormsList.add(mMoreList.get(i));
                            }
                        }
                        mMoreList.clear();
                        mGettingMoreImages = false;
                        mLoadMore = true;

                        mFormsAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, "=====ERROR=====" + databaseError);
                    if (loadMoreProgressBar != null)
                        loadMoreProgressBar.setVisibility(View.GONE);
                }
            });
        }
    }

    private void setAdapter() {
        AlertDialogManager.hideProgressDialog();
        try {
            if (mRecyclerView != null) {
                mFormsAdapter = new FormsAdapter(getActivity(), mFormsList, mForumClickInterface, mSortingWithFormConditionInterface);
                mRecyclerView.setNestedScrollingEnabled(false);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);
                mRecyclerView.setAdapter(mFormsAdapter);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Adapter Exceptions");
        }
    }

    private void setTopUsersAdapter() {
        mTopUsersAdapter = new TopUsersAdapter(getActivity(), mUserArrayList);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        topUsersRV.setLayoutManager(horizontalLayoutManager);
        topUsersRV.setAdapter(mTopUsersAdapter);
    }


    @OnClick({R.id.layoutOpenConditionsLL, R.id.imgAddPostIV, R.id.txtSeeAllTV, R.id.imgCloseIV, R.id.txtPriceTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layoutOpenConditionsLL:
                isSorting = true;
                Intent mIntent = new Intent(getActivity(), HomeSearchTagActivity.class);
                startActivityForResult(mIntent, 393);
                break;
            case R.id.imgAddPostIV:
                startActivity(new Intent(getActivity(), NewAddPostActivity.class));
                break;
            case R.id.txtSeeAllTV:
                Intent userIntent = new Intent(getActivity(), AllUsersActivity.class);
                userIntent.putExtra("VIEW", "USER");
                startActivity(userIntent);
                break;
            case R.id.imgCloseIV:
                isGoPremium = false;
                goPremiumLL.setVisibility(View.GONE);
                break;
            case R.id.txtPriceTV:
                startActivity(new Intent(getActivity(), GoPremiumActivity.class));
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == 393) {
                Conditions mConditions = (Conditions) data.getSerializableExtra("LIST");
                if (mConditions != null) {
                    mSortCondition = mConditions;
                    setSortingTags(mConditions);
                    //Sponsorship functionality
                    setChangesOfSorting(mConditions);
                } else {
                    isSorting = false;
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /*
     * Sorting Functionality
     * Sorting According To TAGS
     *
     * */
    private void setSortingTags(final Conditions mConditions) {
        AlertDialogManager.showProgressDialog(getActivity());
        isSorting = true;
        mFormsList.clear();
        Query mSortTagQuery = mRootRefrence.child("forums").orderByChild("conditions/" + mConditions.getConditionID()).startAt(mConditions.getConditionID()).endAt(mConditions.getConditionID() + "\uf8ff").limitToLast(50);
        mSortTagQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot mDataSnapshot : dataSnapshot.getChildren()) {
                    FormsModel mFormsModel = mDataSnapshot.getValue(FormsModel.class);
                    if (mFormsModel.getIsHidden() instanceof Boolean == false) {
                        if (contains(mFormsList, mFormsModel.getFormID()) == false) {
                            String key = mDataSnapshot.getKey();
                            mFormsModel.setFormID(key);
                            //mTempList.add(mFormsModel);
                            mFormsList.add(mFormsModel);
                            if (dataSnapshot.getChildrenCount() >= mFormsList.size() - 1) {
                                AlertDialogManager.hideProgressDialog();
                                Collections.reverse(mFormsList);
                                mFormsAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: " + databaseError.toString());
                AlertDialogManager.hideProgressDialog();
            }
        });
    }


    boolean contains(ArrayList<FormsModel> list, String mFormID) {
        for (FormsModel item : list) {
            if (item.getFormID().equals(mFormID)) {
                return true;
            }
        }
        return false;
    }


}
