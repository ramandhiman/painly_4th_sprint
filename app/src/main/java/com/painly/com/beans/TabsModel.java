package com.painly.com.beans;

import java.io.Serializable;

/**
 * Created by android-raman on 11/5/18.
 */

public class TabsModel  implements Serializable{
    String id = "",tabTitle = "";


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTabTitle() {
        return tabTitle;
    }

    public void setTabTitle(String tabTitle) {
        this.tabTitle = tabTitle;
    }
}
