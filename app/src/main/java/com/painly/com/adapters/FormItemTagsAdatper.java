package com.painly.com.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.painly.com.R;
import com.painly.com.beans.Conditions;
import com.painly.com.interfaces.SortingWithFormConditionInterface;

import java.util.ArrayList;


/**
 * Created by android-raman on 2/5/18.
 */

public class FormItemTagsAdatper extends RecyclerView.Adapter<FormItemTagsAdatper.ViewHolder> {
    private ArrayList<Conditions> mArrayList;
    private Activity mActivity;
    private SortingWithFormConditionInterface mSortingWithFormConditionInterface;

    public FormItemTagsAdatper(Activity mActivity, ArrayList<Conditions> mArrayList, SortingWithFormConditionInterface mSortingWithFormConditionInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mSortingWithFormConditionInterface = mSortingWithFormConditionInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_forms_tags, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Conditions mConditions = mArrayList.get(position);
        holder.txtTabTV.setText(mConditions.getName());
        holder.txtTabTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSortingWithFormConditionInterface.getSortedCondition(mConditions);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTabTV;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTabTV = (TextView) itemView.findViewById(R.id.txtTabTV);

        }


    }
}
