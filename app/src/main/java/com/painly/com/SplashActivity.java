package com.painly.com;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.activities.HomeActivity;
import com.painly.com.activities.SelectionSignInSignUpActivity;
import com.painly.com.beans.Conditions;
import com.painly.com.beans.NotificationsModel;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;
import com.painly.com.utils.PainlySingleton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends BaseActivity {

    public static int NOTIFICATIONS_COUNTS = 0;
    private static boolean isNOTIFICATIONS = false;
    public String TAG = SplashActivity.this.getClass().getSimpleName();
    int SPLASH_TIME_OUT = 3500;
    String strData = "";
    ArrayList<NotificationsModel> mNotificationsAL = new ArrayList<NotificationsModel>();
    DatabaseReference mRootRefrence;

    ArrayList<Conditions> mConditionsAL = PainlySingleton.getInstance().getmConditionsArrayList();

    FirebaseAuth mFirebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (PainlyPreference.readBoolean(mActivity, PainlyPreference.IS_LOGIN, false)){
            if (FirebaseAuth.getInstance().getCurrentUser() != null){
                if (FirebaseAuth.getInstance().getCurrentUser().getUid() != null){
                    getAllConditions();
                }
            }


        }


        splashWork();

     //   getPreview();
    }

    private void getPreview() {
        String strAPIURL = Constants.GET_IMAGE_LINK;
        Log.e(TAG, "=====API URL====" + strAPIURL);
        StringRequest request = new StringRequest(Request.Method.POST, strAPIURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "===RESPONSE===" + response);
                try {
                    JSONObject mJsonObject = new JSONObject(response);
                    String strStatus = mJsonObject.getString("status");
                    if (strStatus.equals("200")) {
                        if (mJsonObject.getString("img").contains("http")) {
                            //String strLinkImageUrl = mJsonObject.getString("img");
                            Log.e(TAG, "==Image Url Link==" + mJsonObject.getString("img"));
                            //mFormsModel.setPicture(strLinkImageUrl);
                            //imageLoader.displayImage(mJsonObject.getString("img"),holder.imgPostRIV, options);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "===ERROR===" + error.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> mParam = new HashMap<String, String>();
                mParam.put("url", "https://www.nature.com/articles/d41586-019-00483-5");
                return mParam;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }

        };
        PainlyApplication.getInstance().addToRequestQueue(request);
    }


    private void splashWork() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (PainlyPreference.readBoolean(mActivity, PainlyPreference.IS_LOGIN, false)) {
                    if (getIntent().getExtras() != null) {
                        for (String key : getIntent().getExtras().keySet()) {
                            Object value = getIntent().getExtras().get(key);
                            if (key.equalsIgnoreCase("data")) {
                                strData = String.valueOf(value);
                            }
                        }
                    }

                    if (strData.length() > 0) {
                        Intent mIntent = new Intent(mActivity, HomeActivity.class);
                        mIntent.putExtra("STR_NOTI", strData);
                        startActivity(mIntent);
                        finish();
                    } else {
                        startActivity(new Intent(mActivity, HomeActivity.class));
                        finish();
                    }

                } else {
                    startActivity(new Intent(mActivity, SelectionSignInSignUpActivity.class));
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }


    private void getAllConditions() {
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        mRootRefrence.child("conditions").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot mDataSnapshot) {
                for (DataSnapshot mDataSnap : mDataSnapshot.getChildren()) {
                    String conditionID = mDataSnap.getKey();
                    final Conditions mConditions = mDataSnap.getValue(Conditions.class);
                    mConditions.setConditionID(conditionID);
                    mConditionsAL.add(mConditions);
                }
                Log.e(TAG, "=====Splash TAGS SIZE=====" + mConditionsAL.size());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "===ERROR===" + databaseError.toString());
            }
        });
    }


}
