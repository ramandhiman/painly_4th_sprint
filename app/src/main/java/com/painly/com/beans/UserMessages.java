package com.painly.com.beans;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by android-da on 6/11/18.
 */

public class UserMessages implements Serializable {
    HashMap<String,Boolean> messages = new HashMap<String,Boolean>();

    public HashMap<String, Boolean> getMessages() {
        return messages;
    }

    public void setMessages(HashMap<String, Boolean> messages) {
        this.messages = messages;
    }
}
