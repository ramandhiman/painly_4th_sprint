package com.painly.com.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.R;
import com.painly.com.adapters.ChatUsersAdapter;
import com.painly.com.chat.MessageR;
import com.painly.com.chat.MessageUserR;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlySingleton;
import com.painly.com.utils.Utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MessegTabFragment extends Fragment {
    String TAG = MessegTabFragment.this.getClass().getSimpleName();
    View mRootView;

    @BindView(R.id.messagesRV)
    RecyclerView messagesRV;

    @BindView(R.id.withoutUsercHATfRGLL)
    LinearLayout withoutUsercHATfRGLL;

    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeToRefresh;


    Unbinder unbinder;


    DatabaseReference mRootRefrence;
    ChatUsersAdapter mChatUsersAdapter;
    ArrayList<String> mMessageIDArrayList = new ArrayList<String>();
    ArrayList<MessageUserR> mChatUserArrayList = new ArrayList<>();


    public MessegTabFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_messeg_tab, container, false);
        unbinder = ButterKnife.bind(this, mRootView);


        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        AlertDialogManager.hideProgressDialog();

        if (Utilities.isNetworkAvailable(getActivity()))
        {
            if (mChatUserArrayList.size() == 0)
                getChatWithUser();
            else
                setChatUserAdapter();
        }
        else {
            Toast.makeText(getActivity(), getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
          }

        swipeToRefresh.setColorSchemeResources(R.color.dark_red);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeToRefresh.setRefreshing(false);
                getChatWithUser();
            }
        });

        return mRootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void getChatWithUser() {
        AlertDialogManager.showProgressDialog(getActivity());
        mRootRefrence.child("users").child(Constants.USER_ID).child("messages").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mMessageIDArrayList.clear();
                for (DataSnapshot messagesSnapshot : dataSnapshot.getChildren()) {
                    String strMessageID = messagesSnapshot.getKey();
                    Log.e(TAG, "=====MessageID====" + strMessageID);
                    mMessageIDArrayList.add(strMessageID);
                }

                Log.e(TAG, "=====Msg SIZE====" + mMessageIDArrayList.size());
                gettingChatWithUserData();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "===ERROR===" + databaseError.toString());
            }
        });
    }

    private void gettingChatWithUserData() {
        final ArrayList<MessageR> mMessageRarrayList111 = new ArrayList<>();
        for (int i = 0; i < mMessageIDArrayList.size(); i++) {
            mRootRefrence.child("messages").child(mMessageIDArrayList.get(i)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String key = dataSnapshot.getKey();
                    MessageR messageR = dataSnapshot.getValue(MessageR.class);
                    messageR.setMessageID(key);
                    mMessageRarrayList111.add(messageR);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    AlertDialogManager.hideProgressDialog();
                    Log.e(TAG, "====ERRROR====" + databaseError.toString());
                }
            });
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AlertDialogManager.hideProgressDialog();
                getSetChatUser(mMessageRarrayList111);
               // Constants.TAB_CLICK_ONLY = true;
            }
        }, 3000);
    }


    private void getSetChatUser(ArrayList<MessageR> mMessageRarrayList) {
        AlertDialogManager.hideProgressDialog();
        mChatUserArrayList.clear();
        try {
            for (int i = 0; i < mMessageRarrayList.size(); i++) {
                HashMap<String, MessageUserR> mUserHashMap = mMessageRarrayList.get(i).getUsers();

                Iterator<Map.Entry<String, MessageUserR>> it = mMessageRarrayList.get(i).getUsers().entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<String, MessageUserR> pair = (Map.Entry<String, MessageUserR>) it.next();
                    pair.getValue().setuID(pair.getKey());
                    Log.e(TAG, "====KEY====" + pair.getKey());
                    if (!Constants.USER_ID.equals(pair.getValue().getuID())) {
                        if (!mChatUserArrayList.contains(pair.getValue())) {
                            pair.getValue().setmMessagesID(mMessageRarrayList.get(i).getMessageID());
                            mChatUserArrayList.add(pair.getValue());
                        }
                    }
                }
            }

            Collections.reverse(mChatUserArrayList);
            setChatUserAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setChatUserAdapter() {
        if (mChatUserArrayList.size() > 0) {
            if (withoutUsercHATfRGLL != null)
                withoutUsercHATfRGLL.setVisibility(View.GONE);
        } else {
            if (withoutUsercHATfRGLL != null)
                withoutUsercHATfRGLL.setVisibility(View.VISIBLE);
        }

        mChatUsersAdapter = new ChatUsersAdapter(getActivity(), mChatUserArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        messagesRV.setLayoutManager(mLayoutManager);
        messagesRV.setItemAnimator(new DefaultItemAnimator());
        messagesRV.setAdapter(mChatUsersAdapter);
    }

}
