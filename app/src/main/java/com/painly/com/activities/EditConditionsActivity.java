package com.painly.com.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.R;
import com.painly.com.adapters.EditConditionsAdapter;
import com.painly.com.beans.Conditions;
import com.painly.com.beans.UserConditions;
import com.painly.com.font.EditTextArialRegular;
import com.painly.com.interfaces.SelectConditions;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditConditionsActivity extends AppCompatActivity {
    String TAG = EditConditionsActivity.this.getClass().getSimpleName();
    Activity mActivity = EditConditionsActivity.this;

    @BindView(R.id.leftLL)
    LinearLayout leftLL;
    @BindView(R.id.editSearchET)
    EditTextArialRegular editSearchET;
    @BindView(R.id.imgSearchIV)
    ImageView imgSearchIV;
    @BindView(R.id.conditionsRV)
    RecyclerView conditionsRV;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;

    DatabaseReference mRootRefrence;

    EditConditionsAdapter mEditConditionsAdapter;

    ArrayList<Conditions> mConditionsAL = new ArrayList<Conditions>();
    ArrayList<Conditions> filteredList = new ArrayList<Conditions>();
    ArrayList<Conditions> mSelectedTagsArrayList = new ArrayList<Conditions>();
    ArrayList<Conditions> updatedConditionsAL = new ArrayList<Conditions>();
    ArrayList<String> mCurrenUserConditionsAL = new ArrayList<String>();

    String strType = "";


    SelectConditions mSelectConditions = new SelectConditions() {
        @Override
        public void mSelectConditions(Boolean mBoolean, ArrayList<Conditions> mArrayList) {
            if (mBoolean)
                mSelectedTagsArrayList = mArrayList;
            else
                mSelectedTagsArrayList = mArrayList;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_conditions);

        ButterKnife.bind(this);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        if (getIntent() != null) {
            strType = getIntent().getStringExtra("TYPE");
        }
        getConditionsData();
        setSearch();
    }

    private void getConditionsData() {
        mProgressBar.setVisibility(View.VISIBLE);
        mRootRefrence.child("conditions").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mProgressBar.setVisibility(View.GONE);
                for (DataSnapshot formsSnapshot : dataSnapshot.getChildren()) {
                    String conditionID = formsSnapshot.getKey();
                    final Conditions mConditions = formsSnapshot.getValue(Conditions.class);
                    mConditions.setConditionID(conditionID);
                    mConditionsAL.add(mConditions);

                }

                Collections.sort(mConditionsAL, new Comparator<Conditions>() {
                    @Override
                    public int compare(Conditions item, Conditions t1) {
                        String s1 = item.getName();
                        String s2 = t1.getName();
                        return s1.compareToIgnoreCase(s2);
                    }

                });

                getCurrentLoginUserConditions();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mProgressBar.setVisibility(View.GONE);
                Log.e(TAG, "Error::" + databaseError.toString());
                Toast.makeText(mActivity, databaseError.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }


    private void getCurrentLoginUserConditions() {
        mRootRefrence.child("users").child(PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "")).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserConditions mUserConditions = dataSnapshot.getValue(UserConditions.class);
                Collection<String> values = mUserConditions.getConditions().keySet();
                mCurrenUserConditionsAL = new ArrayList<String>(values);

                settingUpPreviousConditions();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "====Error+===" + databaseError.toString());
            }
        });

    }

    private void settingUpPreviousConditions() {
        for (int i = 0; i < mCurrenUserConditionsAL.size(); i++) {
            for (int j = 0; j < mConditionsAL.size(); j++) {
                Conditions mConditions = mConditionsAL.get(j);

                 if (mCurrenUserConditionsAL.get(i).toString().equals(mConditionsAL.get(j).getConditionID())){
                     mConditions.setSelected(true);
                 }
            }
        }

        setAdapter();


    }


    private void setSearch() {
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable mEditable) {
                //after the change calling the method and passing the search input
                String query = mEditable.toString().toLowerCase();
                filteredList.clear();
                for (int i = 0; i < mConditionsAL.size(); i++) {
                    final String text = mConditionsAL.get(i).getName().toLowerCase();
                    if (text.contains(query)) {
                        filteredList.add(mConditionsAL.get(i));
                    }
                }
                mEditConditionsAdapter = new EditConditionsAdapter(mActivity, filteredList, mSelectConditions);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
                conditionsRV.setLayoutManager(mLayoutManager);
                conditionsRV.setAdapter(mEditConditionsAdapter);
            }
        });
    }


    @OnClick({R.id.leftLL, R.id.imgSearchIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.leftLL:
                onBackPressed();
                break;
            case R.id.imgSearchIV:
                break;
        }
    }


    @Override
    public void onBackPressed() {
        if (strType.equals(Constants.FROM_PROFILE)) {
            for (int i = 0; i < mSelectedTagsArrayList.size(); i++){
                if (mSelectedTagsArrayList.get(i).isSelected() == true){
                    updatedConditionsAL.add(mSelectedTagsArrayList.get(i));
                }
            }

            Intent mIntent = new Intent();
            mIntent.putExtra("LIST", updatedConditionsAL);
            setResult(111, mIntent);
            finish();
        }
    }


    private void setAdapter() {
        mEditConditionsAdapter = new EditConditionsAdapter(mActivity, mConditionsAL, mSelectConditions);
        conditionsRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        conditionsRV.setLayoutManager(mLayoutManager);
        conditionsRV.setItemAnimator(new DefaultItemAnimator());
        conditionsRV.setAdapter(mEditConditionsAdapter);
    }

}
