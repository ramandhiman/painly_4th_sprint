package com.painly.com.youtubeSearch;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.painly.com.R;
import com.painly.com.font.EditTextArialRegular;
import com.painly.com.utils.SimpleDividerItemDecoration;
import com.painly.com.utils.Utilities;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class YoutubeSearchActivity extends AppCompatActivity {
    public Dialog progressDialog;
    Activity mActivity = YoutubeSearchActivity.this;
    String TAG = YoutubeSearchActivity.this.getClass().getSimpleName();
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.search_input)
    EditTextArialRegular search_input;
    @BindView(R.id.imgClearEditTextIV)
    ImageView imgClearEditTextIV;
    @BindView(R.id.videos_recycler_view)
    RecyclerView videos_recycler_view;
    @BindView(R.id.mainLL)
    LinearLayout mainLL;
    YoutubeSearchAdapter mYoutubeSearchAdapter;
    YoutubeUrlInterface mYoutubeUrlInterface = new YoutubeUrlInterface() {
        @Override
        public void getYoutubeVideoUrl(VideoItem mVideoItem) {
            Intent mIntent = new Intent();
            mIntent.putExtra("MODEL", mVideoItem);
            setResult(232, mIntent);
            finish();
        }
    };
    //Handler to run a thread which could fill the list after downloading data
    //from the internet and inflating the images, title and description
    private Handler handler;
    //results list of type VideoItem to store the results so that each item
    //int the array list has id, title, description and thumbnail url
    private List<VideoItem> searchResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_search);
        ButterKnife.bind(this);
        setYouTubeSearch();
    }

    private void setYouTubeSearch() {

        videos_recycler_view.setHasFixedSize(true);
        //give RecyclerView a layout manager to set its orientation to vertical
        //by default it is vertical
        videos_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        videos_recycler_view.addItemDecoration(new SimpleDividerItemDecoration(mActivity));

        handler = new Handler();

        search_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    imgClearEditTextIV.setVisibility(View.VISIBLE);
                } else {
                    imgClearEditTextIV.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //add listener to the EditText view which listens to changes that occurs when
        //users changes the text or deletes the text
        //passing object of Textview's EditorActionListener to this method
        search_input.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            //onEditorAction method called when user clicks ok button or any custom
            //button set on the bottom right of keyboard
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //actionId of the respective action is returned as integer which can
                //be checked with our set custom search button in keyboard
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    //setting progress message so that users can understand what is happening
                    showProgressDialog(mActivity);
                    //calling our search method created below with input keyword entered by user
                    //by getText method which returns Editable type, get string by toString method
                    if (Utilities.isNetworkAvailable(mActivity))
                        searchOnYoutube(v.getText().toString());
                    else
                        Toast.makeText(mActivity, getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();

                    //getting instance of the keyboard or any other input from which user types
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    //hiding the keyboard once search button is clicked
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);

                    return false;
                }
                return true;
            }
        });
    }

    //custom search method which takes argument as the keyword for which videos is to be searched
    private void searchOnYoutube(final String keywords) {

        //A thread that will execute the searching and inflating the RecyclerView as and when
        //results are found
        new Thread() {

            //implementing run method
            public void run() {

                //create our YoutubeConnector class's object with Activity context as argument
                YoutubeConnector yc = new YoutubeConnector(mActivity);

                //calling the YoutubeConnector's search method by entered keyword
                //and saving the results in list of type VideoItem class
                searchResults = yc.search(keywords);

                //handler's method used for doing changes in the UI
                handler.post(new Runnable() {

                    //implementing run method of Runnable
                    public void run() {

                        //call method to create Adapter for RecyclerView and filling the list
                        //with thumbnail, title, id and description
                        fillYoutubeVideos();

                        //after the above has been done hiding the ProgressDialog
                        hideProgressDialog();
                    }
                });
            }
            //starting the thread
        }.start();
    }

    //method for creating adapter and setting it to recycler view
    private void fillYoutubeVideos() {

        //object of YoutubeAdapter which will fill the RecyclerView
        mYoutubeSearchAdapter = new YoutubeSearchAdapter(getApplicationContext(), searchResults, mYoutubeUrlInterface);

        //setAdapter to RecyclerView
        videos_recycler_view.setAdapter(mYoutubeSearchAdapter);

        //notify the Adapter that the data has been downloaded so that list can be updapted
        mYoutubeSearchAdapter.notifyDataSetChanged();
    }

    @OnClick({R.id.imgBackIV, R.id.imgClearEditTextIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.imgClearEditTextIV:
                search_input.setText("");
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    public void hideProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }

    }
}
