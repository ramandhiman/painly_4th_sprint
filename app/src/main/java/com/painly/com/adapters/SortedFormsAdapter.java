package com.painly.com.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.painly.com.R;
import com.painly.com.beans.Conditions;
import com.painly.com.interfaces.OnItemClickListener;

import java.util.ArrayList;


/**
 * Created by android-raman on 2/5/18.
 */

public class SortedFormsAdapter extends RecyclerView.Adapter<SortedFormsAdapter.ViewHolder> {
    private ArrayList<Conditions> mArrayList;
    private Activity mActivity;

    public SortedFormsAdapter(Activity mActivity, ArrayList<Conditions> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_details_conditions, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Conditions mConditions = mArrayList.get(position);
        holder.txtTabTV.setText(mConditions.getName());
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTabTV;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTabTV = (TextView) itemView.findViewById(R.id.txtTabTV);
        }


    }
}
