package com.painly.com.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.painly.com.R;
import com.painly.com.activities.NewForumsDetailsActivity;
import com.painly.com.activities.TextDetailsActivity;
import com.painly.com.beans.LibraryModel;

import java.util.ArrayList;


/*
 * Created by android-raman on 2/5/18.
 */

public class LibraryPostsAdapter extends RecyclerView.Adapter<LibraryPostsAdapter.ViewHolder> {
    private ArrayList<LibraryModel> mArrayList;
    private Activity mActivity;

    public LibraryPostsAdapter(Activity mActivity, ArrayList<LibraryModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_library, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final LibraryModel mLibraryModel = mArrayList.get(position);

        if (mLibraryModel.getTitle() != null)
            holder.itemPostTitleTV.setText(mLibraryModel.getTitle());
        if (mLibraryModel.getDescription() != null)
            holder.itemPostDetailsTV.setText(mLibraryModel.getDescription());
        holder.itemLayoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                mIntent.putExtra("FORM_UID", mLibraryModel.getmFormID());
                mActivity.startActivity(mIntent);
            }
        });
        holder.itemParentLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                mIntent.putExtra("FORM_UID", mLibraryModel.getmFormID());
                mActivity.startActivity(mIntent);
            }
        });
        holder.itemPostTitleTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                mIntent.putExtra("FORM_UID", mLibraryModel.getmFormID());
                mActivity.startActivity(mIntent);
            }
        });
        holder.itemPostDetailsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                mIntent.putExtra("FORM_UID", mLibraryModel.getmFormID());
                mActivity.startActivity(mIntent);
            }
        });

       /* holder.itemLayoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLibraryModel.getType().equals("video")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mLibraryModel.getmFormID());
                    mActivity.startActivity(mIntent);
                } else if (mLibraryModel.getType().equals("link")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mLibraryModel.getmFormID());
                    mActivity.startActivity(mIntent);
                } else if (mLibraryModel.getType().equals("text")) {
                    Intent mIntent = new Intent(mActivity, TextDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mLibraryModel.getmFormID());
                    mActivity.startActivity(mIntent);
                } else if (mLibraryModel.getType().equals("picture")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mLibraryModel.getmFormID());
                    mActivity.startActivity(mIntent);
                }
            }
        });
        holder.itemPostDetailsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLibraryModel.getType().equals("video")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mLibraryModel.getmFormID());
                    mActivity.startActivity(mIntent);
                } else if (mLibraryModel.getType().equals("link")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mLibraryModel.getmFormID());
                    mActivity.startActivity(mIntent);
                } else if (mLibraryModel.getType().equals("text")) {
                    Intent mIntent = new Intent(mActivity, TextDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mLibraryModel.getmFormID());
                    mActivity.startActivity(mIntent);
                } else if (mLibraryModel.getType().equals("picture")) {

                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mLibraryModel.getmFormID());
                    mActivity.startActivity(mIntent);
                }
            }
        });
        holder.itemPostTitleTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLibraryModel.getType().equals("video")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mLibraryModel.getmFormID());
                    mActivity.startActivity(mIntent);
                } else if (mLibraryModel.getType().equals("link")) {
                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mLibraryModel.getmFormID());
                    mActivity.startActivity(mIntent);
                } else if (mLibraryModel.getType().equals("text")) {
                    Intent mIntent = new Intent(mActivity, TextDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mLibraryModel.getmFormID());
                    mActivity.startActivity(mIntent);
                } else if (mLibraryModel.getType().equals("picture")) {

                    Intent mIntent = new Intent(mActivity, NewForumsDetailsActivity.class);
                    mIntent.putExtra("FORM_UID", mLibraryModel.getmFormID());
                    mActivity.startActivity(mIntent);
                }
            }
        });
*/

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView itemPostTitleTV, itemPostDetailsTV;
        public LinearLayout itemLayoutLL, itemParentLL;


        public ViewHolder(View itemView) {
            super(itemView);
            itemLayoutLL = (LinearLayout) itemView.findViewById(R.id.itemLayoutLL);
            itemParentLL = (LinearLayout) itemView.findViewById(R.id.itemParentLL);
            itemPostTitleTV = (TextView) itemView.findViewById(R.id.itemPostTitleTV);
            itemPostDetailsTV = (TextView) itemView.findViewById(R.id.itemPostDetailsTV);
        }
    }
}
