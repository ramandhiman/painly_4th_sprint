package com.painly.com.chathead;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.painly.com.R;

/**
 * Created by android-da on 2/4/19.
 */

public class ItemSenderViewHolder extends RecyclerView.ViewHolder {
    public TextView txtMessageTV;
    public LinearLayout loadingLL;


    public ItemSenderViewHolder(View itemView) {
        super(itemView);
        txtMessageTV = (TextView) itemView.findViewById(R.id.txtMessageTV);
        loadingLL = (LinearLayout) itemView.findViewById(R.id.loadingLL);
    }

    public void bindData(final Context context, final ChatHeadModel tempValue, int listCount, int position) {
        if (listCount - 1 == position) {
            loadingLL.setVisibility(View.VISIBLE);
        } else {
            loadingLL.setVisibility(View.GONE);
        }
        txtMessageTV.setVisibility(View.VISIBLE);
        if (tempValue.getTitle() != null && tempValue.getTitle().length() > 0)
            txtMessageTV.setText(tempValue.getTitle());

    }

}
