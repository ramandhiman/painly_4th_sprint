package com.painly.com.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.painly.com.R;
import com.painly.com.font.ButtonGothamMedium;
import com.painly.com.font.EditTextArialRegular;
import com.painly.com.utils.AlertDialogManager;
import com.suke.widget.SwitchButton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUp2Activity extends AppCompatActivity {
    String TAG = SignUp2Activity.this.getClass().getSimpleName();
    Activity mActivity = SignUp2Activity.this;

    @BindView(R.id.editFullNameET)
    EditTextArialRegular editFullNameET;
    @BindView(R.id.editUserNameET)
    EditTextArialRegular editUserNameET;
    @BindView(R.id.editShortBioET)
    EditTextArialRegular editShortBioET;
    @BindView(R.id.switch_button)
    SwitchButton switchButton;
    @BindView(R.id.btnSignUP)
    ButtonGothamMedium btnSignUP;
    @BindView(R.id.imgBackLL)
    LinearLayout imgBackLL;

    String strEmail = "", strPassword = "";
    boolean isMachineLearningEnabled = false;
    ArrayList<String> mUserNameAL = new ArrayList<String>();
    boolean isUserNameExist = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_signup_step2);
        ButterKnife.bind(this);
        if (getIntent() != null) {
            strEmail = getIntent().getStringExtra("EMAIL");
            strPassword = getIntent().getStringExtra("PASSWORD");
            mUserNameAL = (ArrayList<String>) getIntent().getSerializableExtra("UNAL");
        }
        editUserNameET.setHint("@username");

        switchButton.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                isMachineLearningEnabled = isChecked;
            }
        });
    }

    @OnClick({R.id.btnSignUP,R.id.imgBackLL})
    public void onViewClicked(View mView) {
        switch (mView.getId()) {
            case R.id.btnSignUP:
                validate();
                break;
            case R.id.imgBackLL:
                onBackPressed();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void validate() {
        for (int i = 0; i < mUserNameAL.size(); i++) {
            if (editUserNameET.getText().toString().equals(mUserNameAL.get(i))) {
                isUserNameExist = true;
                break;
            }
        }

        if (editUserNameET.getText().toString().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_username));
        } else if (isUserNameExist == true) {
            isUserNameExist = false;
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.username_taken));
        } else if (editUserNameET.getText().toString().contains(" ")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.username_cannot));
        } else if (editFullNameET.getText().toString().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_name));
        } else {
            Intent mIntent = new Intent(mActivity, SignUp3Activity.class);
            mIntent.putExtra("USERNAME", editUserNameET.getText().toString().trim().toLowerCase());
            mIntent.putExtra("NAME", editFullNameET.getText().toString().trim());
            mIntent.putExtra("EMAIL", strEmail);
            mIntent.putExtra("PASSWORD", strPassword);
            mIntent.putExtra("BIO", editShortBioET.getText().toString().trim());
            mIntent.putExtra("M_LANGUAGE",isMachineLearningEnabled);
//            mIntent.putExtra("GENDER", strGender);
            startActivity(mIntent);
        }

    }
}
