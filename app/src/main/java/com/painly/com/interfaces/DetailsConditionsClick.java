package com.painly.com.interfaces;

import com.painly.com.beans.Conditions;

/**
 * Created by android-da on 10/3/18.
 */

public interface DetailsConditionsClick {
    public void getConditionsClick(Conditions mConditions);
}
