package com.painly.com.chathead;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.painly.com.R;
import com.painly.com.utils.Constants;

import java.util.ArrayList;

/**
 * Created by android-da on 2/4/19.
 */

public class ChatHeadAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Activity mActivity;
    ArrayList<ChatHeadModel> mArrayList;

    public ChatHeadAdapter(Activity mActivity, ArrayList<ChatHeadModel> mArrayList/*, HashMap<String, String> mHashMap*/) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == Constants.VIEW_TYPE_SENDER) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.item_chathead_right, parent, false);
            return new ItemSenderViewHolder(view);
        } else if (viewType == Constants.VIEW_TYPE_RECIEVER) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.item_chathead_left, parent, false);
            return new ItemRecieverViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == Constants.VIEW_TYPE_SENDER) {
            ((ItemSenderViewHolder) holder).bindData(mActivity, (ChatHeadModel) mArrayList.get(position),mArrayList.size(),position);
        } else if (holder.getItemViewType() == Constants.VIEW_TYPE_RECIEVER) {
            ((ItemRecieverViewHolder) holder).bindData(mActivity, (ChatHeadModel) mArrayList.get(position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mArrayList.get(position).getIntChatType() == 1)
            return Constants.VIEW_TYPE_SENDER;
        else if (mArrayList.get(position).getIntChatType() == 0)
            return Constants.VIEW_TYPE_RECIEVER;
        return 0;
    }

    @Override
    public int getItemCount() {
        if (mArrayList == null)
            return 0;
        return mArrayList.size();
    }
}
