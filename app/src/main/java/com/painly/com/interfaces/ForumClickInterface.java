package com.painly.com.interfaces;

import com.painly.com.beans.FormsModel;

/**
 * Created by android-da on 7/25/18.
 */

public interface ForumClickInterface {
    public void forumClick(FormsModel mModel, int position);
}
