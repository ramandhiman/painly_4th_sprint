package com.painly.com.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.painly.com.BaseActivity;
import com.painly.com.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TermsActivity extends BaseActivity {

    String TAG = TermsActivity.this.getClass().getSimpleName();
    Activity mActivity = TermsActivity.this;
    @BindView(R.id.layoutLL)
    LinearLayout layoutLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        ButterKnife.bind(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @OnClick(R.id.layoutLL)
    public void onViewClicked() {
        onBackPressed();
    }
}
