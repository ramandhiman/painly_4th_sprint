package com.painly.com.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.painly.com.R;
import com.painly.com.beans.Conditions;
import com.painly.com.interfaces.OnItemClickListener;

import java.util.ArrayList;


/**
 * Created by android-raman on 2/5/18.
 */

public class SignUpCondtionAdapter extends RecyclerView.Adapter<SignUpCondtionAdapter.ViewHolder> {
    private ArrayList<Conditions> mArrayList;
    private Activity mActivity;

    public SignUpCondtionAdapter(Activity mActivity, ArrayList<Conditions> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_signup_conditions, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Conditions mConditions = mArrayList.get(position);
        holder.itemTitleTV.setText(mConditions.getName());
//        holder.imgRemoveConditionIV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mArrayList.remove(position);
//                notifyDataSetChanged();
//            }
//        });

        holder.imgRemoveConditionIV.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView itemTitleTV;
        public ImageView imgRemoveConditionIV;

        public ViewHolder(View itemView) {
            super(itemView);
            itemTitleTV = (TextView) itemView.findViewById(R.id.itemTitleTV);
            imgRemoveConditionIV = (ImageView) itemView.findViewById(R.id.imgRemoveConditionIV);

        }


    }
}
