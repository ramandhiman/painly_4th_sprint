package com.painly.com.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.activities.PainPalLocationActivity;
import com.painly.com.beans.User;
import com.painly.com.interfaces.PainPalsSelectionInterface;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by android-raman on 2/5/18.
 */

public class NewPainPalsAdapter extends RecyclerView.Adapter<NewPainPalsAdapter.ViewHolder> {
    String TAG = NewPainPalsAdapter.this.getClass().getSimpleName();
    private ArrayList<User> mArrayList;
    private Activity mActivity;
    PainPalsSelectionInterface mPainPalsSelectionInterface;

    public NewPainPalsAdapter(Activity mActivity, ArrayList<User> mArrayList,PainPalsSelectionInterface mPainPalsSelectionInterface) {
        this.mArrayList = mArrayList;
        this.mActivity = mActivity;
        this.mPainPalsSelectionInterface = mPainPalsSelectionInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_newpainpals, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final User mUser = mArrayList.get(position);

        if (mUser.getDetails().getImage() != null && mUser.getDetails().getImage().contains("http")) {
            Glide.with(mActivity)
                    .load(mUser.getDetails().getImage())
                    .apply(RequestOptions.placeholderOf(mUser.getDetails().getPlaceholderImage()).error(mUser.getDetails().getPlaceholderImage()))
                    .into(holder.profileRIV);
        } else {
            holder.profileRIV.setImageResource(mUser.getDetails().getPlaceholderImage());
        }

        if (mUser.getDetails() != null) {
            if (mUser.getDetails().getName() != null)
                holder.txtNameTV.setText(mUser.getDetails().getName());

            if (mUser.getDetails().getUsername() != null) {
                if (mUser.getDetails().getUsername().contains("@")) {
                    holder.txtUserNameTV.setText(mUser.getDetails().getUsername());
                } else {
                    holder.txtUserNameTV.setText("@" + mUser.getDetails().getUsername());
                }
            }

            if (mUser.getDetails().getBio() != null)
                holder.txtBioTV.setText(mUser.getDetails().getBio());
        }


        if (mUser.isSelected()) {
            holder.imgCheckWhiteGreyIV.setImageResource(R.drawable.ic_check_white);
            holder.layoutCheckLL.setBackgroundColor(mActivity.getResources().getColor(R.color.colorPrimary));
        } else {
            holder.imgCheckWhiteGreyIV.setImageResource(R.drawable.ic_check_grey);
            holder.layoutCheckLL.setBackgroundColor(mActivity.getResources().getColor(R.color.white));
        }

        /*Location Icon*/
        if (mUser.getDetails() != null) {
            if (mUser.getDetails().getLocation() != null) {
                if (mUser.getDetails().getLocation().getLatitude() != null && mUser.getDetails().getLocation().getLongitude() != null) {
                    holder.imgLocationIV.setVisibility(View.VISIBLE);
                } else {
                    holder.imgLocationIV.setVisibility(View.GONE);
                }
            } else {
                holder.imgLocationIV.setVisibility(View.GONE);
            }
        } else {
            holder.imgLocationIV.setVisibility(View.GONE);
        }


        holder.itemLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mArrayList.get(holder.getAdapterPosition()).isSelected() == true) {
                    mArrayList.get(holder.getAdapterPosition()).setSelected(false);
                    mPainPalsSelectionInterface.getPainPalsAsFavorite(mArrayList);
                } else if (mArrayList.get(holder.getAdapterPosition()).isSelected() == false) {
                    mArrayList.get(holder.getAdapterPosition()).setSelected(true);
                    mPainPalsSelectionInterface.getPainPalsAsFavorite(mArrayList);
                }
                notifyDataSetChanged();
            }
        });

        holder.imgLocationIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, PainPalLocationActivity.class);
                mIntent.putExtra("MODEL", mUser);
                mActivity.startActivity(mIntent);
            }
        });


    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout itemLL, layoutCheckLL;
        public CircleImageView profileRIV;
        public CheckBox checkboxCB;
        public ImageView imgCheckWhiteGreyIV, imgLocationIV;
        public TextView txtNameTV, txtUserNameTV, txtBioTV;

        public ViewHolder(View itemView) {
            super(itemView);
            itemLL = (LinearLayout) itemView.findViewById(R.id.itemLL);
            layoutCheckLL = (LinearLayout) itemView.findViewById(R.id.layoutCheckLL);

            profileRIV = (CircleImageView) itemView.findViewById(R.id.profileRIV);
            checkboxCB = (CheckBox) itemView.findViewById(R.id.checkboxCB);
            imgCheckWhiteGreyIV = (ImageView) itemView.findViewById(R.id.imgCheckWhiteGreyIV);
            imgLocationIV = (ImageView) itemView.findViewById(R.id.imgLocationIV);

            txtNameTV = (TextView) itemView.findViewById(R.id.txtNameTV);
            txtUserNameTV = (TextView) itemView.findViewById(R.id.txtUserNameTV);
            txtBioTV = (TextView) itemView.findViewById(R.id.txtBioTV);

        }

    }
}
