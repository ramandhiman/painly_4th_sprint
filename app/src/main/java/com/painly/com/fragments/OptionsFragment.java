package com.painly.com.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.painly.com.R;
import com.painly.com.activities.EditConditionsActivity;
import com.painly.com.activities.GoPremiumActivity;
import com.painly.com.activities.MyLibraryActivity;
import com.painly.com.activities.NewPainPalsActivity;
import com.painly.com.activities.OpenLinkActivity;
import com.painly.com.activities.PainPalActivity;
import com.painly.com.beans.Conditions;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;
import com.painly.com.utils.Utilities;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class OptionsFragment extends Fragment {
    String TAG = OptionsFragment.this.getClass().getSimpleName();
    View mRooTView;
    @BindView(R.id.emailAddressLL)
    LinearLayout emailAddressLL;
    @BindView(R.id.passwordLL)
    LinearLayout passwordLL;
    @BindView(R.id.dispalyNameLL)
    LinearLayout dispalyNameLL;
    @BindView(R.id.conditionsInterestLL)
    LinearLayout conditionsInterestLL;
    @BindView(R.id.publicBiographyLL)
    LinearLayout publicBiographyLL;
    @BindView(R.id.rematchPainPalsLL)
    LinearLayout rematchPainPalsLL;
    @BindView(R.id.termsConditionsLL)
    LinearLayout termsConditionsLL;
    @BindView(R.id.aboutPainlyLL)
    LinearLayout aboutPainlyLL;
    @BindView(R.id.emailFeedbackLL)
    LinearLayout emailFeedbackLL;

    @BindView(R.id.emailSavedLibraryLL)
    LinearLayout emailSavedLibraryLL;

    Unbinder unbinder;

    DatabaseReference mRootRefrence;
    ArrayList<Conditions> mSelectedConditionsArrayList = new ArrayList<Conditions>();
    @BindView(R.id.premiumLL)
    LinearLayout premiumLL;
    @BindView(R.id.addInviteCodeLL)
    LinearLayout addInviteCodeLL;

    public OptionsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRooTView = inflater.inflate(R.layout.fragment_options, container, false);
        unbinder = ButterKnife.bind(this, mRooTView);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        return mRooTView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.emailAddressLL, R.id.passwordLL, R.id.dispalyNameLL, R.id.conditionsInterestLL, R.id.publicBiographyLL, R.id.rematchPainPalsLL, R.id.termsConditionsLL, R.id.aboutPainlyLL, R.id.emailFeedbackLL, R.id.premiumLL, R.id.addInviteCodeLL, R.id.emailSavedLibraryLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.emailAddressLL:
                updateEmailAddressDialog();
                break;
            case R.id.passwordLL:
                resetPasswordDialog();
                break;
            case R.id.dispalyNameLL:
                updateNameDialog();
                break;
            case R.id.conditionsInterestLL:
                Intent mIntent111 = new Intent(getActivity(), EditConditionsActivity.class);
                mIntent111.putExtra("TYPE", Constants.FROM_PROFILE);
                startActivityForResult(mIntent111, 111);
                break;
            case R.id.publicBiographyLL:
                updateBioDialog();
                break;
            case R.id.rematchPainPalsLL:
//                Intent painpalIntent = new Intent(getActivity(), PainPalActivity.class);
                Intent painpalIntent = new Intent(getActivity(), NewPainPalsActivity.class);
                painpalIntent.putExtra("TYPE", Constants.FROM_PROFILE);
                startActivity(painpalIntent);
                break;
            case R.id.termsConditionsLL:
                Intent termsIntent = new Intent(getActivity(), OpenLinkActivity.class);
                termsIntent.putExtra("LINK", Constants.TERMS_LINK);
                startActivity(termsIntent);
                break;
            case R.id.aboutPainlyLL:
                Intent aboutIntent = new Intent(getActivity(), OpenLinkActivity.class);
                aboutIntent.putExtra("LINK", Constants.ABOUT_PAINLY_LINK);
                startActivity(aboutIntent);
                break;
            case R.id.emailFeedbackLL:
                sendPainlyFeedback();
                break;
            case R.id.premiumLL:
//                Intent premiumIntent = new Intent(getActivity(), GoPremiumActivity.class);
//                startActivity(premiumIntent);
                Toast.makeText(getActivity(),"Coming Soon...",Toast.LENGTH_SHORT).show();
                break;
            case R.id.addInviteCodeLL:
                //showAddInviteCodeDialog();
                Toast.makeText(getActivity(),"Coming Soon...",Toast.LENGTH_SHORT).show();
                break;

            case R.id.emailSavedLibraryLL:
                Intent libraryIntent = new Intent(getActivity(), MyLibraryActivity.class);
                startActivity(libraryIntent);
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == 111) {
                mSelectedConditionsArrayList = (ArrayList<Conditions>) data.getSerializableExtra("LIST");

                if (mSelectedConditionsArrayList.size() > 0) {
                    editYourInterst(mSelectedConditionsArrayList);
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    //*Update Interests*//*
    private void editYourInterst(ArrayList<Conditions> mList) {
        HashMap<String, String> mConditionsHM = new HashMap<String, String>();
        for (int i = 0; i < mList.size(); i++) {
            mConditionsHM.put(mList.get(i).getConditionID(), mList.get(i).getName());
        }

        HashMap<String, Object> parentHM = new HashMap<>();
        parentHM.put("conditions", mConditionsHM);
        mRootRefrence.child("users").child(PainlyPreference.readString(getActivity(), PainlyPreference.USER_ID, "")).updateChildren(parentHM);
    }

    //*Send Feedback*//*
    private void sendPainlyFeedback() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "support@painly.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback for Painly");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Reference id:\n" + PainlyPreference.readString(getActivity(), PainlyPreference.USER_ID, ""));
        startActivity(Intent.createChooser(emailIntent, "Send email..."));

    }

    //*Update Display Name*//*
    private void updateNameDialog() {
        final Dialog alertDialog = new Dialog(getActivity(), R.style.PauseDialog);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_updatename);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final EditText editNameET = (EditText) alertDialog.findViewById(R.id.editNameET);
        editNameET.setText(NewProfileFragment.txtNameTV.getText().toString());
        editNameET.setSelection(editNameET.getText().toString().length());
        Button btnSaveB = (Button) alertDialog.findViewById(R.id.btnSaveB);
        Button btnCancelB = (Button) alertDialog.findViewById(R.id.btnCancelB);

        btnCancelB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        btnSaveB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editNameET.getText().toString().trim().equals("")) {
                    editNameET.setError(getString(R.string.please_enter_name));
                } else {
                    alertDialog.dismiss();
                    updateNameExecute(editNameET.getText().toString().trim());
                }

            }
        });

        alertDialog.show();
    }

    private void updateNameExecute(String strName) {
        //*Update Name*//*
        mRootRefrence.child("users").child(PainlyPreference.readString(getActivity(), PainlyPreference.USER_ID, "")).child("details").child("name").setValue(strName);
        NewProfileFragment.txtNameTV.setText(strName);
    }


    //*Update Name Dialog*//*
    private void updateBioDialog() {
        final Dialog alertDialog = new Dialog(getActivity(), R.style.PauseDialog);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_update_bio);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        final EditText editBioET = (EditText) alertDialog.findViewById(R.id.editBioET);
        editBioET.setText(NewProfileFragment.USER_BIOGRAPHY);
        editBioET.setSelection(NewProfileFragment.USER_BIOGRAPHY.length());
        Button btnSaveB = (Button) alertDialog.findViewById(R.id.btnSaveB);

        btnSaveB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editBioET.getText().toString().trim().equals("")) {
                    editBioET.setError(getString(R.string.please_enter_bio));
                } else {
                    alertDialog.dismiss();
                    updateBioExecute(editBioET.getText().toString().trim());
                }

            }
        });

        alertDialog.show();
    }

    private void updateBioExecute(String strBio) {
        //*Update Name*//*
        mRootRefrence.child("users").child(PainlyPreference.readString(getActivity(), PainlyPreference.USER_ID, "")).child("details").child("bio").setValue(strBio);
    }


    //*Update Email Address Dialog*//*
    private void updateEmailAddressDialog() {
        final Dialog alertDialog = new Dialog(getActivity(), R.style.PauseDialog);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_update_emailaddress);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final EditText editEmailAddressET = (EditText) alertDialog.findViewById(R.id.editEmailAddressET);
        final EditText editPasswordET = (EditText) alertDialog.findViewById(R.id.editPasswordET);
        Button btnSaveB = (Button) alertDialog.findViewById(R.id.btnSaveB);
        Button btnCancelB = (Button) alertDialog.findViewById(R.id.btnCancelB);
        btnCancelB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        btnSaveB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editEmailAddressET.getText().toString().trim().equals("")) {
                    editEmailAddressET.setError(getString(R.string.please_enter_email));
                } else if (!Utilities.isValidEmaillId(editEmailAddressET.getText().toString().trim())) {
                    editEmailAddressET.setError(getString(R.string.please_enter_valid_email));
                } else if (editPasswordET.getText().toString().trim().equals("")) {
                    editPasswordET.setError(getString(R.string.please_enter_password));
                } else {
                    alertDialog.dismiss();
                    reLoginForUpdateEmail(editEmailAddressET.getText().toString().trim(), editPasswordET.getText().toString().trim());
                }

            }
        });

        alertDialog.show();
    }

    private void reLoginForUpdateEmail(String email, String password) {

        try {
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                changeEmailDialog();
                            } else {
                                String strError = task.getException().toString();
                                Log.e(TAG, "====ERROR===" + strError);
                                String mErrorArray[] = strError.split(":");
                                String strMessage = mErrorArray[1];
                                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.error), strMessage);
                            }

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    //*Change Email Dialog*//*
    private void changeEmailDialog() {
        final Dialog alertDialog = new Dialog(getActivity(), R.style.PauseDialog);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_change_email);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final EditText editEmailAddressET = (EditText) alertDialog.findViewById(R.id.editEmailAddressET);
        Button btnSaveB = (Button) alertDialog.findViewById(R.id.btnSaveB);
        Button btnCancelB = (Button) alertDialog.findViewById(R.id.btnCancelB);
        btnCancelB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        btnSaveB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editEmailAddressET.getText().toString().trim().equals("")) {
                    editEmailAddressET.setError(getString(R.string.please_enter_email));
                } else if (!Utilities.isValidEmaillId(editEmailAddressET.getText().toString().trim())) {
                    editEmailAddressET.setError(getString(R.string.please_enter_valid_email));
                } else {
                    alertDialog.dismiss();
                    changeEmailAddress(editEmailAddressET.getText().toString().trim());
                }

            }
        });

        alertDialog.show();
    }

    private void changeEmailAddress(String email) {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        user.updateEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User email address updated.");
                            String strEmail = user.getEmail();
                            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.email_updated), getString(R.string.you_have_updated_your_email) + strEmail);
                        } else {
                            String strError = task.getException().toString();
                            Log.e(TAG, "====ERROR===" + strError);
                            String mErrorArray[] = strError.split(":");
                            String strMessage = mErrorArray[1];
                            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.error), strMessage);
                        }
                    }
                });
    }


    //*Change Email Dialog*//*
    private void resetPasswordDialog() {
        final Dialog alertDialog = new Dialog(getActivity(), R.style.PauseDialog);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_password_reset);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final TextView txtEmailTV = (TextView) alertDialog.findViewById(R.id.txtEmailTV);
        Button btnDismiss = (Button) alertDialog.findViewById(R.id.btnDismiss);

        final String strEmail = PainlyPreference.readString(getActivity(), PainlyPreference.USER_EMAIL_ID, "");

        txtEmailTV.setText(getString(R.string.we_have_sent_your) + strEmail);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                resetPassword(strEmail);
            }
        });

        alertDialog.show();
    }

    private void resetPassword(String strEmail) {
        FirebaseAuth.getInstance().sendPasswordResetEmail(strEmail).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "Email sent.");
                    Toast.makeText(getActivity(), "Email sent Sucessfully", Toast.LENGTH_LONG).show();
                } else {
                    String strError = task.getException().toString();
                    Log.e(TAG, "====ERROR===" + strError);
                    String mErrorArray[] = strError.split(":");
                    String strMessage = mErrorArray[1];
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.error), strMessage);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "====ERROR===" + e.toString());
            }
        });
    }


    private void showAddInviteCodeDialog() {
        final Dialog addInviteCodeDialog = new Dialog(getActivity(), R.style.PauseDialog);
        addInviteCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addInviteCodeDialog.setContentView(R.layout.dialog_add_invitecode);
        addInviteCodeDialog.setCanceledOnTouchOutside(false);
        addInviteCodeDialog.setCancelable(false);
        addInviteCodeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final EditText editInviteCode = (EditText) addInviteCodeDialog.findViewById(R.id.editInviteCode);
        Button btnCancelB = (Button) addInviteCodeDialog.findViewById(R.id.btnCancelB);
        Button btnSaveB = (Button) addInviteCodeDialog.findViewById(R.id.btnSaveB);

        btnCancelB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addInviteCodeDialog.dismiss();
            }
        });
        btnSaveB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editInviteCode.getText().toString().trim().equals("")) {
                    editInviteCode.setError(getString(R.string.please_invite_enter_code));
                } else {
                    addInviteCodeDialog.dismiss();
                    /*Perform Invite code functionality*/
                    showInviteCodeVerificationDialog();
                }

            }
        });

        addInviteCodeDialog.show();
    }

    private void showInviteCodeVerificationDialog() {
        final Dialog verifyDialog = new Dialog(getActivity(), R.style.PauseDialog);
        verifyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verifyDialog.setContentView(R.layout.dialog_invitecode_verification);
        verifyDialog.setCanceledOnTouchOutside(false);
        verifyDialog.setCancelable(false);
        verifyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button btnLetGoB = (Button) verifyDialog.findViewById(R.id.btnLetGoB);

        btnLetGoB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyDialog.dismiss();
            }
        });

        verifyDialog.show();
    }

}
