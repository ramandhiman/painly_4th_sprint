package com.painly.com.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.painly.com.R;
import com.painly.com.utils.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OpenBrowseLinkActivity extends AppCompatActivity {
    Activity mActivity = OpenBrowseLinkActivity.this;
    String TAG = OpenBrowseLinkActivity.this.getClass().getSimpleName();
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.imgMessageIV)
    ImageView imgMessageIV;
    @BindView(R.id.openLinkWV)
    WebView openLinkWV;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;

    String strLinkURL = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_browse_link);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            strLinkURL = getIntent().getStringExtra("LINK");
        }

        if (!Utilities.isNetworkAvailable(mActivity)){
            Toast.makeText(mActivity,getString(R.string.internetconnection),Toast.LENGTH_LONG).show();
        }else{
            openWebView();
        }
    }

    @OnClick({R.id.imgBackIV, R.id.imgMessageIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.imgMessageIV:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void openWebView() {
        openLinkWV.getSettings().setJavaScriptEnabled(true);
        openLinkWV.getSettings().setLoadWithOverviewMode(true);
        openLinkWV.getSettings().setUseWideViewPort(true);
        openLinkWV.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                mProgressBar.setVisibility(View.VISIBLE);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                mProgressBar.setVisibility(View.GONE);
            }
        });
        openLinkWV.loadUrl(strLinkURL);
    }



}
