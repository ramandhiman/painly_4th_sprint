package com.painly.com.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.painly.com.BaseActivity;
import com.painly.com.R;
import com.painly.com.utils.Utilities;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;

import java.net.MalformedURLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PlayVideoActivity extends BaseActivity {
    Activity mActivity = PlayVideoActivity.this;
    String TAG = PlayVideoActivity.this.getClass().getSimpleName();
    String strVideoLink = "";

    @BindView(R.id.youtube_player_view)
    YouTubePlayerView youtubePlayerView;
    @BindView(R.id.imgCloseIV)
    ImageView imgCloseIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);
        ButterKnife.bind(this);
        if (getIntent() != null) {
            strVideoLink = getIntent().getStringExtra("VIDEO_URL");
            playYoutubeVideo(strVideoLink);
        }
    }


    private void playYoutubeVideo(String strURL) {
        String videoId = "";
        try {
            videoId = Utilities.extractYoutubeId(strURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        final String finalVideoId = videoId;
        youtubePlayerView.initialize(new YouTubePlayerInitListener() {
            @Override
            public void onInitSuccess(final YouTubePlayer initializedYouTubePlayer) {
                initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                    @Override
                    public void onReady() {
                        initializedYouTubePlayer.loadVideo(finalVideoId, 0);
                    }
                });
            }
        }, true);

    }

    @OnClick(R.id.imgCloseIV)
    public void onViewClicked() {
        youtubePlayerView.release();
        onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        youtubePlayerView.release();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void setCustomUIYoutube(){

    }
}
