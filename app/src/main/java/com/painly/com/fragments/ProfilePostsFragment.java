package com.painly.com.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.R;
import com.painly.com.adapters.ProfilePostsAdapter;
import com.painly.com.beans.FormsModel;
import com.painly.com.beans.User;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Constants;
import com.painly.com.utils.SimpleDividerItemDecoration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfilePostsFragment extends Fragment {
    String TAG = ProfilePostsFragment.this.getClass().getSimpleName();
    View mView;
    @BindView(R.id.postsRV)
    RecyclerView postsRV;
    Unbinder unbinder;
    ProfilePostsAdapter mProfilePostsAdapter;
    ArrayList<FormsModel> mPostsArraylist = new ArrayList<>();
    DatabaseReference mRootRefrence;
    @BindView(R.id.withoutSavedLL)
    LinearLayout withoutSavedLL;

    public ProfilePostsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_profile_posts, container, false);
        unbinder = ButterKnife.bind(this, mView);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        getUserForums();
        setAdapter();
        return mView;
    }

    private void getUserForums() {
        mRootRefrence.child("users").child(Constants.USER_ID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    User mUser = dataSnapshot.getValue(User.class);
                    HashMap<String, String> mForumsHM = mUser.getForums();
                    Collection<String> values = mForumsHM.keySet();
                    ArrayList<String> strUserFormsAL = new ArrayList<String>(values);
                    getForumData(strUserFormsAL);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "==ERROR==");
            }
        });
    }

    private void getForumData(ArrayList<String> strUserFormsAL) {
        mPostsArraylist.clear();
        for (int i = 0; i < strUserFormsAL.size(); i++) {
            mRootRefrence.child("forums").child(strUserFormsAL.get(i)).orderByKey().addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getKey() != null) {
                        String key = dataSnapshot.getKey();
                        FormsModel tttModel = dataSnapshot.getValue(FormsModel.class);
                        if (tttModel != null) {
                            tttModel.setFormID(key);
                            if (tttModel.getIsHidden() instanceof Boolean == false) {
                                if (contains(mPostsArraylist, tttModel.getFormID()) == false) {
                                    mPostsArraylist.add(tttModel);
                                    Collections.sort(mPostsArraylist);
                                    mProfilePostsAdapter.notifyDataSetChanged();
                                    if (withoutSavedLL != null)
                                        withoutSavedLL.setVisibility(View.GONE);
                                }

                            }

                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, "====ERROR===" + databaseError.toString());
                    AlertDialogManager.hideProgressDialog();
                }
            });
        }
    }


    boolean contains(ArrayList<FormsModel> list, String mFormID) {
        for (FormsModel item : list) {
            if (item.getFormID().equals(mFormID)) {
                return true;
            }
        }
        return false;
    }

    private void setAdapter() {
        if (mPostsArraylist.size() > 0) {
            withoutSavedLL.setVisibility(View.GONE);
        } else {
            withoutSavedLL.setVisibility(View.VISIBLE);
        }
        mProfilePostsAdapter = new ProfilePostsAdapter(getActivity(), mPostsArraylist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        postsRV.setLayoutManager(mLayoutManager);
        postsRV.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        postsRV.setAdapter(mProfilePostsAdapter);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
