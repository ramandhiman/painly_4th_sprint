package com.painly.com.chat;

import java.io.Serializable;

/**
 * Created by android-da on 6/12/18.
 */

public class MessageDataR implements Serializable
{
    private String sender;
    private String text;
    private String timestamp;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
