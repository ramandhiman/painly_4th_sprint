package com.painly.com.chat;

import java.io.Serializable;

/**
 * Created by android-da on 6/12/18.
 */

public class ChatUsersModel implements Serializable{
    Boolean hasUnread = false;
    String image = "";
    String name = "";

    public Boolean getHasUnread() {
        return hasUnread;
    }

    public void setHasUnread(Boolean hasUnread) {
        this.hasUnread = hasUnread;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
