package com.painly.com.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.painly.com.R;
import com.painly.com.beans.PostImageModel;
import com.painly.com.interfaces.SelectImageInterface;


import java.util.ArrayList;


/*
 * Created by android-raman on 2/5/18.
 */

public class PostImagesAdapter extends RecyclerView.Adapter<PostImagesAdapter.ViewHolder> {
    private ArrayList<PostImageModel> mArrayList;
    private Activity mActivity;
    private int lastCheckedPosition = -1;
    boolean isSelect = false;
    SelectImageInterface mSelectImageInterface;

    public PostImagesAdapter(Activity mActivity, ArrayList<PostImageModel> mArrayList,SelectImageInterface mSelectImageInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mSelectImageInterface = mSelectImageInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post_images, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final PostImageModel mPostImageModel = mArrayList.get(position);
        //SetUpdata:

        if (mPostImageModel.getStrImageUrl().contains("http")) {
            Glide.with(mActivity.getApplicationContext())
                    .load(mPostImageModel.getStrImageUrl())
                    .apply(RequestOptions.placeholderOf(R.drawable.icon_preview_ph).error(R.drawable.icon_preview_ph))
                    .into(holder.imgLinkPreviewIV);
        } else {
            //Convert ByteArray to Bitmap:
            if (mPostImageModel.getByteArray() != null){
                Bitmap bmp = BitmapFactory.decodeByteArray(mPostImageModel.getByteArray(), 0, mPostImageModel.getByteArray().length);
                holder.imgLinkPreviewIV.setImageBitmap(bmp);
            }

        }


        if (mPostImageModel.getSelected()) {
            holder.imgSelectUnSelectIV.setVisibility(View.VISIBLE);
        } else {
            holder.imgSelectUnSelectIV.setVisibility(View.GONE);
        }


        if (isSelect){
            holder.radioBTN.setChecked(position == lastCheckedPosition);
            if (holder.radioBTN.isChecked()){
                holder.imgSelectUnSelectIV.setVisibility(View.VISIBLE);
                if (mPostImageModel.getStrImageUrl().contains("http")) {
                    mSelectImageInterface.getSelectedImgUrl(mArrayList.get(position).getStrImageUrl());
                } else {
                    mSelectImageInterface.getSelectedImgByteArray(mArrayList.get(position).getmImageUri());
                }
            }
        }else{
            holder.radioBTN.setChecked(position == mArrayList.size() - 1);
            if (holder.radioBTN.isChecked()){
                holder.imgSelectUnSelectIV.setVisibility(View.VISIBLE);
                if (mPostImageModel.getStrImageUrl().contains("http")) {
                    mSelectImageInterface.getSelectedImgUrl(mArrayList.get(position).getStrImageUrl());
                } else {
                    mSelectImageInterface.getSelectedImgByteArray(mArrayList.get(position).getmImageUri());
                }
            }
        }


        holder.radioBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastCheckedPosition = holder.getAdapterPosition();
                isSelect = true;
                //because of this blinking problem occurs so
                //i have a suggestion to add notifyDataSetChanged();
                //notifyItemRangeChanged(0, list.length);//blink list problem
                notifyDataSetChanged();
            }
        });

    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgLinkPreviewIV, imgSelectUnSelectIV;
        public RadioButton radioBTN;

        public ViewHolder(View itemView) {
            super(itemView);
            imgLinkPreviewIV = (ImageView) itemView.findViewById(R.id.imgLinkPreviewIV);
            imgSelectUnSelectIV = (ImageView) itemView.findViewById(R.id.imgSelectUnSelectIV);
            radioBTN = (RadioButton) itemView.findViewById(R.id.radioBTN);

        }
    }
}
