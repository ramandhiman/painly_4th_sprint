package com.painly.com.chat;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.painly.com.BaseActivity;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.activities.NewAllUsersProfileActivity;
import com.painly.com.beans.User;
import com.painly.com.font.EditTextArialRegular;
import com.painly.com.font.TextViewArialBold;
import com.painly.com.font.TextViewArialRegular;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;
import com.painly.com.utils.Utilities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatwithUserActivity extends BaseActivity {
    public final int GALLERY_REQUEST = 222;
    public Bitmap mBitmapImage;
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;
    public StorageReference mStorageRef;
    Activity mActivity = ChatwithUserActivity.this;
    String TAG = ChatwithUserActivity.this.getClass().getSimpleName();
    //User mUser;
    @BindView(R.id.leftLL)
    LinearLayout leftLL;
    @BindView(R.id.txtNameNameTV)
    TextViewArialBold txtNameNameTV;
    @BindView(R.id.txtUserNameTV)
    TextViewArialRegular txtUserNameTV;
    @BindView(R.id.rightLL)
    LinearLayout rightLL;
    @BindView(R.id.chatRecyclerViewRV)
    RecyclerView chatRecyclerViewRV;
    @BindView(R.id.editMessageET)
    EditTextArialRegular editMessageET;
    @BindView(R.id.imgPutMessageIV)
    ImageView imgPutMessageIV;
    @BindView(R.id.bottomLayoutLL)
    LinearLayout bottomLayoutLL;
    @BindView(R.id.imgProfilePIC)
    com.makeramen.roundedimageview.RoundedImageView imgProfilePIC;
    @BindView(R.id.imgAddImageInChatIV)
    ImageView imgAddImageInChatIV;
    DatabaseReference mRootReference, mMessageImageRef;
    String strPushToken = "";
    String strMessage = "";

    ChatWithUserAdapter mChatWithUserAdapter;

    ArrayList<String> mSenderMsgArrayList = new ArrayList<String>();
    ArrayList<String> mRecieverMsgArrayList = new ArrayList<String>();

    ArrayList<MessageModel> mMessagesArrayList = new ArrayList<MessageModel>();

    boolean isUserAlreadyChat = false;
    String strChatMessegeNodeID = "";

    String strUID = "";
    String strRecieverToken = "";
    String strRecieverDeviceType = "ios";
    String strMessageImage = "";

    User mUser;
    private int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatwith_user);
        ButterKnife.bind(this);
        mRootReference = FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        strPushToken = FirebaseInstanceId.getInstance().getToken();

        if (getIntent() != null) {
            strUID = getIntent().getStringExtra("UID");
            position = getIntent().getIntExtra("POSITION", 0);
            getRecieverUserDetails();
        }
    }


/*    private void updateChatReadStatus(final String strMessageID, final String mTAG) {
        mRootReference.child("messages").child(strMessageID).child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<MessageUserR> mList = new ArrayList<>();
                for (DataSnapshot mDataSnapshot : dataSnapshot.getChildren()) {
                    String strKey = mDataSnapshot.getKey();
                    MessageUserR mMessageUserR = mDataSnapshot.getValue(MessageUserR.class);
                    mMessageUserR.setuID(strKey);
                    mList.add(mMessageUserR);
                }

                for (int i = 0; i < mList.size(); i++) {
                    if (!FirebaseAuth.getInstance().getCurrentUser().getUid().equals(mList.get(i).getuID())) {
                        if (mTAG.equals("Read"))
                            mRootReference.child("messages").child(strMessageID).child("users").child(mList.get(i).getuID()).child("hasUnread").setValue(false);
                        else if (mTAG.equals("UnRead")) {
                            mRootReference.child("messages").child(strMessageID).child("users").child(mList.get(i).getuID()).child("hasUnread").setValue(true);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "===ERROR===" + databaseError.toString());
            }
        });
    }*/

    private void getRecieverUserDetails() {
        mRootReference.child("users").child(strUID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mUser = dataSnapshot.getValue(User.class);

                strRecieverToken = mUser.getDetails().getPushToken();
                strRecieverDeviceType = mUser.getDetails().getDeviceType();
                Log.e(TAG, "===TOKEN===" + strRecieverToken);
                if (mUser.getDetails() != null) {
                    txtNameNameTV.setText(mUser.getDetails().getName());
                    Log.e(TAG, "===Name===" + mUser.getDetails().getName());
                    Log.e(TAG, "===UserName===" + mUser.getDetails().getUsername());
                    if (mUser.getDetails().getUsername().contains("@")) {
                        txtUserNameTV.setText(mUser.getDetails().getUsername());
                    } else {
                        txtUserNameTV.setText("@" + mUser.getDetails().getUsername());
                    }
                    if (mUser.getDetails().getImage() != null && mUser.getDetails().getImage().contains("http")) {
                        Glide.with(getApplicationContext())
                                .load(mUser.getDetails().getImage())
                                .apply(RequestOptions.placeholderOf(PainlyApplication.getInstance().getRandom()).error(PainlyApplication.getInstance().getRandom()))
                                .into(imgProfilePIC);
                    } else {
                        imgProfilePIC.setImageResource(PainlyApplication.getInstance().getRandom());
                    }

                }

                HashMap<String, Boolean> messages = mUser.getMessages();
                Collection<String> values;
                if (messages != null) {
                    values = messages.keySet();
                    mRecieverMsgArrayList = new ArrayList<String>(values);
                }

                getUserMessagesID();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void getUserMessagesID() {
        mRootReference.child("users").child(PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "")).child("messages").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Boolean> messages = (HashMap<String, Boolean>) dataSnapshot.getValue();
                Collection<String> values;
                if (messages != null) {
                    values = messages.keySet();
                    mSenderMsgArrayList = new ArrayList<String>(values);
                }

                for (int i = 0; i < mSenderMsgArrayList.size(); i++) {

                    for (int j = 0; j < mRecieverMsgArrayList.size(); j++) {
                        if (mSenderMsgArrayList.get(i).equals(mRecieverMsgArrayList.get(j))) {
                            isUserAlreadyChat = true;
                            strChatMessegeNodeID = mSenderMsgArrayList.get(i);
                        }
                    }
                }

                /*Getting All Chat of the User*/
                gettingAllChat(strChatMessegeNodeID);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void gettingAllChat(String strChatMessegeNodeID) {
        mRootReference.child("messages").child(strChatMessegeNodeID).child("messages").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mMessagesArrayList.clear();
                for (DataSnapshot mDataSnapshot : dataSnapshot.getChildren()) {
                    String key = mDataSnapshot.getKey();
                    final MessageModel mModel = mDataSnapshot.getValue(MessageModel.class);
                    mModel.setMsgID(key);
                    if (mModel.getSender().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                        mModel.setIntChatType(Constants.VIEW_TYPE_SENDER_MESSAGE);
                    } else {
                        mModel.setIntChatType(Constants.VIEW_TYPE_RECIEVER_MESSAGE);
                    }

                    if (!mMessagesArrayList.contains(mModel))
                        mMessagesArrayList.add(mModel);
                }
                setAdapter();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "===Error===" + databaseError.toString());
            }
        });
    }

    @OnClick({R.id.leftLL, R.id.imgProfilePIC, R.id.imgPutMessageIV, R.id.imgAddImageInChatIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.leftLL:
                onBackPressed();
                break;
            case R.id.imgProfilePIC:
                Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                mIntent.putExtra("UID", strUID);
                mIntent.putExtra("TYPE", Constants.ANOTHER);
                mActivity.startActivity(mIntent);
                break;
            case R.id.imgPutMessageIV:
                Utilities.hideKeyBoad(mActivity, view);
                strMessage = editMessageET.getText().toString().trim();
                validatePutMessage();
                break;

            case R.id.imgAddImageInChatIV:
                /*Show Add Image Popup*/
                showChangePicsDialog();
                break;
        }
    }


    private void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

    private void validatePutMessage() {
        editMessageET.setText("");
        if (strMessage.equals("")) {
            Toast.makeText(mActivity, getString(R.string.please_enter_message), Toast.LENGTH_LONG).show();
        } else {
            checkMsgIfExist();
        }
    }

    private void checkMsgIfExist() {
        /*update the Read unread status*/
        //updateChatReadStatus(strChatMessegeNodeID, "UnRead");

        if (isUserAlreadyChat) {
            //Put Only Message:
            if (strRecieverDeviceType.equals("android")) {
                executeApiForAndroid();
            } else {
                executeApiForiOS();
            }
            putMessage();
        } else {
            if (strRecieverDeviceType.equals("android")) {
                executeApiForAndroid();
            } else {
                executeApiForiOS();
            }
            putAllUserMessageNode();
        }
    }

    private void putMessage() {
        String mChildMsgKey = mRootReference.child(strChatMessegeNodeID).push().getKey();
        String mMsgRef = "messages/" + strChatMessegeNodeID + "/messages/" + mChildMsgKey + "/";
        //Message
        HashMap mMessageHM = new HashMap();
        String strUserID = PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "");
        Log.e(TAG, "====USER ID====" + strUserID);
        mMessageHM.put("sender", strUserID);

        if (strMessageImage != null && strMessageImage.contains("http")) {
            mMessageHM.put("text", "Image Message");
            mMessageHM.put("image", strMessageImage);
            mMessageHM.put("type", "image");
        } else {
            mMessageHM.put("text", strMessage);
        }
        mMessageHM.put("timestamp", Utilities.getCurrentTimeStampComment());


        HashMap mParentHM = new HashMap();
        mParentHM.put(mMsgRef, mMessageHM);
        mRootReference.updateChildren(mParentHM, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    Log.e(TAG, "======SUCCESS======");
                    //editMessageET.setText("");
                }
            }
        });

         /*Update User Notifications Notifications*/
        createNodePutNotificationsData(mChildMsgKey);
        strMessageImage = "";


    }

    private void putAllUserMessageNode() {
        final String mParentMsgKey = mRootReference.child("messages").push().getKey();
        String mChildMsgKey = mRootReference.child(mParentMsgKey).push().getKey();


        String mMsgRef = "messages/" + mParentMsgKey + "/messages/" + mChildMsgKey + "/";
        String mSenderUserRef = "messages/" + mParentMsgKey + "/users/" + PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "") + "/";
        //Not Null Check
        String mReciverUserRef = "messages/" + mParentMsgKey + "/users/" + mUser.getDetails().getUid();

        //Message
        HashMap mMessageHM = new HashMap();
        String strUserID = PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "");
        Log.e(TAG, "====USER ID====" + strUserID);
        mMessageHM.put("sender", strUserID);

        if (strMessageImage != null && strMessageImage.contains("http")) {
            mMessageHM.put("text", "Image Message");
            mMessageHM.put("image", strMessageImage);
            mMessageHM.put("type", "image");
        } else {
            mMessageHM.put("text", strMessage);
        }

        mMessageHM.put("timestamp", Utilities.getCurrentTimeStampComment());


        //Sender User
        String strImage = PainlyPreference.readString(mActivity, PainlyPreference.USER_IMAGE, "");
        String strName = PainlyPreference.readString(mActivity, PainlyPreference.USER_NAME, "");

        Log.e(TAG, "======SENDER User NAME======" + strName);
        Log.e(TAG, "======SENDER User Image======" + strImage);

        HashMap mUserSenderHM = new HashMap();
        mUserSenderHM.put("hasUnread", false);
        mUserSenderHM.put("image", strImage);
        mUserSenderHM.put("name", strName);

        //Reciever User
        HashMap mUserRecieverHM = new HashMap();
        mUserRecieverHM.put("hasUnread", false);
        if (mUser.getDetails().getImage() != null) {
            mUserRecieverHM.put("image", mUser.getDetails().getImage());
        }
        if (mUser.getDetails().getName() != null) {
            mUserRecieverHM.put("name", mUser.getDetails().getName());
        }

        Map mParentHM = new HashMap();
        mParentHM.put(mMsgRef, mMessageHM);
        mParentHM.put(mSenderUserRef, mUserSenderHM);
        mParentHM.put(mReciverUserRef, mUserRecieverHM);

        mRootReference.updateChildren(mParentHM, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    Log.e(TAG, "======SUCCESS======");
                    //editMessageET.setText("");
                    updateSederUserNode(mParentMsgKey);
                }
            }
        });

          /*Update User Notifications Notifications*/
        createNodePutNotificationsData(mChildMsgKey);

        strMessageImage = "";
    }

    private void createNodePutNotificationsData(String messageUID) {
        String strUserName = PainlyPreference.readString(mActivity, PainlyPreference.USER_NAME, "");
        String strUserID = PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "");
        Log.e(TAG, "=====UserName=====" + strUserName);
        HashMap mNotificationHM = new HashMap();
        mNotificationHM.put("isNew", true);
        mNotificationHM.put("message", strUserName + " " + Constants.YOU_HAVE_NEW_MESSAGE);
        mNotificationHM.put("messageUID", messageUID);
        mNotificationHM.put("recieverAuthID", strUID);
        mNotificationHM.put("senderAuthID", strUserID);
        mNotificationHM.put("type", Constants.CHAT);

        mRootReference.child("users").child(strUID).child("notifications").push().setValue(mNotificationHM);
    }

    private void updateSederUserNode(String mParentMsgKey) {
        String strUserID = PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "");
        HashMap mMessageHM = new HashMap();
        mMessageHM.put(mParentMsgKey, true);
        mRootReference.child("users").child(strUserID).child("messages").child(mParentMsgKey).setValue(true);
        updateRecieverUserNode(mParentMsgKey);
    }

    private void updateRecieverUserNode(String mParentMsgKey) {
        HashMap mMessageHM = new HashMap();
        mMessageHM.put(mParentMsgKey, true);
        mRootReference.child("users").child(mUser.getDetails().getUid()).child("messages").child(mParentMsgKey).setValue(true);
    }


    private void setAdapter() {
        mChatWithUserAdapter = new ChatWithUserAdapter(mActivity, mMessagesArrayList);
        chatRecyclerViewRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        chatRecyclerViewRV.setLayoutManager(mLayoutManager);
        chatRecyclerViewRV.setItemAnimator(new DefaultItemAnimator());
        chatRecyclerViewRV.setAdapter(mChatWithUserAdapter);
        mChatWithUserAdapter.notifyDataSetChanged();

        if (editMessageET.hasFocus()) {
            chatRecyclerViewRV.scrollToPosition(mMessagesArrayList.size() - 1);
        }
        chatRecyclerViewRV.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                chatRecyclerViewRV.scrollToPosition(mMessagesArrayList.size() - 1);
            }
        });

        chatRecyclerViewRV.scrollToPosition(mMessagesArrayList.size() - 1);

    }


    //Execute According To IOs Style
    private void executeApiForiOS() {
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, Constants.PAINLY_IOS_NOTIFICATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "===RESPONSE===" + response);
                try {
                    Log.e(TAG, "===SUCESSFULL NOTIFICATIONS===");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "***Error**" + error.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("tokenString", strRecieverToken);
                params.put("message", PainlyPreference.readString(mActivity, PainlyPreference.USER_NAME, "") + " " + Constants.YOU_HAVE_NEW_MESSAGE);
                Log.e(TAG, "====Receiver Token===" + strRecieverToken);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }

        };
        PainlyApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }


    //Execute With Android Style Notifications:
    private void executeApiForAndroid() {
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, Constants.PUSH_NOTIFICATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "===RESPONSE===" + response);
                try {
                    Log.e(TAG, "===SUCESSFULL NOTIFICATIONS===");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "***Error**" + error.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                String strUserName = PainlyPreference.readString(mActivity, PainlyPreference.USER_NAME, "");
                Log.e(TAG, "=====ApiUserName=====" + strUserName);
                Map<String, String> params = new HashMap<String, String>();
                params.put("tokenString", strRecieverToken);
                params.put("message", strUserName + " " + Constants.YOU_HAVE_NEW_MESSAGE);
                params.put("notiType", Constants.CHAT);
                params.put("uID", PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, ""));
                params.put("commentType", "");
                params.put("forumID", "");
                if (strRecieverDeviceType.equals("android")) {
                    params.put("deviceType", "android");
                } else {
                    params.put("deviceType", "ios");
                }


                Log.e(TAG, "====Receiver Token===" + strRecieverToken);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }

        };
        PainlyApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }


    private void showChangePicsDialog() {
        if (checkPermission())
            showAttachImagePopup();
        else
            requestPermission();
    }


    /*Pic Image From Gallery*/
    private void showAttachImagePopup() {
        final Dialog dialog = new Dialog(this,
                android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.dialog_select_pic);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView SendImageTV = (TextView) dialog.findViewById(R.id.SendImageTV);
        TextView txtCancelTV = (TextView) dialog.findViewById(R.id.txtCancelTV);
        SendImageTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });
        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        //requestPermissions(new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
        ActivityCompat.requestPermissions(this, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, GALLERY_REQUEST);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case GALLERY_REQUEST:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    showAttachImagePopup();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    // requestPermission();
                    Log.e(TAG, "==permission denied==");
                }
                break;


        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            File finalFile = new File(getRealPathFromURI(uri));
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 0;
            Bitmap bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
            ExifInterface exifInterface = null;
            try {
                exifInterface = new ExifInterface(finalFile.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }

            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            Matrix matrix = new Matrix();
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.setRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.setRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.setRotate(270);
                    break;
                case ExifInterface.ORIENTATION_NORMAL:
                default:

            }
            mBitmapImage = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            Log.e(TAG, "====BITMAP====" + mBitmapImage);
            uploadMessageImage(mBitmapImage);
        }
    }

    private void uploadMessageImage(Bitmap mBitmap) {
        showProgressDialog(mActivity);
        String strMessageUrl = "";
        try {
            //Upload Message Image:::::
            mMessageImageRef = FirebaseDatabase.getInstance().getReference().child("messages").push();
            StorageReference profileSRef = mStorageRef.child("messages/images/").child(mMessageImageRef.getKey());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();

            UploadTask uploadTask = profileSRef.putBytes(byteArray);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    hideProgressDialog();
                    Log.e(TAG, "****onFailure***");
                    Toast.makeText(mActivity, "" + exception.toString(), Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.e(TAG, "****onSuccess***");
                    hideProgressDialog();
                    strMessageImage = taskSnapshot.getDownloadUrl().toString();

                    Log.e(TAG, "====URL====" + strMessageImage);

                    checkMsgIfExist();
                }
            });

        } catch (Exception mE) {
            Log.e(TAG, "=====ERROR====" + mE);
        }
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

}
