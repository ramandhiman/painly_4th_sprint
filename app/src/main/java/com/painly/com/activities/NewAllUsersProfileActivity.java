package com.painly.com.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.makeramen.roundedimageview.RoundedImageView;
import com.painly.com.BaseActivity;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.adapters.ProfilePostsAdapter;
import com.painly.com.beans.FormsModel;
import com.painly.com.beans.User;
import com.painly.com.chat.ChatwithUserActivity;
import com.painly.com.font.TextViewArialBold;
import com.painly.com.font.TextViewArialRegular;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;
import com.painly.com.utils.SimpleDividerItemDecoration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by android-da on 10/15/18.
 */

public class NewAllUsersProfileActivity extends BaseActivity {
    String TAG = NewAllUsersProfileActivity.this.getClass().getSimpleName();
    Activity mActivity = NewAllUsersProfileActivity.this;
    @BindView(R.id.imgCoverPicIV)
    ImageView imgCoverPicIV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.imgAddProfileIV)
    RoundedImageView imgAddProfileIV;
    @BindView(R.id.txtNameTV)
    TextView txtNameTV;
    @BindView(R.id.txtUserNameTV)
    TextView txtUserNameTV;
    @BindView(R.id.txtPainPalsCountTV)
    TextView txtPainPalsCountTV;
    @BindView(R.id.txtPostsCountsTV)
    TextView txtPostsCountsTV;
    @BindView(R.id.txtMessageTV)
    TextView txtMessageTV;
    @BindView(R.id.txtBiosTV)
    TextView txtBiosTV;
    @BindView(R.id.messageLL)
    LinearLayout messageLL;
    @BindView(R.id.favoriteLL)
    LinearLayout favoriteLL;
    @BindView(R.id.blockLL)
    LinearLayout blockLL;
    @BindView(R.id.postsRV)
    RecyclerView postsRV;
    @BindView(R.id.txtOptionsTV)
    LinearLayout txtOptionsTV;
    @BindView(R.id.optionsLL)
    LinearLayout optionsLL;
    @BindView(R.id.txtPostsLL)
    LinearLayout txtPostsLL;
    @BindView(R.id.txtFavUnFavTV)
    TextViewArialBold txtFavUnFavTV;
    @BindView(R.id.txtFavUnFavMsgTV)
    TextViewArialRegular txtFavUnFavMsgTV;
    @BindView(R.id.txtblockUnblockTV)
    TextViewArialBold txtblockUnblockTV;
    @BindView(R.id.txtblockUnblockMsgTV)
    TextViewArialRegular txtblockUnblockMsgTV;
    int mFormCount = 0, mPainPalsCount = 0;
    DatabaseReference mRootRefrence, mProfileRef, mCoverRef;
    String strCurrentLoginID = "", strUserID = "", strType = "", strPushToken = "", strRecieverDeviceType = "", strRecieverToken = "";
    ProfilePostsAdapter mProfilePostsAdapter;
    ArrayList<FormsModel> mPostsArraylist = new ArrayList<>();
    boolean isFavUnFav = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_all_usersprofile_layout);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        //Current Login User ID:
        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        strCurrentLoginID = currentFirebaseUser.getUid();
        ButterKnife.bind(this);
        setUpLayout();

        //Check Is User Favorite or Not
        setUpFavoriteValue();

        //Check Is User Blocked or Not
        setUpUserBlockedValue();

        setAdapter();
    }


    private void setUpUserBlockedValue() {
        mRootRefrence.child("users").child(Constants.USER_ID).child("blocked").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.hasChild(strUserID)) {
                    //Unblock
                    txtblockUnblockTV.setText(R.string.un_block);
                    txtblockUnblockMsgTV.setText(R.string.let_this_message_you_again);
                } else {
                    //add to Block
                    txtblockUnblockTV.setText(R.string.block);
                    txtblockUnblockMsgTV.setText(R.string.prevent_this_user_from_messaging);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setUpFavoriteValue() {
        mRootRefrence.child("users").child(strCurrentLoginID).child("favorites").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.hasChild(strUserID)) {
                    //Favorite
                    txtFavUnFavTV.setText(R.string.unfavorite);
                    txtFavUnFavMsgTV.setText(R.string.remove_this_user_as_a_painpal);

                } else {
                    //Add to favorite
                    txtFavUnFavTV.setText(R.string.favorite);
                    txtFavUnFavMsgTV.setText(R.string.save_this_user_as);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void setUpLayout() {
        if (getIntent() != null) {
            strType = getIntent().getStringExtra("TYPE");
            strUserID = getIntent().getStringExtra("UID");
            if (strCurrentLoginID.equals(strUserID)) {
                txtOptionsTV.setVisibility(View.GONE);
                optionsLL.setVisibility(View.GONE);
            } else {
                txtOptionsTV.setVisibility(View.VISIBLE);
                optionsLL.setVisibility(View.VISIBLE);
            }

        }

        getUserForums();
    }


    @OnClick({R.id.imgBackIV, R.id.messageLL, R.id.favoriteLL, R.id.blockLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.messageLL:
                Intent msgIntent = new Intent(mActivity, ChatwithUserActivity.class);
                msgIntent.putExtra("UID", strUserID);
                startActivity(msgIntent);
                break;
            case R.id.favoriteLL:
                isFavUnFav = true;
                mAddToFavorite();
                break;
            case R.id.blockLL:
                mBlockUser();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void getUserForums() {
        AlertDialogManager.showProgressDialog(mActivity);
        mRootRefrence.child("users").child(strUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                AlertDialogManager.hideProgressDialog();
                try {
                    mPostsArraylist.clear();
                    User mUser = dataSnapshot.getValue(User.class);
                    if (mUser.getForums() != null) {
                        HashMap<String, String> mForumsHM = mUser.getForums();
                        Collection<String> values = mForumsHM.keySet();
                        ArrayList<String> strUserFormsAL = new ArrayList<String>(values);

                        mFormCount = strUserFormsAL.size();
                        txtPostsLL.setVisibility(View.VISIBLE);
                        getForumData(strUserFormsAL);
                    } else {
                        txtPostsLL.setVisibility(View.GONE);
                    }
                    if (mUser.getFavorites() != null) {
                        HashMap<String, String> mFavoriteHM = mUser.getFavorites();
                        Collection<String> favValues = mFavoriteHM.keySet();
                        ArrayList<String> favAL = new ArrayList<String>(favValues);
                        mPainPalsCount = favAL.size();
                    }
                    setProfileData(mUser);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "==ERROR==");
                AlertDialogManager.hideProgressDialog();
            }
        });
    }

    private void setProfileData(User mUser) {
        strRecieverDeviceType = mUser.getDetails().getDeviceType();
        strRecieverToken = mUser.getDetails().getPushToken();
        txtPainPalsCountTV.setText("" + mPainPalsCount);
//        txtPostsCountsTV.setText("0");
        if (mUser.getDetails().getName() != null)
            txtNameTV.setText(mUser.getDetails().getName());
        if (mUser.getDetails().getUsername() != null) {
            if (mUser.getDetails().getUsername().contains("@")) {
                txtUserNameTV.setText(mUser.getDetails().getUsername());
            } else {
                txtUserNameTV.setText("@" + mUser.getDetails().getUsername());
            }
        }
        if (mUser.getDetails().getBio() != null) {
            txtBiosTV.setText(mUser.getDetails().getBio());
        }
        if (mUser.getDetails().getImage().contains("http")) {
            //Glide Library
            Glide.with(mActivity)
                    .load(mUser.getDetails().getImage())
                    .apply(RequestOptions.placeholderOf(PainlyApplication.getInstance().getRandom()).error(PainlyApplication.getInstance().getRandom()))
                    .into(imgAddProfileIV);
        } else {
            imgAddProfileIV.setImageResource(PainlyApplication.getInstance().getRandom());
        }


        if (mUser.getDetails().getCoverPhoto().contains("http")) {
            //Glide Library
            Glide.with(mActivity)
                    .load(mUser.getDetails().getCoverPhoto())
                    .apply(RequestOptions.placeholderOf(R.drawable.bg_profile_back).error(R.drawable.bg_profile_back))
                    .into(imgCoverPicIV);
        }
    }

    private void getForumData(ArrayList<String> strUserFormsAL) {
        mPostsArraylist.clear();
        for (int i = 0; i < strUserFormsAL.size(); i++) {
            mRootRefrence.child("forums").child(strUserFormsAL.get(i)).orderByKey().addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getKey() != null) {
                        String key = dataSnapshot.getKey();

                        FormsModel tttModel = dataSnapshot.getValue(FormsModel.class);

                        if (tttModel != null) {
                            //if (tttModel.getIsHidden() instanceof Boolean == false) {
                            tttModel.setFormID(key);
                            if (tttModel.getIsHidden() instanceof Boolean == false) {
                                //if (!mPostsArraylist.contains(tttModel)) {
                                if (contains(mPostsArraylist, tttModel.getFormID()) == false) {
                                    mPostsArraylist.add(tttModel);
                                    Collections.sort(mPostsArraylist);
                                    mProfilePostsAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, "====ERROR===" + databaseError.toString());
                    AlertDialogManager.hideProgressDialog();
                }
            });
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isFavUnFav == false) {
                    txtPostsCountsTV.setText("" + mPostsArraylist.size());
                }

            }
        }, 6000);
    }


    boolean contains(ArrayList<FormsModel> list, String mFormID) {
        for (FormsModel item : list) {
            if (item.getFormID().equals(mFormID)) {
                return true;
            }
        }
        return false;
    }

    boolean containsString(ArrayList<String> list, String strString) {
        for (String item : list) {
            if (item.equals(strString)) {
                return true;
            }
        }
        return false;
    }


    private void setAdapter() {
        mProfilePostsAdapter = new ProfilePostsAdapter(mActivity, mPostsArraylist);
        postsRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        postsRV.setLayoutManager(mLayoutManager);
        postsRV.addItemDecoration(new SimpleDividerItemDecoration(mActivity));
        postsRV.setAdapter(mProfilePostsAdapter);
    }


    /*Favorite/Unfavorite  Functionality*/
    private void mAddToFavorite() {
        showProgressDialog(mActivity);
        mRootRefrence.child("users").child(PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "")).child("favorites").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.hasChild(strUserID)) {
                    //do ur stuff
                    removeToFavorite();
                } else {
                    if (strRecieverDeviceType.equals("android")) {
                        executeApiForAndroid();
                    } else {
                        executeApiForiOS();
                    }
                    //add to favorite
                    addToFavorite();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideProgressDialog();
            }
        });
    }

    private void addToFavorite() {
        AlertDialogManager.showProgressDialog(mActivity);
        /*Favorite Node*/
        HashMap<String, String> mFavoriteHM = new HashMap<String, String>();
        mFavoriteHM.put(strUserID, strUserID);
        mRootRefrence.child("users").child(PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "")).child("favorites").child(strUserID).setValue(strUserID);
        txtFavUnFavTV.setText(R.string.unfavorite);
        txtFavUnFavMsgTV.setText(R.string.remove_this_user_as_a_painpal);
        /*Subscribed Node*/
        mRootRefrence.child("users").child(strUserID).child("subscribed").child(PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "")).setValue(PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, ""));

        /*Update User Notifications Notifications*/
        createNodePutNotificationsData();
        hideProgressDialog();
    }

    private void removeToFavorite() {
          /*Favorite Node*/
        mRootRefrence.child("users").child(PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "")).child("favorites").child(strUserID).removeValue();
        txtFavUnFavTV.setText(R.string.favorite);
        txtFavUnFavMsgTV.setText(R.string.save_this_user_as);
        /*Subscribed Node*/
        mRootRefrence.child("users").child(strUserID).child("subscribed").child(PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "")).removeValue();
        hideProgressDialog();
    }


    private void createNodePutNotificationsData() {
        HashMap mNotificationHM = new HashMap();
        mNotificationHM.put("isNew", true);
        mNotificationHM.put("message", PainlyPreference.readString(mActivity, PainlyPreference.USER_NAME, "") + " " + Constants.FAVORITE_YOU);
        mNotificationHM.put("recieverAuthID", strUserID);
        mNotificationHM.put("senderAuthID", Constants.USER_ID);
        mNotificationHM.put("type", Constants.FAVORITE);
        mRootRefrence.child("users").child(strUserID).child("notifications").push().setValue(mNotificationHM);
        AlertDialogManager.hideProgressDialog();
    }

    private void executeApiForAndroid() {//uID,notiType,message,tokenString
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, Constants.PUSH_NOTIFICATION_FAVORITE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "===RESPONSE===" + response);
                try {
                    Log.e(TAG, "===SUCESSFULL NOTIFICATIONS===");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "***Error**" + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("tokenString", strRecieverToken);
                params.put("message", PainlyPreference.readString(mActivity, PainlyPreference.USER_NAME, "") + " " + Constants.FAVORITE_YOU);
                params.put("notiType", Constants.FAVORITE);
                params.put("uID", Constants.USER_ID);
                if (strRecieverDeviceType.equals("android")) {
                    params.put("deviceType", "android");
                } else {
                    params.put("deviceType", "ios");
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }

        };
        PainlyApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }


    //Execute According To IOs Style
    private void executeApiForiOS() {
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, Constants.PAINLY_IOS_NOTIFICATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "===RESPONSE===" + response);
                try {
                    Log.e(TAG, "===SUCESSFULL NOTIFICATIONS===");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "***Error**" + error.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("tokenString", strRecieverToken);
                params.put("message", PainlyPreference.readString(mActivity, PainlyPreference.USER_NAME, "") + " " + Constants.FAVORITE_YOU);
                Log.e(TAG, "====Receiver Token===" + strRecieverToken);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }

        };
        PainlyApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }


    /*Block/ Unbloack Functionality*/
      /*Blocked user Dialog*/
    public void mBlockUser() {
        AlertDialogManager.showProgressDialog(mActivity);
        mRootRefrence.child("users").child(Constants.USER_ID).child("blocked").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                AlertDialogManager.hideProgressDialog();
                if (snapshot.hasChild(strUserID)) {
                    //do ur stuff
                    removeFromBlock();
                } else {
                    addToBlock();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "==ERROR==" + databaseError.toString());
                AlertDialogManager.hideProgressDialog();
            }
        });
    }

    private void removeFromBlock() {
        mRootRefrence.child("users").child(Constants.USER_ID).child("blocked").child(strUserID).removeValue();
        txtblockUnblockTV.setText(R.string.block);
        txtblockUnblockMsgTV.setText(R.string.prevent_this_user_from_messaging);
    }

    private void addToBlock() {
        mRootRefrence.child("users").child(Constants.USER_ID).child("blocked").child(strUserID).setValue(strUserID);
        txtblockUnblockTV.setText(R.string.un_block);
        txtblockUnblockMsgTV.setText(R.string.let_this_message_you_again);
    }


}
