package com.painly.com.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.painly.com.R;
import com.painly.com.activities.NewAllUsersProfileActivity;
import com.painly.com.beans.UserDetails;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;

import java.util.ArrayList;



/*
 * Created by android-raman on 2/5/18.
 */

public class FavoriteUsersAdapter extends RecyclerView.Adapter<FavoriteUsersAdapter.ViewHolder> {
    private ArrayList<UserDetails> mArrayList;
    private Activity mActivity;

    public FavoriteUsersAdapter(Activity mActivity, ArrayList<UserDetails> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favuser, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final UserDetails mUser = mArrayList.get(position);
        if (mUser != null) {
            if (mUser.getImage() != null) {
                Log.e("User Adapter", "***URL***:" + mUser.getImage());
                DisplayImageOptions pp = new DisplayImageOptions.Builder()
                        .showImageOnLoading(R.drawable.icon_round_ph)
                        .showImageForEmptyUri(R.drawable.icon_round_ph)
                        .showImageOnFail(R.drawable.icon_round_ph)
                        .cacheInMemory(true)
                        .considerExifParams(true)
                        .bitmapConfig(Bitmap.Config.RGB_565)
                        .build();

                ImageLoader.getInstance().displayImage(mUser.getImage(), holder.itemProfilePicCIV, pp);
            }

            if (mUser.getName() != null && mUser.getName().length() > 0) {
                holder.txtNameTV.setText(mUser.getName());
            }
            if (mUser.getUsername() != null && mUser.getUsername().length() > 0) {
                holder.txtUserNameTV.setText("@" + mUser.getUsername());
            }
        }

        holder.itemLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUser!= null) {
                    if (PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "").equals(mArrayList.get(position).getUid())) {
                        Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                        mIntent.putExtra("UID", mUser.getUid());
                        mIntent.putExtra("TYPE", Constants.CURRENT);
                        mActivity.startActivity(mIntent);
                    } else {
                        Intent mIntent = new Intent(mActivity, NewAllUsersProfileActivity.class);
                        mIntent.putExtra("UID",  mUser.getUid());
                        mIntent.putExtra("TYPE", Constants.ANOTHER);
                        mActivity.startActivity(mIntent);
                    }
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        //public de.hdodenhof.circleimageview.CircleImageView itemProfilePicCIV;
        public com.makeramen.roundedimageview.RoundedImageView itemProfilePicCIV;
        public TextView txtUserNameTV, txtNameTV;
        public LinearLayout itemLL;

        public ViewHolder(View itemView) {
            super(itemView);
            //itemProfilePicCIV = (de.hdodenhof.circleimageview.CircleImageView) itemView.findViewById(R.id.itemProfilePicCIV);
            itemProfilePicCIV = (com.makeramen.roundedimageview.RoundedImageView) itemView.findViewById(R.id.itemProfilePicCIV);
            txtUserNameTV = (TextView) itemView.findViewById(R.id.txtUserNameTV);
            txtNameTV = (TextView) itemView.findViewById(R.id.txtNameTV);
            itemLL = (LinearLayout) itemView.findViewById(R.id.itemLL);
        }
    }
}
