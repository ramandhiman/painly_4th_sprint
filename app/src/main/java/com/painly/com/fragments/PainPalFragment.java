package com.painly.com.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.activities.AllUsersActivity;
import com.painly.com.adapters.ProfilePainpalsAdapter;
import com.painly.com.beans.FavoriteModel;
import com.painly.com.beans.User;
import com.painly.com.font.EditTextArialRegular;
import com.painly.com.font.TextViewArialRegular;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.PainlyPreference;
import com.painly.com.utils.Utilities;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class PainPalFragment extends Fragment {
    View mView;
    String TAG = PainPalFragment.this.getClass().getSimpleName();
    @BindView(R.id.editSearchET)
    EditTextArialRegular editSearchET;
    @BindView(R.id.txtBrowseUserTV)
    TextViewArialRegular txtBrowseUserTV;
    @BindView(R.id.painPalsRV)
    RecyclerView painPalsRV;
    @BindView(R.id.imgCloseIV)
    ImageView imgCloseIV;
    @BindView(R.id.noPainPalsLL)
    LinearLayout noPainPalsLL;

    Unbinder unbinder;

    ArrayList<User> mPainPalsArrayList = new ArrayList<User>();

    ProfilePainpalsAdapter mPainpalsAdapter;

    DatabaseReference mRootRefrence;

    public PainPalFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_pain_pal, container, false);
        unbinder = ButterKnife.bind(this, mView);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        setSearch();
        setPainPalAdapter();
        getPainPals();
        return mView;
    }

    private void getPainPals() {
        if (Utilities.isNetworkAvailable(getActivity())){
            getFavoritesUser();
        }else{
            Toast.makeText(getActivity(), getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPainPalsArrayList.clear();
        getPainPals();
    }

    private void setSearch() {
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    imgCloseIV.setVisibility(View.VISIBLE);
                } else {
                    imgCloseIV.setVisibility(View.GONE);
                }
                String query = s.toString().toLowerCase();
                filter(query);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        imgCloseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editSearchET.setText("");
                painPalsRV.setLayoutManager(new GridLayoutManager(getActivity(), 3));
                mPainpalsAdapter = new ProfilePainpalsAdapter(getActivity(), mPainPalsArrayList);
                painPalsRV.setAdapter(mPainpalsAdapter);
            }
        });
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<User> filterdNames = new ArrayList<User>();

        //looping through existing elements
        for (User s : mPainPalsArrayList) {
            //if the existing elements contains the search input
            if (s.getDetails().getName().toLowerCase().contains(text.toLowerCase()) || s.getDetails().getUsername().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(s);
            }
        }
        //calling a method of the adapter class and passing the filtered list
        mPainpalsAdapter.filterList(filterdNames);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.txtBrowseUserTV)
    public void onViewClicked() {
        startActivity(new Intent(getActivity(), AllUsersActivity.class));

    }


    private void getFavoritesUser() {
        mRootRefrence.child("users").child(PainlyPreference.readString(getActivity(), PainlyPreference.USER_ID, "")).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                AlertDialogManager.hideProgressDialog();
                ArrayList<String> mFavUserIDsAL = new ArrayList<>();
                mFavUserIDsAL.clear();
                FavoriteModel mFavoriteModel = dataSnapshot.getValue(FavoriteModel.class);
                Collection<String> values = mFavoriteModel.getFavorites().keySet();
                mFavUserIDsAL = new ArrayList<String>(values);
                getFavoriteUserDataDetails(mFavUserIDsAL);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "====Error+===" + databaseError.toString());
            }
        });
    }

    private void getFavoriteUserDataDetails(ArrayList<String> mFavUserIDsAL) {
        mPainPalsArrayList.clear();
        if (mFavUserIDsAL.size() > 0) {
            if (noPainPalsLL != null)
                noPainPalsLL.setVisibility(View.GONE);
        } else {
            if (noPainPalsLL != null)
                noPainPalsLL.setVisibility(View.VISIBLE);
        }
        for (int i = 0; i < mFavUserIDsAL.size(); i++) {
            mRootRefrence.child("users").child(mFavUserIDsAL.get(i)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    AlertDialogManager.hideProgressDialog();
                    final User mUser = dataSnapshot.getValue(User.class);

                    if (mUser != null){
                        if (mUser.getDetails() != null){
                            mUser.getDetails().setPlaceholderImage(PainlyApplication.getInstance().getRandom());
                            if (!mUser.getDetails().getUid().equals(FirebaseAuth.getInstance().getUid())) {
                                if (contains(mPainPalsArrayList, mUser.getDetails().getUid()) == false) {
                                    mPainPalsArrayList.add(mUser);
                                    mPainpalsAdapter.notifyDataSetChanged();
                                }
                            }
                        }

                    }



                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    AlertDialogManager.hideProgressDialog();
                }
            });

        }

    }

    boolean contains(ArrayList<User> list, String mUserID) {
        for (User item : list) {
            if (item.getDetails().getUid().equals(mUserID)) {
                return true;
            }
        }
        return false;
    }


    private void setPainPalAdapter() {
        painPalsRV.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        mPainpalsAdapter = new ProfilePainpalsAdapter(getActivity(), mPainPalsArrayList);
        painPalsRV.setAdapter(mPainpalsAdapter);

    }


}
