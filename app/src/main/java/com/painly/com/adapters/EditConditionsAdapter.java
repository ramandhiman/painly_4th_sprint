package com.painly.com.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.painly.com.R;
import com.painly.com.beans.Conditions;
import com.painly.com.interfaces.SelectConditions;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by android-raman on 2/5/18.
 */

public class EditConditionsAdapter extends RecyclerView.Adapter<EditConditionsAdapter.ViewHolder> {
    private ArrayList<Conditions> mArrayList;
    private Activity mActivity;
    private SelectConditions mSelectConditions;
    private ArrayList<Conditions> mSelectedArrayList = new ArrayList<Conditions>();

    public EditConditionsAdapter(Activity mActivity, ArrayList<Conditions> mArrayList, SelectConditions mSelectConditions) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mSelectConditions = mSelectConditions;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tags, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Conditions mConditions = mArrayList.get(position);
        holder.itemTitleTV.setText(mConditions.getName());

        holder.itemCheckBox.setChecked(mArrayList.get(position).isSelected());

        if (mArrayList.get(position).isSelected()) {
            holder.imgCheckIV.setVisibility(View.VISIBLE);
            holder.itemTitleTV.setTextColor(mActivity.getResources().getColor(R.color.theme_color));

        } else {
            holder.imgCheckIV.setVisibility(View.GONE);
            holder.itemTitleTV.setTextColor(mActivity.getResources().getColor(R.color.dark_txt_color));
        }

        holder.itemCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.itemCheckBox.isChecked() == true) {
                    mArrayList.get(holder.getAdapterPosition()).setSelected(true);
                    //mSelectedArrayList.add(mConditions);
                    mSelectConditions.mSelectConditions(true,mArrayList);
                    holder.imgCheckIV.setVisibility(View.VISIBLE);
                    holder.itemTitleTV.setTextColor(mActivity.getResources().getColor(R.color.theme_color));
                } else if (holder.itemCheckBox.isChecked() == false){
                    mArrayList.get(holder.getAdapterPosition()).setSelected(false);
                    //mSelectedArrayList.remove(mConditions);
                    mSelectConditions.mSelectConditions(false, mArrayList);
                    holder.imgCheckIV.setVisibility(View.GONE);
                    holder.itemTitleTV.setTextColor(mActivity.getResources().getColor(R.color.dark_txt_color));
                }
            }

        });


        //Sponsorship Layouts.
        if (mConditions.getSponsorship() != null){
            if (mConditions.getSponsorship().getImage() != null && mConditions.getSponsorship().getImage().contains("http")){
                holder.sponserShipLL.setVisibility(View.VISIBLE);
                holder.sponserShipNameTV.setText(mConditions.getSponsorship().getName());
                //Sponsorship Image
                Glide.with(mActivity)
                        .load(mConditions.getSponsorship().getImage())
                        .apply(RequestOptions.placeholderOf(R.drawable.icon_round_ph).error(R.drawable.icon_round_ph))
                        .into(holder.sponsershipIV);

            }else{
                holder.sponserShipLL.setVisibility(View.GONE);
            }
        }else{
            holder.sponserShipLL.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView itemTitleTV;
        public CheckBox itemCheckBox;
        public RelativeLayout itemLayoutLL;
        public ImageView imgCheckIV;
        //Sponsorship
        public CircleImageView sponsershipIV;
        public TextView sponserShipNameTV;
        public LinearLayout sponserShipLL;

        public ViewHolder(View itemView) {
            super(itemView);
            itemTitleTV = (TextView) itemView.findViewById(R.id.itemTitleTV);
            itemCheckBox = (CheckBox) itemView.findViewById(R.id.itemCheckBox);
            itemLayoutLL = (RelativeLayout) itemView.findViewById(R.id.itemLayoutLL);
            imgCheckIV = (ImageView) itemView.findViewById(R.id.imgCheckIV);

            //Sponsership
            sponserShipLL = (LinearLayout) itemView.findViewById(R.id.sponserShipLL);
            sponserShipNameTV = (TextView) itemView.findViewById(R.id.sponserShipNameTV);
            sponsershipIV = (CircleImageView) itemView.findViewById(R.id.sponsershipIV);

        }


    }
}
