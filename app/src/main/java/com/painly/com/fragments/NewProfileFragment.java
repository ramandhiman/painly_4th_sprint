package com.painly.com.fragments;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.koushikdutta.ion.builder.Builders;
import com.painly.com.PainlyApplication;
import com.painly.com.R;
import com.painly.com.activities.LoginActivity;
import com.painly.com.adapters.ProfilePagerAdapter;
import com.painly.com.beans.UserDetails;
import com.painly.com.font.TextViewArialBold;
import com.painly.com.font.TextViewGothamMedium;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.PainlyPreference;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;


public class NewProfileFragment extends Fragment {
    String TAG = NewProfileFragment.this.getClass().getSimpleName();
    public static final int GALLERY_REQUEST = 222;
    public Bitmap mBitmapImage;
    public String mPictureType = "";
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;
    public StorageReference mStorageRef;


    View mView;
    @BindView(R.id.txtLogoutTV)
    TextViewArialBold txtLogoutTV;
    @BindView(R.id.profileRIV)
    CircleImageView profileRIV;
    public static TextView txtNameTV;
    @BindView(R.id.txtUserNameTV)
    TextView txtUserNameTV;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.pager)
    ViewPager pager;
    Unbinder unbinder;
    ProfilePagerAdapter mProfilePagerAdapter;
    public static String USER_BIOGRAPHY = "";



    private Dialog progressDialog;

    DatabaseReference mRootRefrence, mProfileRef, mCoverRef;
    Uri mProfileUrl, mCoverUrl;
    boolean isProfilePic = false, isCoverPic = false;


    public NewProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.new_fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, mView);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        txtNameTV= (TextView) mView.findViewById(R.id.txtNameTV);
        setUpTabsWithPager(mView);
        getUserProfileData();

        return mView;
    }

    private void setUpTabsWithPager(View v) {
        //Creating our pager adapter
        mProfilePagerAdapter = new ProfilePagerAdapter(getActivity().getSupportFragmentManager());
        mProfilePagerAdapter.addFrag(new NotificationFragment(), getString(R.string.notifications));
        mProfilePagerAdapter.addFrag(new OptionsFragment(), getString(R.string.options));
        mProfilePagerAdapter.addFrag(new PainPalFragment(), getString(R.string.painpals));
        mProfilePagerAdapter.addFrag(new ProfilePostsFragment(), getString(R.string.myposts));
        pager.setAdapter(mProfilePagerAdapter);

        tabLayout.setTabTextColors(Color.GRAY, Color.BLACK);
        tabLayout.setSelectedTabIndicatorHeight(10);
        tabLayout.setSelectedTabIndicatorColor(getActivity().getResources().getColor(R.color.theme_color));
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setupWithViewPager(pager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void getUserProfileData() {
        mRootRefrence.child("users").child(PainlyPreference.readString(getActivity(), PainlyPreference.USER_ID, "")).child("details").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    UserDetails mUserDetails = dataSnapshot.getValue(UserDetails.class);
                    setProfileData(mUserDetails);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG,"==ERROR=="+databaseError.toString());
            }
        });
    }

    private void setProfileData(UserDetails mUserDetails) {
        if (mUserDetails.getBio() != null){
            USER_BIOGRAPHY = mUserDetails.getBio();
        }
        //By Glide
        if (mUserDetails.getImage() != null && mUserDetails.getImage().contains("http")){
            Glide.with(getActivity().getApplicationContext())
                    .load(mUserDetails.getImage())
                    .apply(RequestOptions.placeholderOf(PainlyApplication.getInstance().getRandom()).error(PainlyApplication.getInstance().getRandom()))
                    .into(profileRIV);
        }else{
            profileRIV.setImageResource(PainlyApplication.getInstance().getRandom());
        }


        txtNameTV.setText(mUserDetails.getName());
        if (mUserDetails.getUsername().contains("@")) {
            txtUserNameTV.setText(mUserDetails.getUsername());
        } else {
            txtUserNameTV.setText("@" + mUserDetails.getUsername());
        }


    }

    @OnClick({R.id.txtLogoutTV,R.id.profileRIV})
    public void onViewClicked(View mView) {
        switch (mView.getId()){
            case R.id.txtLogoutTV:
                logout();
                break;
            case R.id.profileRIV:
                showChangePicsDialog();
                break;
        }

    }

    private void logout() {
        mRootRefrence.child("users").child(PainlyPreference.readString(getActivity(), PainlyPreference.USER_ID, "")).child("details").child("pushToken").removeValue();

        SharedPreferences preferences = PainlyPreference.getPreferences(getActivity());
        SharedPreferences.Editor editor = preferences.edit();

        editor.clear();
        editor.commit();

        FirebaseAuth.getInstance().signOut();

        Intent mIntent = new Intent(getActivity(), LoginActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mIntent);
    }


    private void showChangePicsDialog() {
        if (checkPermission())
            openCameraGalleryDialog();
        else
            requestPermission();
    }

    public void openCameraGalleryDialog() {
        final Dialog dialog = new Dialog(getActivity(), R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_change_pp_cp);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtProfilePicTV = (TextView) dialog.findViewById(R.id.txtProfilePicTV);
        TextView txtCoverPicTV = (TextView) dialog.findViewById(R.id.txtCoverPicTV);
        Button btnCancelB = (Button) dialog.findViewById(R.id.btnCancelB);

        btnCancelB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        txtProfilePicTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mPictureType = getString(R.string.profilepic);
                openGallery();
            }
        });
        txtCoverPicTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mPictureType = getString(R.string.coverpic);
                openGallery();
            }
        });

        dialog.show();
    }


    private void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(getActivity(), writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(getActivity(), writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(getActivity(), writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        requestPermissions(new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    openCameraGalleryDialog();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    // requestPermission();
                    Log.e(TAG, "==permission denied==");
                }
                break;


        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            File finalFile = new File(getRealPathFromURI(uri));
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 0;
            Bitmap bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
            ExifInterface exifInterface = null;
            try {
                exifInterface = new ExifInterface(finalFile.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }

            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            Matrix matrix = new Matrix();
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.setRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.setRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.setRotate(270);
                    break;
                case ExifInterface.ORIENTATION_NORMAL:
                default:

            }
            mBitmapImage = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            if (mPictureType.equals(getString(R.string.profilepic))) {
                profileRIV.setImageBitmap(mBitmapImage);
                isProfilePic = true;
                uploadProfilePic();
            } else if (mPictureType.equals(getString(R.string.coverpic))) {
                isCoverPic = true;
                uploadCoverPic(mBitmapImage);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }



    private void uploadProfilePic() {
        showProgressDialog(getActivity());
        try {
            //Upload Profile Pic:::::
            mProfileRef = FirebaseDatabase.getInstance().getReference().child("users").push();
            StorageReference profileSRef = mStorageRef.child("profilePictures/").child(mProfileRef.getKey());
            profileRIV.setDrawingCacheEnabled(true);
            profileRIV.buildDrawingCache();
            Bitmap bitmap = ((BitmapDrawable) profileRIV.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteArray = baos.toByteArray();

            UploadTask uploadTask = profileSRef.putBytes(byteArray);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    hideProgressDialog();
                    Log.e(TAG, "****onFailure***");
                    AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.error), exception.toString());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.e(TAG, "****onSuccess***");
                    hideProgressDialog();
                    mProfileUrl = taskSnapshot.getDownloadUrl();
                    updatePictures();
                }
            });

        } catch (Exception mE) {
            Log.e(TAG, "=====ERROR====" + mE);
        }
    }

    /*Update Cover Pic*/
    private void uploadCoverPic(Bitmap mBitmapImage) {
        showProgressDialog(getActivity());
        try {
            //Upload Profile Pic:::::
            mCoverRef = FirebaseDatabase.getInstance().getReference().child("users").push();
            StorageReference profileSRef = mStorageRef.child("coverPhotos/").child(mCoverRef.getKey());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            mBitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteArray = baos.toByteArray();

            UploadTask uploadTask = profileSRef.putBytes(byteArray);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    hideProgressDialog();
                    Log.e(TAG, "****onFailure***");
                    AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.error), exception.toString());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.e(TAG, "****onSuccess***");
                    hideProgressDialog();
                    mCoverUrl = taskSnapshot.getDownloadUrl();
                    updatePictures();
                }
            });

        } catch (Exception mE) {
            Log.e(TAG, "=====ERROR====" + mE);
        }
    }



    private void updatePictures() {
        /*Upload Profile Pic & Cover Pic*/
        String strUserID = FirebaseAuth.getInstance().getUid();
        Toast.makeText(getActivity(),"Your new picture is live!",Toast.LENGTH_SHORT).show();
        if (isProfilePic) {
            if (mProfileUrl != null) {
                mRootRefrence.child("users").child(strUserID).child("details").child("image").setValue(mProfileUrl.toString());
                isProfilePic = false;
            }
        }
        if (isCoverPic) {
            if (mCoverUrl != null) {
                mRootRefrence.child("users").child(strUserID).child("details").child("coverPhoto").setValue(mCoverUrl.toString());
                isCoverPic = false;
            }
        }
    }


    private void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    private void hideProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }

    }





}
