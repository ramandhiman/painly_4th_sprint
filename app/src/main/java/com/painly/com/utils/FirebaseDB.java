package com.painly.com.utils;


import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * Created by android-raman on 9/5/18.
 */

public class FirebaseDB {
    //Live Refrence
//    public static DatabaseReference mBaseDataBaseRefrence = FirebaseDatabase.getInstance().getReferenceFromUrl("https://painly-edd49.firebaseio.com/");
//    public static StorageReference mBaseStorageRefrence = FirebaseStorage.getInstance().getReferenceFromUrl("gs://painly-edd49.appspot.com");

    //Development Refrence
    public static DatabaseReference mBaseDataBaseRefrence = FirebaseDatabase.getInstance().getReferenceFromUrl("https://painlydev.firebaseio.com/");
    public static StorageReference mBaseStorageRefrence = FirebaseStorage.getInstance().getReferenceFromUrl("gs://painlydev.appspot.com/");

}
