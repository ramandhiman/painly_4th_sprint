package com.painly.com.views;

/**
 * Created by android-da on 2/19/19.
 */

public interface OnHomePressedListener {
    void onHomePressed();
    void onHomeLongPressed();
}
