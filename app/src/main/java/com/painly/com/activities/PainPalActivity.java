package com.painly.com.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.BaseActivity;
import com.painly.com.R;
import com.painly.com.adapters.PainPalsAdapter;
import com.painly.com.beans.User;
import com.painly.com.beans.UserConditions;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Constants;
import com.painly.com.utils.PainlyPreference;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.InfiniteScrollAdapter;
import com.yarolegovich.discretescrollview.transform.Pivot;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PainPalActivity extends BaseActivity implements DiscreteScrollView.OnItemChangedListener {
    String TAG = PainPalActivity.this.getClass().getSimpleName();
    Activity mActivity = PainPalActivity.this;
    DiscreteScrollView painPalPickerDSV__;
    TextView txtDONEPP;
    LinearLayout noPainPalFoundLL;
    InfiniteScrollAdapter infiniteAdapter;
    PainPalsAdapter mPainPalsAdapter;
    ArrayList<User> mUsersArrayList = new ArrayList<User>();
    ArrayList<String> mCurrenUserConditionsAL = new ArrayList<String>();
    ArrayList<User> mPainPalsArrayList = new ArrayList<User>();
    DatabaseReference mRootRefrence;
    String strType = "";

    @Override
    public void onCurrentItemChanged(@Nullable RecyclerView.ViewHolder viewHolder, int adapterPosition) {
        int positionInDataSet = infiniteAdapter.getRealPosition(adapterPosition);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pain_pal);
        painPalPickerDSV__ = (DiscreteScrollView) findViewById(R.id.painPalPickerDSV__);
        txtDONEPP = (TextView) findViewById(R.id.txtDONEPP);
        noPainPalFoundLL = (LinearLayout) findViewById(R.id.noPainPalFoundLL);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();

        if (getIntent() != null) {
            strType = getIntent().getStringExtra("TYPE");
        }
        getCurrentLoginUserConditions();
        setClickListner();
    }

    public void setClickListner() {
        txtDONEPP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strType.equals(Constants.FROM_PROFILE)) {
                    finish();
                } else if (strType.equals(Constants.FROM_SIGNUP)) {

                    HashMap<String, String> mFavoriteHM = new HashMap<String, String>();

                    for (int i = 0; i < mPainPalsArrayList.size(); i++) {
                        if (mPainPalsArrayList.get(i).getDetails().getUid() != null)
                            mFavoriteHM.put(mPainPalsArrayList.get(i).getDetails().getUid(), mPainPalsArrayList.get(i).getDetails().getUid());
                    }
                    String strUserID = PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "");
                    mRootRefrence.child("users").child(strUserID).child("favorites").setValue(mFavoriteHM);

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void getCurrentLoginUserConditions() {
        mRootRefrence.child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    UserConditions mUserConditions = dataSnapshot.getValue(UserConditions.class);
                    if (mUserConditions.getConditions() != null) {
                        Collection<String> values = mUserConditions.getConditions().keySet();
                        mCurrenUserConditionsAL = new ArrayList<String>(values);
                    }
                    getUsersData();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "====Error+===" + databaseError.toString());
            }
        });
    }

    private void getUsersData() {
        mRootRefrence.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mUsersArrayList.clear();
                try {
                    for (DataSnapshot formsSnapshot : dataSnapshot.getChildren()) {
                        final User mUser = formsSnapshot.getValue(User.class);
                        if (!mUsersArrayList.contains(mUser))
                            mUsersArrayList.add(mUser);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //*Getting PainPals From UsersLists*//*
                getPainPalsFromUsers(mUsersArrayList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //AlertDialogManager.hideProgressDialog();
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), databaseError.toString());
            }
        });
    }

    private void getPainPalsFromUsers(ArrayList<User> mUsersArrayList) {
        for (int i = 0; i < mCurrenUserConditionsAL.size(); i++) {
            for (int j = 0; j < mUsersArrayList.size(); j++) {
                if (mUsersArrayList.get(j).getConditions() != null) {
                    Iterator trav = mUsersArrayList.get(j).getConditions().entrySet().iterator();
                    while (trav.hasNext()) {
                        Map.Entry record = (Map.Entry) trav.next();
                        if (mCurrenUserConditionsAL.get(i).equals(record.getKey())) {
                            if (!mPainPalsArrayList.contains(mUsersArrayList.get(j)))
                                mPainPalsArrayList.add(mUsersArrayList.get(j));
                        }
                    }

                }
            }
        }


        String mUserID = PainlyPreference.readString(mActivity, PainlyPreference.USER_ID, "");
        if (mPainPalsArrayList.size() > 0) {
            for (int i = 0; i < mPainPalsArrayList.size(); i++) {
                User mUser = mPainPalsArrayList.get(i);
                if (mUser.getDetails().getUid() != null) {
                    if (mUser.getDetails().getUid().toString().equals(mUserID)) {
                        mPainPalsArrayList.remove(mUser);
                    }
                }
            }

            setPainPalAdapter();
        }
    }


    private void setPainPalAdapter() {
        if (mPainPalsArrayList.size() > 0) {
            noPainPalFoundLL.setVisibility(View.GONE);
        } else {
            noPainPalFoundLL.setVisibility(View.VISIBLE);
        }

        painPalPickerDSV__.addOnItemChangedListener(this);
        infiniteAdapter = InfiniteScrollAdapter.wrap(new PainPalsAdapter(mActivity, mPainPalsArrayList));
        mPainPalsAdapter = new PainPalsAdapter(mActivity, mPainPalsArrayList);
        painPalPickerDSV__.setAdapter(infiniteAdapter);
        painPalPickerDSV__.setItemTransformer(new ScaleTransformer.Builder()
                .setPivotX(Pivot.X.CENTER)
                .build());
    }


    @Override
    public void onBackPressed() {
        if (strType.equals(Constants.FROM_PROFILE)) {
            finish();
        }
    }


}
