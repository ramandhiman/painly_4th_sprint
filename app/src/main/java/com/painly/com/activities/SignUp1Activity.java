package com.painly.com.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.painly.com.R;
import com.painly.com.font.ButtonGothamMedium;
import com.painly.com.font.EditTextArialRegular;
import com.painly.com.font.TextViewArialRegular;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Constants;
import com.painly.com.utils.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUp1Activity extends AppCompatActivity {
    String TAG = SignUp1Activity.this.getClass().getSimpleName();
    Activity mActivity = SignUp1Activity.this;
    @BindView(R.id.editEmailET)
    EditTextArialRegular editEmailET;
    @BindView(R.id.editPasswordET)
    EditTextArialRegular editPasswordET;
    @BindView(R.id.btnSignUP)
    ButtonGothamMedium btnSignUP;
    @BindView(R.id.txtTermsConditionsTV)
    TextViewArialRegular txtTermsConditionsTV;
    @BindView(R.id.imgBackLL)
    LinearLayout imgBackLL;

    FirebaseAuth mFirebaseAuth;
    DatabaseReference mRooReference;
    ArrayList<String> mUsersNameAL = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_signup_step1);
        ButterKnife.bind(this);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mRooReference = FirebaseDatabase.getInstance().getReference();

        if (getIntent() != null) {
            mUsersNameAL = (ArrayList<String>) getIntent().getSerializableExtra("UNAL");
            Log.e(TAG, "==UserName List Size==" + mUsersNameAL.size());
        }
    }

    @OnClick({R.id.btnSignUP, R.id.txtTermsConditionsTV, R.id.imgBackLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSignUP:
                validate();
                break;
            case R.id.txtTermsConditionsTV:
                Intent termsIntent = new Intent(mActivity, OpenLinkActivity.class);
                termsIntent.putExtra("LINK", Constants.TERMS_LINK);
                startActivity(termsIntent);
                break;
            case R.id.imgBackLL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent mIntent = new Intent(mActivity, SelectionSignInSignUpActivity.class);
        startActivity(mIntent);
        finish();
    }


    private void validate() {
        if (editEmailET.getText().toString().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_email));
        } else if (!Utilities.isValidEmaillId(editEmailET.getText().toString())) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_valid_email));
        } else if (editPasswordET.getText().toString().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_password));
        } else {

            final String email = editEmailET.getText().toString().trim();
            final String password = editPasswordET.getText().toString().trim();
            if (Utilities.isNetworkAvailable(mActivity)){
                executeIfExistOrNot(email,password);
            }else{
                Toast.makeText(mActivity, getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
            }



        }

    }

    private void executeIfExistOrNot(String email, String password) {
        AlertDialogManager.showProgressDialog(mActivity);
        mFirebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            AlertDialogManager.hideProgressDialog();
                            Intent mIntent = new Intent(mActivity, SignUp2Activity.class);
                            mIntent.putExtra("EMAIL", editEmailET.getText().toString().trim());
                            mIntent.putExtra("PASSWORD", editPasswordET.getText().toString().trim());
                            mIntent.putExtra("UNAL", mUsersNameAL);
                            startActivity(mIntent);
                        } else {
                            AlertDialogManager.hideProgressDialog();
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.email_address));
                        }

                    }
                });
    }
}
