package com.painly.com.beans;

import java.io.Serializable;

/**
 * Created by android-da on 11/13/18.
 */

public class LibraryModel implements Serializable {
    String mFormID = "";
    String description = "";
    String title = "";
    String type = "";

    public String getmFormID() {
        return mFormID;
    }

    public void setmFormID(String mFormID) {
        this.mFormID = mFormID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
