package com.painly.com.font;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Dharmani Apps on 1/18/2018.
 */

public class EditTextGabriolaItalic extends EditText {
    public EditTextGabriolaItalic(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public EditTextGabriolaItalic(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public EditTextGabriolaItalic(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new GabriolaItablic(context).getFontFamily());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
