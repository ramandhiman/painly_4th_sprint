package com.painly.com.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.painly.com.R;
import com.painly.com.activities.HomeActivity;
import com.painly.com.adapters.ChatUsersAdapter;
import com.painly.com.chat.MessageR;
import com.painly.com.chat.MessageUserR;
import com.painly.com.font.EditTextArialRegular;
import com.painly.com.font.TextViewArialBold;
import com.painly.com.utils.AlertDialogManager;
import com.painly.com.utils.Constants;
import com.painly.com.utils.SimpleDividerItemDecoration;
import com.painly.com.utils.Utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ChatFragment extends Fragment {

    String TAG = ChatFragment.this.getClass().getSimpleName();
    View mView;

    @BindView(R.id.messagesRV)
    RecyclerView messagesRV;
    @BindView(R.id.withoutUsercHATfRGLL)
    LinearLayout withoutUsercHATfRGLL;
    @BindView(R.id.txtViewPainPalsTV)
    TextViewArialBold txtViewPainPalsTV;
    @BindView(R.id.editSearchET)
    EditTextArialRegular editSearchET;

    Unbinder unbinder;
    ChatUsersAdapter mChatUsersAdapter;
    ArrayList<MessageUserR> mChatUserArrayList = new ArrayList<>();
    ArrayList<MessageUserR> mUpdatedUserArrayList = new ArrayList<>();

    DatabaseReference mRootRefrence;

    public ChatFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_chat, container, false);
        unbinder = ButterKnife.bind(this, mView);
        mRootRefrence = FirebaseDatabase.getInstance().getReference();
        AlertDialogManager.hideProgressDialog();
        setUsersSearch();
        return mView;
    }

    private void setUsersSearch() {
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String query = s.toString().toLowerCase();
                filter(query);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }


    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<MessageUserR> filterdNames = new ArrayList<MessageUserR>();

        //looping through existing elements
        for (MessageUserR s : mUpdatedUserArrayList) {
            //if the existing elements contains the search input
            if (s.getName().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(s);
            }
        }

        if (mUpdatedUserArrayList.size() > 0)
            //calling a method of the adapter class and passing the filtered list
            mChatUsersAdapter.filterList(filterdNames);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.txtViewPainPalsTV)
    public void onViewClicked() {
        ((HomeActivity) getActivity()).addFragmentToView(new NewProfileFragment(), false, null);
        ((HomeActivity) getActivity()).profileClick();
    }


    @Override
    public void onResume() {
        super.onResume();
        mChatUserArrayList.clear();
        mUpdatedUserArrayList.clear();
        if (Utilities.isNetworkAvailable(getActivity())) {
            getChatWithUser();
        } else {
            Toast.makeText(getActivity(), getString(R.string.internetconnection), Toast.LENGTH_SHORT).show();
        }
    }


    private void getChatWithUser() {
        AlertDialogManager.showProgressDialog(getActivity());
        mRootRefrence.child("users").child(Constants.USER_ID).child("messages").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<String> mMessageIDArrayList = new ArrayList<String>();
                for (DataSnapshot messagesSnapshot : dataSnapshot.getChildren()) {
                    String strMessageID = messagesSnapshot.getKey();
                    mMessageIDArrayList.add(strMessageID);
                }

                gettingChatWithUserData(mMessageIDArrayList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "===ERROR===" + databaseError.toString());
            }
        });
    }


    private void gettingChatWithUserData(ArrayList<String> mMessageIDArrayList) {
        final ArrayList<MessageR> mMessageRarrayList111 = new ArrayList<>();
        for (int i = 0; i < mMessageIDArrayList.size(); i++) {
            mRootRefrence.child("messages").child(mMessageIDArrayList.get(i)).orderByPriority().addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null){
                        if (dataSnapshot.getKey() != null) {
                            String key = dataSnapshot.getKey();
                            MessageR messageR = dataSnapshot.getValue(MessageR.class);
                            if (key != null && key.length() > 0){
                                messageR.setMessageID(key);
                                mMessageRarrayList111.add(messageR);
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    AlertDialogManager.hideProgressDialog();
                    Log.e(TAG, "====ERRROR====" + databaseError.toString());
                }
            });
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AlertDialogManager.hideProgressDialog();
                getSetChatUser(mMessageRarrayList111);
            }
        }, 5000);
    }

    private void getSetChatUser(ArrayList<MessageR> mMessageRarrayList) {
        AlertDialogManager.hideProgressDialog();
        try {
            for (int i = 0; i < mMessageRarrayList.size(); i++) {
                HashMap<String, MessageUserR> mUserHashMap = mMessageRarrayList.get(i).getUsers();

                Iterator<Map.Entry<String, MessageUserR>> it = mMessageRarrayList.get(i).getUsers().entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<String, MessageUserR> pair = (Map.Entry<String, MessageUserR>) it.next();
                    pair.getValue().setuID(pair.getKey());
                    if (!Constants.USER_ID.equals(pair.getValue().getuID())) {
                        if (!mChatUserArrayList.contains(pair.getValue())) {
                            pair.getValue().setmMessagesID(mMessageRarrayList.get(i).getMessageID());
                            if (contains(mChatUserArrayList, pair.getValue().getuID()) == false){
                                mChatUserArrayList.add(pair.getValue());
                            }
                        }
                    }
                }
            }
            Collections.reverse(mChatUserArrayList);
            gettingChatUserLastMsgDetails();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    boolean contains(ArrayList<MessageUserR> list, String uID) {
        for (MessageUserR item : list) {
            if (item.getuID().equals(uID)) {
                return true;
            }
        }
        return false;
    }
    private void gettingChatUserLastMsgDetails() {

        Log.e(TAG, "=====Normal User List SIZE======" + mChatUserArrayList.size());

        for (int i = 0; i < mChatUserArrayList.size(); i++) {
            DatabaseReference mRootRefrence = FirebaseDatabase.getInstance().getReference();
            Query lastQuery = mRootRefrence.child("messages").child(mChatUserArrayList.get(i).getmMessagesID()).child("messages").orderByKey().limitToLast(1);

            final int finalI = i;
            lastQuery.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String strLastMsg = "";
                    String strLastTimeStamp = "";
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        strLastMsg = child.child("text").getValue().toString();
                        strLastTimeStamp = child.child("timestamp").getValue().toString();
                    }

                    MessageUserR messageUserR = mChatUserArrayList.get(finalI);
                    messageUserR.setStrLastMessageText(strLastMsg);
                    messageUserR.setStrLastMessageTimeStamp(strLastTimeStamp);

                    mUpdatedUserArrayList.add(messageUserR);

                    if (mUpdatedUserArrayList.size() > 0) {
                        if (withoutUsercHATfRGLL != null)
                            withoutUsercHATfRGLL.setVisibility(View.GONE);
                    } else {
                        if (withoutUsercHATfRGLL != null)
                            withoutUsercHATfRGLL.setVisibility(View.VISIBLE);
                    }
                    if (mChatUserArrayList.size() == mUpdatedUserArrayList.size()) {
                        setChatUserAdapter();
                    }

                    HomeActivity.MSG_NOTIFICATIONS_COUNTS = 0;
                    HomeActivity activity = (HomeActivity) getActivity();

                    /*Check Message Noti cout*/
                    for (int i = 0; i < mUpdatedUserArrayList.size(); i++) {
                        MessageUserR mModel = mUpdatedUserArrayList.get(i);
                        if (mModel.isHasUnread()) {
                            HomeActivity.MSG_NOTIFICATIONS_COUNTS++;
                        }
                    }

                    int mCount = HomeActivity.MSG_NOTIFICATIONS_COUNTS;
                    //activity.updateMsgNotiCount(mCount);
                    HomeActivity.mMsgNotiBadge.setNumber(mCount);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, databaseError.toString());
                }
            });
        }
    }

    private void setChatUserAdapter() {
        Collections.sort(mUpdatedUserArrayList);
        Collections.reverse(mUpdatedUserArrayList);
        if (messagesRV != null) {
            mChatUsersAdapter = new ChatUsersAdapter(getActivity(), mUpdatedUserArrayList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            messagesRV.setLayoutManager(mLayoutManager);
            messagesRV.setItemAnimator(new DefaultItemAnimator());
            messagesRV.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
            messagesRV.setAdapter(mChatUsersAdapter);
        }
    }


}
